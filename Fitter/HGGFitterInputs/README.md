# How to use HGGfitter:
The int note for the fitter is here: https://cds.cern.ch/record/2151063/files/ATL-COM-PHYS-2016-442.pdf     <---  This is very well documented 

1. Use instructions on page https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Hfitter

2. If you use newer version of root (ROOT 6.20) please use branch v05-13-00_6.20 for HGGfitter
   (I use this one, no need to switch to older version of root)

3. Next time just do "source setup.sh" in HGGfitter package - it will automatically setup Hfitter


### Creation of workspace and running limits:
```
1. Need to make the Config files. Current version of config files is in folder ***"Hc_loose_config"***

2. in Run directory of HGGfitter:
   1. create a folder "run_folder"
   2. in "run_folder" add scripts form run_hc and create folders "log", "results", "workspace"
   3. in "log", "results", "workspace" create the "folder_TODAYDATE"
   4. go to the "run" folder of HGGfitter and run the script as follows:
   set the workspace - python Hc_run/HcGenerate.py ../datacards/Hc/Hc_fit_loose/hc_simple.dat folder_TODAYDATE asimov
   run the fit - python Hc_run/HcCompute.py folder_name limit exp
   
   Here folder_TODAYDATE is the folder you created in step 3 
   
   --> HcGenerate.py will create the workspace
   --> HcCompute.py will compute the limits
   
   In logs you will find the HGGfitter output
```


### What is in Hc_loose_config folder

```
1. hc_bkgcont_loosecat.dat  - background function - non-resonant with parameters obtined form the ExpoPoly2 fit
2. hc_bkgHiggs_loosecat.dat - backgorund function - reosnant with parameters obtained form DSCB fit 
3. hc_signal_loosecat.dat   - signal function - with parameters form DBCB fit 
4. hc_nsignalSimple.dat     - simple configuration where we compute mu, includes nSignal, nBackground
5. hc_nsignalSys.dat        - computes mu but with systematic model 
6. hc_loosecat.dat          - includes all dat files above + definition of obserable 
7. hc_simple.dat            - includes hc_loosecat.dat but with a setting mu = 0 expectedd limits

In "hc_loosecat.dat" you can choose - either perform a simple fit without sistematics or with systemtics 
including either "hc_nsignalSimple.dat" OR "hc_nsignalSys.dat"
```

Most of the data files are simple, the most important is ***hc_nsignalSys.dat*** file. \
Here, all the systematics are collected. Here you set the "master formula"

***The basic systematic is written as***
```
constraint dLumiAux   = RooGaussian("dLumiAux",   "dLumi",   1) 
dLumiAux   = 0 const=true min=-5 max=+5  
dLumi   = 0 min=-5 max=+5 title="#theta_{lumi}" 
```
***Propagation can be done as:***
```
(in case of up,down variations) 
sigmaLumiup = 1.7 
sigmaLumidn = 1.8 
//divide by 100 because the numbers stated in % 
formula onePlusSigmaLumiUp = (1 + sigmaLumiup/100)   
formula onePlusSigmaLumiDn = (1 - sigmaLumidn/100)
interpolate kLumi along dLumi using (1, onePlusSigmaLumiDn, onePlusSigmaLumiUp, 4)
``` 
 ***or*** 
 ```
sigmaLumi = 1.7 
(in case of one value for up, down variations) 
formula kLumi  = (exp(sqrt(ln(1 + sigmaLumi^2)   * dLumi)) 
```


### Then, the final yields

**1. for the background1 (non-resonant)**
```
nBackground1 = 30318.774  min=0 max=100000  (the value of real background events will be fitted) 
```

**2. for background2 (resonant):**
```
formula nBackground2 = (nGGFSM0*kGGFTh + nVBFSM0*kVBFTh + nTTHSM0*kTTHTh + nWPHSM0*kWPHTh + nWMHSM0*kWMHTh + nQZHSM0*kQZHTh + nGZHSM0*kGZHTh + nBBHSM0*kBBHTh)*kLumi*kID*kISO*kTrig*kVertex*kPES*kPER*kJESJER) \
```
where ***k*TH*** are the theory uncertainties on Cross sections taken from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HiggsPropertiesTheorySystematics \
and k* are the experimental uncertainties. 

**3. Signal:**
```
formula nSignal = (mu *nSignalSM*kLumi*kID*kISO*kTrig*kVertex*kPES*kPER*kJESJER + nSS*dBkg) 
```
with ***mu*** - varied in the fit, ***nSignal*** - MC prediction, ***nSS*** - Number of spurious signals 

**Important**, for now Background2 and Signal have the same experimental uncertainties, in reality they will be different, but having one NP so there will be propper correlation between them 


