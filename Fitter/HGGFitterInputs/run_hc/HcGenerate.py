#!/usr/bin/python
#recycled script form LowHigh HGam :)
import time
from GenWS import GenWS
import multiprocessing as multiproc
import os
import sys

def fill(base, val, wildcard) :
  replist = tuple(val for i in range(0, base.count(wildcard)))
  return base.replace(wildcard, '%g') % replist


def help():
  print"Usage : python HcGenerate.py <datacard> <folder> <datafile> [asimov datacard]"
  print"        datacard : datacard of the fit model"
  print"        folder   : sub folder names for log/<folder> and workspace/<folder>"
  print"        datafile : data file to be used in the fit, if set to 'asimov' generate asimov data set"
  print"        asimov datacard (optional) : datacard to generate asomiv data (if different than the fit one)"



if __name__=="__main__":
    """Launch create_workspace of Hfitter in parallel"""
    start = time.time()

    datacard=''
    folder=''
    datafile=''
   
    
    
    if(len(sys.argv)<4 or len(sys.argv)>5) :
      print "Error : wrong number of arguments."
      help()
      sys.exit(0)

    datacard=sys.argv[1]
    folder  =sys.argv[2]
    datafile=sys.argv[3]
    asimov_datacard = sys.argv[4] if(len(sys.argv)==5) else "" 
    obs=True
    
    if(datafile=='asimov') : obs=False


    wsDir = "Hc_run/workspace/"+folder
    logDir= "Hc_run/log/"+folder

    if( not os.path.isdir(wsDir)):
      print "Error : workspace folder ",wsDir,"does not exist :-("
      sys.exit(1)

    if( not os.path.isdir(logDir)):
      print "Error : log folder ",logDir,"does not exist :-("
      sys.exit(1) 

    inputs = []

    opts=" --verbosity 2"
    
    if obs :
      opts+=" --output-file Hc_run/workspace/"+folder+"/Hc_data"
    else :
      opts+=" --output-file Hc_run/workspace/"+folder+"/Hc_asimov"
      
    opts+=" --datacard "+datacard

    if asimov_datacard!="" :
      opts+=" --asimov-datacard "+asimov_datacard

    if obs :
      opts+=" --data-file "+datafile
    else :
      opts+=" --asimov "
      
    opts+=" --model-name modelHc"
    opts+=" --ws-name wsHc"

    if obs:
      opts+=" --data-output-name obsData"
    else :
      opts+=" --data-output-name expData"
      
    
    #opts+=" --binned"
  
  
    massrange =125.
    inputs +=[ (opts,massrange) ]



    # Initialise the pool of workers
    pool_size = 1 # cannot do more than 1 cpu because of memory leak?
    
    pool = multiproc.Pool( processes=pool_size, initializer=None )

    print 'Generating workspaces for all the mass points...'
    pool_outputs = pool.map(GenWS,inputs)
    pool.close() # no more tasks
    pool.join()  # wrap up current tasks
    print 'All mass points have been generated successfully.'

    end = time.time()
    duration = end-start
    print "Execution time {0} min.".format( (end-start)/60. )

    for m,out in pool_outputs:
        file = open('Hc_run/log/'+folder+'/Hc_'+str(m)+'.log','w')
        file.write( out )
        file.close()
