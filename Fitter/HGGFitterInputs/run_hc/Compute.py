import subprocess as proc
import os
import sys

def Compute( (opts,mass) ):
    """Call compute.py scripts of Hfitter"""
    command = [ './compute.py' ] + opts.split() 
    #print command
    print "Computing interval for mass point {0} GeV...".format(mass)
    #out = 'test'
    out = proc.check_output( command, stderr=proc.STDOUT )
    print "Mass point {0} GeV done.".format(mass)
    return (mass,out)
