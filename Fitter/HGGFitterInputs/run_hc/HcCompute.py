#!/usr/bin/python
import time
from Compute import Compute
import multiprocessing as multiproc
import os
import sys

def fill(base, val) :
  replist = tuple(val for i in range(0, base.count('%')))
  return base.replace('%', '%g') % replist

if __name__=="__main__":
    """Launch compute Hfitter in parallel"""
    start = time.time()

    folder = ''
    computType = 'pvalue'
    realData = False
  
    
    if(len(sys.argv)==2):
        folder = sys.argv[1] 
    elif(len(sys.argv)==3):
        folder = sys.argv[1]
        computType = sys.argv[2]
    elif(len(sys.argv)==4):
        folder = sys.argv[1]
        computType = sys.argv[2]
        realData   = (sys.argv[3]=="obs")
    elif(len(sys.argv)==5):
        folder = sys.argv[1]
        computType = sys.argv[2]
        realData   = (sys.argv[3]=="obs")

    else:
      folder = ''
      computType = 'pvalue'

    

    wsDir = "Hc_run/workspace/"+folder
    resDir= "Hc_run/results/"+folder
    logDir= "Hc_run/log/"+folder

    if( not os.path.isdir(wsDir)):
      print "Error : workspace folder ",wsDir,"does not exist :-("
      sys.exit(1)

    if( not os.path.isdir(resDir)):
      print "Error : results folder ",resDir,"does not exist :-("
      sys.exit(1) 

    if( not os.path.isdir(logDir)):
      print "Error : log folder ",logDir,"does not exist :-("
      sys.exit(1) 

    inputs = []

    if( computType=='pvalue' ):
      opts="  --pvalue mu"
    elif ( computType=='limit' ):
      opts="  --limit mu"
    else:
      print "Error : unknown computation type ",computType," :-("
      sys.exit(1)

    if realData :
      opts+=" -w "+wsDir+"/Hc_data_%.root"
      opts+=" -o "+resDir+"/Hc_obs_"+computType+".root"
      opts+=" --log "+logDir+"/Hc_obs_"+computType+"_%.log"
    else:
      opts+=" -w "+wsDir+"/Hc_asimov.root"
      opts+=" -o "+resDir+"/Hc_exp_"+computType+".root"
      opts+=" --log "+logDir+"/Hc_exp_"+computType+".log"
      

    opts+=" --model-name modelHc"
    opts+=" --ws-name wsHc"

    if realData :
      opts+=" --data-name obsData"
    else :
      opts+=" --data-name expData"
      
    if computType=='limit' :
      opts+=" -v2"#" -v1"
      opts+=" --limit-init-hypo 40"#" --limit-init-hypo 100"  - this is the initial guess of what your limit is 
      if realData :
        opts+=" --bands 0"
      else:
        opts+=" --bands 2"#opts+=" --bands 0"  -- number of bands you want to report 
      opts+=" --fit-options Robust:Offset:Offcheck:NotPosDefOK"#opts+=" --fit-options Robust:Offset:Offcheck:NotPosDefOK:Strategy(2):Verbose(2)"#
    else :
      opts+=" --fit-options Robust:Offset:Offcheck:NotPosDefOK:Strategy(2):Verbose(2)"
      opts+=" -v2"
      
  
    #opts+=" --binned"

    # print opts
    # sys.exit(0)
  
    massrange =125.
    inputs +=[ (opts,massrange) ]


    # Initialise the pool of workers
    pool_size = 4
    
    pool = multiproc.Pool( processes=pool_size, initializer=None )

    print 'Computing expected limit for all the mass points...'
    pool_outputs = pool.map(Compute,inputs)
    pool.close() # no more tasks
    pool.join()  # wrap up current tasks
    print 'All mass points have been computed successfully.'

    end = time.time()
    duration = end-start
    print "Execution time {0} min.".format( (end-start)/60. )

    for m,out in pool_outputs:
        file = open('Hc_run/log/'+folder+'/multiExpLim_'+str(m)+'.log','w')
        file.write( out )
        file.close()
