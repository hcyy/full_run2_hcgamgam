import subprocess as proc
import os
import sys

def GenWS( (opts,massrange) ):
    """Call create_workspace.py scripts of Hfitter"""
    command = [ './create_workspace.py' ] + opts.split() 
    #print command
    print "Generating workspaces for mass range {0} GeV...".format(massrange)
    #out = "test"
    out = proc.check_output( command, stderr=proc.STDOUT )
    print "Workspaces for mass range {0} GeV done.".format(massrange)
    return (massrange,out)
