#!/usr/bin/env python

import os
import sys
import argparse
import math
from GibbsKernel import *
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel, RBF, Matern, RationalQuadratic
import numpy as np
import matplotlib
matplotlib.use("Agg") # stops matplotlib crashing when remote disconneted for a while
import matplotlib.pyplot as plt
from matplotlib import cm
# from sklearn.externals import joblib
from scipy.special import erf
from scipy import stats
import uproot
import mplhep as hep


class GP():

    def __init__(self, discretisation, calcFullCovMat, memorySaverMode, normaliseData):
        self.discretisation = discretisation
        self.calcFullCovMat = calcFullCovMat # This includes all uncertainty correlations, but has major memory requirements
        self.normaliseData = normaliseData
        self.varNames = []
        self.varRanges = []
        self.varRangesOriginal = []
        self.varIsLog = []
        self.varGrids = []
        self.memorySaverMode = memorySaverMode
        if self.memorySaverMode: self.calcFullCovMat=False

    def AddVariable(self, name, varRange, isLog):
        self.varNames.append(name)
        self.varIsLog.append(isLog)
        self.varRangesOriginal.append(varRange)
        if isLog: varRange = [np.log(varRange[0]), np.log(varRange[1])]
        self.varRanges.append(varRange)
        self.varGrids.append(np.arange(varRange[0], (self.discretisation*varRange[1]-varRange[0]-(varRange[1]-varRange[0])/2.)/float(self.discretisation-1), (varRange[1]-varRange[0])/float(self.discretisation-1)))

    def AddData(self, X, Y, cutoff=-1., noise=[]):
        self.trainDataValues = X
        self.trainDataTargets = Y
        for var in range(len(self.varRanges)):
            nData = self.trainDataTargets.shape[0]
            for i in range(nData):
                j = nData-i-1
                if self.trainDataValues[j,var]<self.varRangesOriginal[var][0] or self.trainDataValues[j,var]>self.varRangesOriginal[var][1]:
                    self.trainDataValues = np.delete(self.trainDataValues, j, axis=0)
                    self.trainDataTargets = np.delete(self.trainDataTargets, j, axis=0)
        self.cutoff = cutoff
        if self.cutoff>0.:
            self.trainDataValuesCut = np.copy(self.trainDataValues)
            self.trainDataTargetsCut = np.copy(self.trainDataTargets)
            nData = self.trainDataTargets.shape[0]
            for i in range(nData):
                j = nData-i-1
                if self.trainDataTargets[j]>self.cutoff:
                    self.trainDataValues = np.delete(self.trainDataValues, j, axis=0)
                    self.trainDataTargets = np.delete(self.trainDataTargets, j, axis=0)
                else:
                    self.trainDataValuesCut = np.delete(self.trainDataValuesCut, j, axis=0)
                    self.trainDataTargetsCut = np.delete(self.trainDataTargetsCut, j, axis=0)
        self.noise = noise
        for var in range(len(self.varIsLog)):
            if self.varIsLog[var]:
                self.trainDataValues[:,var] = np.log(self.trainDataValues[:,var])
                if self.cutoff>0.: self.trainDataValuesCut[:,var] = np.log(self.trainDataValuesCut[:,var])
        if self.normaliseData:
            for var in range(len(self.varRanges)):
                self.trainDataValues[:,var]=(self.trainDataValues[:,var]-self.varRanges[var][0])/(self.varRanges[var][1]-self.varRanges[var][0])
                if self.cutoff>0.: self.trainDataValuesCut[:,var]=(self.trainDataValuesCut[:,var]-self.varRanges[var][0])/(self.varRanges[var][1]-self.varRanges[var][0])
                self.varRanges[var]=[0., 1.]
                self.varGrids[var]=np.arange(self.varRanges[var][0], (self.discretisation*self.varRanges[var][1]-self.varRanges[var][0]-(self.varRanges[var][1]-self.varRanges[var][0])/2.)/float(self.discretisation-1), (self.varRanges[var][1]-self.varRanges[var][0])/float(self.discretisation-1))
            self.trainDataTargetsMean = np.mean(self.trainDataTargets)
            self.trainDataTargetsStd = np.std(self.trainDataTargets)
            self.trainDataTargets=(self.trainDataTargets-self.trainDataTargetsMean)/self.trainDataTargetsStd
            if self.cutoff>0.: self.trainDataTargetsCut=(self.trainDataTargetsCut-self.trainDataTargetsMean)/self.trainDataTargetsStd
            self.noise = self.noise/self.trainDataTargetsStd

    def BuildGrids(self, nLoop):
        assert(nLoop>=1)
        if nLoop>1:
            for i in range(self.discretisation):
                tempList = list(self.gridMapKey)
                tempList[len(self.varNames)-nLoop] = int(i)
                self.gridMapKey = tuple(tempList)
                self.BuildGrids(nLoop-1)
        elif nLoop==1:
            for i in range(self.discretisation):
                tempList = list(self.gridMapKey)
                tempList[len(self.varNames)-nLoop] = int(i)
                self.gridMapKey = tuple(tempList)
                self.gridMap[self.gridMapKey] = self.gridMapValue
                self.gridMapValue += 1
                flatGridEntry = []
                for j in range(len(self.gridMapKey)):
                    flatGridEntry.append(self.varGrids[j][self.gridMapKey[j]])
                self.flatGrid.append(np.array(flatGridEntry))

    def BuildGP(self, kernel="rbf", amplitude=1., lengthScale=0.05, auxPar=0.5):
        self.amplitude = amplitude
        self.lengthScale = lengthScale
        # if kernel=="rbf": self.kernel = ConstantKernel(amplitude**2)*RBF(lengthScale)
        # if kernel=="rbf": self.kernel = ConstantKernel(constant_value=14.93893389299668, constant_value_bounds="fixed")*RBF(length_scale=0.9970398196623173, length_scale_bounds="fixed") # for c-tag
        if kernel=="rbf": self.kernel = ConstantKernel(constant_value=38.19121308334387, constant_value_bounds="fixed")*RBF(length_scale=1.079862395549628, length_scale_bounds="fixed") # for non-c-tag
        # if kernel=="rbf": self.kernel = ConstantKernel(constant_value=amplitude**2, constant_value_bounds="fixed")*RBF(length_scale=lengthScale, length_scale_bounds="fixed")
        elif kernel=="matern": self.kernel = ConstantKernel(amplitude**2)*Matern(lengthScale, (1.e-5, 10000.), auxPar)
        elif kernel=="rq": self.kernel = ConstantKernel(amplitude**2)*RationalQuadratic(lengthScale, auxPar)
        elif kernel=="gibbs": self.kernel = ConstantKernel(amplitude**2)*Gibbs(1.0, (1e-5,1e5), 1.0, (1e-5,1e5))
        self.gp = GaussianProcessRegressor(kernel=self.kernel, alpha=np.square(self.noise), copy_X_train=False, normalize_y=self.normaliseData)
        # self.gp = GaussianProcessRegressor(kernel=self.kernel, alpha=np.square(self.noise), copy_X_train=False, normalize_y=self.normaliseData, optimizer=None)
        # self.gp = GaussianProcessRegressor(kernel=self.kernel, alpha=np.square(self.noise), copy_X_train=False, normalize_y=self.normaliseData, n_restarts_optimizer=100)
        if self.trainDataValues.size>0: self.gp.fit(self.trainDataValues, self.trainDataTargets)
        # self.flatGrid = np.stack(self.varGrids, axis=1)
        self.flatGrid = []
        self.gridMap = {}
        self.gridMapValue = 0
        self.gridMapKey = (-1,)*len(self.varNames)
        self.BuildGrids(len(self.varNames))
        if self.calcFullCovMat:
            self.mu_post, self.unc_post = self.gp.predict(self.flatGrid, return_cov=True)
        elif self.memorySaverMode:
            self.mu_post = np.zeros((len(self.flatGrid)))
            self.unc_post = np.zeros((len(self.flatGrid)))
            for i in range(len(self.flatGrid)):
                self.mu_post[i], self.unc_post[i] = self.gp.predict([self.flatGrid[i]], return_std=True)
        else:
            self.mu_post, self.unc_post = self.gp.predict(self.flatGrid, return_std=True)

    def CreatePopulation(self, populationSize):
        self.population = []
        for i in range(populationSize):
            individual = []
            for var in range(len(self.varNames)):
                if self.varIsLog[var]: individual.append(math.exp(np.random.uniform(math.log(self.varRanges[var][0]), math.log(self.varRanges[var][1]))))
                else: individual.append(np.random.uniform(self.varRanges[var][0], self.varRanges[var][1]))
            self.population.append(individual)

    def PlotPopulation(self):
        if not os.path.exists("Outputs"): os.mkdir("Outputs")
        for var in range(len(self.varNames)):
            varVals = []
            for i in range(len(self.population)):
                varVals.append(self.population[i][var])
            fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8,8))
            axs.hist(varVals, bins=100, range=self.varRanges[var], density=True, histtype="step", linewidth=1, edgecolor="black")
            axs.title.set_text("")
            plt.xlabel(self.varNames[var])
            plt.ylabel("Individuals")
            fig.tight_layout(pad=2.0)
            fig.savefig("Outputs/"+self.varNames[var]+".png")
            plt.close()

    def FindMinimum(self):
        minX = np.argmin(self.mu_post)
        self.minY = np.amin(self.mu_post)
        for gridPoint in range(len(self.flatGrid)):
            if gridPoint==minX:
                self.minima = []
                self.minimaErr = []
                for var in range(len(self.varNames)):
                    self.minima.append(self.flatGrid[gridPoint][var])
                    self.minimaErr.append((self.varRanges[var][1]-self.varRanges[var][0])/float(self.discretisation-1)/2.)
                    if self.calcFullCovMat: self.minYErr = np.sqrt(np.diag(self.unc_post)[gridPoint])
                    else: self.minYErr = self.unc_post[gridPoint]
                print("Minimum gridPoint found at %s +- %s: %f +- %f" % (str(self.minima), str(self.minimaErr), self.minY, self.minYErr))

    def GetVal(self, args, normalised):
        assert(len(self.varNames)==1)
        if normalised: args = (np.array(args)-self.varRangesOriginal[0][0])/(self.varRangesOriginal[0][1]-self.varRangesOriginal[0][0])
        means, covs = self.gp.predict(args, return_cov=True)
        if normalised:
            means = means*self.trainDataTargetsStd+self.trainDataTargetsMean
            covs = covs*self.trainDataTargetsStd*self.trainDataTargetsStd
        return means, covs

    def EIScan(self, means, stds, currentMin, gridPoints=[], excludedPoints=[]):
        maxEI = -1.
        bestGridPoint = -1
        for gridPoint in range(self.discretisation**len(self.varNames)):
            if gridPoints!=[]:
                pointOutsideRange = False
                for var in range(len(self.varRanges)):
                    if gridPoints[gridPoint][var]<self.varRanges[var][0] or gridPoints[gridPoint][var]>self.varRanges[var][1]:
                        pointOutsideRange = True
                        break
                if pointOutsideRange: continue
            if excludedPoints!=[]:
                pointExcluded = False
                for excludedPoint in excludedPoints:
                    if gridPoint==excludedPoint:
                        pointExcluded = True
                        break
                if pointExcluded: continue
            mean = np.copy(means[gridPoint])
            std = np.copy(stds[gridPoint])
            EI = (currentMin-mean)*(1.+erf((currentMin-mean)/(std*np.sqrt(2.))))/2.+std**2.*stats.norm(mean, std).pdf(currentMin)
            # EI = (currentMin-mean)*stats.norm(mean, std).cdf(currentMin)+std**2.*stats.norm(mean, std).pdf(currentMin) # slower
            if EI>maxEI:
                bestGridPoint = int(gridPoint)
                maxEI = float(EI)
        return bestGridPoint, maxEI

    def PlotMarginalisedGP(self, var, binWidth=1., histMax=1100., normalised=False, ratioRange=0.1, blindedData=[], ctagCat=0):

        if not os.path.exists("Outputs"): os.mkdir("Outputs")
        matplotlib.rcParams.update({"font.size": 18})
        plt.xticks(fontsize=18)
        plt.yticks(fontsize=18)

        # Calculate GPR mean and uncertainty projections
        mu_marginalised = np.zeros((self.discretisation))
        uncertainty_marginalised = np.zeros((self.discretisation))
        for i in range(self.discretisation):
            listND = []
            for j in range(len(self.varNames)): listND.append(self.discretisation)
            indicesND = np.zeros(listND, dtype=bool)
            tempIndices = np.zeros(listND, dtype=bool)
            indicesND[tuple([slice(None)]*var+[i])] = True
            indices = np.reshape(indicesND, self.discretisation**len(self.varNames))
            mu_marginalised[i] = np.sum(self.mu_post[indices])/np.sum(indices)
            if self.calcFullCovMat: uncertainty_marginalised[i]=np.sqrt(indices.dot(self.unc_post).dot(indices))/np.sum(indices)
            else: uncertainty_marginalised[i]=np.sqrt(np.sum(self.unc_post[indices]*self.unc_post[indices]))/np.sum(indices)

        # Undo normalisation if appropriate
        if self.normaliseData:
            trainDataValues = np.array(self.trainDataValues)
            varGrids = list(self.varGrids)
            for var in range(len(self.varRangesOriginal)):
                trainDataValues[:,var]=trainDataValues[:,var]*(self.varRangesOriginal[var][1]-self.varRangesOriginal[var][0])+self.varRangesOriginal[var][0]
                varGrids[var]=varGrids[var]*(self.varRangesOriginal[var][1]-self.varRangesOriginal[var][0])+self.varRangesOriginal[var][0]
            trainDataTargets = np.array(self.trainDataTargets)*self.trainDataTargetsStd+self.trainDataTargetsMean
            mu_marginalised = mu_marginalised*self.trainDataTargetsStd+self.trainDataTargetsMean
            uncertainty_marginalised = uncertainty_marginalised*self.trainDataTargetsStd
            noise = np.array(self.noise*self.trainDataTargetsStd)
        else:
            trainDataValues = np.array(self.trainDataValues)
            varGrids = list(self.varGrids)
            trainDataTargets = np.array(self.trainDataTargets)
            noise = np.array(self.noise)

        # fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(16,8))
        # fig, axs = plt.subplots(nrows=2, ncols=1, figsize=(8,16))
        fig, axs = plt.subplots(nrows=2, ncols=1, figsize=(8,8), gridspec_kw={"height_ratios": [1, 0.5]})

        # Data
        if trainDataValues is not None:
            # axs[0].plot(trainDataValues[:,var], trainDataTargets, "o", color="black", label="Data")
            axs[0].errorbar(trainDataValues[:,var], trainDataTargets, yerr=noise, fmt="o", color="black", label="Data")

        # Blinded data
        if blindedData!=[]:
            axs[0].errorbar(blindedData[0], blindedData[1], yerr=blindedData[2], fmt="o", color="red", label="Data (Blinded)")

        # Excluded data
        if self.cutoff>0.:
            if trainDataValuesCut is not None:
                axs[0].plot(trainDataValuesCut[:,var], self.trainDataTargetsCut, "rx", color="red", label="Cut Data")

        # GPR mean and bands
        if self.calcFullCovMat:
            axs[0].plot(varGrids[var], mu_marginalised, label=r"GPR Mean", color="C0")
            axs[0].fill_between(varGrids[var], mu_marginalised+uncertainty_marginalised, mu_marginalised-uncertainty_marginalised, alpha=0.2, color="C0", label=r"GPR $1\sigma$ CI")
            axs[0].fill_between(varGrids[var], mu_marginalised+uncertainty_marginalised, mu_marginalised+2.*uncertainty_marginalised, alpha=0.1, color="C0", label=r"GPR $2\sigma$ CI")
            axs[0].fill_between(varGrids[var], mu_marginalised-2.*uncertainty_marginalised, mu_marginalised-uncertainty_marginalised, alpha=0.1, color="C0")
        else:
            axs[0].plot(varGrids[var], mu_marginalised, label=r"GPR Mean ($1\sigma$ and $2\sigma$ CI)", color="C0")

        # Ratio plot
        axs[1].plot(varGrids[var], np.ones_like(varGrids[var]), label=r"GPR Mean ($1\sigma$ and $2\sigma$ CI)", color="C0")
        if self.calcFullCovMat:
            axs[1].fill_between(varGrids[var], (mu_marginalised+uncertainty_marginalised)/mu_marginalised, (mu_marginalised-uncertainty_marginalised)/mu_marginalised, alpha=0.2, color="C0")
            axs[1].fill_between(varGrids[var], (mu_marginalised+uncertainty_marginalised)/mu_marginalised, (mu_marginalised+2.*uncertainty_marginalised)/mu_marginalised, alpha=0.1, color="C0")
            axs[1].fill_between(varGrids[var], (mu_marginalised-2.*uncertainty_marginalised)/mu_marginalised, (mu_marginalised-uncertainty_marginalised)/mu_marginalised, alpha=0.1, color="C0")
            if trainDataValues is not None:
                means, covs = self.GetVal((trainDataValues[:,var]).reshape(-1, 1), self.normaliseData)
                # axs[1].plot(trainDataValues[:,var], trainDataTargets/means, "rx", color="black", label="Data")
                axs[1].errorbar(trainDataValues[:,var], trainDataTargets/means, yerr=noise/means, fmt="o", color="black", label="Data")
            if blindedData!=[]:
                means, covs = self.GetVal((blindedData[0]).reshape(-1, 1), self.normaliseData)
                axs[1].errorbar(blindedData[0], blindedData[1]/means, yerr=blindedData[2]/means, fmt="o", color="red", label="Data (Blinded)")
        else:
            axs[1].plot(varGrids[var], mu_marginalised, label="GPR Mean", color="C0")
            print("Uncertainty band not coded for self.calcFullCovMat==False")

        # Style changes and make plot
        plt.setp(axs[0].spines.values(), linewidth=2)
        plt.setp(axs[1].spines.values(), linewidth=2)
        axs[0].set_xticklabels([])
        axs[0].xaxis.set_tick_params(which="both", width=2, length=5, direction="in")
        axs[0].yaxis.set_tick_params(which="both", width=2, length=5, direction="in")
        axs[1].xaxis.set_tick_params(which="both", width=2, length=5, direction="in")
        axs[1].yaxis.set_tick_params(which="both", width=2, length=5, direction="in")
        axs[0].legend(loc="upper right", frameon=False, handlelength=1, fontsize=20)
        fig.tight_layout(pad=2.)
        oldPos = axs[0].get_position()
        print(oldPos.y0)
        axs[0].set_position([oldPos.x0, oldPos.y0-0.1, oldPos.width, oldPos.height+0.1])
        # fig.subplots_adjust(bottom=0.0)
        # axs[1].legend(loc="upper right", frameon=False, handlelength=1, fontsize=20)
        axs[1].set_ylim(1.-ratioRange, 1.+ratioRange)
        axs[0].set_xlim(103., 162.)
        axs[1].set_xlim(103., 162.)
        axs[0].set_xlabel("")
        axs[1].set_xlabel(r"m$_{\gamma\gamma}$ [GeV]", size=20)
        axs[0].set_ylabel("Events / 0.5 GeV", size=20)
        axs[1].set_ylabel("Data / GPR Mean", size=20)
        axs[0].text(0.28, 0.1, r"$\sqrt{s}$=13 TeV, 140 fb$^{-1}$", verticalalignment="center", horizontalalignment="center", transform=axs[0].transAxes, fontsize=20)
        # axs[1].text(0.3, 0.1, r"$\sqrt{s}$=13 TeV, 140 fb$^{-1}$", verticalalignment="center", horizontalalignment="center", transform=axs[1].transAxes, fontsize=20)
        axs[0].text(0.15, 0.18, r"ATLAS", fontweight="bold", style="italic", verticalalignment="center", horizontalalignment="center", transform=axs[0].transAxes, fontsize=20)
        if ctagCat==1:
            axs[0].text(0.26, 0.26, r"$c$-tag signal region", verticalalignment="center", horizontalalignment="center", transform=axs[0].transAxes, fontsize=20)
        elif ctagCat==0:
            axs[0].text(0.31, 0.26, r"Non-$c$-tag signal region", verticalalignment="center", horizontalalignment="center", transform=axs[0].transAxes, fontsize=20)
        # axs[1].text(0.25, 0.18, r"ATLAS $\mathrm{Internal}$", fontweight="bold", style="italic", verticalalignment="center", horizontalalignment="center", transform=axs[1].transAxes, fontsize=20)
        plt.savefig("Outputs/GP_var"+str(var)+".png")
        plt.close()


if __name__ == "__main__":

    plt.style.use(hep.style.ATLAS)

    np.set_printoptions(threshold=sys.maxsize)

    parser = argparse.ArgumentParser()
    parser.add_argument("--nTrainData", default=200, type=int)
    args = parser.parse_args()
    nTrainData = args.nTrainData

    nTrainData = 0
    discretisation = 600
    memorySaverMode = False
    calcFullCovMat = True
    normaliseData = True
    cutoff = -1.                                    # skim data with metric>cutoff, applied to remove outliers from failed attempts that scew optimisation
    noiseConst = 1.                                 # noise on data, set to 1.e-10 (computational reasons) if data noise-free, else set to data uncertainty
    # noiseConst = 1./7.643068718                     # noise on data, set to 1.e-10 (computational reasons) if data noise-free, else set to data uncertainty
    # noiseConst = 1.e-3                              # noise on data, set to 1.e-10 (computational reasons) if data noise-free, else set to data uncertainty
    kernel = "rbf"                                  # choice of GPR kernel, options: rbf, matern
    amplitude = 1.                                  # amplitude of prior uncertainty, set to the y-scale under consideration
    lengthScale = 1./110. if normaliseData else 0.5 # length scale of correlations, set to required scale to cover full volume of input space
    auxPar = 10.                                    # auxPar of non-RBF kernel

    useSavedData = True
    useMC = False
    useUnweightedMC = False
    useMCStats = False
    useSSTemplate = False
    doSignalContaminationTest = False
    mHMin = 105.
    mHMax = 160.
    mHBlindMin = 130.
    mHBlindMax = 120.
    nBins = 110
    histMax = 300.
    doPlot = True
    ctagCat = 1           # c-tag category 1 is tagged and 0 is not
    lumiA=3219.56+32994.9;
    lumiD=44307.4;
    lumiE=58450.1;

    # print("Instantiating GP")
    gp = GP(discretisation, calcFullCovMat, memorySaverMode, normaliseData)

    # print("Adding variables")
    gp.AddVariable("myy", [mHMin, mHMax], False)

    if useSavedData:
        if useSSTemplate:
            if nBins!=110:
                print("nBins!=110 for useSSTemplate==True exiting")
                exit()
            if useMC:
                print("useMC==True for useSSTemplate==True exiting")
                exit()
            histCenters = np.array([mHMin+0.5*(mHMax-mHMin)/float(nBins)])
            for i in range(1, nBins):
                binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
                histCenters = np.append(histCenters, [binCenter], axis=0)
            if ctagCat==0:
                histContents = np.array([6855.85, 6757.62, 6653.51, 6537.68, 6466.3, 6364.93, 6272.83, 6191.44, 6086.66, 6002.37, 5914.93, 5823.59, 5749.11, 5671.36, 5574.79, 5491.37, 5414.36, 5334.19, 5261.84, 5194.18, 5104.5, 5033.51, 4977.34, 4886.48, 4831.19, 4757.78, 4693.31, 4622.21, 4561.75, 4480.72, 4437.11, 4351.97, 4299.23, 4241.06, 4190, 4122.27, 4055.68, 4016.58, 3955.85, 3901.59, 3849.81, 3783.29, 3751.13, 3709.7, 3636.2, 3610.05, 3540.98, 3504.06, 3464.07, 3392.6, 3364.3, 3311.51, 3275.39, 3224.31, 3191.66, 3128.62, 3104.83, 3047.2, 3022.8, 2963.82, 2923.65, 2897.38, 2848.91, 2825.69, 2779.03, 2739.83, 2707.28, 2677.26, 2636.46, 2604.32, 2577.53, 2536.71, 2510.83, 2466.21, 2448.03, 2417.33, 2382.67, 2354.77, 2327.6, 2296.83, 2261.91, 2228.45, 2204.99, 2179.7, 2142.66, 2128.45, 2111.18, 2067.65, 2055.54, 2026.61, 2000.06, 1967.56, 1949.69, 1919.7, 1900.23, 1876.3, 1845.89, 1824.23, 1820.09, 1784.59, 1775.52, 1738.65, 1721.82, 1703.27, 1674.72, 1645.94, 1627.56, 1615.77, 1595.22, 1572.5])
                if useMCStats:
                    histUncertainties = np.array([11.6508, 11.6062, 11.4515, 11.3234, 13.4114, 11.2174, 11.1849, 11.1049, 10.9397, 10.8417, 12.1637, 10.7837, 10.6354, 10.5713, 10.4419, 10.5137, 10.2855, 10.6269, 10.155, 10.0838, 9.96726, 9.87306, 9.87752, 9.79374, 9.66092, 9.59927, 9.5737, 9.59504, 9.3836, 9.33281, 9.27096, 9.13868, 9.12675, 9.0736, 8.98653, 8.89644, 8.80632, 8.88897, 8.71648, 8.69515, 8.61353, 8.48953, 8.79227, 8.58264, 8.34035, 8.33627, 8.23783, 8.18326, 8.11226, 8.05237, 8.04486, 7.95033, 7.93862, 7.833, 7.89003, 7.69109, 7.66041, 7.74081, 7.59357, 7.51003, 7.45085, 7.40539, 7.34989, 7.35172, 7.21461, 7.2556, 7.18522, 7.09441, 7.04471, 7.04022, 7.05405, 6.90444, 6.8448, 6.81286, 6.73608, 6.73898, 6.73877, 6.72339, 6.66311, 6.98277, 6.50392, 6.46928, 6.40202, 6.35759, 6.27845, 6.31341, 6.42382, 6.17469, 6.21627, 6.14192, 6.10437, 6.06267, 5.9883, 5.97224, 5.97384, 5.88079, 5.81803, 5.78461, 5.82748, 5.72283, 5.78355, 5.71329, 5.60714, 5.6108, 5.53386, 5.5074, 5.45936, 5.43594, 5.41737, 5.34051])
            elif ctagCat==1:
                histContents = np.array([648.233, 638.316, 629.416, 623.516, 614.425, 606.029, 590.76, 586.985, 573.324, 565.167, 558.403, 549.144, 546.464, 536.668, 531.351, 521.06, 517.511, 510.21, 501.832, 492.908, 491.915, 475.679, 472.408, 471.056, 463.679, 455.553, 447.676, 437.21, 432.895, 429.418, 425.942, 420.678, 413.797, 408.537, 398.807, 396.001, 388.688, 381.249, 380.08, 377.218, 364.521, 364.04, 361.107, 355.709, 345.436, 345.856, 337.436, 336.32, 333.814, 329.887, 321.559, 316.782, 313.408, 311.686, 305.814, 300.573, 294.58, 295.615, 289.001, 280.955, 281.631, 279.163, 275.561, 271.327, 267.098, 265.973, 260.679, 254.285, 255.428, 251.742, 247.796, 243.321, 244.079, 237.135, 238.751, 233.225, 227.654, 226.648, 220.326, 220.556, 218.425, 213.276, 214.53, 208.158, 207.575, 204.625, 204.863, 202.564, 197.603, 193.433, 193.927, 187.068, 188.782, 186.335, 181.621, 181.743, 179.118, 176.525, 173.383, 172.814, 171.316, 163.935, 163.619, 162.271, 158.956, 159.714, 155.685, 155.37, 154.699, 151.094])
                if useMCStats:
                    histUncertainties = np.array([3.4456, 3.41826, 3.38798, 3.38936, 3.39777, 3.35705, 3.28139, 3.26768, 3.26039, 3.18425, 3.18583, 3.16307, 3.14556, 3.11374, 3.11994, 3.0833, 3.07963, 3.03246, 3.01489, 2.97746, 2.95987, 2.95633, 2.90783, 2.91952, 2.914, 2.8994, 2.84582, 2.83097, 2.79919, 2.76203, 2.79138, 2.76221, 2.72292, 2.69848, 2.70458, 2.68694, 2.67461, 2.588, 2.64148, 2.64405, 2.55871, 2.56722, 2.55017, 2.55681, 2.50796, 2.5022, 2.48734, 2.43799, 2.45837, 2.41162, 2.39686, 2.4015, 2.37428, 2.38344, 2.36846, 2.32657, 2.26403, 2.32602, 2.27774, 2.23062, 2.25177, 2.25701, 2.23759, 2.1904, 2.2055, 2.17544, 2.14597, 2.09964, 2.13787, 2.2126, 2.10799, 2.07546, 2.09197, 2.04746, 2.06434, 2.02386, 1.97761, 1.99693, 1.99012, 1.98112, 1.96563, 1.9584, 1.94648, 1.93499, 1.91261, 1.88926, 1.91485, 1.8853, 1.90995, 1.84919, 1.8706, 1.80176, 1.83434, 1.82307, 1.76688, 1.77465, 1.75758, 1.75435, 1.73067, 1.80258, 1.72413, 1.68973, 1.71516, 1.67598, 1.657, 1.67153, 1.63683, 1.64442, 1.63441, 1.63744])
        elif useMC:
            histCenters = np.array([mHMin+0.5*(mHMax-mHMin)/float(nBins)])
            for i in range(1, nBins):
                binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
                histCenters = np.append(histCenters, [binCenter], axis=0)
            if not useUnweightedMC and not useMCStats:
                if nBins==55:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                elif nBins==110:
                    if ctagCat==0:
                        histContents = np.array([6199.39193942, 6127.31260797, 6063.52762534, 5900.48659452, 5818.87759209, 5754.07016399, 5681.59312375, 5629.06757649, 5497.72417734, 5499.58703086, 5406.71860537, 5327.37229229, 5260.26365144, 5245.9434796, 5080.08726825, 5025.33279008, 4979.91281382, 4877.20570375, 4806.01216746, 4729.81820359, 4644.44211986, 4651.0448917, 4547.51155039, 4546.53867502, 4465.0816329, 4390.7968297, 4292.24169372, 4306.96427375, 4203.52937298, 4179.25652173, 4130.21105464, 4071.41987158, 3998.67499379, 3993.94103711, 3918.67878221, 3806.75173353, 3785.95895317, 3714.85621224, 3679.61977633, 3654.16350152, 3613.26655419, 3534.06421019, 3470.60133834, 3480.10056571, 3406.96292298, 3408.91471905, 3343.29126358, 3271.48611095, 3236.58689955, 3195.87851785, 3173.53656656, 3100.83422336, 3083.807715, 3056.44188992, 2963.40070605, 2955.59071032, 2915.24975443, 2921.04162688, 2822.81962686, 2810.17742057, 2800.46164591, 2721.15664781, 2643.68512639, 2707.32456627, 2639.87759724, 2547.1966208, 2564.60207211, 2542.97245529, 2525.92209922, 2459.18119301, 2434.61917283, 2411.96769273, 2400.1614021, 2389.53687275, 2327.477606, 2313.57144544, 2327.23227095, 2259.99626107, 2214.61758665, 2240.46631018, 2168.31110655, 2136.02560239, 2140.80274803, 2148.01266655, 2066.93282629, 2052.83464954, 2001.38128052, 1998.95715859, 1987.70614493, 1954.25085944, 1876.47499732, 1927.80947617, 1899.33768975, 1857.38921374, 1847.81191552, 1827.72061099, 1772.29428699, 1758.70335233, 1755.32894622, 1732.42599772, 1711.69571361, 1703.45700174, 1688.14243401, 1641.51304767, 1653.19344072, 1593.23367053, 1608.73178508, 1608.25055058, 1613.25390121, 1563.73024897])
                    elif ctagCat==1:
                        histContents = np.array([712.75293085, 713.90187598, 660.90355014, 663.26153745, 642.81450929, 647.10573957, 636.74042443, 630.01866659, 626.54430317, 626.42157386, 624.51330155, 599.31074367, 562.09035968, 578.94618338, 582.66479707, 553.49129212, 553.67631053, 557.67197531, 513.19971937, 549.70989554, 536.91062758, 506.16003069, 491.52512992, 497.24272732, 498.07812004, 493.31540732, 466.11713957, 465.27153851, 472.07259103, 467.81751422, 460.26920502, 455.0241781, 444.72621822, 442.45571251, 444.69303598, 425.04289608, 417.08655175, 420.64258292, 412.08107436, 409.95552994, 403.92915834, 392.67122317, 397.03226525, 387.3556702, 372.69077205, 381.72895751, 380.47129727, 374.84704807, 358.61910009, 363.80090107, 351.18185982, 361.81458724, 360.9666976, 340.08306397, 346.3066431, 333.24533604, 304.62115637, 321.91976092, 332.58254854, 318.77580959, 330.04724348, 311.60285279, 306.54503813, 299.18830145, 292.01639787, 296.3569155, 290.54422774, 290.49267426, 285.81478547, 267.9957313, 268.92275807, 257.14839041, 273.30812427, 276.60994235, 261.69511671, 240.98514568, 252.76721046, 262.25144451, 238.99335596, 238.6566427, 243.20283187, 245.02042431, 230.95745714, 249.39140489, 242.14942445, 234.24715036, 218.11295851, 234.81734169, 217.71192892, 222.58375473, 230.27377421, 218.81043717, 216.91600489, 211.84324042, 207.04306619, 204.61979991, 218.18749806, 194.78511767, 197.83245126, 190.84421223, 197.34281424, 177.77234062, 186.69054224, 182.1033707, 183.07475726, 185.44817603, 177.78113968, 173.11877005, 172.51053268, 175.86008289])
                elif nBins==275:
                    if ctagCat==0:
                        histContents = np.array([2482.20163473, 2495.38796434, 2485.30737586, 2435.00184894, 2428.80572352, 2431.79310127, 2415.4796623, 2388.24498963, 2351.68201202, 2376.81445464, 2348.43422496, 2328.62018722, 2305.6088949, 2303.35886565, 2286.92558335, 2313.43534017, 2241.3478297, 2291.78018423, 2229.64893848, 2234.44840766, 2187.55139304, 2200.43907926, 2206.63777973, 2203.14888367, 2199.5340725, 2156.74017035, 2165.20711806, 2141.80449432, 2142.74102745, 2127.59808749, 2139.7081515, 2092.04459817, 2088.26159056, 2112.01234574, 2074.18044508, 2028.18437314, 2043.84941936, 2015.61917098, 1992.81574715, 2024.95134769, 2022.77401539, 1976.15970231, 1968.41976498, 1952.92748064, 1936.83755426, 1924.29260117, 1919.54297903, 1900.44495517, 1861.68234489, 1929.86749079, 1871.50524037, 1846.58035579, 1886.71286502, 1837.06404339, 1853.62450698, 1811.65757875, 1836.96441169, 1808.2810227, 1826.03843196, 1811.10878033, 1818.68831086, 1772.48400659, 1770.51592554, 1767.46352996, 1726.72668963, 1702.01579474, 1720.1245384, 1745.64751232, 1710.5482959, 1720.86982611, 1690.87571886, 1682.45275315, 1664.07406704, 1676.70285921, 1668.68049645, 1632.31905894, 1682.93662684, 1630.89429042, 1624.69981955, 1630.78113047, 1600.36898175, 1594.5262939, 1590.87718269, 1597.10212723, 1609.74144533, 1565.57476323, 1581.17603057, 1535.51882336, 1499.07677614, 1544.08412243, 1523.17449309, 1528.0780056, 1501.07528203, 1475.86493365, 1472.62245106, 1457.93667835, 1475.87791859, 1495.13872791, 1466.09241848, 1438.73753452, 1427.07739834, 1451.98260934, 1457.24138035, 1407.93327629, 1403.09610007, 1398.94072323, 1374.49057199, 1396.43237706, 1379.85661343, 1400.98161833, 1367.86820948, 1364.05214842, 1367.84986829, 1382.12161433, 1333.98580152, 1346.67089534, 1347.98545334, 1325.4189145, 1298.7813313, 1295.92078005, 1298.29877425, 1284.85310404, 1286.42860562, 1263.05128146, 1299.83365204, 1280.27532107, 1274.4597028, 1244.40841598, 1244.91313803, 1230.31421204, 1221.29129072, 1219.71880494, 1267.46388119, 1216.66226239, 1215.11336568, 1180.25789338, 1187.42590372, 1189.86273012, 1162.3951235, 1199.04976566, 1180.0946572, 1161.30415049, 1150.44359131, 1166.12958911, 1178.31939319, 1116.11873408, 1132.77710636, 1147.94942329, 1117.96107679, 1118.1907069, 1133.05621051, 1115.06826794, 1103.05683109, 1066.03014009, 1104.40684409, 1045.12878774, 1066.53932772, 1085.46976267, 1084.26352947, 1069.60828506, 1063.48477354, 1058.38703857, 1013.37768809, 1046.6899259, 1005.13479195, 1021.81965318, 1036.7959296, 1026.19497456, 1012.75827132, 1010.00569874, 1002.58099396, 1033.39747716, 984.57544768, 990.59111339, 973.95826004, 968.08865571, 980.3972017, 958.03837194, 980.69032611, 959.3723101, 951.7284611, 961.9728588, 956.19025308, 951.2732019, 968.53349997, 948.64146835, 929.99168786, 916.60811719, 922.44568025, 923.36209779, 937.64857803, 933.53978154, 891.51180231, 913.42155286, 911.10681729, 886.01320966, 889.60694803, 872.69000678, 878.13649709, 928.63723527, 867.44389562, 891.40094164, 834.72669374, 859.81438714, 850.95079081, 840.00770003, 866.78131232, 878.40907437, 862.73613788, 840.88118998, 822.98818937, 825.7008685, 829.26972459, 842.56890932, 799.23978406, 809.69415133, 803.26277212, 792.26592664, 793.2116904, 801.90389861, 792.94724993, 793.58042249, 793.33213295, 775.45923358, 786.63796542, 778.25961679, 722.6592947, 776.09876581, 768.4312451, 758.83555108, 747.15658463, 768.79787571, 761.06740688, 723.62306155, 756.08197472, 752.75173955, 731.87429001, 726.20104627, 743.94163918, 720.7638115, 701.26755534, 712.1715365, 713.18467199, 712.97197345, 691.40190204, 694.75382598, 708.86030438, 700.08474242, 688.60424052, 695.45183064, 686.80544934, 698.15045427, 678.35682574, 667.97455028, 683.86543573, 689.22854371, 672.36830797, 650.57411678, 663.12698861, 654.3575246, 672.08653387, 652.26207833, 642.06696741, 639.2397743, 640.77175733, 656.67186117, 632.93084475, 644.65250565, 656.2732024, 626.45392169, 658.98903297, 640.90909602, 632.60881815, 619.34955396, 625.12764908])
                    elif ctagCat==1:
                        histContents = np.array([293.55935278, 282.20068153, 274.84288563, 284.01620143, 292.03568546, 250.28026441, 278.73123744, 266.5211588, 259.08916317, 269.54326377, 252.99826218, 260.81887582, 255.92935492, 266.11893844, 254.05481749, 273.23977809, 240.46736834, 254.94330503, 251.91182403, 246.19681553, 257.99897729, 247.47603298, 243.44950669, 258.5782267, 245.46313336, 249.81612712, 251.24847831, 241.02059739, 244.18406251, 237.55477989, 229.85618517, 219.60830341, 222.13350686, 234.64510318, 234.79344444, 220.29513189, 244.90528463, 230.89514774, 222.32869183, 217.73183311, 217.68348692, 216.23493312, 234.43827351, 228.71708728, 214.274505, 198.88813862, 211.138489, 210.32244562, 229.37836051, 213.18218116, 217.37501929, 212.08005585, 202.40589128, 203.9332077, 207.27648414, 188.69407947, 204.62607539, 185.38127659, 203.76579334, 206.30063244, 209.2875503, 192.24451015, 199.88661431, 188.24577413, 201.72907847, 190.64607723, 182.65596184, 184.3702267, 185.38409358, 188.33231873, 196.70158185, 174.61609417, 195.44696677, 178.84951428, 194.27594818, 138.96961335, 145.16617248, 131.9412517, 143.53270625, 153.38670327, 138.89603111, 147.78619002, 144.04002601, 138.48018637, 131.84732806, 143.51149845, 125.45452523, 146.71758718, 132.85734147, 131.0110268, 121.0092268, 122.81751841, 126.93628908, 123.22762673, 132.55025627, 135.03519739, 137.31727299, 126.91830978, 126.35621424, 125.73136373, 133.00031057, 132.71870455, 121.39255857, 136.44651799, 118.09200459, 127.48109086, 122.59425634, 120.22326954, 116.9603584, 118.47436444, 119.82089704, 111.58906905, 123.92974908, 115.25653606, 117.77706213, 110.92443269, 115.6599445, 122.19707698, 120.10267118, 112.15277665, 118.63234884, 108.8645575, 114.3042062, 106.04606821, 105.96333603, 106.38324505, 109.29383741, 106.55219272, 107.45249432, 96.38937897, 121.16234916, 104.53518359, 98.44200038, 108.53023261, 117.24830088, 96.28285847, 107.32731613, 105.23170182, 98.71260004, 95.12578594, 104.80193274, 93.64509987, 106.64499348, 104.89754853, 105.02908035, 96.05303966, 94.21180657, 100.20670393, 94.514036, 92.6644125, 101.94841706, 96.29850518, 94.58696155, 102.85497501, 92.53439737, 88.56363812, 95.40063824, 98.06026552, 97.67914084, 100.64517932, 87.02974275, 113.43075814, 94.46143133, 93.57566817, 87.89897443, 81.4310638, 91.07460811, 89.68352518, 91.83422449, 98.90687862, 78.22519751, 96.12650184, 88.54376498, 88.48597725, 88.91424207, 86.1040348, 95.8694731, 89.43963243, 89.66168135, 88.00938971, 86.18095174, 84.96155838, 89.94595989, 87.82466004, 79.84611526, 79.92374239, 88.04548935, 80.13973563, 85.48163742, 78.07226131, 91.3034053, 80.41360115, 88.85897337, 80.1646299, 72.23200601, 74.89577513, 84.46120502, 72.58187697, 80.625609, 76.11219738, 82.01593497, 76.70079967, 73.10787149, 72.62931539, 70.66123334, 74.49963548, 73.13227293, 77.33272643, 74.56095246, 69.26832564, 70.16074635, 79.19472276, 70.25371216, 72.46238005, 76.45137198, 70.99622055, 73.29476324, 70.1109589, 66.99676191, 69.50120514, 69.19533074, 68.36337726, 68.15584373, 70.70208548, 71.95397837])
                else:
                    print("No saved MC for nBins=%d" % nBins)
            elif useUnweightedMC:
                if nBins==55:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                elif nBins==110:
                    if ctagCat==0:
                        histContents = np.array([49198., 48205., 47696., 46856., 46131., 45456., 44790., 44786., 43704., 43309., 42363., 41837., 41601., 41224., 39940., 39503., 39110., 38356., 37982., 37447., 36791., 36576., 35759., 35440., 35100., 34497., 33802., 33567., 33098., 32534., 32151., 31714., 31171., 30902., 30278., 29691., 29556., 28952., 28658., 28558., 28164., 27470., 27100., 26768., 26508., 26291., 25815., 25418., 25179., 24758., 24525., 23987., 23728., 23846., 23255., 22763., 22449., 22450., 21995., 21896., 21517., 21282., 20717., 20881., 20409., 19956., 19918., 19664., 19391., 19199., 18823., 18510., 18481., 18318., 17958., 17870., 17762., 17453., 17100., 17206., 16766., 16641., 16329., 16286., 15896., 15801., 15470., 15303., 15230., 15115., 14538., 14782., 14594., 14384., 14262., 13986., 13818., 13488., 13554., 13489., 13284., 13074., 12925., 12719., 12580., 12426., 12236., 12182., 12219., 12093.])
                    elif ctagCat==1:
                        histContents = np.array([5332., 5274., 5041., 4959., 4782., 4921., 4665., 4714., 4640., 4684., 4653., 4502., 4313., 4383., 4448., 4177., 4186., 4089., 3968., 4043., 3983., 3856., 3826., 3756., 3692., 3683., 3486., 3588., 3576., 3473., 3432., 3375., 3297., 3248., 3310., 3101., 3150., 3078., 3070., 3009., 2982., 2883., 2880., 2759., 2812., 2830., 2811., 2754., 2625., 2637., 2581., 2628., 2586., 2478., 2548., 2448., 2381., 2387., 2442., 2377., 2394., 2329., 2236., 2193., 2166., 2219., 2086., 2069., 2082., 1984., 1985., 1984., 1961., 1963., 1877., 1857., 1853., 1873., 1750., 1833., 1760., 1760., 1716., 1786., 1721., 1651., 1631., 1680., 1604., 1641., 1592., 1569., 1601., 1533., 1502., 1474., 1530., 1453., 1400., 1384., 1414., 1325., 1355., 1329., 1338., 1337., 1307., 1287., 1256., 1262.])
                elif nBins==275:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                else:
                    print("No saved MC for nBins=%d" % nBins)
            else:
                print("No saved MC for useUnweightedMC==False and useMCStats==True")
        else:
            histCenters = np.array([[mHMin+0.5*(mHMax-mHMin)/float(nBins)]])
            for i in range(1, nBins):
                binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
                if binCenter>=mHBlindMax and binCenter<=mHBlindMin: continue
                histCenters = np.append(histCenters, [[binCenter]], axis=0)
            if nBins==55:
                if ctagCat==0:
                    histContents = np.array([13636., 13190., 12584., 12561., 11938., 11614., 11124., 10835., 10603., 10314., 9972., 9969., 9600., 9093., 8942., 6464., 6429., 6259., 5991., 5790., 5811., 5527., 5368., 5285., 5182., 4936., 4870., 4783., 4773., 4414., 4341., 4381., 4181., 4106., 4143., 3890., 3826., 3714., 3621., 3538., 3436., 3343., 3300., 3315., 3205.])
                elif ctagCat==1:
                    histContents = np.array([1405., 1447., 1365., 1317., 1252., 1259., 1254., 1234., 1119., 1086., 1073., 1050., 1023., 959., 980., 737., 680., 723., 692., 590., 589., 566., 619., 565., 517., 557., 542., 528., 531., 437., 538., 503., 444., 463., 411., 432., 390., 401., 408., 398., 377., 412., 362., 402., 340.])
            elif nBins==110: # Modified 21Feb23
                if ctagCat==0:
                    histContents = np.array([6993., 6814., 6792., 6664., 6427., 6327., 6500., 6348., 6147., 5942., 5886., 5863., 5611., 5744., 5523., 5518., 5429., 5322., 5242., 5120., 5122., 5130., 5087., 5030., 4867., 4739., 4768., 4518., 4590., 4572., 3304., 3273., 3279., 3263., 3169., 3062., 3036., 3019., 2942., 2950., 2939., 2894., 2874., 2740., 2791., 2654., 2763., 2610., 2636., 2556., 2565., 2524., 2379., 2493., 2449., 2464., 2399., 2359., 2236., 2198., 2204., 2268., 2245., 2159., 2150., 2119., 2132., 2120., 2038., 2003., 1982., 2023., 1934., 1894., 1918., 1846., 1846., 1779., 1842., 1827., 1730., 1704., 1713., 1761., 1627., 1661., 1678., 1696., 1623., 1564])
                    if doSignalContaminationTest:
                        contaminationSig = np.array([0.00833911, 0.00439661, 0.00369818, 0.0046727, 0.00759872, 0.00720641, 0.0086886, 0.0118637, 0.0108799, 0.00889766, 0.0105946, 0.0136522, 0.017168, 0.0169973, 0.0189905, 0.018624, 0.0298496, 0.0315199, 0.0322636, 0.0465325, 0.070027, 0.0677895, 0.0840317, 0.108839, 0.15128, 0.168299, 0.222134, 0.287239, 0.399502, 0.58242, 0.703282, 0.403314, 0.281748, 0.170888, 0.108815, 0.0859361, 0.062039, 0.0391808, 0.0259872, 0.0213657, 0.013491, 0.0103358, 0.0117638, 0.00478023, 0.00667743, 0.00320873, 0.00295932, 0.00226618, 0.00289559, 0.00277379, 0.0024194, 0.00175494, 0.00378178, 0.000953959, 0.00192691, 0.00184052, 0.00118373, 0.00124365, 0.00101854, 0.000924361, 0.00058244, 0.00026835, 0.00168124, 0.000900111, 0.00196065, 0.00178597, 0.00104225, 0.00091662, 0.00158802, 0.00262094, 0.000982299, 0.000956211, 0.00184419, 0.000552281, 0.00154178, 0.00106159, 0.000820389, 0.000714093, 0.000824327, 0.000832307, 0.00174121, 0.00190692, 0.0014749, 0.000759313, 0.00103366, 0.00177165, 0.000712917, 0.000654306, 0.00112038, 0.000737657]) # SR bins: , 0.833839, 1.30252, 2.01514, 3.12062, 4.74735, 7.62798, 11.1701, 15.748, 20.1228, 23.2509, 23.6728, 21.3234, 17.3891, 13.0653, 9.38141, 6.28803, 4.13097, 2.61488, 1.72381, 1.09186
                        histContents = histContents + 5.*contaminationSig
                        contaminationBkd = np.array([0.0404623, 0.054425, 0.0556908, 0.0504368, 0.0524775, 0.074528, 0.082115, 0.0888001, 0.131045, 0.111824, 0.14904, 0.173599, 0.193097, 0.211882, 0.257794, 0.285077, 0.348843, 0.399903, 0.504087, 0.56209, 0.674188, 0.815909, 1.02159, 1.30371, 1.63544, 2.05569, 2.68777, 3.63282, 4.96709, 7.19467, 8.74173, 5.58459, 3.55341, 2.32756, 1.51493, 0.970045, 0.637272, 0.470933, 0.292089, 0.206577, 0.163863, 0.102007, 0.0890414, 0.0607292, 0.0478541, 0.0334582, 0.026027, 0.0150666, 0.018851, 0.0128835, 0.0148736, 0.0112879, 0.0111941, 0.00748687, 0.00859865, 0.0133183, 0.00402586, 0.00606386, 0.00652299, 0.00924475, 0.00663459, 0.00639704, 0.00691594, 0.0052778, 0.00437735, 0.00525384, 0.00668137, 0.00384135, 0.00578496, 0.00526119, 0.00523483, 0.00408251, 0.00461016, 0.00474472, 0.00388306, 0.00592995, 0.00651044, 0.00584704, 0.0045128, 0.00629507, 0.00551769, 0.00404727, 0.00398998, 0.0044441, 0.00485241, 0.00543826, 0.00453236, 0.00332195, 0.00527697, 0.00330844]) # SR bins: , 10.224, 15.3894, 23.6515, 36.96, 57.2002, 86.8778, 128.171, 174.799, 220.156, 249.371, 254.074, 231.789, 191.573, 146.852, 106.841, 73.7216, 49.407, 32.4962, 21.0651, 13.3196
                        histContents = histContents + 5.*(4.0/55.5)*contaminationBkd # measured uncertainty taken from larger uncertainty from: https://arxiv.org/abs/2207.08615
                elif ctagCat==1:
                    histContents = np.array([643., 634., 653., 646., 611., 594., 588., 585., 576., 542., 552., 571., 569., 559., 553., 554., 542., 504., 465., 477., 485., 472., 485., 446., 444., 469., 431., 441., 436., 444., 327., 323., 305., 332., 329., 304., 313., 293., 294., 245., 275., 266., 248., 270., 267., 265., 256., 240., 255., 220., 251., 246., 263., 236., 225., 252., 234., 215., 215., 219., 253., 215., 241., 182., 199., 193., 231., 179., 203., 201., 181., 184., 188., 174., 172., 186., 204., 162., 176., 165., 171., 171., 194., 185., 146., 189., 163., 157., 154., 148])
                    if doSignalContaminationTest:
                        contaminationSig = np.array([0.00168051, 0.00127254, 0.00126493, 0.00268276, 0.00105908, 0.0043419, 0.00196841, 0.00332332, 0.00617802, 0.00516344, 0.00397782, 0.0049382, 0.00648435, 0.00660974, 0.00497008, 0.00997515, 0.00889791, 0.0166934, 0.0122072, 0.0233954, 0.0246516, 0.0312504, 0.0350335, 0.0397602, 0.0603128, 0.0725622, 0.0831303, 0.12868, 0.167597, 0.221804, 0.25208, 0.155707, 0.0967721, 0.0645579, 0.0403622, 0.0271084, 0.0140005, 0.0106018, 0.00799541, 0.00413916, 0.00516434, 0.00171683, 0.00341377, 0.00111369, 0.00115038, 0.000546879, 0.000369927, 0.000519016, 0.00069846, 0.000589488, 0.000355616, 0.000951946, 0.000505136, 0.000427518, 0.00012114, 0.000958068, 0.00039579, 0.000711661, 0.000276416, 0.000141474, 0.000265682, 0.000219418, 0.00029312, 0.000198161, 0.00048753, 0.000186659, 0.000292066, 0.000167404, 0.000403066, 0.000367273, 0.000193847, 0.000605084, 9.76958e-05, 0.000271001, 0.000295972, 0.000245286, 0.00160295, 0.000210451, 0.000360923, 0.000240284, 0.000836091, 0.00108869, 0.00024389, 0.000559324, 0.000160934, 0.00022218, 0.000215279, 0.000373133, 0.000313932, 0.000390388]) # SR bins: , 0.329052, 0.469679, 0.780462, 1.2245, 1.9179, 2.98181, 4.51305, 6.35282, 8.19285, 9.48282, 9.67097, 8.66386, 6.92238, 5.11213, 3.49726, 2.38915, 1.54844, 0.997274, 0.629207, 0.394685
                        histContents = histContents + 5.*contaminationSig
                        contaminationBkd = np.array([0.0016301, 0.00183322, 0.00350856, 0.00243583, 0.00214301, 0.00492332, 0.00485672, 0.00396, 0.00507234, 0.00672723, 0.00448652, 0.00916645, 0.00531165, 0.0116551, 0.00879293, 0.00991789, 0.0129268, 0.028323, 0.0292175, 0.0362775, 0.0423487, 0.0546181, 0.0480917, 0.0791057, 0.0748255, 0.0928555, 0.141868, 0.18538, 0.261942, 0.362464, 0.389191, 0.285453, 0.161117, 0.119043, 0.0689176, 0.0409079, 0.0270136, 0.0217298, 0.0118522, 0.00741499, 0.00606898, 0.00597849, 0.00735652, 0.00243337, 0.00114038, 0.00115899, 0.000717214, 0.000629384, 0.000530237, 0.000485894, 0.000525339, 0.000333494, 0.000843678, 0.00025898, 0.000500376, 0.000311631, 0.00168799, 0.000664278, 0.000284871, 0.00039422, 0.00050087, 0.000343749, 0.000388807, 0.000351661, 0.000411861, 0.000310557, 0.000280598, 0.000256349, 0.000445191, 0.000287719, 0.000457845, 0.001108, 0.000186609, 0.000200714, 0.000295687, 0.000355636, 0.000119692, 0.000505042, 0.000305103, 0.000119361, 0.000538902, 0.000251238, 0.000115154, 0.000150763, 0.00111119, 0.000194085, 0.000773542, 0.000565493, 0.000132132, 0.000147874]) # SR bins: , 0.539696, 0.791581, 1.24533, 1.92245, 2.88945, 4.55243, 6.73414, 9.28926, 11.5026, 13.2651, 13.3617, 12.1714, 9.91711, 7.58891, 5.3593, 3.60043, 2.48501, 1.56907, 1.05134, 0.654969
                        histContents = histContents + 5.*(4.0/55.5)*contaminationBkd # measured uncertainty taken from larger uncertainty from: https://arxiv.org/abs/2207.08615
            # elif nBins==110: # pre-21Feb23
            #     if ctagCat==0:
            #         histContents = np.array([6926., 6710., 6682., 6508., 6382., 6202., 6283., 6278., 6171., 5767., 5886., 5728., 5569., 5555., 5443., 5392., 5447., 5156., 5297., 5017., 4971., 5001., 5077., 4892., 4863., 4737., 4592., 4501., 4520., 4422., 3275., 3189., 3205., 3224., 3166., 3093., 3023., 2968., 2861., 2929., 2910., 2901., 2746., 2781., 2740., 2628., 2653., 2632., 2625., 2557., 2490., 2446., 2396., 2474., 2392., 2391., 2413., 2360., 2206., 2208., 2166., 2175., 2214., 2167., 2075., 2106., 2083., 2023., 2145., 1998., 1963., 1927., 1901., 1925., 1884., 1830., 1819., 1802., 1734., 1804., 1729., 1707., 1665., 1678., 1674., 1626., 1705., 1610., 1585., 1620.])
            #     elif ctagCat==1:
            #         histContents = np.array([709., 696., 738., 709., 655., 710., 646., 671., 643., 609., 613., 646., 630., 624., 605., 629., 575., 544., 524., 562., 546., 527., 532., 518., 513., 510., 488., 471., 525., 455., 364., 373., 333., 347., 351., 372., 355., 337., 303., 287., 288., 301., 289., 277., 310., 309., 271., 294., 275., 242., 282., 275., 275., 267., 256., 272., 262., 269., 232., 205., 271., 267., 268., 235., 228., 216., 244., 219., 201., 210., 217., 215., 208., 182., 205., 196., 200., 208., 200., 198., 183., 194., 228., 184., 203., 159., 192., 210., 176., 164.])
            elif nBins==275:
                if ctagCat==0:
                    histContents = np.array([2716., 2874., 2668., 2688., 2690., 2669., 2703., 2561., 2640., 2617., 2566., 2534., 2502., 2513., 2469., 2555., 2491., 2488., 2575., 2452., 2496., 2438., 2435., 2331., 2238., 2367., 2368., 2312., 2305., 2262., 2267., 2183., 2231., 2247., 2196., 2200., 2125., 2137., 2208., 2165., 2176., 2165., 2164., 2076., 2022., 2148., 2096., 2057., 1968., 2045., 1954., 2024., 1987., 1992., 2015., 2076., 1967., 2003., 2009., 1914., 1962., 1959., 1873., 1926., 1880., 1858., 1777., 1843., 1834., 1781., 1842., 1782., 1765., 1743., 1810., 1328., 1335., 1237., 1295., 1269., 1299., 1260., 1313., 1284., 1273., 1299., 1265., 1232., 1248., 1215., 1233., 1159., 1208., 1199., 1192., 1137., 1135., 1164., 1234., 1120., 1212., 1144., 1140., 1161., 1154., 1111., 1072., 1141., 1125., 1078., 1121., 1112., 991., 1059., 1085., 1054., 1052., 1088., 1052., 1039., 1027., 1086., 1041., 1004., 1024., 992., 988., 976., 1038., 942., 945., 973., 973., 987., 992., 948., 941., 984., 987., 923., 980., 970., 953., 892., 978., 884., 902., 864., 911., 853., 850., 887., 864., 877., 863., 907., 894., 848., 825., 907., 841., 804., 862., 860., 814., 807., 874., 831., 789., 805., 870., 870., 783., 834., 786., 779., 798., 765., 777., 771., 782., 777., 725., 778., 764., 748., 766., 759., 733., 708., 704., 718., 773., 762., 664., 694., 719., 644., 782., 699., 674., 690., 707., 670., 695., 671., 654., 679., 655., 684., 692., 648., 660., 650., 650., 693., 684., 659., 629., 650., 648., 612., 658., 631., 656.])
                elif ctagCat==1:
                    histContents = np.array([303., 281., 273., 298., 250., 322., 282., 272., 291., 280., 257., 256., 286., 281., 285., 264., 252., 275., 251., 275., 255., 257., 242., 239., 259., 259., 249., 235., 243., 273., 261., 246., 248., 244., 255., 226., 253., 237., 258., 260., 218., 242., 234., 216., 209., 212., 216., 206., 221., 231., 220., 223., 183., 229., 218., 234., 211., 190., 208., 207., 191., 211., 224., 199., 198., 223., 164., 209., 179., 184., 200., 210., 215., 170., 185., 136., 146., 157., 151., 147., 123., 147., 138., 127., 145., 137., 151., 133., 163., 139., 164., 131., 138., 144., 115., 117., 136., 112., 114., 111., 112., 113., 117., 140., 107., 123., 118., 106., 118., 101., 118., 126., 117., 136., 122., 106., 110., 113., 127., 109., 100., 113., 105., 96., 103., 118., 110., 115., 106., 108., 115., 106., 110., 117., 94., 106., 99., 109., 102., 112., 106., 109., 98., 106., 112., 94., 101., 71., 95., 76., 123., 98., 116., 92., 109., 116., 98., 106., 96., 87., 87., 87., 108., 88., 74., 89., 96., 100., 86., 92., 84., 86., 70., 83., 88., 90., 89., 86., 82., 85., 78., 83., 87., 74., 68., 82., 80., 76., 87., 76., 82., 87., 73., 85., 81., 92., 79., 71., 71., 85., 79., 73., 81., 67., 77., 93., 98., 62., 87., 72., 92., 71., 75., 59., 65., 81., 75., 64., 94., 88., 71., 74., 65., 73., 57.])
            else:
                print("No saved data for nBins=%d" % nBins)
    elif useMC:
        # print("Reading dataset")
        print("Reading MC16a")
        inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16a.Sherpa2_diphoton_myy_90_175.MxAODDetailed.e6452_s3126_r9364_p4097_h025.root")
        sumWA = inFile["CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"].pandas().values[3,0]
        inTree = inFile["CollectionTree"]
        isPassed = inTree.array("HGamEventInfoAuxDyn.isPassed")
        ctag = inTree.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose")
        mH = inTree.array("HGamEventInfoAuxDyn.m_yy")
        weights = inTree.array("HGamEventInfoAuxDyn.weight")
        weights *= inTree.array("HGamEventInfoAuxDyn.weightJvt")
        weights *= inTree.array("HGamEventInfoAuxDyn.crossSectionBRfilterEff")
        weights *= lumiA
        weights /= sumWA
        print("Reading MC16d")
        inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16d.Sherpa2_diphoton_myy_90_175.MxAODDetailed.e6452_s3126_r10201_p4097_h025.root")
        sumWD = inFile["CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"].pandas().values[3,0]
        inTree = inFile["CollectionTree"]
        isPassed = np.append(isPassed, inTree.array("HGamEventInfoAuxDyn.isPassed"))
        ctag = np.append(ctag, inTree.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        mH = np.append(mH, inTree.array("HGamEventInfoAuxDyn.m_yy"))
        weightsD = inTree.array("HGamEventInfoAuxDyn.weight")
        weightsD *= inTree.array("HGamEventInfoAuxDyn.weightJvt")
        weightsD *= inTree.array("HGamEventInfoAuxDyn.crossSectionBRfilterEff")
        weightsD *= lumiD
        weightsD /= sumWD
        weights = np.append(weights, weightsD)
        print("Reading MC16e")
        inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16e.Sherpa2_diphoton_myy_90_175.MxAODDetailed.e6452_s3126_r10724_p4097_h025.root")
        sumWE = inFile["CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"].pandas().values[3,0]
        inTree = inFile["CollectionTree"]
        isPassed = np.append(isPassed, inTree.array("HGamEventInfoAuxDyn.isPassed"))
        ctag = np.append(ctag, inTree.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        mH = np.append(mH, inTree.array("HGamEventInfoAuxDyn.m_yy"))
        weightsE = inTree.array("HGamEventInfoAuxDyn.weight")
        weightsE *= inTree.array("HGamEventInfoAuxDyn.weightJvt")
        weightsE *= inTree.array("HGamEventInfoAuxDyn.crossSectionBRfilterEff")
        weightsE *= lumiE
        weightsE /= sumWE
        weights = np.append(weights, weightsE)
        # Apply cuts
        mH = mH/1000.
        ctag = ctag[isPassed==True]
        mH = mH[isPassed==True]
        weights = weights[isPassed==True]
        mH = mH[ctag==ctagCat]
        weights = weights[ctag==ctagCat]
        weights = weights[mH>mHMin]
        weights = weights[mH<mHMax]
        mH = mH[mH>mHMin]
        mH = mH[mH<mHMax]
        if useUnweightedMC: weights = np.ones_like(weights)

        histContents = np.array(stats.binned_statistic(mH, weights, statistic="sum", bins=nBins, range=(mHMin, mHMax))[0])
        histEdges = np.array(stats.binned_statistic(mH, weights, statistic="sum", bins=nBins, range=(mHMin, mHMax))[1])
        histCenters = histEdges[:-1]+(histEdges[1]-histEdges[0])/2.
        if useMCStats:
            histUncertainties = np.zeros_like(histContents)
            for i in range(histUncertainties.shape[0]):
                if i==histUncertainties.shape[0]-1: weightsInBin = weights[(histEdges[i]<=mH)*(mH<=histEdges[i+1])]
                else: weightsInBin = weights[(histEdges[i]<=mH)*(mH<histEdges[i+1])]
                histUncertainties[i] = np.sqrt(np.sum(weightsInBin*weightsInBin))
    else:
        # print("Reading dataset")
        print("Reading 2015 data")
        tree15 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data15_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        print("Reading 2016 data")
        tree16 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data16_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        print("Reading 2017 data")
        tree17 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data17_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        print("Reading 2018 data")
        tree18 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data18_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        isPassed = tree15.array("HGamEventInfoAuxDyn.isPassed")
        isPassed = np.append(isPassed, tree16.array("HGamEventInfoAuxDyn.isPassed"))
        isPassed = np.append(isPassed, tree17.array("HGamEventInfoAuxDyn.isPassed"))
        isPassed = np.append(isPassed, tree18.array("HGamEventInfoAuxDyn.isPassed"))
        ctag = tree15.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose")
        ctag = np.append(ctag, tree16.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        ctag = np.append(ctag, tree17.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        ctag = np.append(ctag, tree18.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        mH = tree15.array("HGamEventInfoAuxDyn.m_yy")
        mH = np.append(mH, tree16.array("HGamEventInfoAuxDyn.m_yy"))
        mH = np.append(mH, tree17.array("HGamEventInfoAuxDyn.m_yy"))
        mH = np.append(mH, tree18.array("HGamEventInfoAuxDyn.m_yy"))
        mH = mH/1000.
        ctag = ctag[isPassed==True]
        mH = mH[isPassed==True]
        mH = mH[ctag==ctagCat]
        mH = mH[mH>mHMin]
        mH = mH[mH<mHMax]
        mH = mH[(mH>mHBlindMin)+(mH<mHBlindMax)]
        # inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data15_13TeV.physics_Main.MxAOD.p4096.h025.root")
        # df = inFile["CollectionTree"].pandas.df()
        # df = df[df["HGamEventInfoAuxDyn.isPassed"]==True]
        # df = df[df["HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"]==ctagCat]
        # df = df[df["HGamEventInfoAuxDyn.m_yy"]>mHMin]
        # df = df[df["HGamEventInfoAuxDyn.m_yy"]<mHMax]
        # df = df[(df["HGamEventInfoAuxDyn.m_yy"]>mHBlindMin)+(df["HGamEventInfoAuxDyn.m_yy"]<mHBlindMax)]
        # mH = df["HGamEventInfoAuxDyn.m_yy"].values

        histContents = np.array(stats.binned_statistic(mH, np.ones_like(mH), statistic="sum", bins=nBins, range=(mHMin, mHMax))[0])
        histEdges = np.array(stats.binned_statistic(mH, np.ones_like(mH), statistic="sum", bins=nBins, range=(mHMin, mHMax))[1])
        histCenters = histEdges[:-1]+(histEdges[1]-histEdges[0])/2.

        histContents = histContents[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]
        histCenters = histCenters[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]

        histCenters = np.expand_dims(histCenters, axis=1)

    if not useSavedData:
        print("Inputs:")
        print(histCenters)
        print(histContents)
        if useMCStats:
            print("MC Bin Uncertainties:")
            print(histUncertainties)
        print(np.sum(histContents))

    if useMC or useSSTemplate:

        SRContents = np.array(histContents[(histCenters<=mHBlindMin)*(histCenters>=mHBlindMax)])
        print("SR MC:")
        print(SRContents)

        if useMCStats:
            SRUncertainties = np.array(histUncertainties[(histCenters<=mHBlindMin)*(histCenters>=mHBlindMax)])
        SRCenters = np.array(histCenters[(histCenters<=mHBlindMin)*(histCenters>=mHBlindMax)])

        histContents = histContents[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]
        if useMCStats:
            histUncertainties = histUncertainties[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]
        histCenters = histCenters[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]

        histCenters = np.expand_dims(histCenters, axis=1)

    # print("Adding data")
    if useMCStats:
        gp.AddData(histCenters, histContents, cutoff, noiseConst*histUncertainties)
    else:
        gp.AddData(histCenters, histContents, cutoff, noiseConst*np.sqrt(histContents))

    # print("Building GP")
    # gp.BuildGP("rbf", amplitude, lengthScale)
    gp.BuildGP(kernel, amplitude, lengthScale, auxPar)

    # print("Finding optimal point")
    # gp.FindMinimum()

    # if np.amin(np.linalg.eigvals(covBins))<0: covBins += -2.*np.amin(np.linalg.eigvals(covBins))*np.identity(10)
    print("SR:")
    requestedMasses = np.array([[mHMin+0.5*(mHMax-mHMin)/float(nBins)]])
    for i in range(1, nBins):
        binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
        if not (binCenter>=mHBlindMax and binCenter<=mHBlindMin): continue
        requestedMasses = np.append(requestedMasses, [[binCenter]], axis=0)
    if not (requestedMasses[0]>=mHBlindMax and requestedMasses[0]<=mHBlindMin):
        requestedMasses = np.delete(requestedMasses, 0, axis=0)
    meanBins, covBins = gp.GetVal(requestedMasses, normaliseData)
    print("Means:")
    print(meanBins)
    print(np.sum(meanBins))
    print("Covariances:")
    print(covBins)
    print(np.sum(covBins))

    print("All:")
    requestedMasses = []
    firstMass = mHMin+float(mHMax-mHMin)/float(nBins)/2.0
    while firstMass<mHMax:
        thisMass = [firstMass]
        requestedMasses.append(thisMass)
        firstMass+=float(mHMax-mHMin)/float(nBins)
    meanBins, covBins = gp.GetVal(requestedMasses, normaliseData)
    print("Means:")
    print(meanBins)
    print("Covariances:")
    print(covBins)
    # for i in range(10):
    #     diag = np.sqrt(covBins[i,i])
    #     covBins[i,:] = covBins[i,:]/diag
    #     covBins[:,i] = covBins[:,i]/diag
    # print("Covariances:")
    # print(covBins)
    # print("Inverse Covariances:")
    # print(np.linalg.inv(covBins))
    # print("Eigenvals of Covariance Matrix:")
    # print(np.linalg.eigvals(covBins))
    # print("Eigendecomposition of Covariance Matrix:")
    # print(np.linalg.eigh(covBins))
    # print("Covariance times Inverse Covariance Matrix:")
    # print(np.matmul(covBins, np.linalg.inv(covBins)))

    if doPlot:
        print("Plotting GP")
        if useMC:
            if useMCStats:
                gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, SRUncertainties], ctagCat)
            else:
                gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, np.sqrt(SRContents)], ctagCat)
        elif useSSTemplate:
            if useMCStats:
                gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, SRUncertainties], ctagCat)
            else:
                gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, np.sqrt(SRContents)], ctagCat)
        else:
            SRCentersList = []
            for i in range(20): SRCentersList.append(120.25+float(i*0.5))
            SRCenters = np.array(SRCentersList)
            if ctagCat==1:
                SRContents = [393.00000, 396.00000, 404.00000, 436.00000, 382.00000, 365.00000, 403.00000, 419.00000, 401.00000, 413.00000, 410.00000, 375.00000, 402.00000, 383.00000, 390.00000, 344.00000, 342.00000, 351.00000, 328.00000, 340.00000]
            else:
                SRContents = [4435.0000, 4436.0000, 4277.0000, 4309.0000, 4330.0000, 4095.0000, 4221.0000, 4056.0000, 4233.0000, 4159.0000, 4076.0000, 4123.0000, 3868.0000, 3789.0000, 3702.0000, 3602.0000, 3616.0000, 3563.0000, 3452.0000, 3373.0000]
            # gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, np.sqrt(SRContents)], ctagCat)
            gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.09, [], ctagCat)

        # gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.025 if ctagCat==1 else 0.025)

    print("Kernel hyperparameters (post-fit, transformed), both:")
    print(gp.gp.kernel_.get_params())

            
