#!/usr/bin/env python

import os
import sys
import argparse
import math
# from GibbsKernel import *
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel, RBF, Matern, RationalQuadratic
import numpy as np
# import matplotlib
# matplotlib.use("Agg") # stops matplotlib crashing when remote disconneted for a while
# import matplotlib.pyplot as plt
# from matplotlib import cm
# from sklearn.externals import joblib
from scipy.special import erf
from scipy import stats
import uproot


class GP():

    def __init__(self, discretisation, calcFullCovMat, memorySaverMode, normaliseData):
        self.discretisation = discretisation
        self.calcFullCovMat = calcFullCovMat # This includes all uncertainty correlations, but has major memory requirements
        self.normaliseData = normaliseData
        self.varNames = []
        self.varRanges = []
        self.varRangesOriginal = []
        self.varIsLog = []
        self.varGrids = []
        self.memorySaverMode = memorySaverMode
        if self.memorySaverMode: self.calcFullCovMat=False

    def AddVariable(self, name, varRange, isLog):
        self.varNames.append(name)
        self.varIsLog.append(isLog)
        self.varRangesOriginal.append(varRange)
        if isLog: varRange = [np.log(varRange[0]), np.log(varRange[1])]
        self.varRanges.append(varRange)
        self.varGrids.append(np.arange(varRange[0], (self.discretisation*varRange[1]-varRange[0]-(varRange[1]-varRange[0])/2.)/float(self.discretisation-1), (varRange[1]-varRange[0])/float(self.discretisation-1)))

    def AddData(self, X, Y, cutoff=-1., noise=[]):
        self.trainDataValues = X
        self.trainDataTargets = Y
        for var in range(len(self.varRanges)):
            nData = self.trainDataTargets.shape[0]
            for i in range(nData):
                j = nData-i-1
                if self.trainDataValues[j,var]<self.varRangesOriginal[var][0] or self.trainDataValues[j,var]>self.varRangesOriginal[var][1]:
                    self.trainDataValues = np.delete(self.trainDataValues, j, axis=0)
                    self.trainDataTargets = np.delete(self.trainDataTargets, j, axis=0)
        self.cutoff = cutoff
        if self.cutoff>0.:
            self.trainDataValuesCut = np.copy(self.trainDataValues)
            self.trainDataTargetsCut = np.copy(self.trainDataTargets)
            nData = self.trainDataTargets.shape[0]
            for i in range(nData):
                j = nData-i-1
                if self.trainDataTargets[j]>self.cutoff:
                    self.trainDataValues = np.delete(self.trainDataValues, j, axis=0)
                    self.trainDataTargets = np.delete(self.trainDataTargets, j, axis=0)
                else:
                    self.trainDataValuesCut = np.delete(self.trainDataValuesCut, j, axis=0)
                    self.trainDataTargetsCut = np.delete(self.trainDataTargetsCut, j, axis=0)
        self.noise = noise
        for var in range(len(self.varIsLog)):
            if self.varIsLog[var]:
                self.trainDataValues[:,var] = np.log(self.trainDataValues[:,var])
                if self.cutoff>0.: self.trainDataValuesCut[:,var] = np.log(self.trainDataValuesCut[:,var])
        if self.normaliseData:
            for var in range(len(self.varRanges)):
                self.trainDataValues[:,var]=(self.trainDataValues[:,var]-self.varRanges[var][0])/(self.varRanges[var][1]-self.varRanges[var][0])
                if self.cutoff>0.: self.trainDataValuesCut[:,var]=(self.trainDataValuesCut[:,var]-self.varRanges[var][0])/(self.varRanges[var][1]-self.varRanges[var][0])
                self.varRanges[var]=[0., 1.]
                self.varGrids[var]=np.arange(self.varRanges[var][0], (self.discretisation*self.varRanges[var][1]-self.varRanges[var][0]-(self.varRanges[var][1]-self.varRanges[var][0])/2.)/float(self.discretisation-1), (self.varRanges[var][1]-self.varRanges[var][0])/float(self.discretisation-1))
            self.trainDataTargetsMean = np.mean(self.trainDataTargets)
            self.trainDataTargetsStd = np.std(self.trainDataTargets)
            self.trainDataTargets=(self.trainDataTargets-self.trainDataTargetsMean)/self.trainDataTargetsStd
            if self.cutoff>0.: self.trainDataTargetsCut=(self.trainDataTargetsCut-self.trainDataTargetsMean)/self.trainDataTargetsStd
            self.noise = self.noise/self.trainDataTargetsStd

    def BuildGrids(self, nLoop):
        assert(nLoop>=1)
        if nLoop>1:
            for i in range(self.discretisation):
                tempList = list(self.gridMapKey)
                tempList[len(self.varNames)-nLoop] = int(i)
                self.gridMapKey = tuple(tempList)
                self.BuildGrids(nLoop-1)
        elif nLoop==1:
            for i in range(self.discretisation):
                tempList = list(self.gridMapKey)
                tempList[len(self.varNames)-nLoop] = int(i)
                self.gridMapKey = tuple(tempList)
                self.gridMap[self.gridMapKey] = self.gridMapValue
                self.gridMapValue += 1
                flatGridEntry = []
                for j in range(len(self.gridMapKey)):
                    flatGridEntry.append(self.varGrids[j][self.gridMapKey[j]])
                self.flatGrid.append(np.array(flatGridEntry))

    def BuildGP(self, kernel="rbf", amplitude=1., lengthScale=0.05, auxPar=0.5):
        self.amplitude = amplitude
        self.lengthScale = lengthScale
        if kernel=="rbf": self.kernel = ConstantKernel(amplitude**2)*RBF(lengthScale)
        # if kernel=="rbf": self.kernel = ConstantKernel(constant_value=amplitude**2, constant_value_bounds="fixed")*RBF(length_scale=lengthScale, length_scale_bounds="fixed")
        elif kernel=="matern": self.kernel = ConstantKernel(amplitude**2)*Matern(lengthScale, (1.e-5, 10000.), auxPar)
        elif kernel=="rq": self.kernel = ConstantKernel(amplitude**2)*RationalQuadratic(lengthScale, auxPar)
        # elif kernel=="gibbs": self.kernel = ConstantKernel(amplitude**2)*Gibbs(1.0, (1e-5,1e5), 1.0, (1e-5,1e5))
        self.gp = GaussianProcessRegressor(kernel=self.kernel, alpha=np.square(self.noise), copy_X_train=False, normalize_y=self.normaliseData)
        # self.gp = GaussianProcessRegressor(kernel=self.kernel, alpha=np.square(self.noise), copy_X_train=False, normalize_y=self.normaliseData, optimizer=None)
        # self.gp = GaussianProcessRegressor(kernel=self.kernel, alpha=np.square(self.noise), copy_X_train=False, normalize_y=self.normaliseData, n_restarts_optimizer=100)
        if self.trainDataValues.size>0: self.gp.fit(self.trainDataValues, self.trainDataTargets)
        # self.flatGrid = np.stack(self.varGrids, axis=1)
        self.flatGrid = []
        self.gridMap = {}
        self.gridMapValue = 0
        self.gridMapKey = (-1,)*len(self.varNames)
        self.BuildGrids(len(self.varNames))
        if self.calcFullCovMat:
            self.mu_post, self.unc_post = self.gp.predict(self.flatGrid, return_cov=True)
        elif self.memorySaverMode:
            self.mu_post = np.zeros((len(self.flatGrid)))
            self.unc_post = np.zeros((len(self.flatGrid)))
            for i in range(len(self.flatGrid)):
                self.mu_post[i], self.unc_post[i] = self.gp.predict([self.flatGrid[i]], return_std=True)
        else:
            self.mu_post, self.unc_post = self.gp.predict(self.flatGrid, return_std=True)

    def CreatePopulation(self, populationSize):
        self.population = []
        for i in range(populationSize):
            individual = []
            for var in range(len(self.varNames)):
                if self.varIsLog[var]: individual.append(math.exp(np.random.uniform(math.log(self.varRanges[var][0]), math.log(self.varRanges[var][1]))))
                else: individual.append(np.random.uniform(self.varRanges[var][0], self.varRanges[var][1]))
            self.population.append(individual)

    # def PlotPopulation(self):
    #     if not os.path.exists("Outputs"): os.mkdir("Outputs")
    #     for var in range(len(self.varNames)):
    #         varVals = []
    #         for i in range(len(self.population)):
    #             varVals.append(self.population[i][var])
    #         fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8,8))
    #         axs.hist(varVals, bins=100, range=self.varRanges[var], density=True, histtype="step", linewidth=1, edgecolor="black")
    #         axs.title.set_text("")
    #         plt.xlabel(self.varNames[var])
    #         plt.ylabel("Individuals")
    #         fig.tight_layout(pad=2.0)
    #         fig.savefig("Outputs/"+self.varNames[var]+".png")
    #         plt.close()

    def FindMinimum(self):
        minX = np.argmin(self.mu_post)
        self.minY = np.amin(self.mu_post)
        for gridPoint in range(len(self.flatGrid)):
            if gridPoint==minX:
                self.minima = []
                self.minimaErr = []
                for var in range(len(self.varNames)):
                    self.minima.append(self.flatGrid[gridPoint][var])
                    self.minimaErr.append((self.varRanges[var][1]-self.varRanges[var][0])/float(self.discretisation-1)/2.)
                    if self.calcFullCovMat: self.minYErr = np.sqrt(np.diag(self.unc_post)[gridPoint])
                    else: self.minYErr = self.unc_post[gridPoint]
                print("Minimum gridPoint found at %s +- %s: %f +- %f" % (str(self.minima), str(self.minimaErr), self.minY, self.minYErr))

    def GetVal(self, args, normalised):
        assert(len(self.varNames)==1)
        if normalised: args = (np.array(args)-self.varRangesOriginal[0][0])/(self.varRangesOriginal[0][1]-self.varRangesOriginal[0][0])
        means, covs = self.gp.predict(args, return_cov=True)
        if normalised:
            means = means*self.trainDataTargetsStd+self.trainDataTargetsMean
            covs = covs*self.trainDataTargetsStd*self.trainDataTargetsStd
        return means, covs

    def EIScan(self, means, stds, currentMin, gridPoints=[], excludedPoints=[]):
        maxEI = -1.
        bestGridPoint = -1
        for gridPoint in range(self.discretisation**len(self.varNames)):
            if gridPoints!=[]:
                pointOutsideRange = False
                for var in range(len(self.varRanges)):
                    if gridPoints[gridPoint][var]<self.varRanges[var][0] or gridPoints[gridPoint][var]>self.varRanges[var][1]:
                        pointOutsideRange = True
                        break
                if pointOutsideRange: continue
            if excludedPoints!=[]:
                pointExcluded = False
                for excludedPoint in excludedPoints:
                    if gridPoint==excludedPoint:
                        pointExcluded = True
                        break
                if pointExcluded: continue
            mean = np.copy(means[gridPoint])
            std = np.copy(stds[gridPoint])
            EI = (currentMin-mean)*(1.+erf((currentMin-mean)/(std*np.sqrt(2.))))/2.+std**2.*stats.norm(mean, std).pdf(currentMin)
            # EI = (currentMin-mean)*stats.norm(mean, std).cdf(currentMin)+std**2.*stats.norm(mean, std).pdf(currentMin) # slower
            if EI>maxEI:
                bestGridPoint = int(gridPoint)
                maxEI = float(EI)
        return bestGridPoint, maxEI

    # def PlotMarginalisedGP(self, var, binWidth=1., histMax=1100., normalised=False, ratioRange=0.1, blindedData=[]):

    #     if not os.path.exists("Outputs"): os.mkdir("Outputs")
    #     matplotlib.rcParams.update({"font.size": 18})
    #     plt.xticks(fontsize=18)
    #     plt.yticks(fontsize=18)

    #     # Calculate GPR mean and uncertainty projections
    #     mu_marginalised = np.zeros((self.discretisation))
    #     uncertainty_marginalised = np.zeros((self.discretisation))
    #     for i in range(self.discretisation):
    #         listND = []
    #         for j in range(len(self.varNames)): listND.append(self.discretisation)
    #         indicesND = np.zeros(listND, dtype=bool)
    #         tempIndices = np.zeros(listND, dtype=bool)
    #         indicesND[tuple([slice(None)]*var+[i])] = True
    #         indices = np.reshape(indicesND, self.discretisation**len(self.varNames))
    #         mu_marginalised[i] = np.sum(self.mu_post[indices])/np.sum(indices)
    #         if self.calcFullCovMat: uncertainty_marginalised[i]=np.sqrt(indices.dot(self.unc_post).dot(indices))/np.sum(indices)
    #         else: uncertainty_marginalised[i]=np.sqrt(np.sum(self.unc_post[indices]*self.unc_post[indices]))/np.sum(indices)

    #     # Undo normalisation if appropriate
    #     if self.normaliseData:
    #         trainDataValues = np.array(self.trainDataValues)
    #         varGrids = list(self.varGrids)
    #         for var in range(len(self.varRangesOriginal)):
    #             trainDataValues[:,var]=trainDataValues[:,var]*(self.varRangesOriginal[var][1]-self.varRangesOriginal[var][0])+self.varRangesOriginal[var][0]
    #             varGrids[var]=varGrids[var]*(self.varRangesOriginal[var][1]-self.varRangesOriginal[var][0])+self.varRangesOriginal[var][0]
    #         trainDataTargets = np.array(self.trainDataTargets)*self.trainDataTargetsStd+self.trainDataTargetsMean
    #         mu_marginalised = mu_marginalised*self.trainDataTargetsStd+self.trainDataTargetsMean
    #         uncertainty_marginalised = uncertainty_marginalised*self.trainDataTargetsStd
    #         noise = np.array(self.noise*self.trainDataTargetsStd)
    #     else:
    #         trainDataValues = np.array(self.trainDataValues)
    #         varGrids = list(self.varGrids)
    #         trainDataTargets = np.array(self.trainDataTargets)
    #         noise = np.array(self.noise)

    #     fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(16,8))

    #     # Data
    #     if trainDataValues is not None:
    #         # axs[0].plot(trainDataValues[:,var], trainDataTargets, "o", color="black", label="Data")
    #         axs[0].errorbar(trainDataValues[:,var], trainDataTargets, yerr=noise, fmt="o", color="black", label="Data")

    #     # Blinded data
    #     if blindedData!=[]:
    #         axs[0].errorbar(blindedData[0], blindedData[1], yerr=blindedData[2], fmt="o", color="red", label="Data (Blinded)")

    #     # Excluded data
    #     if self.cutoff>0.:
    #         if trainDataValuesCut is not None:
    #             axs[0].plot(trainDataValuesCut[:,var], self.trainDataTargetsCut, "rx", color="red", label="Cut Data")

    #     # GPR mean and bands
    #     if self.calcFullCovMat:
    #         axs[0].plot(varGrids[var], mu_marginalised, label="GPR Mean (1 & 2 sig CI)", color="C0")
    #         axs[0].fill_between(varGrids[var], mu_marginalised+uncertainty_marginalised, mu_marginalised-uncertainty_marginalised, alpha=0.2, color="C0")
    #         axs[0].fill_between(varGrids[var], mu_marginalised+uncertainty_marginalised, mu_marginalised+2.*uncertainty_marginalised, alpha=0.1, color="C0")
    #         axs[0].fill_between(varGrids[var], mu_marginalised-2.*uncertainty_marginalised, mu_marginalised-uncertainty_marginalised, alpha=0.1, color="C0")
    #     else:
    #         axs[0].plot(varGrids[var], mu_marginalised, label="GPR Mean (1 & 2 sig CI)", color="C0")

    #     # Ratio plot
    #     axs[1].plot(varGrids[var], np.ones_like(varGrids[var]), label="GPR Mean (1 & 2 sig CI)", color="C0")
    #     if self.calcFullCovMat:
    #         axs[1].fill_between(varGrids[var], (mu_marginalised+uncertainty_marginalised)/mu_marginalised, (mu_marginalised-uncertainty_marginalised)/mu_marginalised, alpha=0.2, color="C0")
    #         axs[1].fill_between(varGrids[var], (mu_marginalised+uncertainty_marginalised)/mu_marginalised, (mu_marginalised+2.*uncertainty_marginalised)/mu_marginalised, alpha=0.1, color="C0")
    #         axs[1].fill_between(varGrids[var], (mu_marginalised-2.*uncertainty_marginalised)/mu_marginalised, (mu_marginalised-uncertainty_marginalised)/mu_marginalised, alpha=0.1, color="C0")
    #         if trainDataValues is not None:
    #             means, covs = self.GetVal((trainDataValues[:,var]).reshape(-1, 1), self.normaliseData)
    #             # axs[1].plot(trainDataValues[:,var], trainDataTargets/means, "rx", color="black", label="Data")
    #             axs[1].errorbar(trainDataValues[:,var], trainDataTargets/means, yerr=noise/means, fmt="o", color="black", label="Data")
    #         if blindedData!=[]:
    #             means, covs = self.GetVal((blindedData[0]).reshape(-1, 1), self.normaliseData)
    #             axs[1].errorbar(blindedData[0], blindedData[1]/means, yerr=blindedData[2]/means, fmt="o", color="red", label="Data (Blinded)")
    #     else:
    #         axs[1].plot(varGrids[var], mu_marginalised, label="GPR Mean", color="C0")
    #         print("Uncertainty band not coded for self.calcFullCovMat==False")

    #     # Style changes and make plot
    #     plt.setp(axs[0].spines.values(), linewidth=2)
    #     plt.setp(axs[1].spines.values(), linewidth=2)
    #     axs[0].xaxis.set_tick_params(which="both", width=2, length=5, direction="in")
    #     axs[0].yaxis.set_tick_params(which="both", width=2, length=5, direction="in")
    #     axs[1].xaxis.set_tick_params(which="both", width=2, length=5, direction="in")
    #     axs[1].yaxis.set_tick_params(which="both", width=2, length=5, direction="in")
    #     axs[0].legend(loc="upper right", frameon=False, handlelength=1)
    #     fig.tight_layout(pad=3.)
    #     axs[1].legend(loc="upper right", frameon=False, handlelength=1)
    #     axs[1].set_ylim(1.-ratioRange, 1.+ratioRange)
    #     axs[0].set_xlabel(r"$m_{\gamma\gamma}$ [GeV]", size=22)
    #     axs[1].set_xlabel(r"$m_{\gamma\gamma}$ [GeV]", size=22)
    #     axs[0].set_ylabel("Events / GeV", size=22)
    #     axs[1].set_ylabel("Data / GPR Mean", size=22)
    #     axs[0].text(0.25, 0.1, r"ATLAS $\mathrm{Internal}$", fontweight="bold", style="italic", verticalalignment="center", horizontalalignment="center", transform=axs[0].transAxes)
    #     axs[1].text(0.25, 0.1, r"ATLAS $\mathrm{Internal}$", fontweight="bold", style="italic", verticalalignment="center", horizontalalignment="center", transform=axs[1].transAxes)
    #     plt.savefig("Outputs/GP_var"+str(var)+".png")
    #     plt.close()


if __name__ == "__main__":

    np.set_printoptions(threshold=sys.maxsize)

    parser = argparse.ArgumentParser()
    parser.add_argument("--nTrainData", default=200, type=int)
    parser.add_argument("--ctagCat", default=-1, type=int)
    parser.add_argument("--histNum", default=-1, type=int)
    args = parser.parse_args()
    nTrainData = args.nTrainData
    ctagCat = args.ctagCat
    histNum = args.histNum

    nTrainData = 0
    discretisation = 600
    memorySaverMode = False
    calcFullCovMat = True
    normaliseData = True
    cutoff = -1.                                    # skim data with metric>cutoff, applied to remove outliers from failed attempts that scew optimisation
    noiseConst = 1.                                 # noise on data, set to 1.e-10 (computational reasons) if data noise-free, else set to data uncertainty
    kernel = "rbf"                                  # choice of GPR kernel, options: rbf, matern
    amplitude = 1.                                  # amplitude of prior uncertainty, set to the y-scale under consideration
    lengthScale = 1./110. if normaliseData else 0.5 # length scale of correlations, set to required scale to cover full volume of input space
    auxPar = 10.                                    # auxPar of non-RBF kernel

    useSavedData = True
    useMC = False
    useUnweightedMC = False
    useMCStats = False
    useSSTemplate = False
    doSignalContaminationTest = False
    mHMin = 105.
    mHMax = 160.
    mHBlindMin = 130.
    mHBlindMax = 120.
    nBins = 110
    histMax = 300.
    doPlot = True
    # ctagCat = 1           # c-tag category 1 is tagged and 0 is not
    lumiA=3219.56+32994.9;
    lumiD=44307.4;
    lumiE=58450.1;

    # print("Instantiating GP")
    gp = GP(discretisation, calcFullCovMat, memorySaverMode, normaliseData)

    # print("Adding variables")
    gp.AddVariable("myy", [mHMin, mHMax], False)

    if useSavedData:
        if useSSTemplate:
            if nBins!=110:
                print("nBins!=110 for useSSTemplate==True exiting")
                exit()
            if useMC:
                print("useMC==True for useSSTemplate==True exiting")
                exit()
            histCenters = np.array([mHMin+0.5*(mHMax-mHMin)/float(nBins)])
            for i in range(1, nBins):
                binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
                histCenters = np.append(histCenters, [binCenter], axis=0)
            if ctagCat==0:
                histContents = np.array([])
                if useMCStats:
                    histUncertainties = np.array([])
            elif ctagCat==1:
                histContents = np.array([])
                if useMCStats:
                    histUncertainties = np.array([])
        elif useMC:
            histCenters = np.array([mHMin+0.5*(mHMax-mHMin)/float(nBins)])
            for i in range(1, nBins):
                binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
                histCenters = np.append(histCenters, [binCenter], axis=0)
            if not useUnweightedMC and not useMCStats:
                if nBins==55:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                elif nBins==110:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                elif nBins==275:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                else:
                    print("No saved MC for nBins=%d" % nBins)
            elif useUnweightedMC:
                if nBins==55:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                elif nBins==110:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                elif nBins==275:
                    if ctagCat==0:
                        histContents = np.array([])
                    elif ctagCat==1:
                        histContents = np.array([])
                else:
                    print("No saved MC for nBins=%d" % nBins)
            else:
                print("No saved MC for useUnweightedMC==False and useMCStats==True")
        else:
            histCenters = np.array([[mHMin+0.5*(mHMax-mHMin)/float(nBins)]])
            for i in range(1, nBins):
                binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
                if binCenter>=mHBlindMax and binCenter<=mHBlindMin: continue
                histCenters = np.append(histCenters, [[binCenter]], axis=0)
            if nBins==55:
                if ctagCat==0:
                    histContents = np.array([])
                elif ctagCat==1:
                    histContents = np.array([])
            elif nBins==110:
                # inFile = uproot.open("/afs/cern.ch/user/e/ereynold/private/cH/full_run2_hcgamgam/GPRBkd/Misc/ToyTest/toys.root")
                inFile = uproot.open("toys.root")
                if ctagCat==0: histName = "nonctag_"+str(histNum)
                elif ctagCat==1: histName = "ctag_"+str(histNum)
                histContents = inFile[histName].values
                # print(histContents)
                histContents = np.delete(histContents, np.arange(30,50), axis=0)
                # print(histContents)
            elif nBins==275:
                if ctagCat==0:
                    histContents = np.array([])
                elif ctagCat==1:
                    histContents = np.array([])
            else:
                print("No saved data for nBins=%d" % nBins)
    elif useMC:
        # print("Reading dataset")
        print("Reading MC16a")
        inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16a.Sherpa2_diphoton_myy_90_175.MxAODDetailed.e6452_s3126_r9364_p4097_h025.root")
        sumWA = inFile["CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"].pandas().values[3,0]
        inTree = inFile["CollectionTree"]
        isPassed = inTree.array("HGamEventInfoAuxDyn.isPassed")
        ctag = inTree.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose")
        mH = inTree.array("HGamEventInfoAuxDyn.m_yy")
        weights = inTree.array("HGamEventInfoAuxDyn.weight")
        weights *= inTree.array("HGamEventInfoAuxDyn.weightJvt")
        weights *= inTree.array("HGamEventInfoAuxDyn.crossSectionBRfilterEff")
        weights *= lumiA
        weights /= sumWA
        print("Reading MC16d")
        inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16d.Sherpa2_diphoton_myy_90_175.MxAODDetailed.e6452_s3126_r10201_p4097_h025.root")
        sumWD = inFile["CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"].pandas().values[3,0]
        inTree = inFile["CollectionTree"]
        isPassed = np.append(isPassed, inTree.array("HGamEventInfoAuxDyn.isPassed"))
        ctag = np.append(ctag, inTree.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        mH = np.append(mH, inTree.array("HGamEventInfoAuxDyn.m_yy"))
        weightsD = inTree.array("HGamEventInfoAuxDyn.weight")
        weightsD *= inTree.array("HGamEventInfoAuxDyn.weightJvt")
        weightsD *= inTree.array("HGamEventInfoAuxDyn.crossSectionBRfilterEff")
        weightsD *= lumiD
        weightsD /= sumWD
        weights = np.append(weights, weightsD)
        print("Reading MC16e")
        inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16e.Sherpa2_diphoton_myy_90_175.MxAODDetailed.e6452_s3126_r10724_p4097_h025.root")
        sumWE = inFile["CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"].pandas().values[3,0]
        inTree = inFile["CollectionTree"]
        isPassed = np.append(isPassed, inTree.array("HGamEventInfoAuxDyn.isPassed"))
        ctag = np.append(ctag, inTree.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        mH = np.append(mH, inTree.array("HGamEventInfoAuxDyn.m_yy"))
        weightsE = inTree.array("HGamEventInfoAuxDyn.weight")
        weightsE *= inTree.array("HGamEventInfoAuxDyn.weightJvt")
        weightsE *= inTree.array("HGamEventInfoAuxDyn.crossSectionBRfilterEff")
        weightsE *= lumiE
        weightsE /= sumWE
        weights = np.append(weights, weightsE)
        # Apply cuts
        mH = mH/1000.
        ctag = ctag[isPassed==True]
        mH = mH[isPassed==True]
        weights = weights[isPassed==True]
        mH = mH[ctag==ctagCat]
        weights = weights[ctag==ctagCat]
        weights = weights[mH>mHMin]
        weights = weights[mH<mHMax]
        mH = mH[mH>mHMin]
        mH = mH[mH<mHMax]
        if useUnweightedMC: weights = np.ones_like(weights)

        histContents = np.array(stats.binned_statistic(mH, weights, statistic="sum", bins=nBins, range=(mHMin, mHMax))[0])
        histEdges = np.array(stats.binned_statistic(mH, weights, statistic="sum", bins=nBins, range=(mHMin, mHMax))[1])
        histCenters = histEdges[:-1]+(histEdges[1]-histEdges[0])/2.
        if useMCStats:
            histUncertainties = np.zeros_like(histContents)
            for i in range(histUncertainties.shape[0]):
                if i==histUncertainties.shape[0]-1: weightsInBin = weights[(histEdges[i]<=mH)*(mH<=histEdges[i+1])]
                else: weightsInBin = weights[(histEdges[i]<=mH)*(mH<histEdges[i+1])]
                histUncertainties[i] = np.sqrt(np.sum(weightsInBin*weightsInBin))
    else:
        # print("Reading dataset")
        print("Reading 2015 data")
        tree15 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data15_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        print("Reading 2016 data")
        tree16 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data16_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        print("Reading 2017 data")
        tree17 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data17_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        print("Reading 2018 data")
        tree18 = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data18_13TeV.physics_Main.MxAOD.p4096.h025.root")["CollectionTree"]
        isPassed = tree15.array("HGamEventInfoAuxDyn.isPassed")
        isPassed = np.append(isPassed, tree16.array("HGamEventInfoAuxDyn.isPassed"))
        isPassed = np.append(isPassed, tree17.array("HGamEventInfoAuxDyn.isPassed"))
        isPassed = np.append(isPassed, tree18.array("HGamEventInfoAuxDyn.isPassed"))
        ctag = tree15.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose")
        ctag = np.append(ctag, tree16.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        ctag = np.append(ctag, tree17.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        ctag = np.append(ctag, tree18.array("HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"))
        mH = tree15.array("HGamEventInfoAuxDyn.m_yy")
        mH = np.append(mH, tree16.array("HGamEventInfoAuxDyn.m_yy"))
        mH = np.append(mH, tree17.array("HGamEventInfoAuxDyn.m_yy"))
        mH = np.append(mH, tree18.array("HGamEventInfoAuxDyn.m_yy"))
        mH = mH/1000.
        ctag = ctag[isPassed==True]
        mH = mH[isPassed==True]
        mH = mH[ctag==ctagCat]
        mH = mH[mH>mHMin]
        mH = mH[mH<mHMax]
        mH = mH[(mH>mHBlindMin)+(mH<mHBlindMax)]
        # inFile = uproot.open("/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/data15_13TeV.physics_Main.MxAOD.p4096.h025.root")
        # df = inFile["CollectionTree"].pandas.df()
        # df = df[df["HGamEventInfoAuxDyn.isPassed"]==True]
        # df = df[df["HGamEventInfoAuxDyn.Hcgam_Atleast1jisloose"]==ctagCat]
        # df = df[df["HGamEventInfoAuxDyn.m_yy"]>mHMin]
        # df = df[df["HGamEventInfoAuxDyn.m_yy"]<mHMax]
        # df = df[(df["HGamEventInfoAuxDyn.m_yy"]>mHBlindMin)+(df["HGamEventInfoAuxDyn.m_yy"]<mHBlindMax)]
        # mH = df["HGamEventInfoAuxDyn.m_yy"].values

        histContents = np.array(stats.binned_statistic(mH, np.ones_like(mH), statistic="sum", bins=nBins, range=(mHMin, mHMax))[0])
        histEdges = np.array(stats.binned_statistic(mH, np.ones_like(mH), statistic="sum", bins=nBins, range=(mHMin, mHMax))[1])
        histCenters = histEdges[:-1]+(histEdges[1]-histEdges[0])/2.

        histContents = histContents[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]
        histCenters = histCenters[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]

        histCenters = np.expand_dims(histCenters, axis=1)

    if not useSavedData:
        print("Inputs:")
        print(histCenters)
        print(histContents)
        if useMCStats:
            print("MC Bin Uncertainties:")
            print(histUncertainties)
        print(np.sum(histContents))

    if useMC or useSSTemplate:

        SRContents = np.array(histContents[(histCenters<=mHBlindMin)*(histCenters>=mHBlindMax)])
        print("SR MC:")
        print(SRContents)

        if useMCStats:
            SRUncertainties = np.array(histUncertainties[(histCenters<=mHBlindMin)*(histCenters>=mHBlindMax)])
        SRCenters = np.array(histCenters[(histCenters<=mHBlindMin)*(histCenters>=mHBlindMax)])

        histContents = histContents[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]
        if useMCStats:
            histUncertainties = histUncertainties[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]
        histCenters = histCenters[(histCenters>mHBlindMin)+(histCenters<mHBlindMax)]

        histCenters = np.expand_dims(histCenters, axis=1)

    # print("Adding data")
    if useMCStats:
        gp.AddData(histCenters, histContents, cutoff, noiseConst*histUncertainties)
    else:
        gp.AddData(histCenters, histContents, cutoff, noiseConst*np.sqrt(histContents))

    # print("Building GP")
    # gp.BuildGP("rbf", amplitude, lengthScale)
    gp.BuildGP(kernel, amplitude, lengthScale, auxPar)

    # print("Finding optimal point")
    # gp.FindMinimum()

    # if np.amin(np.linalg.eigvals(covBins))<0: covBins += -2.*np.amin(np.linalg.eigvals(covBins))*np.identity(10)
    # print("SR:")
    requestedMasses = np.array([[mHMin+0.5*(mHMax-mHMin)/float(nBins)]])
    for i in range(1, nBins):
        binCenter = mHMin+(float(i)+0.5)*(mHMax-mHMin)/float(nBins)
        if not (binCenter>=mHBlindMax and binCenter<=mHBlindMin): continue
        requestedMasses = np.append(requestedMasses, [[binCenter]], axis=0)
    if not (requestedMasses[0]>=mHBlindMax and requestedMasses[0]<=mHBlindMin):
        requestedMasses = np.delete(requestedMasses, 0, axis=0)
    meanBins, covBins = gp.GetVal(requestedMasses, normaliseData)

    # print("Means:")
    print(meanBins)
    # print("Covariances:")
    print(covBins)

    # print("All:")
    # requestedMasses = []
    # firstMass = 100.5
    # while firstMass<160.:
    #     thisMass = [firstMass]
    #     requestedMasses.append(thisMass)
    #     firstMass+=1.
    # meanBins, covBins = gp.GetVal(requestedMasses, normaliseData)
    # print("Means:")
    # print(meanBins)
    # print("Covariances:")
    # print(covBins)
    # for i in range(10):
    #     diag = np.sqrt(covBins[i,i])
    #     covBins[i,:] = covBins[i,:]/diag
    #     covBins[:,i] = covBins[:,i]/diag
    # print("Covariances:")
    # print(covBins)
    # print("Inverse Covariances:")
    # print(np.linalg.inv(covBins))
    # print("Eigenvals of Covariance Matrix:")
    # print(np.linalg.eigvals(covBins))
    # print("Eigendecomposition of Covariance Matrix:")
    # print(np.linalg.eigh(covBins))
    # print("Covariance times Inverse Covariance Matrix:")
    # print(np.matmul(covBins, np.linalg.inv(covBins)))

    # if doPlot:
    #     # print("Plotting GP")
    #     if useMC:
    #         if useMCStats:
    #             gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, SRUncertainties])
    #         else:
    #             gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, np.sqrt(SRContents)])
    #     elif useSSTemplate:
    #         if useMCStats:
    #             gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, SRUncertainties])
    #         else:
    #             gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12, [SRCenters, SRContents, np.sqrt(SRContents)])
    #     else:
    #         gp.PlotMarginalisedGP(0, (mHMax-mHMin)/float(nBins), histMax, normaliseData, 0.35 if ctagCat==1 else 0.12)

    # print("Kernel hyperparameters (post-fit, transformed), both:")
    # print(gp.gp.kernel_.get_params())

            
