#include "GPRWSbuilder.h"

// Tight limits due to fact that most H+c cross seciton not sensitive to yc

// Fit options
const int category = 2; // c-tag category (0: nonCTag, 1: cTag, 2: both)
const int nBins = 20; // number of SR bins in fit
const double injectMu = 0.; // to inject signal to Asimov dataset
const double dampingCTag = 1.e-3; // damping factor for c-tag category
const double dampingNoCTag = 1.e-4; // damping factor for no c-tag category
const bool relativeDamping = true; // to use relative damping of GPR covariance matrix
const bool doPlots = false; // to plot observable
const bool doLimit = false; // to calculate limit
const bool doMC = false; // to use MC
const bool doMCFastSim = false; // to use fast-sim MC
const bool doMCUnw = false; // to use unweighted MC
const bool doSSTemplate = false; // to use SS template
const bool doSSTemplateMCStats = false; // to use SS template with MC stats
const bool doUnblind = false; // to unblind data or MC, set to false to use Asimov
const double asimovScale = 1.0; // to test scaled Asimov
const double asimovTilt = 0.0; // to test tilt of Asimov
const double egScalePull = 0.0; // to test eg scale
const double egResPull = 0.0; // to test eg res
const bool doAutoScan = true; // to use autoscan in limit (recommended value: true)
const int scanPoints = 1000; // required if doAutoScan==false
const bool doMuBkd = false; // to let bkd float freely
const bool doMuBkdAsPOI = false; // to make bkd PoI
const bool doXSecSig = true; // to inlcude cross-section in PoI definition
const bool doHalfRes = false; // to implement a dataset for resonant signal and bkds which is gaussian with mean and width set to average of nominal and +1 sigma variation of EG resolution NP

// Systematics to be included
const bool useGprSys = true; // updated 21Feb23
const bool useCTagSys = false; // updated 23Feb23
const bool useCTagVTXSys = false; // added 23Feb23
const bool useCTagLightAdHocSys = false; // added 24Feb23
const bool usePRWSys = false; // added 23Feb23
const bool usePhotonSys = false; // added 23Feb23
const bool usePhotonShapeSys = false; // added 23Feb23, fixed 06Mar23
const bool usePhotonShapeSysTest = false; // added 23Feb23, fixed 06Mar23
//const bool useResBkdXSecSys = false; // need to update
const bool useJetSys = false; // added 25Feb23
const bool useModelSys  = false; //added 26Feb2023
// const bool useTheoSys  = false; //added 26Mar23
const bool useBRSys = false;
const bool usePDFSys = false;
const bool useQCDSys = false;
const bool useAlphasSys = false;
const bool useSS = false;
const bool useHHF = false;
// no need to add lumi, as it is 0.83%<1% in all categories as of 23Feb23
// no need to add trigger, as it was evaluted with photons and found to be <1% in all categories as of 23Feb23
// need to add modelling


int main(int argc, char* argv[])
{
  double gprMeansNonCTagData[20];
  double gprCovsNonCTagData[20*20];
  double gprMeansCTagData[20];
  double gprCovsCTagData[20*20];

  const int fileNumber = atoi(argv[1]);
  TString fileName = "/afs/cern.ch/user/e/ereynold/private/cH/full_run2_hcgamgam/GPRBkd/Misc/ToyTest/GPR/Logs/";
  fileName = fileName+((TString) TString::Itoa(fileNumber, 10));
  fileName = fileName+((TString) ".out");
  std::cout<<"fileName = "<<fileName<<std::endl;
  std::ifstream infile(fileName);

  std::string line;
  int readStage = 0;
  int arrayIndex = 0;
  while(std::getline(infile, line))
    {
      TString lineEntries = (TString) line;

      lineEntries.ReplaceAll("]", "");
      lineEntries.ReplaceAll("[", "");

      if(readStage>=1)
	{
	  Ssiz_t from = 0;
	  TString numberStr = "";
	  while(lineEntries.Tokenize(numberStr, from))
	    {
	      // std::cout<<numberStr<<std::endl;
	      // if(numberStr.IsFloat()) std::cout<<atof(numberStr)<<std::endl;
	      if(readStage==1 and numberStr.IsFloat()) {gprMeansNonCTagData[arrayIndex] = atof(numberStr); arrayIndex+=1;}
	      if(readStage==2 and numberStr.IsFloat()) {gprCovsNonCTagData[arrayIndex] = atof(numberStr); arrayIndex+=1;}
	      if(readStage==3 and numberStr.IsFloat()) {gprMeansCTagData[arrayIndex] = atof(numberStr); arrayIndex+=1;}
	      if(readStage==4 and numberStr.IsFloat()) {gprCovsCTagData[arrayIndex] = atof(numberStr); arrayIndex+=1;}
	    }
	}

      if(readStage==4 and line.find("]]") != std::string::npos) {readStage = 5; arrayIndex = 0;}
      if(readStage==3 and line.find("]") != std::string::npos) {readStage = 4; arrayIndex = 0;}
      if(readStage==2 and line.find("]]") != std::string::npos) {readStage = 3; arrayIndex = 0;}
      if(readStage==1 and line.find("]") != std::string::npos) {readStage = 2; arrayIndex = 0;}
      if(readStage==0 and line.find("Setting up uproot") != std::string::npos) {readStage = 1; arrayIndex = 0;}
    }

  // for(int i=0; i<20; i++) std::cout<<gprMeansNonCTagData[i]<<std::endl;
  // std::cout<<std::endl;
  // for(int i=0; i<400; i++) std::cout<<gprCovsNonCTagData[i]<<std::endl;
  // std::cout<<std::endl;
  // for(int i=0; i<20; i++) std::cout<<gprMeansCTagData[i]<<std::endl;
  // std::cout<<std::endl;
  // for(int i=0; i<400; i++) std::cout<<gprCovsCTagData[i]<<std::endl;

  // return 0;

  if(doSSTemplateMCStats)
    {
      std::cout<<"WARNING: doSSTemplateMCStats==true, and MC stats not setup in fit."<<std::endl;
    }

  //////////////////////////////////////////////////////////////////////
  // Instantiate Workspace, Fit Variable, and POI

  // Create a new empty workspacex
  RooWorkspace *workspace = new RooWorkspace("workspace", "");

  // Sets for ModelConfig
  RooArgSet *allNPs = new RooArgSet("allNPs");
  RooArgSet *allGlobObs = new RooArgSet("allGlobObs");
  workspace->import(*allNPs);
  workspace->import(*allGlobObs);

  //////////////////////////////////////////////////////////////////////
  // Create Signal Model

  RooRealVar *mu = NULL; // too wide causes issues in autoscan, too narrow influences results, in pb when doXSecSig = true
  if(doXSecSig) mu = new RooRealVar("mu", "", 0., -1.e3, 1.e3);
  else mu = new RooRealVar("mu", "", 0., -1.e1, 1.e1);

  const double BR = 0.227/100.;
  const double xsecXBR_cH = 0.064468;
  const double xsecXBR_bbH = 1.104;
  const double xsecXBR_ggZH = 0.2782;
  const double xsecXBR_ggFH = 110.1;
  const double xsecXBR_ttH = 1.150;
  const double xsecXBR_WmH = 1.206;
  const double xsecXBR_WpH = 1.902;
  const double xsecXBR_VBFH = 8.578;
  const double xsecXBR_ZH = 1.725;
  RooConstVar *xSecReciprocal = new RooConstVar("xSecReciprocal", "", 1000.*BR/(xsecXBR_cH+xsecXBR_bbH+xsecXBR_ggZH+xsecXBR_ggFH+xsecXBR_ttH+xsecXBR_WmH+xsecXBR_WpH+xsecXBR_VBFH+xsecXBR_ZH)); // in pb

  // RooStats::HistFactory::FlexibleInterpVar *fiv_SS = GetFIVTerm("SS", "All", -1.03625e-01, 1.03625e-01, workspace, 0.0, 0);
  RooStats::HistFactory::FlexibleInterpVar *fiv_SS;
  if(doXSecSig) fiv_SS = GetFIVTerm("SS", "All", -1.*SS/xSecReciprocal->getVal(), SS/xSecReciprocal->getVal(), workspace, 1.0, 0);
  else fiv_SS = GetFIVTerm("SS", "All", -1.*SS, SS, workspace, 1.0, 0);
  RooConstVar *SSOffset = new RooConstVar("SSOffset", "", -1.0);
  workspace->var("alpha_SS")->setConstant(not useSS);
  RooArgList *muPlusSSArgList = new RooArgList("muPlusSSArgList");
  muPlusSSArgList->add(*mu);
  muPlusSSArgList->add(*fiv_SS);
  muPlusSSArgList->add(*SSOffset);
  RooAddition *muPlusSS = new RooAddition("muPlusSS", "", *muPlusSSArgList);

  RooStats::HistFactory::FlexibleInterpVar *fiv_THBR = GetFIVTerm("THBR", "All", -1.*THBR_Do, THBR_Up, workspace);
  workspace->var("alpha_THBR")->setConstant(not useBRSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagC0_Sig_cTagCat = GetFIVTerm("cTagC0", "SigCTag", cTagC0_Sig_cTagCat_Do, cTagC0_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagC0_Sig_nonCTagCat = GetFIVTerm("cTagC0", "SigNonCTag", cTagC0_Sig_nonCTagCat_Do, cTagC0_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagC0")->setConstant(not useCTagSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL0_Sig_cTagCat = GetFIVTerm("cTagL0", "SigCTag", cTagL0_Sig_cTagCat_Do, cTagL0_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL0_Sig_nonCTagCat = GetFIVTerm("cTagL0", "SigNonCTag", cTagL0_Sig_nonCTagCat_Do, cTagL0_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagL0")->setConstant(not useCTagSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL1_Sig_cTagCat = GetFIVTerm("cTagL1", "SigCTag", cTagL1_Sig_cTagCat_Do, cTagL1_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL1_Sig_nonCTagCat = GetFIVTerm("cTagL1", "SigNonCTag", cTagL1_Sig_nonCTagCat_Do, cTagL1_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagL1")->setConstant(not useCTagSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagPowHW7_Sig_cTagCat = GetFIVTerm("cTagPowHW7", "SigCTag", cTagPowHW7_Sig_cTagCat_Do, cTagPowHW7_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagPowHW7_Sig_nonCTagCat = GetFIVTerm("cTagPowHW7", "SigNonCTag", cTagPowHW7_Sig_nonCTagCat_Do, cTagPowHW7_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagPowHW7")->setConstant(not useCTagSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagaMcPy8_Sig_cTagCat = GetFIVTerm("cTagaMcPy8", "SigCTag", cTagaMcPy8_Sig_cTagCat_Do, cTagaMcPy8_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagaMcPy8_Sig_nonCTagCat = GetFIVTerm("cTagaMcPy8", "SigNonCTag", cTagaMcPy8_Sig_nonCTagCat_Do, cTagaMcPy8_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagaMcPy8")->setConstant(not useCTagSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_PRW_Sig_cTagCat = GetFIVTerm("PRW", "SigCTag", PRW_Sig_cTagCat_Do, PRW_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_PRW_Sig_nonCTagCat = GetFIVTerm("PRW", "SigNonCTag", PRW_Sig_nonCTagCat_Do, PRW_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_PRW")->setConstant(not usePRWSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ID_Sig_cTagCat = GetFIVTerm("PH_EFF_ID", "SigCTag", PH_EFF_ID_Sig_cTagCat_Do, PH_EFF_ID_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ID_Sig_nonCTagCat = GetFIVTerm("PH_EFF_ID", "SigNonCTag", PH_EFF_ID_Sig_nonCTagCat_Do, PH_EFF_ID_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_PH_EFF_ID")->setConstant(not usePhotonSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ISO_Sig_cTagCat = GetFIVTerm("PH_EFF_ISO", "SigCTag", PH_EFF_ISO_Sig_cTagCat_Do, PH_EFF_ISO_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ISO_Sig_nonCTagCat = GetFIVTerm("PH_EFF_ISO", "SigNonCTag", PH_EFF_ISO_Sig_nonCTagCat_Do, PH_EFF_ISO_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_PH_EFF_ISO")->setConstant(not usePhotonSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagVTX_Sig_cTagCat = GetFIVTerm("cTagVTXSig", "CTag", cTagVTX_Sig_cTagCat_Do, cTagVTX_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagVTX_Sig_nonCTagCat = GetFIVTerm("cTagVTXSig", "NonCTag", cTagVTX_Sig_nonCTagCat_Do, cTagVTX_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagVTXSig")->setConstant(not useCTagVTXSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagLightAdHoc_Sig_cTagCat = GetFIVTerm("cTagLightAdHoc", "SigCTag", cTagLightAdHoc_Sig_cTagCat_Do, cTagLightAdHoc_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagLightAdHoc_Sig_nonCTagCat = GetFIVTerm("cTagLightAdHoc", "SigNonCTag", cTagLightAdHoc_Sig_nonCTagCat_Do, cTagLightAdHoc_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagLightAdHoc")->setConstant(not useCTagLightAdHocSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EffectiveNP_1_Sig_cTagCat = GetFIVTerm("JET_EffectiveNP_1", "SigCTag", JET_EffectiveNP_1_Sig_cTagCat_Do, JET_EffectiveNP_1_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EffectiveNP_1_Sig_nonCTagCat = GetFIVTerm("JET_EffectiveNP_1", "SigNonCTag", JET_EffectiveNP_1_Sig_nonCTagCat_Do, JET_EffectiveNP_1_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_EffectiveNP_1")->setConstant(not useJetSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EtaIntercalibration_Modelling_Sig_cTagCat = GetFIVTerm("JET_EtaIntercalibration_Modelling", "SigCTag", JET_EtaIntercalibration_Modelling_Sig_cTagCat_Do, JET_EtaIntercalibration_Modelling_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EtaIntercalibration_Modelling_Sig_nonCTagCat = GetFIVTerm("JET_EtaIntercalibration_Modelling", "SigNonCTag", JET_EtaIntercalibration_Modelling_Sig_nonCTagCat_Do, JET_EtaIntercalibration_Modelling_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_EtaIntercalibration_Modelling")->setConstant(not useJetSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Composition_Sig_cTagCat = GetFIVTerm("JET_Flavor_Composition", "SigCTag", JET_Flavor_Composition_Sig_cTagCat_Do, JET_Flavor_Composition_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Composition_Sig_nonCTagCat = GetFIVTerm("JET_Flavor_Composition", "SigNonCTag", JET_Flavor_Composition_Sig_nonCTagCat_Do, JET_Flavor_Composition_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_Flavor_Composition")->setConstant(not useJetSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Response_Sig_cTagCat = GetFIVTerm("JET_Flavor_Response", "SigCTag", JET_Flavor_Response_Sig_cTagCat_Do, JET_Flavor_Response_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Response_Sig_nonCTagCat = GetFIVTerm("JET_Flavor_Response", "SigNonCTag", JET_Flavor_Response_Sig_nonCTagCat_Do, JET_Flavor_Response_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_Flavor_Response")->setConstant(not useJetSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_JER_EffectiveNP_4_Sig_cTagCat = GetFIVTerm("JET_JER_EffectiveNP_4", "SigCTag", JET_JER_EffectiveNP_4_Sig_cTagCat_Do, JET_JER_EffectiveNP_4_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_JER_EffectiveNP_4_Sig_nonCTagCat = GetFIVTerm("JET_JER_EffectiveNP_4", "SigNonCTag", JET_JER_EffectiveNP_4_Sig_nonCTagCat_Do, JET_JER_EffectiveNP_4_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_JER_EffectiveNP_4")->setConstant(not useJetSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_RhoTopology_Sig_cTagCat = GetFIVTerm("JET_Pileup_RhoTopology", "SigCTag", JET_Pileup_RhoTopology_Sig_cTagCat_Do, JET_Pileup_RhoTopology_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_RhoTopology_Sig_nonCTagCat = GetFIVTerm("JET_Pileup_RhoTopology", "SigNonCTag", JET_Pileup_RhoTopology_Sig_nonCTagCat_Do, JET_Pileup_RhoTopology_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_Pileup_RhoTopology")->setConstant(not useJetSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_OffsetNPV_Sig_cTagCat = GetFIVTerm("JET_Pileup_OffsetNPV", "SigCTag", JET_Pileup_OffsetNPV_Sig_cTagCat_Do, JET_Pileup_OffsetNPV_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_OffsetNPV_Sig_nonCTagCat = GetFIVTerm("JET_Pileup_OffsetNPV", "SigNonCTag", JET_Pileup_OffsetNPV_Sig_nonCTagCat_Do, JET_Pileup_OffsetNPV_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_JET_Pileup_OffsetNPV")->setConstant(not useJetSys);


  //Modelling - Generator - PS
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_GGF_Sig_cTagCat = GetFIVTerm("MOD_ALGPS_GGF", "SigCTag", -1.*MOD_ALGPS_GGF_Sig_cTagCat, MOD_ALGPS_GGF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_GGF_Sig_nonCTagCat = GetFIVTerm("MOD_ALGPS_GGF", "SigNonCTag", -1.*MOD_ALGPS_GGF_Sig_nonCTagCat, MOD_ALGPS_GGF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_ALGPS_GGF")->setConstant(not useModelSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_VBF_Sig_cTagCat = GetFIVTerm("MOD_ALGPS_VBF", "SigCTag", -1.*MOD_ALGPS_VBF_Sig_cTagCat, MOD_ALGPS_VBF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_VBF_Sig_nonCTagCat = GetFIVTerm("MOD_ALGPS_VBF", "SigNonCTag", -1.*MOD_ALGPS_VBF_Sig_nonCTagCat, MOD_ALGPS_VBF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_ALGPS_VBF")->setConstant(not useModelSys);


  // //Theory uncertainty of the cross section

  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Sig_cTagCat = GetFIVTerm("THEO_GGF_Sig", "CTag", THEO_GGF_Sig_cTagCat_Do, THEO_GGF_Sig_cTagCat_Up, workspace);
  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Sig_noncTagCat = GetFIVTerm("THEO_GGF_Sig", "NonCTag", THEO_GGF_Sig_nonCTagCat_Do, THEO_GGF_Sig_nonCTagCat_Up, workspace);
  // workspace->var("alpha_THEO_GGF_Sig")->setConstant(not useTheoSys or doXSecSig);

  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_TTH_Sig_cTagCat = GetFIVTerm("THEO_TTH_Sig", "CTag", THEO_TTH_Sig_cTagCat_Do, THEO_TTH_Sig_cTagCat_Up, workspace);
  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_TTH_Sig_noncTagCat = GetFIVTerm("THEO_TTH_Sig", "NonCTag", THEO_TTH_Sig_nonCTagCat_Do, THEO_TTH_Sig_nonCTagCat_Up, workspace);
  // workspace->var("alpha_THEO_TTH_Sig")->setConstant(not useTheoSys or doXSecSig);

  //RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Sig_cTagCat = GetFIVTerm("THEO_GGF_Sig", "CTag", -1.*crossSection, crossSection, workspace);
  //RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Sig_noncTagCat = GetFIVTerm("THEO_GGF_Sig", "NonCTag", -1.*crossSection, crossSection, workspace);
  //workspace->var("alpha_THEO_GGF_Sig")->setConstant(not useTheoSys or doXSecSig or doMuBkdAsPOI);


  //Modelling - PDF - TEO
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_PDF_VBF_Sig_cTagCat = GetFIVTerm("MOD_PDF_VBF", "SigCTag", -1.*MOD_PDF_VBF_Sig_cTagCat, MOD_PDF_VBF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_PDF_VBF_Sig_nonCTagCat = GetFIVTerm("MOD_PDF_VBF", "SigNonCTag", -1.*MOD_PDF_VBF_Sig_nonCTagCat, MOD_PDF_VBF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_PDF_VBF")->setConstant(not usePDFSys);


  //Modelling - QCD - TEO - 4NP for GGF and 3NP for ggZH
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGF_Sig_cTagCat = GetFIVTerm("MOD_QCD_MU_GGF", "SigCTag", -1.*MOD_QCD_MU_GGF_Sig_cTagCat, MOD_QCD_MU_GGF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGF_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_MU_GGF", "SigNonCTag", -1.*MOD_QCD_MU_GGF_Sig_nonCTagCat, MOD_QCD_MU_GGF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MU_GGF")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_RES_GGF_Sig_cTagCat = GetFIVTerm("MOD_QCD_RES_GGF", "SigCTag", -1.*MOD_QCD_RES_GGF_Sig_cTagCat, MOD_QCD_RES_GGF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_RES_GGF_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_RES_GGF", "SigNonCTag", -1.*MOD_QCD_RES_GGF_Sig_nonCTagCat, MOD_QCD_RES_GGF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_RES_GGF")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGF_Sig_cTagCat = GetFIVTerm("MOD_QCD_MIG01_GGF", "SigCTag", -1.*MOD_QCD_MIG01_GGF_Sig_cTagCat, MOD_QCD_MIG01_GGF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGF_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_MIG01_GGF", "SigNonCTag", -1.*MOD_QCD_MIG01_GGF_Sig_nonCTagCat, MOD_QCD_MIG01_GGF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG01_GGF")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGF_Sig_cTagCat = GetFIVTerm("MOD_QCD_MIG12_GGF", "SigCTag", -1.*MOD_QCD_MIG12_GGF_Sig_cTagCat, MOD_QCD_MIG12_GGF_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGF_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_MIG12_GGF", "SigNonCTag", -1.*MOD_QCD_MIG12_GGF_Sig_nonCTagCat, MOD_QCD_MIG12_GGF_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG12_GGF")->setConstant(not useQCDSys);


  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGZH_Sig_cTagCat = GetFIVTerm("MOD_QCD_MU_GGZH", "SigCTag", -1.*MOD_QCD_MU_GGZH_Sig_cTagCat, MOD_QCD_MU_GGZH_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGZH_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_MU_GGZH", "SigNonCTag", -1.*MOD_QCD_MU_GGZH_Sig_nonCTagCat, MOD_QCD_MU_GGZH_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MU_GGZH")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGZH_Sig_cTagCat = GetFIVTerm("MOD_QCD_MIG01_GGZH", "SigCTag", -1.*MOD_QCD_MIG01_GGZH_Sig_cTagCat, MOD_QCD_MIG01_GGZH_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGZH_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_MIG01_GGZH", "SigNonCTag", -1.*MOD_QCD_MIG01_GGZH_Sig_nonCTagCat, MOD_QCD_MIG01_GGZH_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG01_GGZH")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGZH_Sig_cTagCat = GetFIVTerm("MOD_QCD_MIG12_GGZH", "SigCTag", -1.*MOD_QCD_MIG12_GGZH_Sig_cTagCat, MOD_QCD_MIG12_GGZH_Sig_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGZH_Sig_nonCTagCat = GetFIVTerm("MOD_QCD_MIG12_GGZH", "SigNonCTag", -1.*MOD_QCD_MIG12_GGZH_Sig_nonCTagCat, MOD_QCD_MIG12_GGZH_Sig_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG12_GGZH")->setConstant(not useQCDSys);


  //Modelling -TEO Alpha_s
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALPHAS_GGF_Sig_cTagCat = GetFIVTerm("MOD_ALPHAS_GGF", "SigCTag", MOD_ALPHAS_GGF_Sig_cTagCat_Do, MOD_ALPHAS_GGF_Sig_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALPHAS_GGF_Sig_nonCTagCat = GetFIVTerm("MOD_ALPHAS_GGF", "SigNonCTag", MOD_ALPHAS_GGF_Sig_nonCTagCat_Do, MOD_ALPHAS_GGF_Sig_nonCTagCat_Up, workspace);
  workspace->var("alpha_MOD_ALPHAS_GGF")->setConstant(not useAlphasSys);

  // RooStats::HistFactory::FlexibleInterpVar *fiv_SSTest = GetFIVTerm("SSTest", -0.29, 0.29, workspace);
  if(usePhotonShapeSysTest)
    {
      for(int i=0; i<nBins; i++)
        {
          fiv_EG_RESOLUTION_ALL_Sig_cTagCat[i] = GetFlatFIVTerm("EG_RESOLUTION_ALL", "SigCTagBin"+((TString) std::to_string(i)), EG_RESOLUTION_ALL_Sig_cTagCat_Do[i], EG_RESOLUTION_ALL_Sig_cTagCat_Up[i], workspace);
          fiv_EG_RESOLUTION_ALL_Sig_nonCTagCat[i] = GetFlatFIVTerm("EG_RESOLUTION_ALL", "SigNonCTagBin"+((TString) std::to_string(i)), EG_RESOLUTION_ALL_Sig_nonCTagCat_Do[i], EG_RESOLUTION_ALL_Sig_nonCTagCat_Up[i], workspace);

          fiv_EG_SCALE_ALL_Sig_cTagCat[i] = GetFlatFIVTerm("EG_SCALE_ALL", "SigCTagBin"+((TString) std::to_string(i)), EG_SCALE_ALL_Sig_cTagCat_Do[i], EG_SCALE_ALL_Sig_cTagCat_Up[i], workspace);
          fiv_EG_SCALE_ALL_Sig_nonCTagCat[i] = GetFlatFIVTerm("EG_SCALE_ALL", "SigNonCTagBin"+((TString) std::to_string(i)), EG_SCALE_ALL_Sig_nonCTagCat_Do[i], EG_SCALE_ALL_Sig_nonCTagCat_Up[i], workspace);
        }
    }
  else
    {
      for(int i=0; i<nBins; i++)
        {
          fiv_EG_RESOLUTION_ALL_Sig_cTagCat[i] = GetFIVTerm("EG_RESOLUTION_ALL", "SigCTagBin"+((TString) std::to_string(i)), EG_RESOLUTION_ALL_Sig_cTagCat_Do[i], EG_RESOLUTION_ALL_Sig_cTagCat_Up[i], workspace);
          fiv_EG_RESOLUTION_ALL_Sig_nonCTagCat[i] = GetFIVTerm("EG_RESOLUTION_ALL", "SigNonCTagBin"+((TString) std::to_string(i)), EG_RESOLUTION_ALL_Sig_nonCTagCat_Do[i], EG_RESOLUTION_ALL_Sig_nonCTagCat_Up[i], workspace);

          fiv_EG_SCALE_ALL_Sig_cTagCat[i] = GetFIVTerm("EG_SCALE_ALL", "SigCTagBin"+((TString) std::to_string(i)), EG_SCALE_ALL_Sig_cTagCat_Do[i], EG_SCALE_ALL_Sig_cTagCat_Up[i], workspace);
          fiv_EG_SCALE_ALL_Sig_nonCTagCat[i] = GetFIVTerm("EG_SCALE_ALL", "SigNonCTagBin"+((TString) std::to_string(i)), EG_SCALE_ALL_Sig_nonCTagCat_Do[i], EG_SCALE_ALL_Sig_nonCTagCat_Up[i], workspace);
        }
    }
  workspace->var("alpha_EG_RESOLUTION_ALL")->setConstant(not usePhotonShapeSys);
  workspace->var("alpha_EG_SCALE_ALL")->setConstant(not usePhotonShapeSys);

  std::map<int, RooRealVar*> binConstraintsSigNonCTag = {};
  std::map<int, RooArgList*> binArgListSigNonCTag = {};
  std::map<int, RooProduct*> binNomsSigNonCTag = {};
  for(int i=0; i<nBins; i++)
    {
      binConstraintsSigNonCTag[i] = new RooRealVar((TString) ("binConstraintsSigNonCTag"+std::to_string(i)), "", sigNonCTag[i]);
      binConstraintsSigNonCTag[i]->setConstant();
      binArgListSigNonCTag[i] = new RooArgList((TString) ("binArgListSigNonCTag"+std::to_string(i)));
      binArgListSigNonCTag[i]->add(*muPlusSS);
      if(doXSecSig) binArgListSigNonCTag[i]->add(*xSecReciprocal);
      binArgListSigNonCTag[i]->add(*fiv_cTagC0_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_cTagL0_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_cTagL1_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_cTagPowHW7_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_cTagaMcPy8_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_PRW_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_PH_EFF_ID_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_PH_EFF_ISO_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_cTagLightAdHoc_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_EffectiveNP_1_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_EtaIntercalibration_Modelling_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_Flavor_Composition_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_Flavor_Response_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_JER_EffectiveNP_4_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_Pileup_RhoTopology_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_JET_Pileup_OffsetNPV_Sig_nonCTagCat);
      // binArgListSigNonCTag[i]->add(*fiv_SSTest);
      binArgListSigNonCTag[i]->add(*fiv_EG_RESOLUTION_ALL_Sig_nonCTagCat[i]);
      binArgListSigNonCTag[i]->add(*fiv_EG_SCALE_ALL_Sig_nonCTagCat[i]);
      binArgListSigNonCTag[i]->add(*fiv_MOD_ALGPS_GGF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_ALGPS_VBF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_THBR);
      //binArgListSigNonCTag[i]->add(*fiv_THEO_GGF_Sig_noncTagCat);
      // binArgListSigNonCTag[i]->add(*fiv_THEO_TTH_Sig_noncTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_PDF_VBF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_MU_GGF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_RES_GGF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_MIG01_GGF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_MIG12_GGF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_MU_GGZH_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_MIG01_GGZH_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_QCD_MIG12_GGZH_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*fiv_MOD_ALPHAS_GGF_Sig_nonCTagCat);
      binArgListSigNonCTag[i]->add(*binConstraintsSigNonCTag[i]);
      binNomsSigNonCTag[i] = new RooProduct((TString) ("binNomsSigNonCTag"+std::to_string(i)), "", *binArgListSigNonCTag[i]);
    }

  std::map<int, RooRealVar*> binConstraintsSigCTag = {};
  std::map<int, RooArgList*> binArgListSigCTag = {};
  std::map<int, RooProduct*> binNomsSigCTag = {};
  for(int i=0; i<nBins; i++)
    {
      binConstraintsSigCTag[i] = new RooRealVar((TString) ("binConstraintsSigCTag"+std::to_string(i)), "", sigCTag[i]);
      binConstraintsSigCTag[i]->setConstant();
      binArgListSigCTag[i] = new RooArgList((TString) ("binArgListSigCTag"+std::to_string(i)));
      binArgListSigCTag[i]->add(*muPlusSS);
      if(doXSecSig) binArgListSigCTag[i]->add(*xSecReciprocal);
      binArgListSigCTag[i]->add(*fiv_cTagC0_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagL0_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagL1_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagPowHW7_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagaMcPy8_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagVTX_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagVTX_Sig_nonCTagCat);
      binArgListSigCTag[i]->add(*fiv_PRW_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_PH_EFF_ID_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_PH_EFF_ISO_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_cTagLightAdHoc_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_EffectiveNP_1_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_EtaIntercalibration_Modelling_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_Flavor_Composition_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_Flavor_Response_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_JER_EffectiveNP_4_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_Pileup_RhoTopology_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_JET_Pileup_OffsetNPV_Sig_cTagCat);
      // binArgListSigCTag[i]->add(*fiv_SSTest);
      binArgListSigCTag[i]->add(*fiv_EG_RESOLUTION_ALL_Sig_cTagCat[i]);
      binArgListSigCTag[i]->add(*fiv_EG_SCALE_ALL_Sig_cTagCat[i]);
      binArgListSigCTag[i]->add(*fiv_MOD_ALGPS_GGF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_ALGPS_VBF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_THBR);
      //binArgListSigCTag[i]->add(*fiv_THEO_GGF_Sig_cTagCat);
      // binArgListSigCTag[i]->add(*fiv_THEO_TTH_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_PDF_VBF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_MU_GGF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_RES_GGF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_MIG01_GGF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_MIG12_GGF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_MU_GGZH_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_MIG01_GGZH_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_QCD_MIG12_GGZH_Sig_cTagCat);
      binArgListSigCTag[i]->add(*fiv_MOD_ALPHAS_GGF_Sig_cTagCat);
      binArgListSigCTag[i]->add(*binConstraintsSigCTag[i]);
      binNomsSigCTag[i] = new RooProduct((TString) ("binNomsSigCTag"+std::to_string(i)), "", *binArgListSigCTag[i]);
    }


  //////////////////////////////////////////////////////////////////////
  // Creating Resonant Background PDF

  RooRealVar *muBkd = NULL;
  if(doMuBkd) muBkd = new RooRealVar("muBkd", "", 1., -1.e0, 3.e0); // too wide causes issues in autoscan, too narrow influences results

  //const double sigmaDown_ResBkdXSec = TMath::Sqrt( TMath::Power((kggfdown),2) + TMath::Power((kvbfdown),2) + TMath::Power((ktthfdown),2) + TMath::Power((kwphdown),2) + TMath::Power((kwmhdown),2) + TMath::Power((kzzhdown),2) + TMath::Power((kggzhdown),2) + TMath::Power((kbbhdown),2))/SigmaTot;
  //const double sigmaUp_ResBkdXSec = TMath::Sqrt( TMath::Power((kggfup),2) + TMath::Power((kvbfup),2) + TMath::Power((ktthfup),2) + TMath::Power((kwphup),2) + TMath::Power((kwmhup),2) + TMath::Power((kzzhup),2) + TMath::Power((kggzhup),2) + TMath::Power((kbbhup),2))/SigmaTot;
  //RooStats::HistFactory::FlexibleInterpVar *fiv_ResBkdXSec = GetFIVTerm("ResBkdXSec", "Bkd", -1.*sigmaDown_ResBkdXSec, sigmaUp_ResBkdXSec, workspace);
  //workspace->var("alpha_ResBkdXSec")->setConstant(not useResBkdXSecSys);


  RooStats::HistFactory::FlexibleInterpVar *fiv_HHF_cTagCat = GetFIVTerm("HHF", "cTag", -1.*HHF_cTagCat, HHF_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_HHF_nonCTagCat = GetFIVTerm("HHF", "nonCTag", -1.*HHF_nonCTagCat, HHF_nonCTagCat, workspace);
  workspace->var("alpha_HHF")->setConstant(not useHHF);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagC0_Bkd_cTagCat = GetFIVTerm("cTagC0", "BkdCTagCat", cTagC0_Bkd_cTagCat_Do, cTagC0_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagC0_Bkd_nonCTagCat = GetFIVTerm("cTagC0", "BkdNonCTagCat", cTagC0_Bkd_nonCTagCat_Do, cTagC0_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL0_Bkd_cTagCat = GetFIVTerm("cTagL0", "BkdCTagCat", cTagL0_Bkd_cTagCat_Do, cTagL0_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL0_Bkd_nonCTagCat = GetFIVTerm("cTagL0", "BkdNonCTagCat", cTagL0_Bkd_nonCTagCat_Do, cTagL0_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL1_Bkd_cTagCat = GetFIVTerm("cTagL1", "BkdCTagCat", cTagL1_Bkd_cTagCat_Do, cTagL1_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagL1_Bkd_nonCTagCat = GetFIVTerm("cTagL1", "BkdNonCTagCat", cTagL1_Bkd_nonCTagCat_Do, cTagL1_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagPowHW7_Bkd_cTagCat = GetFIVTerm("cTagPowHW7", "BkdCTagCat", cTagPowHW7_Bkd_cTagCat_Do, cTagPowHW7_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagPowHW7_Bkd_nonCTagCat = GetFIVTerm("cTagPowHW7", "BkdNonCTagCat", cTagPowHW7_Bkd_nonCTagCat_Do, cTagPowHW7_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagaMcPy8_Bkd_cTagCat = GetFIVTerm("cTagaMcPy8", "BkdCTagCat", cTagaMcPy8_Bkd_cTagCat_Do, cTagaMcPy8_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagaMcPy8_Bkd_nonCTagCat = GetFIVTerm("cTagaMcPy8", "BkdNonCTagCat", cTagaMcPy8_Bkd_nonCTagCat_Do, cTagaMcPy8_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_PRW_Bkd_cTagCat = GetFIVTerm("PRW", "BkdCTagCat", PRW_Bkd_cTagCat_Do, PRW_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_PRW_Bkd_nonCTagCat = GetFIVTerm("PRW", "BkdNonCTagCat", PRW_Bkd_nonCTagCat_Do, PRW_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ID_Bkd_cTagCat = GetFIVTerm("PH_EFF_ID", "BkdCTagCat", PH_EFF_ID_Bkd_cTagCat_Do, PH_EFF_ID_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ID_Bkd_nonCTagCat = GetFIVTerm("PH_EFF_ID", "BkdNonCTagCat", PH_EFF_ID_Bkd_nonCTagCat_Do, PH_EFF_ID_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ISO_Bkd_cTagCat = GetFIVTerm("PH_EFF_ISO", "BkdCTagCat", PH_EFF_ISO_Bkd_cTagCat_Do, PH_EFF_ISO_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_PH_EFF_ISO_Bkd_nonCTagCat = GetFIVTerm("PH_EFF_ISO", "BkdNonCTagCat", PH_EFF_ISO_Bkd_nonCTagCat_Do, PH_EFF_ISO_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagVTX_Bkd_cTagCat = GetFIVTerm("cTagVTXBkd", "CTagCat", cTagVTX_Bkd_cTagCat_Do, cTagVTX_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagVTX_Bkd_nonCTagCat = GetFIVTerm("cTagVTXBkd", "CTagCat", cTagVTX_Bkd_nonCTagCat_Do, cTagVTX_Bkd_nonCTagCat_Up, workspace);
  workspace->var("alpha_cTagVTXBkd")->setConstant(not useCTagVTXSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagLightAdHoc_Bkd_cTagCat = GetFIVTerm("cTagLightAdHoc", "BkdCTagCat", cTagLightAdHoc_Bkd_cTagCat_Do, cTagLightAdHoc_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_cTagLightAdHoc_Bkd_nonCTagCat = GetFIVTerm("cTagLightAdHoc", "BkdNonCTagCat", cTagLightAdHoc_Bkd_nonCTagCat_Do, cTagLightAdHoc_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EffectiveNP_1_Bkd_cTagCat = GetFIVTerm("JET_EffectiveNP_1", "BkdCTagCat", JET_EffectiveNP_1_Bkd_cTagCat_Do, JET_EffectiveNP_1_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EffectiveNP_1_Bkd_nonCTagCat = GetFIVTerm("JET_EffectiveNP_1", "BkdNonCTagCat", JET_EffectiveNP_1_Bkd_nonCTagCat_Do, JET_EffectiveNP_1_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EtaIntercalibration_Modelling_Bkd_cTagCat = GetFIVTerm("JET_EtaIntercalibration_Modelling", "BkdCTagCat", JET_EtaIntercalibration_Modelling_Bkd_cTagCat_Do, JET_EtaIntercalibration_Modelling_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_EtaIntercalibration_Modelling_Bkd_nonCTagCat = GetFIVTerm("JET_EtaIntercalibration_Modelling", "BkdNonCTagCat", JET_EtaIntercalibration_Modelling_Bkd_nonCTagCat_Do, JET_EtaIntercalibration_Modelling_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Composition_Bkd_cTagCat = GetFIVTerm("JET_Flavor_Composition", "BkdCTagCat", JET_Flavor_Composition_Bkd_cTagCat_Do, JET_Flavor_Composition_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Composition_Bkd_nonCTagCat = GetFIVTerm("JET_Flavor_Composition", "BkdNonCTagCat", JET_Flavor_Composition_Bkd_nonCTagCat_Do, JET_Flavor_Composition_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Response_Bkd_cTagCat = GetFIVTerm("JET_Flavor_Response", "BkdCTagCat", JET_Flavor_Response_Bkd_cTagCat_Do, JET_Flavor_Response_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Flavor_Response_Bkd_nonCTagCat = GetFIVTerm("JET_Flavor_Response", "BkdNonCTagCat", JET_Flavor_Response_Bkd_nonCTagCat_Do, JET_Flavor_Response_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_JER_EffectiveNP_4_Bkd_cTagCat = GetFIVTerm("JET_JER_EffectiveNP_4", "BkdCTagCat", JET_JER_EffectiveNP_4_Bkd_cTagCat_Do, JET_JER_EffectiveNP_4_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_JER_EffectiveNP_4_Bkd_nonCTagCat = GetFIVTerm("JET_JER_EffectiveNP_4", "BkdNonCTagCat", JET_JER_EffectiveNP_4_Bkd_nonCTagCat_Do, JET_JER_EffectiveNP_4_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_RhoTopology_Bkd_cTagCat = GetFIVTerm("JET_Pileup_RhoTopology", "BkdCTagCat", JET_Pileup_RhoTopology_Bkd_cTagCat_Do, JET_Pileup_RhoTopology_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_RhoTopology_Bkd_nonCTagCat = GetFIVTerm("JET_Pileup_RhoTopology", "BkdNonCTagCat", JET_Pileup_RhoTopology_Bkd_nonCTagCat_Do, JET_Pileup_RhoTopology_Bkd_nonCTagCat_Up, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_OffsetNPV_Bkd_cTagCat = GetFIVTerm("JET_Pileup_OffsetNPV", "BkdCTagCat", JET_Pileup_OffsetNPV_Bkd_cTagCat_Do, JET_Pileup_OffsetNPV_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_JET_Pileup_OffsetNPV_Bkd_nonCTagCat = GetFIVTerm("JET_Pileup_OffsetNPV", "BkdNonCTagCat", JET_Pileup_OffsetNPV_Bkd_nonCTagCat_Do, JET_Pileup_OffsetNPV_Bkd_nonCTagCat_Up, workspace);



  //Modelling - Generator - PS
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_GGF_Bkd_cTagCat = GetFIVTerm("MOD_ALGPS_GGF", "BkdCTagCat", -1.*MOD_ALGPS_GGF_Bkd_cTagCat, MOD_ALGPS_GGF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_GGF_Bkd_nonCTagCat = GetFIVTerm("MOD_ALGPS_GGF", "BkdNonCTagCat", -1.*MOD_ALGPS_GGF_Bkd_nonCTagCat, MOD_ALGPS_GGF_Bkd_nonCTagCat, workspace);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_VBF_Bkd_cTagCat = GetFIVTerm("MOD_ALGPS_VBF", "BkdCTagCat", -1.*MOD_ALGPS_VBF_Bkd_cTagCat, MOD_ALGPS_VBF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALGPS_VBF_Bkd_nonCTagCat = GetFIVTerm("MOD_ALGPS_VBF", "BkdNonCTagCat", -1.*MOD_ALGPS_VBF_Bkd_nonCTagCat, MOD_ALGPS_VBF_Bkd_nonCTagCat, workspace);


  //Theory uncertainty of the cross section

  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Bkd_cTagCat = GetFIVTerm("THEO_GGF_Bkd", "CTagCat", THEO_GGF_Bkd_cTagCat_Do, THEO_GGF_Bkd_cTagCat_Up, workspace);
  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Bkd_noncTagCat = GetFIVTerm("THEO_GGF_Bkd", "NonCTagCat", THEO_GGF_Bkd_nonCTagCat_Do, THEO_GGF_Bkd_nonCTagCat_Up, workspace);
  // workspace->var("alpha_THEO_GGF_Bkd")->setConstant(not useTheoSys or doMuBkd);

  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_TTH_Bkd_cTagCat = GetFIVTerm("THEO_TTH_Bkd", "CTagCat", THEO_TTH_Bkd_cTagCat_Do, THEO_TTH_Bkd_cTagCat_Up, workspace);
  // RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_TTH_Bkd_noncTagCat = GetFIVTerm("THEO_TTH_Bkd", "NonCTagCat", THEO_TTH_Bkd_nonCTagCat_Do, THEO_TTH_Bkd_nonCTagCat_Up, workspace);
  // workspace->var("alpha_THEO_TTH_Bkd")->setConstant(not useTheoSys or doMuBkd);

  //RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Bkd_cTagCat = GetFIVTerm("THEO_GGF_Bkd", "CTagCat", -1.*crossSection, crossSection, workspace);
  //RooStats::HistFactory::FlexibleInterpVar *fiv_THEO_GGF_Bkd_noncTagCat = GetFIVTerm("THEO_GGF_Bkd", "NonCTagCat", -1.*crossSection, crossSection, workspace);
  //workspace->var("alpha_THEO_GGF_Bkd")->setConstant(not useTheoSys or (doMuBkd and not doMuBkdAsPOI));


  //Modelling - TEO - PDF
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_PDF_VBF_Bkd_cTagCat = GetFIVTerm("MOD_PDF_VBF", "BkdCTagCat", -1.*MOD_PDF_VBF_Bkd_cTagCat, MOD_PDF_VBF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_PDF_VBF_Bkd_nonCTagCat = GetFIVTerm("MOD_PDF_VBF", "BkdNonCTagCat", -1.*MOD_PDF_VBF_Bkd_nonCTagCat, MOD_PDF_VBF_Bkd_nonCTagCat, workspace);


  //Modelling - QCD - TEO - 4NP for GGF and 3NP for ggZH
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGF_Bkd_cTagCat = GetFIVTerm("MOD_QCD_MU_GGF", "BkdCTagCat", -1.*MOD_QCD_MU_GGF_Bkd_cTagCat, MOD_QCD_MU_GGF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGF_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_MU_GGF", "BkdNonCTagCat", -1.*MOD_QCD_MU_GGF_Bkd_nonCTagCat, MOD_QCD_MU_GGF_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MU_GGF")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_RES_GGF_Bkd_cTagCat = GetFIVTerm("MOD_QCD_RES_GGF", "BkdCTagCat", -1.*MOD_QCD_RES_GGF_Bkd_cTagCat, MOD_QCD_RES_GGF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_RES_GGF_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_RES_GGF", "BkdNonCTagCat", -1.*MOD_QCD_RES_GGF_Bkd_nonCTagCat, MOD_QCD_RES_GGF_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_RES_GGF")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGF_Bkd_cTagCat = GetFIVTerm("MOD_QCD_MIG01_GGF", "BkdCTagCat", -1.*MOD_QCD_MIG01_GGF_Bkd_cTagCat, MOD_QCD_MIG01_GGF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGF_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_MIG01_GGF", "BkdNonCTagCat", -1.*MOD_QCD_MIG01_GGF_Bkd_nonCTagCat, MOD_QCD_MIG01_GGF_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG01_GGF")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGF_Bkd_cTagCat = GetFIVTerm("MOD_QCD_MIG12_GGF", "BkdCTagCat", -1.*MOD_QCD_MIG12_GGF_Bkd_cTagCat, MOD_QCD_MIG12_GGF_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGF_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_MIG12_GGF", "BkdNonCTagCat", -1.*MOD_QCD_MIG12_GGF_Bkd_nonCTagCat, MOD_QCD_MIG12_GGF_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG12_GGF")->setConstant(not useQCDSys);


  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGZH_Bkd_cTagCat = GetFIVTerm("MOD_QCD_MU_GGZH", "BkdCTagCat", -1.*MOD_QCD_MU_GGZH_Bkd_cTagCat, MOD_QCD_MU_GGZH_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MU_GGZH_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_MU_GGZH", "BkdNonCTagCat", -1.*MOD_QCD_MU_GGZH_Bkd_nonCTagCat, MOD_QCD_MU_GGZH_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MU_GGZH")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGZH_Bkd_cTagCat = GetFIVTerm("MOD_QCD_MIG01_GGZH", "BkdCTagCat", -1.*MOD_QCD_MIG01_GGZH_Bkd_cTagCat, MOD_QCD_MIG01_GGZH_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG01_GGZH_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_MIG01_GGZH", "BkdNonCTagCat", -1.*MOD_QCD_MIG01_GGZH_Bkd_nonCTagCat, MOD_QCD_MIG01_GGZH_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG01_GGZH")->setConstant(not useQCDSys);

  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGZH_Bkd_cTagCat = GetFIVTerm("MOD_QCD_MIG12_GGZH", "BkdCTagCat", -1.*MOD_QCD_MIG12_GGZH_Bkd_cTagCat, MOD_QCD_MIG12_GGZH_Bkd_cTagCat, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_QCD_MIG12_GGZH_Bkd_nonCTagCat = GetFIVTerm("MOD_QCD_MIG12_GGZH", "BkdNonCTagCat", -1.*MOD_QCD_MIG12_GGZH_Bkd_nonCTagCat, MOD_QCD_MIG12_GGZH_Bkd_nonCTagCat, workspace);
  workspace->var("alpha_MOD_QCD_MIG12_GGZH")->setConstant(not useQCDSys);


  //Modelling - TEO - Alpha_s
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALPHAS_GGF_Bkd_cTagCat = GetFIVTerm("MOD_ALPHAS_GGF", "BkdCTagCat", MOD_ALPHAS_GGF_Bkd_cTagCat_Do, MOD_ALPHAS_GGF_Bkd_cTagCat_Up, workspace);
  RooStats::HistFactory::FlexibleInterpVar *fiv_MOD_ALPHAS_GGF_Bkd_nonCTagCat = GetFIVTerm("MOD_ALPHAS_GGF", "BkdNonCTagCat", MOD_ALPHAS_GGF_Bkd_nonCTagCat_Do, MOD_ALPHAS_GGF_Bkd_nonCTagCat_Up, workspace);





  for(int i=0; i<nBins; i++)
    {
      fiv_EG_RESOLUTION_ALL_Bkd_cTagCat[i] = GetFIVTerm("EG_RESOLUTION_ALL", "BkdCTagBin"+((TString) std::to_string(i)), EG_RESOLUTION_ALL_Bkd_cTagCat_Do[i], EG_RESOLUTION_ALL_Bkd_cTagCat_Up[i], workspace);
      fiv_EG_RESOLUTION_ALL_Bkd_nonCTagCat[i] = GetFIVTerm("EG_RESOLUTION_ALL", "BkdNonCTagBin"+((TString) std::to_string(i)), EG_RESOLUTION_ALL_Bkd_nonCTagCat_Do[i], EG_RESOLUTION_ALL_Bkd_nonCTagCat_Up[i], workspace);

      fiv_EG_SCALE_ALL_Bkd_cTagCat[i] = GetFIVTerm("EG_SCALE_ALL", "BkdCTagBin"+((TString) std::to_string(i)), EG_SCALE_ALL_Bkd_cTagCat_Do[i], EG_SCALE_ALL_Bkd_cTagCat_Up[i], workspace);
      fiv_EG_SCALE_ALL_Bkd_nonCTagCat[i] = GetFIVTerm("EG_SCALE_ALL", "BkdNonCTagBin"+((TString) std::to_string(i)), EG_SCALE_ALL_Bkd_nonCTagCat_Do[i], EG_SCALE_ALL_Bkd_nonCTagCat_Up[i], workspace);
    }

  std::map<int, RooRealVar*> binConstraintsResBkdNonCTag = {}; //for bin content of the bkd
  std::map<int, RooArgList*>  binArgListResBkdNonCTag = {}; //provides list of things you want to store
  std::map<int, RooProduct*>  binNomsResBkdNonCTag = {};//this is a product of mu2 and the bin content  mu2*nBkd in each bin
  for(int i=0; i<nBins; i++)
    {
      binConstraintsResBkdNonCTag[i] = new RooRealVar((TString) ("binConstraintsResBkdNonCTag"+std::to_string(i)), "", resBkdNonCTag[i]);
      binConstraintsResBkdNonCTag[i]->setConstant();
      binArgListResBkdNonCTag[i] = new RooArgList((TString) ("binArgListResBkdNonCTag"+std::to_string(i)));
      if(doMuBkd) binArgListResBkdNonCTag[i]->add(*muBkd);
      //binArgListResBkdNonCTag[i]->add(*fiv_ResBkdXSec);
      binArgListResBkdNonCTag[i]->add(*fiv_HHF_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_THBR);
      binArgListResBkdNonCTag[i]->add(*fiv_cTagC0_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_cTagL0_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_cTagL1_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_cTagPowHW7_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_cTagaMcPy8_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_PRW_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_PH_EFF_ID_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_PH_EFF_ISO_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_cTagLightAdHoc_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_EffectiveNP_1_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_EtaIntercalibration_Modelling_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_Flavor_Composition_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_Flavor_Response_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_JER_EffectiveNP_4_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_Pileup_RhoTopology_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_JET_Pileup_OffsetNPV_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_EG_RESOLUTION_ALL_Bkd_nonCTagCat[i]);
      binArgListResBkdNonCTag[i]->add(*fiv_EG_SCALE_ALL_Bkd_nonCTagCat[i]);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_ALGPS_GGF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_ALGPS_VBF_Bkd_nonCTagCat);
      //binArgListResBkdNonCTag[i]->add(*fiv_THEO_GGF_Bkd_noncTagCat);
      // binArgListResBkdNonCTag[i]->add(*fiv_THEO_TTH_Bkd_noncTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_PDF_VBF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_MU_GGF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_RES_GGF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_MIG01_GGF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_MIG12_GGF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_MU_GGZH_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_MIG01_GGZH_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_QCD_MIG12_GGZH_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*fiv_MOD_ALPHAS_GGF_Bkd_nonCTagCat);
      binArgListResBkdNonCTag[i]->add(*binConstraintsResBkdNonCTag[i]);
      binNomsResBkdNonCTag[i] = new RooProduct((TString) ("binNomsResBkdNonCTag"+std::to_string(i)), "", *binArgListResBkdNonCTag[i]);
    }

  std::map<int, RooRealVar*> binConstraintsResBkdCTag = {};
  std::map<int, RooArgList*>  binArgListResBkdCTag = {};
  std::map<int, RooProduct*>  binNomsResBkdCTag = {};
  for(int i=0; i<nBins; i++)
    {
      binConstraintsResBkdCTag[i] = new RooRealVar((TString) ("binConstraintsResBkdCTag"+std::to_string(i)), "", resBkdCTag[i]);
      binConstraintsResBkdCTag[i]->setConstant();
      binArgListResBkdCTag[i] = new RooArgList((TString) ("binArgListResBkdCTag"+std::to_string(i)));
      if(doMuBkd) binArgListResBkdCTag[i]->add(*muBkd);
      //binArgListResBkdCTag[i]->add(*fiv_ResBkdXSec);
      binArgListResBkdCTag[i]->add(*fiv_HHF_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_THBR);
      binArgListResBkdCTag[i]->add(*fiv_cTagC0_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagL0_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagL1_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagPowHW7_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagaMcPy8_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagVTX_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagVTX_Bkd_nonCTagCat);
      binArgListResBkdCTag[i]->add(*fiv_PRW_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_PH_EFF_ID_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_PH_EFF_ISO_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_cTagLightAdHoc_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_EffectiveNP_1_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_EtaIntercalibration_Modelling_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_Flavor_Composition_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_Flavor_Response_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_JER_EffectiveNP_4_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_Pileup_RhoTopology_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_JET_Pileup_OffsetNPV_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_EG_RESOLUTION_ALL_Bkd_cTagCat[i]);
      binArgListResBkdCTag[i]->add(*fiv_EG_SCALE_ALL_Bkd_cTagCat[i]);
      binArgListResBkdCTag[i]->add(*binConstraintsResBkdCTag[i]);
      binArgListResBkdCTag[i]->add(*fiv_MOD_ALGPS_GGF_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_ALGPS_VBF_Bkd_cTagCat);
      //binArgListResBkdCTag[i]->add(*fiv_THEO_GGF_Bkd_cTagCat);
      // binArgListResBkdCTag[i]->add(*fiv_THEO_TTH_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_PDF_VBF_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_MU_GGF_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_RES_GGF_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_MIG01_GGF_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_MIG12_GGF_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_MU_GGZH_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_MIG01_GGZH_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_QCD_MIG12_GGZH_Bkd_cTagCat);
      binArgListResBkdCTag[i]->add(*fiv_MOD_ALPHAS_GGF_Bkd_cTagCat);
      binNomsResBkdCTag[i] = new RooProduct((TString) ("binNomsResBkdCTag"+std::to_string(i)), "", *binArgListResBkdCTag[i]);
    }

  //////////////////////////////////////////////////////////////////////
  // Creating NR Background PDF

  double gprMeansNonCTag[nBins];
  double gprMeansCTag[nBins];
  double gprCovsCTag[nBins*nBins];
  double gprCovsNonCTag[nBins*nBins];
  if(doMCUnw)
    {
      for(int i=0; i<nBins; i++)
        {
          gprMeansNonCTag[i] = doMCFastSim ? gprMeansNonCTagMCFastSimUnw[i] : gprMeansNonCTagMCUnw[i];
          gprMeansCTag[i] = doMCFastSim ? gprMeansCTagMCFastSimUnw[i] : gprMeansCTagMCUnw[i];
        }
      for(int i=0; i<nBins*nBins; i++)
        {
          gprCovsNonCTag[i] = doMCFastSim ? gprCovsNonCTagMCFastSimUnw[i] : gprCovsNonCTagMCUnw[i];
          gprCovsCTag[i] = doMCFastSim ? gprCovsCTagMCFastSimUnw[i] : gprCovsCTagMCUnw[i];
        }
    }
  else if(doMC)
    {
      for(int i=0; i<nBins; i++)
        {
          gprMeansNonCTag[i] = doMCFastSim ? gprMeansNonCTagMCFastSim[i] : gprMeansNonCTagMC[i];
          gprMeansCTag[i] = doMCFastSim ? gprMeansCTagMCFastSim[i] : gprMeansCTagMC[i];
        }
      for(int i=0; i<nBins*nBins; i++)
        {
          gprCovsNonCTag[i] = doMCFastSim ? gprCovsNonCTagMCFastSim[i] : gprCovsNonCTagMC[i];
          gprCovsCTag[i] = doMCFastSim ? gprCovsCTagMCFastSim[i] : gprCovsCTagMC[i];
        }
    }
  else if(doSSTemplate)
    {
      for(int i=0; i<nBins; i++)
        {
          gprMeansNonCTag[i] = gprMeansNonCTagSSTemplate[i];
          gprMeansCTag[i] = gprMeansCTagSSTemplate[i];
        }
      for(int i=0; i<nBins*nBins; i++)
        {
          gprCovsNonCTag[i] = gprCovsNonCTagSSTemplate[i];
          gprCovsCTag[i] = gprCovsCTagSSTemplate[i];
        }
    }
  else if(doSSTemplateMCStats)
    {
      for(int i=0; i<nBins; i++)
        {
          gprMeansNonCTag[i] = gprMeansNonCTagSSTemplateMCStats[i];
          gprMeansCTag[i] = gprMeansCTagSSTemplateMCStats[i];
        }
      for(int i=0; i<nBins*nBins; i++)
        {
          gprCovsNonCTag[i] = gprCovsNonCTagSSTemplateMCStats[i];
          gprCovsCTag[i] = gprCovsCTagSSTemplateMCStats[i];
        }
    }
  else
    {
      for(int i=0; i<nBins; i++)
        {
          gprMeansNonCTag[i] = gprMeansNonCTagData[i];
          gprMeansCTag[i] = gprMeansCTagData[i];
        }
      for(int i=0; i<nBins*nBins; i++)
        {
          gprCovsNonCTag[i] = gprCovsNonCTagData[i];
          gprCovsCTag[i] = gprCovsCTagData[i];
        }
    }

  for(int i=0; i<nBins*nBins; i++)
    {
      if((i-i/nBins)%nBins==0)
        {
          if(relativeDamping)
            {
              gprCovsNonCTag[i]*=(1.+dampingNoCTag);
              gprCovsCTag[i]*=(1.+dampingCTag);
            }
          else
            {
              gprCovsNonCTag[i]+=dampingNoCTag;
              gprCovsCTag[i]+=dampingCTag;
            }
        }
    }

  std::map<int, RooRealVar*> binConstraintsNRBkdNonCTag = {};
  std::map<int, RooRealVar*> binNomsNRBkdNonCTag = {};
  RooArgList *constraintListNRBkdNonCTag = new RooArgList("constraintListNRBkdNonCTag");
  RooArgList *muListNRBkdNonCTag = new RooArgList("muListNRBkdNonCTag");
  for(int i=0; i<nBins; i++)
    {
      binConstraintsNRBkdNonCTag[i] = new RooRealVar((TString) ("binConstraintsNRBkdNonCTag"+std::to_string(i)), "", gprMeansNonCTag[i]);
      binConstraintsNRBkdNonCTag[i]->setConstant();
      binNomsNRBkdNonCTag[i] = new RooRealVar((TString) ("binNomsNRBkdNonCTag"+std::to_string(i)), "", gprMeansNonCTag[i], 0.0*gprMeansNonCTag[i], 2.0*gprMeansNonCTag[i]);
      constraintListNRBkdNonCTag->add(*binConstraintsNRBkdNonCTag[i]);
      muListNRBkdNonCTag->add(*binNomsNRBkdNonCTag[i]);
    }
  TMatrixDSym *covNRBkdNonCTag = new TMatrixDSym(nBins);
  covNRBkdNonCTag->SetMatrixArray(gprCovsNonCTag);
  RooMultiVarGaussian *NRBkdModelNonCTag = new RooMultiVarGaussian("NRBkdModelNonCTag", "", *constraintListNRBkdNonCTag, *muListNRBkdNonCTag, *covNRBkdNonCTag);//Constrain on the Bkd

  std::map<int, RooRealVar*> binConstraintsNRBkdCTag = {};
  std::map<int, RooRealVar*> binNomsNRBkdCTag = {};
  RooArgList *constraintListNRBkdCTag = new RooArgList("constraintListNRBkdCTag");
  RooArgList *muListNRBkdCTag = new RooArgList("muListNRBkdCTag");
  for(int i=0; i<nBins; i++)
    {
      binConstraintsNRBkdCTag[i] = new RooRealVar((TString) ("binConstraintsNRBkdCTag"+std::to_string(i)), "", gprMeansCTag[i]);
      binConstraintsNRBkdCTag[i]->setConstant();
      binNomsNRBkdCTag[i] = new RooRealVar((TString) ("binNomsNRBkdCTag"+std::to_string(i)), "", gprMeansCTag[i], 0.0*gprMeansCTag[i], 2.0*gprMeansCTag[i]);
      constraintListNRBkdCTag->add(*binConstraintsNRBkdCTag[i]);
      muListNRBkdCTag->add(*binNomsNRBkdCTag[i]);
    }
  TMatrixDSym *covNRBkdCTag = new TMatrixDSym(nBins);
  covNRBkdCTag->SetMatrixArray(gprCovsCTag);
  RooMultiVarGaussian *NRBkdModelCTag = new RooMultiVarGaussian("NRBkdModelCTag", "", *constraintListNRBkdCTag, *muListNRBkdCTag, *covNRBkdCTag); //Constrain on the Bkd

  // // For symmetric uncertainty
  // double sigmaUp_ResBkdXSec = TMath::Sqrt( TMath::Power((kggfup),2) + TMath::Power((kvbfup),2) + TMath::Power((ktthfup),2) + TMath::Power((kwphup),2) + TMath::Power((kwmhup),2) + TMath::Power((kzzhhup),2) + TMath::Power((kggzhhup),2) + TMath::Power((kbbhup),2))/SigmaTot;
  // RooRealVar* mean_ResBkdXSec  = new RooRealVar("mean_ResBkdXSec","mean_ResBkdXSec",1.);
  // RooRealVar* sigma_ResBkdXSec = new RooRealVar("sigma_ResBkdXSec","sigma_ResBkdXSec",sigmaUp_ResBkdXSec);
  // RooGaussian *resBkdModel = new RooGaussian("resBkdModel","",*muResBkd, *mean_ResBkdXSec , *sigma_ResBkdXSec);
  // muResBkd->setConstant(not useGprSys);
  // workspace->extendSet("allGlobObs", mean_ResBkdXSec->GetName());
  // workspace->extendSet("allGlobObs", sigma_ResBkdXSec->GetName());


  //////////////////////////////////////////////////////////////////////
  // Create PDF and Import to Workspace

  RooArgList *allPdfs = new RooArgList("allPdfs");
  if(category==0 or category==2) allPdfs->add(*NRBkdModelNonCTag);
  if(category==1 or category==2) allPdfs->add(*NRBkdModelCTag);
  // allPdfs->add(*resBkdModel); // Adding res background

  std::map<int, RooAddition*> binNoms = {}; //sum of the pdfs
  std::map<int, RooRealVar*>  binVals = {};
  std::map<int, RooArgList*>  binLists = {};
  std::map<int, RooPoisson*>  binPoissons = {};
  RooArgSet *allBinVals = new RooArgSet("allBinVals");

  if(category==0 or category==2)
    {
      for(int i=0; i<nBins; i++)
        {
          binLists[nBins+i] = new RooArgList((TString) ("binLists"+std::to_string(nBins+i)));
          binLists[nBins+i]->add(*binNomsSigNonCTag[i]);
          binLists[nBins+i]->add(*binNomsNRBkdNonCTag[i]);
          binLists[nBins+i]->add(*binNomsResBkdNonCTag[i]);
          binNomsNRBkdNonCTag[i]->setConstant(not useGprSys);
          binNoms[nBins+i] = new RooAddition((TString) ("binNoms"+std::to_string(nBins+i)), "", *binLists[nBins+i]);
          binVals[nBins+i] = new RooRealVar((TString) ("binVals"+std::to_string(nBins+i)), "", gprMeansNonCTag[i]);
          allBinVals->add(*binVals[nBins+i]);
          binPoissons[nBins+i] = new RooPoisson((TString) ("binPoissons"+std::to_string(nBins+i)), "", *binVals[nBins+i], *binNoms[nBins+i]);
          binPoissons[nBins+i]->setNoRounding(true);
          allPdfs->add(*binPoissons[nBins+i]);
        }
    }

  if(category==1 or category==2)
    {
      for(int i=0; i<nBins; i++)
        {
          binLists[i] = new RooArgList((TString) ("binLists"+std::to_string(i)));
          binLists[i]->add(*binNomsSigCTag[i]);
          binLists[i]->add(*binNomsNRBkdCTag[i]);
          binLists[i]->add(*binNomsResBkdCTag[i]);
          binNomsNRBkdCTag[i]->setConstant(not useGprSys);
          binNoms[i] = new RooAddition((TString) ("binNoms"+std::to_string(i)), "", *binLists[i]);
          binVals[i] = new RooRealVar((TString) ("binVals"+std::to_string(i)), "", gprMeansCTag[i]);
          allBinVals->add(*binVals[i]);
          binPoissons[i] = new RooPoisson((TString) ("binPoissons"+std::to_string(i)), "", *binVals[i], *binNoms[i]);
          binPoissons[i]->setNoRounding(true);
          allPdfs->add(*binPoissons[i]);
        }
    }

  RooArgSet pdfsAll = workspace->allPdfs();
  TIterator *pdfIter = pdfsAll.createIterator();
  RooAbsPdf *pdfIterTmp;
  while((pdfIterTmp = (RooAbsPdf*) pdfIter->Next()))
    {
      TString pdfName = pdfIterTmp->GetName();
      TString npName = pdfName;
      npName.ReplaceAll("Constraint_", "");
      if(not workspace->var(npName)->isConstant())
        {
          std::cout<<"Adding uncertainty: "<<pdfName<<std::endl;
          allPdfs->add(*pdfIterTmp);
        }
    }
  RooProdPdf *fullModel = new RooProdPdf("fullModel", "", *allPdfs);
  workspace->import(*fullModel);

  for(int i=0; i<nBins; i++)
    {
      workspace->extendSet("allNPs", binNomsNRBkdNonCTag[i]->GetName());
      workspace->extendSet("allNPs", binNomsNRBkdCTag[i]->GetName());
      workspace->extendSet("allGlobObs", binConstraintsSigNonCTag[i]->GetName());
      workspace->extendSet("allGlobObs", binConstraintsSigCTag[i]->GetName());
      workspace->extendSet("allGlobObs", binConstraintsResBkdNonCTag[i]->GetName());
      workspace->extendSet("allGlobObs", binConstraintsResBkdCTag[i]->GetName());
      workspace->extendSet("allGlobObs", binConstraintsNRBkdNonCTag[i]->GetName());
      workspace->extendSet("allGlobObs", binConstraintsNRBkdCTag[i]->GetName());
    }

  //////////////////////////////////////////////////////////////////////
  // Create Dataset

  double srNonCTag[nBins];
  double srCTag[nBins];

  for(int i=0; i<nBins; i++)
    {
      if(doMCUnw)
        {
          srNonCTag[i] = doMCFastSim ? srNonCTagMCFastSimUnw[i] : srNonCTagMCUnw[i];
          srCTag[i] = doMCFastSim ? srCTagMCFastSimUnw[i] : srCTagMCUnw[i];
        }
      else if(doMC)
        {
          srNonCTag[i] = doMCFastSim ? srNonCTagMCFastSim[i] : srNonCTagMC[i];
          srCTag[i] = doMCFastSim ? srCTagMCFastSim[i] : srCTagMC[i];
        }
      else if(doSSTemplate)
        {
          srNonCTag[i] = srNonCTagSSTemplate[i];
          srCTag[i] = srCTagSSTemplate[i];
        }
      else if(doSSTemplateMCStats)
        {
          srNonCTag[i] = srNonCTagSSTemplateMCStats[i];
          srCTag[i] = srCTagSSTemplateMCStats[i];
        }

      srNonCTag[i] += resBkdNonCTag[i];
      srCTag[i] += resBkdCTag[i];
    }

  RooDataSet *data = new RooDataSet("data", "", *allBinVals);//unbinned data sets RooDataSet

  if(doUnblind)
    {
      if(not (doMC or doMCUnw or doSSTemplate or doSSTemplateMCStats))
        {
          std::cout<<"Not coded unblinding yet for data"<<std::endl;
          return 1;
        }
      else
        {
          if(category==0 or category==2)
            {
              for(int i=0; i<nBins; i++)
                {
                  binVals[nBins+i]->setVal(srNonCTag[i]);
                }
            }

          if(category==1 or category==2)
            {
              for(int i=0; i<nBins; i++)
                {
                  binVals[i]->setVal(srCTag[i]);
                }
            }
        }

      data->add(*allBinVals);

      if(category==0 or category==2)
        {
          for(int i=0; i<nBins; i++)
            {
              binVals[nBins+i]->setVal(gprMeansNonCTag[i]+resBkdNonCTag[i]);
            }
        }

      if(category==1 or category==2)
        {
          for(int i=0; i<nBins; i++)
            {
              binVals[i]->setVal(gprMeansCTag[i]+resBkdCTag[i]);
            }
        }
    }
  else
    {
      TFile *toysFile = new TFile("toys.root");

      TString histNameNonCTag = "nonctag_";
      histNameNonCTag = histNameNonCTag+((TString) TString::Itoa(fileNumber, 10));
      TH1F *dataNonCTag = (TH1F*) toysFile->Get(histNameNonCTag);

      TString histNameCTag = "ctag_";
      histNameCTag = histNameCTag+((TString) TString::Itoa(fileNumber, 10));
      TH1F *dataCTag = (TH1F*) toysFile->Get(histNameCTag);

      if(category==0 or category==2)
        {
          for(int i=0; i<nBins; i++)
            {
	      binVals[nBins+i]->setVal(dataNonCTag->GetBinContent(i+30+1)+resBkdNonCTag[i]);
            }
        }

      if(category==1 or category==2)
        {
          for(int i=0; i<nBins; i++)
            {
	      binVals[i]->setVal(dataCTag->GetBinContent(i+30+1)+resBkdCTag[i]);
            }
        }

      data->add(*allBinVals);

      if(category==0 or category==2)
        {
          for(int i=0; i<nBins; i++)
            {
              if(doMuBkdAsPOI)
                binVals[nBins+i]->setVal(gprMeansNonCTag[i]+sigNonCTag[i]);
              else
                binVals[nBins+i]->setVal(gprMeansNonCTag[i]+resBkdNonCTag[i]);
            }
        }

      if(category==1 or category==2)
        {
          for(int i=0; i<nBins; i++)
            {
              if(doMuBkdAsPOI)
                binVals[i]->setVal(gprMeansCTag[i]+sigCTag[i]);
              else
                binVals[i]->setVal(gprMeansCTag[i]+resBkdCTag[i]);
            }
        }
    }

  workspace->import(*data);

  //////////////////////////////////////////////////////////////////////
  // Do Fit and Make Plots

  RooFitResult *fitRes = fullModel->fitTo(*data, RooFit::Strategy(1), RooFit::Minos(true), RooFit::Offset(true), RooFit::SumW2Error(false), RooFit::Save(true), RooFit::PrintEvalErrors(-1));
  std::cout<<"Fit Result: ^^^"<<std::endl;

  std::cout<<"printValue:";
  fitRes->printValue(std::cout);
  std::cout<<std::endl;

  if(doMuBkd)
    {
      std::cout<<"Correlation!!!:"<<std::endl;
      std::cout<<fitRes->correlation(*mu, *muBkd)<<std::endl;
    }

  // // std::cout<<"floatParsFinal:"<<std::endl;
  // // for(int i=0; i<21; i++)
  // //   {
  // //     fitRes->floatParsFinal().at(i)->GetName();
  // //     ((RooRealVar*) fitRes->floatParsFinal().at(i))->getError();
  // //     ((RooRealVar*) fitRes->floatParsFinal().at(i))->getValV();
  // //   }

  // std::cout<<"Fit Result:"<<std::endl;
  // fitRes->Print();

  if(doPlots)
    {
      if(category==0 or category==2)
        {
          TH1D *histSig = new TH1D("histSig", "", nBins, 120., 130.);
          TH1D *histResBkdPre = new TH1D("histResBkdPre", "", nBins, 120., 130.);
          TH1D *histNRBkdPre = new TH1D("histNRBkdPre", "", nBins, 120., 130.);
          TH1D *histTotBkdPre = new TH1D("histTotBkdPre", "", nBins, 120., 130.);
          TH1D *histTotBkdPost = new TH1D("histTotBkdPost", "", nBins, 120., 130.);
          TH1D *histTotSigBkdPost = new TH1D("histTotSigBkdPost", "", nBins, 120., 130.);
          TH1D *histData = new TH1D("histData", "", nBins, 120., 130.);

          double max = -1.;
          for(int i=0; i<nBins; i++)
            {
              histSig->SetBinContent(i+1, 10.*sigNonCTag[i]);
              histResBkdPre->SetBinContent(i+1, resBkdNonCTag[i]);
              histNRBkdPre->SetBinContent(i+1, gprMeansNonCTag[i]);
              histTotBkdPre->SetBinContent(i+1, gprMeansNonCTag[i]+resBkdNonCTag[i]);
              histTotBkdPost->SetBinContent(i+1, binNomsNRBkdNonCTag[i]->getVal()+binNomsResBkdNonCTag[i]->getVal());
              histTotSigBkdPost->SetBinContent(i+1, binNoms[nBins+i]->getVal());
              if(doUnblind) histData->SetBinContent(i+1, srNonCTag[i]);
              else histData->SetBinContent(i+1, injectMu*(1+fabs(egResPull)*(egResPull<=0 ? EG_RESOLUTION_ALL_Sig_nonCTagCat_Do[i] : EG_RESOLUTION_ALL_Sig_nonCTagCat_Up[i]))*(1+fabs(egScalePull)*(egScalePull<=0 ? EG_SCALE_ALL_Sig_nonCTagCat_Do[i] : EG_SCALE_ALL_Sig_nonCTagCat_Up[i]))*sigNonCTag[i]+asimovScale*(1.+asimovTilt*(i/4.5-1.))*(gprMeansNonCTag[i]+(1+fabs(egResPull)*(egResPull<=0 ? EG_RESOLUTION_ALL_Bkd_nonCTagCat_Do[i] : EG_RESOLUTION_ALL_Bkd_nonCTagCat_Up[i]))*(1+fabs(egScalePull)*(egScalePull<=0 ? EG_SCALE_ALL_Bkd_nonCTagCat_Do[i] : EG_SCALE_ALL_Bkd_nonCTagCat_Up[i]))*resBkdNonCTag[i]));
              if(histSig->GetBinContent(i+1)>max) max=histSig->GetBinContent(i+1);
              if(histResBkdPre->GetBinContent(i+1)>max) max=histResBkdPre->GetBinContent(i+1);
              if(histNRBkdPre->GetBinContent(i+1)>max) max=histNRBkdPre->GetBinContent(i+1);
              if(histTotBkdPre->GetBinContent(i+1)>max) max=histTotBkdPre->GetBinContent(i+1);
              if(histTotBkdPost->GetBinContent(i+1)>max) max=histTotBkdPost->GetBinContent(i+1);
              if(histTotSigBkdPost->GetBinContent(i+1)>max) max=histTotSigBkdPost->GetBinContent(i+1);
              if(histData->GetBinContent(i+1)>max) max=histData->GetBinContent(i+1);
            }

          TCanvas *canvas = new TCanvas("canvas", "", 600, 600);
          canvas->SetTopMargin(0.05);
          canvas->SetBottomMargin(0.15);
          canvas->SetLeftMargin(0.15);
          canvas->SetRightMargin(0.05);
          histSig->Draw();
          histSig->SetStats(0);
          histSig->SetTitle("");
          histSig->SetLineWidth(2);
          histSig->SetLineColor(kGreen-2);
          histSig->SetLineStyle(2);
          histSig->GetYaxis()->SetRangeUser(0., 1.1*max);
          histSig->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
          histSig->GetYaxis()->SetTitle("Events / 0.5 GeV");
          histSig->GetXaxis()->SetTitleOffset(1.5);
          histSig->GetYaxis()->SetTitleOffset(2.0);
          histNRBkdPre->Draw("same");
          histNRBkdPre->SetLineColor(kRed);
          histNRBkdPre->SetLineWidth(2);
          histNRBkdPre->SetLineStyle(2);
          histTotBkdPre->Draw("same");
          histTotBkdPre->SetLineColor(kBlack);
          histTotBkdPre->SetLineWidth(2);
          histTotBkdPre->SetLineStyle(2);
          histTotSigBkdPost->Draw("same");
          histTotSigBkdPost->SetLineColor(kGreen-2);
          histTotSigBkdPost->SetLineWidth(2);
          histTotBkdPost->Draw("same");
          histTotBkdPost->SetLineColor(kBlack);
          histTotBkdPost->SetLineWidth(2);
          histData->Draw("sameepx0");
          histData->SetLineColor(kBlack);
          histData->SetLineWidth(2);
          histData->SetMarkerStyle(20);
          histResBkdPre->Draw("same");
          histResBkdPre->SetLineColor(kBlue);
          histResBkdPre->SetLineWidth(2);
          histResBkdPre->SetLineStyle(2);

          TLegend *leg = new TLegend(0.18, 0.44, 0.4, 0.74);
          leg->SetTextSize(0.035);
          leg->SetTextFont(42);
          leg->AddEntry(histSig, "Signal (pre-fit#times10)", "l");
          leg->AddEntry(histResBkdPre, "Res bkd (pre-fit)", "l");
          leg->AddEntry(histNRBkdPre, "Non-res bkd (pre-fit)", "l");
          leg->AddEntry(histTotBkdPre, "Total bkd (pre-fit)", "l");
          leg->AddEntry(histTotBkdPost, "Total bkd (post-fit)", "l");
          leg->AddEntry(histTotSigBkdPost, "Total sig+bkd (post-fit)", "l");
          leg->AddEntry(histData, "Data", "pe");
          leg->SetBorderSize(0);
          leg->Draw("same");

          TLatex *latex = new TLatex();
          latex->SetTextSize(0.035);
          latex->SetNDC();
          latex->DrawLatex(0.18,0.4,"#it{ATLAS} #bf{Internal}");
          latex->DrawLatex(0.18,0.35,"#bf{#sqrt{s}=13 TeV, 140 fb^{-1}}");

          canvas->SaveAs("GPRFitPlotNonCTag.png");

          delete histSig;
          delete histResBkdPre;
          delete histNRBkdPre;
          delete histTotBkdPre;
          delete histTotBkdPost;
          delete histTotSigBkdPost;
          delete histData;
          delete canvas;
          delete leg;
          delete latex;
        }

      if(category==1 or category==2)
        {
          TH1D *histSig = new TH1D("histSig", "", nBins, 120., 130.);
          TH1D *histResBkdPre = new TH1D("histResBkdPre", "", nBins, 120., 130.);
          TH1D *histNRBkdPre = new TH1D("histNRBkdPre", "", nBins, 120., 130.);
          TH1D *histTotBkdPre = new TH1D("histTotBkdPre", "", nBins, 120., 130.);
          TH1D *histTotBkdPost = new TH1D("histTotBkdPost", "", nBins, 120., 130.);
          TH1D *histTotSigBkdPost = new TH1D("histTotSigBkdPost", "", nBins, 120., 130.);
          TH1D *histData = new TH1D("histData", "", nBins, 120., 130.);

          double max = -1.;
          for(int i=0; i<nBins; i++)
            {
              histSig->SetBinContent(i+1, 10.*sigCTag[i]);
              histResBkdPre->SetBinContent(i+1, resBkdCTag[i]);
              histNRBkdPre->SetBinContent(i+1, gprMeansCTag[i]);
              histTotBkdPre->SetBinContent(i+1, gprMeansCTag[i]+resBkdCTag[i]);
              histTotBkdPost->SetBinContent(i+1, binNomsNRBkdCTag[i]->getVal()+binNomsResBkdCTag[i]->getVal());
              histTotSigBkdPost->SetBinContent(i+1, binNoms[i]->getVal());
              if(doUnblind) histData->SetBinContent(i+1, srCTag[i]);
              else histData->SetBinContent(i+1, injectMu*(1+fabs(egResPull)*(egResPull<=0 ? EG_RESOLUTION_ALL_Sig_cTagCat_Do[i] : EG_RESOLUTION_ALL_Sig_cTagCat_Up[i]))*(1+fabs(egScalePull)*(egScalePull<=0 ? EG_SCALE_ALL_Sig_cTagCat_Do[i] : EG_SCALE_ALL_Sig_cTagCat_Up[i]))*sigCTag[i]+asimovScale*(1.+asimovTilt*(i/4.5-1.))*(gprMeansCTag[i]+(1+fabs(egResPull)*(egResPull<=0 ? EG_RESOLUTION_ALL_Bkd_cTagCat_Do[i] : EG_RESOLUTION_ALL_Bkd_cTagCat_Up[i]))*(1+fabs(egScalePull)*(egScalePull<=0 ? EG_SCALE_ALL_Bkd_cTagCat_Do[i] : EG_SCALE_ALL_Bkd_cTagCat_Up[i]))*resBkdCTag[i]));
              if(histSig->GetBinContent(i+1)>max) max=histSig->GetBinContent(i+1);
              if(histResBkdPre->GetBinContent(i+1)>max) max=histResBkdPre->GetBinContent(i+1);
              if(histNRBkdPre->GetBinContent(i+1)>max) max=histNRBkdPre->GetBinContent(i+1);
              if(histTotBkdPre->GetBinContent(i+1)>max) max=histTotBkdPre->GetBinContent(i+1);
              if(histTotBkdPost->GetBinContent(i+1)>max) max=histTotBkdPost->GetBinContent(i+1);
              if(histTotSigBkdPost->GetBinContent(i+1)>max) max=histTotSigBkdPost->GetBinContent(i+1);
              if(histData->GetBinContent(i+1)>max) max=histData->GetBinContent(i+1);
            }

          TCanvas *canvas = new TCanvas("canvas", "", 600, 600);
          canvas->SetTopMargin(0.05);
          canvas->SetBottomMargin(0.15);
          canvas->SetLeftMargin(0.15);
          canvas->SetRightMargin(0.05);
          histSig->Draw();
          histSig->SetStats(0);
          histSig->SetTitle("");
          histSig->SetLineWidth(2);
          histSig->SetLineColor(kGreen-2);
          histSig->SetLineStyle(2);
          histSig->GetYaxis()->SetRangeUser(0., 1.1*max);
          histSig->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
          histSig->GetYaxis()->SetTitle("Events / 0.5 GeV");
          histSig->GetXaxis()->SetTitleOffset(1.5);
          histSig->GetYaxis()->SetTitleOffset(2.0);
          histNRBkdPre->Draw("same");
          histNRBkdPre->SetLineColor(kRed);
          histNRBkdPre->SetLineWidth(2);
          histNRBkdPre->SetLineStyle(2);
          histTotBkdPre->Draw("same");
          histTotBkdPre->SetLineColor(kBlack);
          histTotBkdPre->SetLineWidth(2);
          histTotBkdPre->SetLineStyle(2);
          histTotSigBkdPost->Draw("same");
          histTotSigBkdPost->SetLineColor(kGreen-2);
          histTotSigBkdPost->SetLineWidth(2);
          histTotBkdPost->Draw("same");
          histTotBkdPost->SetLineColor(kBlack);
          histTotBkdPost->SetLineWidth(2);
          histData->Draw("sameepx0");
          histData->SetLineColor(kBlack);
          histData->SetLineWidth(2);
          histData->SetMarkerStyle(20);
          histResBkdPre->Draw("same");
          histResBkdPre->SetLineColor(kBlue);
          histResBkdPre->SetLineWidth(2);
          histResBkdPre->SetLineStyle(2);

          TLegend *leg = new TLegend(0.18, 0.44, 0.4, 0.74);
          leg->SetTextSize(0.035);
          leg->SetTextFont(42);
          leg->AddEntry(histSig, "Signal (pre-fit#times10)", "l");
          leg->AddEntry(histResBkdPre, "Res bkd (pre-fit)", "l");
          leg->AddEntry(histNRBkdPre, "Non-res bkd (pre-fit)", "l");
          leg->AddEntry(histTotBkdPre, "Total bkd (pre-fit)", "l");
          leg->AddEntry(histTotBkdPost, "Total bkd (post-fit)", "l");
          leg->AddEntry(histTotSigBkdPost, "Total sig+bkd (post-fit)", "l");
          leg->AddEntry(histData, "Data", "ep");
          leg->SetBorderSize(0);
          leg->Draw("same");

          TLatex *latex = new TLatex();
          latex->SetTextSize(0.035);
          latex->SetNDC();
          latex->DrawLatex(0.18,0.4,"#it{ATLAS} #bf{Internal}");
          latex->DrawLatex(0.18,0.35,"#bf{#sqrt{s}=13 TeV, 140 fb^{-1}}");

          canvas->SaveAs("GPRFitPlotCTag.png");

          delete histSig;
          delete histResBkdPre;
          delete histNRBkdPre;
          delete histTotBkdPre;
          delete histTotBkdPost;
          delete histTotSigBkdPost;
          delete histData;
          delete canvas;
          delete leg;
          delete latex;
        }
    }

  if(doLimit)
    {
      RooStats::ModelConfig *modelSB = new RooStats::ModelConfig("modelSB", workspace);
      modelSB->SetPdf(*fullModel);
      if(doMuBkdAsPOI)
        modelSB->SetParametersOfInterest(RooArgSet(*muBkd));
      else
        modelSB->SetParametersOfInterest(RooArgSet(*mu));
      modelSB->SetObservables(*allBinVals);
      modelSB->SetNuisanceParameters(*workspace->set("allNPs")); // need to add
      modelSB->SetGlobalObservables(*workspace->set("allGlobObs")); // need to add
      if(doMuBkdAsPOI)
        {
          muBkd->setVal(1.0);
          modelSB->SetSnapshot(RooArgSet(*muBkd));//used to calculate expected signif.
        }
      else
        {
          mu->setVal(1.0);
          modelSB->SetSnapshot(RooArgSet(*mu));//used to calculate expected signif.
        }
      workspace->import(*modelSB);

      RooStats::ModelConfig *modelB = modelSB->Clone("modelB");
      if(doMuBkdAsPOI)
        {
          muBkd->setVal(0.0);
          modelB->SetSnapshot(RooArgSet(*muBkd));
        }
      else
        {
          mu->setVal(0.0);
          modelB->SetSnapshot(RooArgSet(*mu));
        }
      workspace->import(*modelB);

      workspace->Print();
      workspace->writeToFile("test_ws.root", kTRUE);


      std::cout<<"Limit:"<<std::endl;
      RooStats::AsymptoticCalculator *asymCalc = new RooStats::AsymptoticCalculator(*data, *modelB, *modelSB);
      asymCalc->SetOneSided(true);
      RooStats::HypoTestInverter *hypoTestInv = new RooStats::HypoTestInverter(*asymCalc);
      hypoTestInv->SetConfidenceLevel(0.95);
      hypoTestInv->UseCLs(true);
      hypoTestInv->SetVerbose(0);
      if(doAutoScan)
        {
          hypoTestInv->SetAutoScan();
        }
      else
        {
          const double limitEst = 20.;
          hypoTestInv->SetFixedScan(scanPoints, injectMu, 2.0*limitEst-injectMu);
        }
      auto interval = hypoTestInv->GetInterval();
      std::cout<<"Limit = "<<interval->UpperLimit()<<std::endl;
    }

  return 0;
}


RooStats::HistFactory::FlexibleInterpVar *GetFIVTerm(const TString name, const TString fivName, const double priorDown, const double priorUp, RooWorkspace *workspace, const double nominal, const int interpCode)
{
  TString nuis_id = "alpha_" + name;
  RooRealVar *nuis_term = workspace->var(nuis_id);
  if(not nuis_term)
    {
      TString constr_id  = "Constraint_alpha_" + name;
      TString nuisnom_id = "alpha_nominal_" + name;
      workspace->factory(TString::Format("Gaussian::%s(%s[0., -5., 5.], %s[0., -5., 5.], 1.)", constr_id.Data(), nuis_id.Data(), nuisnom_id.Data()));
      workspace->var(nuisnom_id)->setConstant();
      workspace->extendSet("allGlobObs", nuisnom_id); // the nominal value as a global observable
      nuis_term = workspace->var(nuis_id);
      nuis_term->setError(1);
      workspace->extendSet("allNPs", nuis_id); // nuis_term is a nuisance parameter
    }
  RooStats::HistFactory::FlexibleInterpVar *fiv = new RooStats::HistFactory::FlexibleInterpVar("fiv_"+name+"_"+fivName, "", RooArgList(*nuis_term), nominal, std::vector<double>({nominal+priorDown}), std::vector<double>({nominal+priorUp}));
  fiv->setAllInterpCodes(interpCode); // HistFactory interpolation code
  return fiv;
}


RooStats::HistFactory::FlexibleInterpVar *GetFlatFIVTerm(const TString name, const TString fivName, const double priorDown, const double priorUp, RooWorkspace *workspace, const double nominal)
{
  TString nuis_id = "alpha_" + name;
  RooRealVar *nuis_term = workspace->var(nuis_id);
  if(not nuis_term)
    {
      TString constr_id  = "Constraint_alpha_" + name;
      TString nuisnom_id = "alpha_nominal_" + name;
      workspace->factory(TString::Format("Uniform::%s(%s[0., -25., 25.])", constr_id.Data(), nuis_id.Data()));
      nuis_term = workspace->var(nuis_id);
      workspace->extendSet("allNPs", nuis_id); // nuis_term is a nuisance parameter
    }
  RooStats::HistFactory::FlexibleInterpVar *fiv = new RooStats::HistFactory::FlexibleInterpVar("fiv_"+name+"_"+fivName, "", RooArgList(*nuis_term), nominal, std::vector<double>({nominal+priorDown}), std::vector<double>({nominal+priorUp}));
  fiv->setAllInterpCodes(4); // HistFactory interpolation code
  return fiv;
}
