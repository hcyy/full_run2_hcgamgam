#!/usr/bin/env python

import numpy as np    # Tensor manipulation
import matplotlib     # Plotting
matplotlib.use("Agg") # Stops matplotlib crashing when remote disconneted
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy.stats import norm

if __name__ == "__main__":

    pulls = []

    for i_toy in range(1000):
        
        file = open("Logs_withoutGPRUncertainty/"+str(i_toy)+".out", "r")

        for line in file:

            # if "  41  mu" not in line: continue
            if "   1  mu" not in line: continue

            muValAndErrs = line.split()

            # print(muValAndErrs)

            mu = float(muValAndErrs[2])
            muHess = float(muValAndErrs[3])
            muMinosDo = float(muValAndErrs[4])
            muMinosUp = float(muValAndErrs[5])

            # pull = mu/muHess
            pull = mu/abs(muMinosDo) if mu>0. else mu/abs(muMinosUp)
            pulls.append(pull)

    (mu, sigma) = norm.fit(pulls)

    fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8,8))
    n, bins, patches = axs.hist(pulls, bins=40, range=(-4.0, 4.0), histtype="step", linewidth=1, edgecolor="black")
    # n, bins, patches = axs.hist(pulls, bins=40, range=(-4.0, 4.0), density=True, histtype="step", linewidth=1, edgecolor="black")
    axs.title.set_text("")
    plt.xlabel("Pull")
    plt.ylabel("Toys")
    fig.tight_layout(pad=2.0)

    y = mlab.normpdf(bins, mu, sigma)
    l = plt.plot(bins, 1000.*y*8./40., "r--", linewidth=1)

    # plt.title(r"$\mu=%.3f,\ \sigma=%.3f$" %(mu, sigma))
    print("mu = %f" % mu)
    print("sigma = %f" % sigma)

    fig.savefig("ToyPulls.png")
    plt.close()
    
    
