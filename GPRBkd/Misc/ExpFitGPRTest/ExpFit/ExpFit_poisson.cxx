#include "ExpFit.h"


int main(int argc, char* argv[])
{
  const int nBins = 110;
  const bool ctagCat = false;

  std::vector<double> centers;
  for(double center=105.25; center<160.; center+=0.5)
    {
      if(center>120. and center<130.) continue;
      centers.push_back(center);
    }

  std::vector<double> contents;
  if(ctagCat)
    contents = {643., 634., 653., 646., 611., 594., 588., 585., 576., 542., 552., 571., 569., 559., 553., 554., 542., 504., 465., 477., 485., 472., 485., 446., 444., 469., 431., 441., 436., 444., 327., 323., 305., 332., 329., 304., 313., 293., 294., 245., 275., 266., 248., 270., 267., 265., 256., 240., 255., 220., 251., 246., 263., 236., 225., 252., 234., 215., 215., 219., 253., 215., 241., 182., 199., 193., 231., 179., 203., 201., 181., 184., 188., 174., 172., 186., 204., 162., 176., 165., 171., 171., 194., 185., 146., 189., 163., 157., 154., 148};
  else
    contents = {6993., 6814., 6792., 6664., 6427., 6327., 6500., 6348., 6147., 5942., 5886., 5863., 5611., 5744., 5523., 5518., 5429., 5322., 5242., 5120., 5122., 5130., 5087., 5030., 4867., 4739., 4768., 4518., 4590., 4572., 3304., 3273., 3279., 3263., 3169., 3062., 3036., 3019., 2942., 2950., 2939., 2894., 2874., 2740., 2791., 2654., 2763., 2610., 2636., 2556., 2565., 2524., 2379., 2493., 2449., 2464., 2399., 2359., 2236., 2198., 2204., 2268., 2245., 2159., 2150., 2119., 2132., 2120., 2038., 2003., 1982., 2023., 1934., 1894., 1918., 1846., 1846., 1779., 1842., 1827., 1730., 1704., 1713., 1761., 1627., 1661., 1678., 1696., 1623., 1564};

  RooRealVar *a;
  RooRealVar *b;
  RooRealVar *c;

  if(ctagCat)
    {
      a = new RooRealVar("a", "", 1.e3, 0, 1.e5);
      b = new RooRealVar("b", "", 1.e-2, 0.e0, 1.e0);
      c = new RooRealVar("c", "", 0., -1.e2, 1.e2);
    }
  else
    {
      a = new RooRealVar("a", "", 9e4, 1e3, 1.e6);
      b = new RooRealVar("b", "", 2.5e-2, 0.e0, 1.e-1);
      c = new RooRealVar("c", "", 0., -1.e1, 1.e1);
    }    

  std::map<int, RooConstVar*> binCenters = {};
  std::map<int, RooRealVar*> binContents = {};
  std::map<int, RooFormulaVar*> binExp = {};
  std::map<int, RooPoisson*> poissons = {};
  RooArgList *allPdfs = new RooArgList("allPdfs");
  RooArgSet *allBinVals = new RooArgSet("allBinVals");
  for(int i=0; i<nBins-20; i++)
    {
      binCenters[i] = new RooConstVar((TString) ("binCenter"+std::to_string(i)), "", centers[i]);
      binContents[i] = new RooRealVar((TString) ("binContent"+std::to_string(i)), "", contents[i], 0., 10000.);
      allBinVals->add(*binContents[i]);
      binExp[i] = new RooFormulaVar((TString) ("exp"+std::to_string(i)), "", (TString) ("a*TMath::Exp(-b*"+std::to_string(centers[i])+")+c"), RooArgList(*a, *b, *c));
      poissons[i] = new RooPoisson((TString) ("poisson"+std::to_string(i)), "", *binContents[i], *binExp[i]);
      poissons[i]->setNoRounding(true);
      allPdfs->add(*poissons[i]);
    }

  RooProdPdf *fullModel = new RooProdPdf("fullModel", "", *allPdfs);
  
  RooDataSet *data = new RooDataSet("data", "", *allBinVals);
  data->add(*allBinVals);

  RooFitResult *fitRes = fullModel->fitTo(*data, RooFit::Strategy(1), RooFit::Minos(true), RooFit::Offset(true), RooFit::SumW2Error(false), RooFit::Save(true));
  // RooFitResult *fitRes = fullModel->fitTo(*data);
  fitRes->printValue(std::cout);
  std::cout<<std::endl;

  ///////////////
  // Plot
  
  TH1D *histBkd = new TH1D("histBkd", "", nBins, 105., 160.);
  TH1D *histData = new TH1D("histData", "", nBins, 105., 160.);

  bool crossBlind = false;

  for(int i=0; i<nBins-20; i++)
    {
      const double center = centers[i];
      if(center<125.)
        {
          histBkd->SetBinContent(i+1, binExp[i]->getVal());
          histData->SetBinContent(i+1, binContents[i]->getVal());
        }
      else
        {
          if(not crossBlind)
            {
              for(int ii=0; ii<20; ii++)
                {
                  histBkd->SetBinContent(i+ii+1, 0.);
                  histData->SetBinContent(i+ii+1, 0.);
                }
              crossBlind = true;
            }
          histBkd->SetBinContent(i+21, binExp[i]->getVal());
          histData->SetBinContent(i+21, binContents[i]->getVal());
        }
    }

  TCanvas *canvas = new TCanvas("canvas", "", 600, 600);
  canvas->SetTopMargin(0.05);
  canvas->SetBottomMargin(0.15);
  canvas->SetLeftMargin(0.15);
  canvas->SetRightMargin(0.05);
  histBkd->Draw();
  histBkd->SetStats(0);
  histBkd->SetTitle("");
  histBkd->SetLineWidth(2);
  histBkd->SetLineColor(kGreen-2);
  histBkd->SetLineStyle(2);
  if(ctagCat) histBkd->GetYaxis()->SetRangeUser(0., 750.);
  else histBkd->GetYaxis()->SetRangeUser(0., 7500.);
  histBkd->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  histBkd->GetYaxis()->SetTitle("Events / 0.5 GeV");
  histBkd->GetXaxis()->SetTitleOffset(1.5);
  histBkd->GetYaxis()->SetTitleOffset(2.0);
  histData->Draw("same");
  histData->SetLineColor(kRed);
  histData->SetLineWidth(2);
  histData->SetLineStyle(2);

  TLegend *leg = new TLegend(0.68, 0.79, 0.9, 0.94);
  leg->SetTextSize(0.035);
  leg->SetTextFont(42);
  leg->AddEntry(histBkd, "Background", "l");
  leg->AddEntry(histData, "Data", "l");
  leg->SetBorderSize(0);
  leg->Draw("same");

  TLatex *latex = new TLatex();
  latex->SetTextSize(0.035);
  latex->SetNDC();
  latex->DrawLatex(0.65,0.75,"#it{ATLAS} #bf{Internal}");
  latex->DrawLatex(0.65,0.67,"#bf{#sqrt{s}=13 TeV, 140 fb^{-1}}");

  canvas->SaveAs("Fit.png");

  delete histBkd;
  delete histData;
  delete canvas;
  delete leg;
  delete latex;

  return 0;
}
