#include "ExpFit.h"


int main(int argc, char* argv[])
{
  const int nBins = 110;
  const bool ctagCat = true;

  std::vector<double> centers;
  for(double center=105.25; center<160.; center+=0.5)
    {
      if(center>120. and center<130.) continue;
      centers.push_back(center);
    }

  std::vector<double> contents;
  if(ctagCat)
    contents = {643., 634., 653., 646., 611., 594., 588., 585., 576., 542., 552., 571., 569., 559., 553., 554., 542., 504., 465., 477., 485., 472., 485., 446., 444., 469., 431., 441., 436., 444., 327., 323., 305., 332., 329., 304., 313., 293., 294., 245., 275., 266., 248., 270., 267., 265., 256., 240., 255., 220., 251., 246., 263., 236., 225., 252., 234., 215., 215., 219., 253., 215., 241., 182., 199., 193., 231., 179., 203., 201., 181., 184., 188., 174., 172., 186., 204., 162., 176., 165., 171., 171., 194., 185., 146., 189., 163., 157., 154., 148};
  else
    contents = {6993., 6814., 6792., 6664., 6427., 6327., 6500., 6348., 6147., 5942., 5886., 5863., 5611., 5744., 5523., 5518., 5429., 5322., 5242., 5120., 5122., 5130., 5087., 5030., 4867., 4739., 4768., 4518., 4590., 4572., 3304., 3273., 3279., 3263., 3169., 3062., 3036., 3019., 2942., 2950., 2939., 2894., 2874., 2740., 2791., 2654., 2763., 2610., 2636., 2556., 2565., 2524., 2379., 2493., 2449., 2464., 2399., 2359., 2236., 2198., 2204., 2268., 2245., 2159., 2150., 2119., 2132., 2120., 2038., 2003., 1982., 2023., 1934., 1894., 1918., 1846., 1846., 1779., 1842., 1827., 1730., 1704., 1713., 1761., 1627., 1661., 1678., 1696., 1623., 1564};

  TGraphErrors *data = new TGraphErrors(90);
  for(int i=0; i<90; i++)
    {
      data->SetPoint(i, centers[i], contents[i]);
      data->SetPointError(i, 0, TMath::Sqrt(contents[i]));
    }

  // TF1 *exp = new TF1("exp", "[0]*exp(-1*[1]*x)+[2]", 105., 160.);
  // if(ctagCat)
  //   {
  //     exp->SetParameter(0, 1e5);
  //     exp->SetParLimits(0, 1e3, 1.e6);
  //     exp->SetParameter(1, 2.5e-2);
  //     exp->SetParLimits(1, 0.e0, 1.e-1);
  //     exp->SetParameter(2, 0.);
  //     exp->SetParLimits(2, -1.e3, 1.e3);
  //   }
  // else
  //   {
  //     exp->SetParameter(0, 1e5);
  //     exp->SetParLimits(0, 1e3, 1.e6);
  //     exp->SetParameter(1, 2.5e-2);
  //     exp->SetParLimits(1, 0.e0, 1.e-1);
  //     exp->SetParameter(2, 0.);
  //     exp->SetParLimits(2, -1.e3, 1.e3);
  //   }

  TF1 *exp = new TF1("exp", "[0]*exp(-1*[1]*x)", 105., 160.);
  if(ctagCat)
    {
      exp->SetParameter(0, 1e5);
      exp->SetParLimits(0, 1e3, 1.e6);
      exp->SetParameter(1, 2.5e-2);
      exp->SetParLimits(1, 0.e0, 1.e-1);
    }
  else
    {
      exp->SetParameter(0, 1e5);
      exp->SetParLimits(0, 1e3, 1.e6);
      exp->SetParameter(1, 2.5e-2);
      exp->SetParLimits(1, 0.e0, 1.e-1);
    }

  data->Fit(exp, "", "", 105., 160.);

  TCanvas *canvas = new TCanvas("canvas", "", 600, 600);
  data->Draw("");
  // data->SetLineWidth(0);
  exp->Draw("same");
  canvas->SaveAs("Fit.png");  

  return 0;
}
