#include <vector>
#include <cmath>
#include <iostream>
#include <string>

#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"

#include "RooFitResult.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooMultiVarGaussian.h"
#include "RooCBShape.h"
#include "RooPlot.h"
#include "RooMinuit.h"
#include "RooConstVar.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooNDKeysPdf.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooFormulaVar.h"
#include "RooProduct.h"
#include "RooAddition.h"
#include "RooProdPdf.h"
#include "RooPoisson.h"
#include "RooGenericPdf.h"
#include "RooExtendPdf.h"
#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooMinimizer.h"
#include "RooRandom.h"
#include "RooNLLVar.h"
#include "RooEffProd.h"
// #include "PiecewiseInterpolation.h" // Simple vertical interpolation of the PDFs
// #include "RooLinearMorph.h" // Vertical interpolation of CDFs
#include "RooMomentMorph.h" // transform models with linear transformation in observables that adjust 1st and 2nd moment
#include "RooMomentMorphND.h" // transform models with linear transformation in observables that adjust 1st and 2nd moment
// #include "RooStarMomentMorph.h" // Weird one made for Higgs analysis#include "RooStats/ModelConfig.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/AsymptoticCalculator.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include "RooProfileLL.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestResult.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/MCMCCalculator.h"
#include "RooStats/UniformProposal.h"
#include "RooStats/FeldmanCousins.h"
#include "RooStats/NumberCountingPdfFactory.h"
#include "RooStats/ConfInterval.h"
#include "RooStats/PointSetInterval.h"
#include "RooStats/LikelihoodInterval.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooStats/RooStatsUtils.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/MCMCInterval.h"
#include "RooStats/MCMCIntervalPlot.h"
#include "RooStats/ProposalFunction.h"
#include "RooStats/ProposalHelper.h"
#include "RooFitResult.h"
#include "TGraph2D.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodBDT.h"


/////////////////////////////////////////
// Normal forward declarations

int main(int argc, char* argv[]);

