import numpy as np
#import matplotlib.pyplot as plt
import ROOT
from array import array


#define functions you want to use
def expopoly2(x, a1, a2):
    return np.exp((x - 100) / 100 * (a1 + a2 * (x - 100) / 100))


def pow2(x, p1, p2, p3):
    return (x/100.)**p1 + p2*((x/100.)**p3)

#From the SS template fits
a1 = -2.9414e+00
a2 =  4.2918e-01

p1 = -2.8111e+00
p2 = -1.9420e-01
p3 = -1.1791e+00


#Yields From the SS templates
yields_nonctag = 385940
yields_ctag = 36899
root_file = ROOT.TFile("toys.root", "RECREATE")

for i in range(1000):

	num_events_nonctag = np.random.poisson(yields_nonctag)
	num_events_ctag = np.random.poisson(yields_ctag)

	x_vals_exp = np.random.uniform(105,160,num_events_ctag)
	y_vals_exp = expopoly2(x_vals_exp,a1,a2)

	x_vals_pow = np.random.uniform(105,160,num_events_nonctag)
	y_vals_pow = pow2(x_vals_pow, p1, p2, p3)


	sf_ctag = num_events_ctag / np.sum(y_vals_exp)
	sf_nonctag = num_events_nonctag / np.sum(y_vals_pow)

	#sf to match stats from the templates
	y_vals_exp = y_vals_exp * sf_ctag
	y_vals_pow = y_vals_pow * sf_nonctag


	hist_name = "ctag_%d" % i
	hist_title = "toy %d for m_yy ctag" % i

	hist_name1 = "nonctag_%d" % i
	hist_title1 = "toy %d for m_yy nonctag" % i

	ctag = ROOT.TH1F(hist_name, hist_title, int((160-105)/0.5), 105, 160)
	nonctag = ROOT.TH1F(hist_name1, hist_title1, int((160-105)/0.5), 105, 160)


	# Fill the ROOT histogram with x_values and y_values
	for x, y in zip(x_vals_exp, y_vals_exp):
		ctag.Fill(x, y)

	for bin in range(1, ctag.GetNbinsX() + 1):
		bin_error = np.sqrt(ctag.GetBinContent(bin))
		ctag.SetBinError(bin, bin_error)


	for x, y in zip(x_vals_pow, y_vals_pow):
		nonctag.Fill(x, y)

	for bin in range(1, nonctag.GetNbinsX() + 1):
		bin_error = np.sqrt(nonctag.GetBinContent(bin))
		nonctag.SetBinError(bin, bin_error)


	ctag.Write()
	nonctag.Write()

root_file.Close()

