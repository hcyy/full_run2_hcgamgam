import numpy as np
from scipy.stats import poisson
import ROOT

# Define your pow2 function
def pow2(x, p1, p2, p3):
    return (x/100.)**p1 + p2*((x/100.)**p3)

#define functions you want to use
def expopoly2(x, a1, a2):
    return np.exp((x - 100) / 100 * (a1 + a2 * (x - 100) / 100))

# Define your parameters
p1 = -2.8111e+00
p2 = -1.9420e-01
p3 = -1.1791e+00

a1 = -2.9414e+00
a2 =  4.2918e-01



# Number of toy datasets
n_datasets = 1000

# x range
x_min = 105
x_max = 160

#define your mu = expected number of events

yields_ctag = 36899
yields_nonctag = 385940

# Create a large array of x values within your desired range
x_values = np.linspace(x_min, x_max, 100000000)

# Calculate the weights for each x value according to pow2 function
weights_ctag = pow2(x_values, p1, p2, p3)
weights_nonctag = expopoly2(x_values, a1, a2)

# Normalize the weights so that they form a valid probability distribution
weights_ctag /= weights_ctag.sum()

weights_nonctag /= weights_nonctag.sum()

root_file = ROOT.TFile("toys.root", "RECREATE")

for i in range(n_datasets):
    # Number of events in this toy, drawn from a Poisson distribution
    n_events_ctag = poisson.rvs(yields_ctag)
    n_events_nonctag = poisson.rvs(yields_nonctag)

    # Draw n_events random x values according to the weights
    random_x_values_ctag = np.random.choice(x_values, size=n_events_ctag, p=weights_ctag)
    random_x_values_nonctag = np.random.choice(x_values, size=n_events_nonctag, p=weights_nonctag)

    hist_name = "ctag_%d" % i
    hist_title = "toy %d for m_yy ctag" % i

    hist_name1 = "nonctag_%d" % i
    hist_title1 = "toy %d for m_yy nonctag" % i

    ctag = ROOT.TH1F(hist_name, hist_title, 110, x_min, x_max)
    nonctag = ROOT.TH1F(hist_name1, hist_title1, 110, x_min, x_max)


    for x in random_x_values_ctag:
    	ctag.Fill(x)

    for x in random_x_values_nonctag:
    	nonctag.Fill(x)


    ctag.Write()
    nonctag.Write()


root_file.Close()
