void InjectionTest()
{
  TCanvas *canvas = new TCanvas();
  TGraphErrors *gr = new TGraphErrors(11);
  
  gr->SetPoint(0, 0.0, 0.00000e+00);
  gr->SetPoint(1, 1.0, 9.98594e-01);
  gr->SetPoint(2, 2.5, 2.49527e+00);
  gr->SetPoint(3, 5.0, 4.99785e+00);
  gr->SetPoint(4, 6.14118, 6.14396e+00);

  gr->SetPointError(0, 0, 9.71841e-01);
  gr->SetPointError(1, 0, 1.03122e+00);
  gr->SetPointError(2, 0, 1.12802e+00);
  gr->SetPointError(3, 0, 1.29542e+00);
  gr->SetPointError(4, 0, 1.46768e+00);

  gr->Draw("ACP");
  gr->SetTitle("");
  gr->SetMarkerStyle(8);
  gr->GetXaxis()->SetTitle("Injected Signal #sigma(H+c)#timesBR(H#rightarrow#gamma#gamma)/(#sigma_{SM}(H+c)#timesBR_{SM}(H#rightarrow#gamma#gamma))");
  gr->GetYaxis()->SetTitle("Fitted Signal #sigma(H+c)#timesBR(H#rightarrow#gamma#gamma)/(#sigma_{SM}(H+c)#timesBR_{SM}(H#rightarrow#gamma#gamma))");
  canvas->SetGrid();
  canvas->SaveAs("InjectionTest.pdf");
}
