#include "TCanvas.h"
#include "TF1.h"
#include "TH1F.h"

#include "../TransformTool/HistoTransform.h"

// int main(int argc, char* argv[]) {
void example()
{

  // create example histograms
  TH1* hBkg = new TH1F("hBkg", "hBkg", 1000, -10., 10.);
  TH1* hSig = new TH1F("hSig", "hSig", 1000, -10., 10.);
  hBkg->Sumw2();
  hSig->Sumw2();
  hBkg->FillRandom("pol0", 10000);
  hSig->FillRandom("gaus", 10000);
  hBkg->Scale(1. / 100);
  hSig->Scale(1. / 1000);

  // create transformation
  HistoTransform histoTrafo;
  histoTrafo.trafoFzSig = 0.3;
  histoTrafo.trafoFzBkg = 4;
  int         method    = 12; // 12 = "Transformation F"
  float       maxUnc    = 1;
  vector<int> bins      = histoTrafo.getRebinBins(hBkg, hSig, method, maxUnc);

  // draw original
  TCanvas* c1 = new TCanvas();
  hSig->SetLineColor(kRed);
  hBkg->DrawCopy("");
  hSig->DrawCopy("same");

  // transform any histogram
  histoTrafo.rebinHisto(hBkg, &bins);
  histoTrafo.rebinHisto(hSig, &bins);

  // draw transformed
  TCanvas* c2 = new TCanvas();
  hBkg->GetYaxis()->SetRangeUser(0, 40);
  hSig->Scale(10.); // scale for visibility
  hBkg->Draw("");
  hSig->Draw("same");
}
