Tool for applying binning algorithms ("transformations") to finely binned histograms.

A documention is given by Chapter 5.6 of https://cds.cern.ch/record/2232472

Example for running the transformation code:  
```bash
git clone ssh://git@gitlab.cern.ch:7999/CxAODFramework/TransformTool.git
cd TransformTool/
root util/run_example.C
```

