file(REMOVE_RECURSE
  "../FitCrossCheckForLimits_C.so"
  "../LikelihoodLandscape_C.so"
  "../macros/drawPlot_pulls_C.so"
  "../macros/runBreakdown_C.so"
  "../macros/runPulls_C.so"
  "../muHatModes_C.so"
  "../newGetMuHat_C.so"
  "../runAsymptoticsCLs_C.so"
  "../runSig_C.so"
  "CMakeFiles/FitTools"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/FitTools.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
