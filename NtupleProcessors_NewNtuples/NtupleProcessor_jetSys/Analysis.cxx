// Put for event loop back

#include "Analysis.h"

//////////////////////////////////////////////////////////////
// Configurable Options

//////////////////////////////////////////////////////////////
// Samples to Run Over

std::vector<int> ControlHist::samples_data = {};
std::vector<int> ControlHist::samples_signal = {Sample::cH};
// std::vector<int> ControlHist::samples_background = {Sample::ggFH, Sample::VBFH, Sample::WmH, Sample::WpH, Sample::ZH, Sample::ggZH, Sample::ttH, Sample::bbH, Sample::yy, Sample::tHjb, Sample::tWH};
std::vector<int> ControlHist::samples_background = {Sample::ggFH, Sample::VBFH, Sample::WmH, Sample::WpH, Sample::ZH, Sample::ggZH, Sample::ttH, Sample::bbH, Sample::yy, Sample::tWH};
std::vector<int> ControlHist::cuts = {Cut::All};
const bool doTest = false;
const int testEvents = 1000;
const bool doMC16a = true;
const bool doMC16d = true;
const bool doMC16e = true;
const bool doLeadJetCutflow = false;
const bool doTightCTag = false;
// const int cutflowIndex = 0;
const int cutflowIndexForNorm = 3;
const float BRHyy = 0.00227;
const bool normHists = false;

double passPtpassEta=0;
double passBothInd=0;
double passPtnotEta=0;
double notPtpassEta=0;
double notPtnotEta=0;

std::map<int, double> sumW;
std::map<int, double> sumW2;
std::map<int, double> nEv;

bool doCTagCat = true;
bool doSignal = true;
const int nBins = 20;

int sys = Systematics::Nominal;

int main(int argc, char* argv[])
{
  if(argc!=4)
    {
      std::cout<<"argc = "<<argc<<std::endl;
      return 1;
    }

  doCTagCat = (bool) atoi(argv[1]);
  doSignal = (bool) atoi(argv[2]);
  sys = atoi(argv[3]);

  std::cout<<"doCTagCat = "<<doCTagCat<<std::endl;
  std::cout<<"doSignal = "<<doSignal<<std::endl;
  std::cout<<"sys = "<<sysStrings[sys]<<std::endl;

  SetupSamples();
  SetupControlHists();

  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.aMCnloPy8_tWH125.MxAODJetSimpleJERSys.e7425_s3126_r9364_p4180_h027_hc.root"}, Sample::tWH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.MGPy8_cH_yc_Hgammagamma.MxAODJetSimpleJERSys.e8292_s3126_r9364_p5482_h027_hc.root"}, Sample::cH, true, PileupProfile::MC16a);
  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r9364_p4180_h027_hc.root/mc16a.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r9364_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r9364_p4180_h027_hc.root/mc16a.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r9364_p4180_h027_hc.002.root"}, Sample::tHjb, true, PileupProfile::MC16a);
  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r9364_p4380_h027_hc.root/mc16a.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r9364_p4380_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r9364_p4380_h027_hc.root/mc16a.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r9364_p4380_h027_hc.002.root"}, Sample::cHOLD, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r9364_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r9364_p4180_h027_hc.002.root"}, Sample::bbH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r9364_p4180_h027_hc.005.root"}, Sample::VBFH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r9364_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r9364_p4207_h027_hc.002.root"}, Sample::ggZH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.005.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.006.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.007.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.008.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.009.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r9364_p4180_h027_hc.010.root"}, Sample::ggFH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r9364_p4180_h027_hc.004.root"}, Sample::ttH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.003.root"}, Sample::WmH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r9364_p4207_h027_hc.003.root"}, Sample::WpH, true, PileupProfile::MC16a);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.005.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.006.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.007.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/JetSimpleJERSys/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.root/mc16a.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r9364_p4207_h027_hc.008.root"}, Sample::ZH, true, PileupProfile::MC16a);

  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.aMCnloPy8_tWH125.MxAODJetSimpleJERSys.e7425_s3126_r10201_p4180_h027_hc.root"}, Sample::tWH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.MGPy8_cH_yc_Hgammagamma.MxAODJetSimpleJERSys.e8292_s3126_r10201_p5482_h027_hc.root"}, Sample::cH, true, PileupProfile::MC16d);
  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10201_p4180_h027_hc.root/mc16d.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10201_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10201_p4180_h027_hc.root/mc16d.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10201_p4180_h027_hc.002.root"}, Sample::tHjb, true, PileupProfile::MC16d);
  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10201_p4380_h027_hc.root/mc16d.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10201_p4380_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10201_p4380_h027_hc.root/mc16d.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10201_p4380_h027_hc.002.root"}, Sample::cHOLD, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10201_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10201_p4180_h027_hc.002.root"}, Sample::bbH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10201_p4180_h027_hc.005.root"}, Sample::VBFH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10201_p4207_h027_hc.005.root"}, Sample::ggZH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.005.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.006.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.007.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.008.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10201_p4180_h027_hc.009.root"}, Sample::ggFH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.root/mc16d.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10201_p4180_h027_hc.005.root"}, Sample::ttH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.004.root"}, Sample::WmH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10201_p4207_h027_hc.005.root"}, Sample::WpH, true, PileupProfile::MC16d);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16d/JetSimpleJERSys/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.root/mc16d.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10201_p4207_h027_hc.005.root"}, Sample::ZH, true, PileupProfile::MC16d);

  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.aMCnloPy8_tWH125.MxAODJetSimpleJERSys.e7425_s3126_r10724_p4180_h027_hc.root"}, Sample::tWH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.MGPy8_cH_yc_Hgammagamma.MxAODJetSimpleJERSys.e8292_s3126_r10724_p5482_h027_hc.root"}, Sample::cH, true, PileupProfile::MC16e);
  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10724_p4180_h027_hc.root/mc16e.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10724_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10724_p4180_h027_hc.root/mc16e.aMCnloPy8_tHjb125_4fl_shw_fix.MxAODJetSimpleJERSys.e7305_s3126_r10724_p4180_h027_hc.002.root"}, Sample::tHjb, true, PileupProfile::MC16e);
  // Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.root/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.root/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.root/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.root/mc16e.MGPy8_cH_Hgammagamma.MxAODJetSimpleJERSys.e7180_s3126_r10724_p4380_h027_hc.004.root"}, Sample::cHOLD, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10724_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_bbH125.MxAODJetSimpleJERSys.e6050_s3126_r10724_p4180_h027_hc.002.root"}, Sample::bbH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8EG_NNPDF30_VBFH125.MxAODJetSimpleJERSys.e6970_s3126_r10724_p4180_h027_hc.005.root"}, Sample::VBFH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10724_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10724_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ggZH125.MxAODJetSimpleJERSys.e5762_s3126_r10724_p4207_h027_hc.003.root"}, Sample::ggZH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.005.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.006.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.007.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.008.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.009.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.010.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.011.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.012.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.013.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.014.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.015.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.016.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.017.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.018.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.019.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.020.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.021.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.022.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.023.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.024.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.025.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.026.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.027.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.028.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.029.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.030.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.031.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.032.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.033.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.034.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.035.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.036.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.037.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.038.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.039.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.040.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.041.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.042.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.043.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.044.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.045.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.046.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.047.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.048.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.049.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.050.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.051.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.052.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.053.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.054.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_NNLOPS_ggH125.MxAODJetSimpleJERSys.e5607_s3126_r10724_p4180_h027_hc.055.root"}, Sample::ggFH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.005.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.root/mc16e.PowhegPy8_ttH125_fixweight.MxAODJetSimpleJERSys.e7488_s3126_r10724_p4180_h027_hc.006.root"}, Sample::ttH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WmH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.005.root"}, Sample::WmH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_WpH125J.MxAODJetSimpleJERSys.e5734_s3126_r10724_p4207_h027_hc.004.root"}, Sample::WpH, true, PileupProfile::MC16e);
  Process({"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.001.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.002.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.003.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.004.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.005.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.006.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.007.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.008.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.009.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.010.root", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16e/JetSimpleJERSys/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.root/mc16e.PowhegPy8_ZH125J.MxAODJetSimpleJERSys.e5743_s3126_r10724_p4207_h027_hc.011.root"}, Sample::ZH, true, PileupProfile::MC16e);
   
  TH1D *myy = ControlHists[ControlHistName::myy]->GetTotalHist1D(Cut::All);

  TF1 fit = TF1("fit", "gaus(0)");
  myy->Fit(&fit, "", "");
  fit.Draw("SAME");

  SaveControlHists("Signals.root", {ControlHistName::DL1, ControlHistName::DL1r, ControlHistName::myy});

  PlotControlHists();
  PrintEfficiencies();

  const double total = passPtpassEta+passBothInd+passPtnotEta+notPtpassEta+notPtnotEta;
  std::cout<<"passPtpassEta="<<passPtpassEta/total*100.<<"%"<<std::endl;
  std::cout<<"passBothInd="<<passBothInd/total*100.<<"%"<<std::endl;
  std::cout<<"passPtnotEta="<<passPtnotEta/total*100.<<"%"<<std::endl;
  std::cout<<"notPtpassEta="<<notPtpassEta/total*100.<<"%"<<std::endl;
  std::cout<<"notPtnotEta="<<notPtnotEta/total*100.<<"%"<<std::endl;

  std::cout<<std::endl;

  std::cout<<"sumW all = "<<sumW[Sample::Total]<<" +- "<<TMath::Sqrt(sumW2[Sample::Total])<<std::endl;
  std::cout<<"nEv all = "<<nEv[Sample::Total]<<" +- "<<TMath::Sqrt(nEv[Sample::Total])<<std::endl;

  std::cout<<std::endl;

  for(int i=0; i<samples.size(); i++)
    {
      int sample = samples[i];
      TString sampleName = ControlHist::int2string_sample[sample];
      std::cout<<"sumW "<<sampleName<<" = "<<sumW[sample]<<" +- "<<TMath::Sqrt(sumW2[sample])<<std::endl;
      std::cout<<"nEv "<<sampleName<<" = "<<nEv[sample]<<" +- "<<TMath::Sqrt(nEv[sample])<<std::endl;
    }

  std::cout<<std::endl;

  std::cout<<"Total bin contents: ";
  for(int i=1; i<=nBins; i++)
    {
      double binContent=0.;
      for(int s=0; s<samples.size(); s++)
	binContent += ControlHists[ControlHistName::myy]->GetHist1D(samples[s], Cut::All)->GetBinContent(i);
      if(i==nBins) std::cout<<binContent<<std::endl;
      else std::cout<<binContent<<", ";
    }

  std::cout<<std::endl;

  for(int i=0; i<samples.size(); i++)
    {
      int sample = samples[i];
      TString sampleName = ControlHist::int2string_sample[sample];
      std::cout<<"Bin contents of "<<sampleName<<": ";
      for(int i=1; i<=nBins; i++)
	{
	  double binContent=ControlHists[ControlHistName::myy]->GetHist1D(sample, Cut::All)->GetBinContent(i);
	  if(i==nBins) std::cout<<binContent<<std::endl;
	  else std::cout<<binContent<<", ";
	}
    }

  return 0;
}


// Define the function which processes the events
void Process(const std::vector<TString> filePaths, const int sample, const bool isMC, const int pileupProfile)
{
  std::cout<<"Processing: ";
  for(int i=0; i<filePaths.size(); i++) std::cout<<filePaths[i];
  std::cout<<std::endl;

  float norm=1;
  if(isMC)
    {
      // if(pileupProfile==PileupProfile::MC16a) norm*=lumi_MC16a;
      // else if(pileupProfile==PileupProfile::MC16d) norm*=lumi_MC16d;
      // else if(pileupProfile==PileupProfile::MC16e) norm*=lumi_MC16e;
      if(pileupProfile==PileupProfile::MC16a) norm*=lumi_MC16a/GetSumWeights(filePaths, sample, cutflowIndexForNorm);
      else if(pileupProfile==PileupProfile::MC16d) norm*=lumi_MC16d/GetSumWeights(filePaths, sample, cutflowIndexForNorm);
      else if(pileupProfile==PileupProfile::MC16e) norm*=lumi_MC16e/GetSumWeights(filePaths, sample, cutflowIndexForNorm);
    }

  // const bool isSignal = std::any_of(ControlHist::samples_signal.cbegin(), ControlHist::samples_signal.cend(), [&](int ch_sample){return ch_sample==sample;});

  bool firstEvent = true;

  // std::cout<<"1"<<std::endl;

  for(int i_filePath = 0; i_filePath<filePaths.size(); i_filePath++)
    {
      TString filePath = filePaths[i_filePath];

      Reader *reader = InitReader(filePath);

      const int nEvents = reader->fChain->GetEntries();

      // std::cout<<"2"<<std::endl;

      for(int event=0; event<nEvents; event++)
	{ //Event loop
	  // std::cout<<event<<"/"<<nEvents<<" processed successfully in file "<<i_filePath+1<<"/"<<filePaths.size()<<std::endl;
	  if(event!=0 and event%10000==0) std::cout<<event<<"/"<<nEvents<<" processed successfully in file "<<i_filePath+1<<"/"<<filePaths.size()<<std::endl;

	  // if(filePath=="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/Nominal/mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126_r9364_p4180_h027.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126_r9364_p4180_h027.002.root" and event==124572) continue; // skipping seg fault event

	  if(doTest and event==testEvents)
	    {
	      delete reader;
	      return;
	    }

	  // std::cout<<"2.1"<<std::endl;

	  // std::cout<<nEvents<<std::endl;
	  // std::cout<<event<<std::endl;

	  reader->GetEntry(event);

	  // std::cout<<"2.2"<<std::endl;

	  if(isMC and firstEvent)
	    {
	      const float xsecBrFilterEff = reader->HGamEventInfoAuxDyn_crossSectionBRfilterEff;
	      norm *= xsecBrFilterEff;
	      float initialNEventsFile = xsecBrFilterEff;
	      // if(isSignal) initialNEventsFile *= BRHyy;
	      if(pileupProfile==PileupProfile::MC16a) initialNEventsFile *= lumi_MC16a;
	      else if(pileupProfile==PileupProfile::MC16d) initialNEventsFile *= lumi_MC16d;
	      else if(pileupProfile==PileupProfile::MC16e) initialNEventsFile *= lumi_MC16e;
	      initialNEvents[sample] += initialNEventsFile;
	      firstEvent = false;
	    }

	  // std::cout<<"2.2"<<std::endl;

	  float eventWeight=norm;
	  if(isMC)
	    {
	      if(sys==Systematics::Nominal)
		{
		  eventWeight*=reader->HGamEventInfoAuxDyn_weight;
		  eventWeight*=reader->HGamEventInfoAuxDyn_Hc_weightjvt;
		  eventWeight*=reader->HGamEventInfoAuxDyn_Hc_weightCtag;
		}
	      else if(sys==Systematics::JET_BJES_Response__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_BJES_Response__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1up;
		}
	      else if(sys==Systematics::JET_BJES_Response__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_BJES_Response__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_1__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_1__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_1__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_1__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_2__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_2__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_2__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_2__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_3__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_3__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_3__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_3__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_4__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_4__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_4__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_4__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_5__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_5__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_5__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_5__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_6__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_6__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_6__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_6__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_7__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_7__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_7__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_7__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1down;
		}
	      else if(sys==Systematics::JET_EffectiveNP_8restTerm__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1up;
		}
	      else if(sys==Systematics::JET_EffectiveNP_8restTerm__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1down;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_Modelling__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1up;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_Modelling__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1down;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_2018data__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1up;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_2018data__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1down;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_highE__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1up;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_highE__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1down;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_negEta__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1up;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_negEta__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1down;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_posEta__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1up;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_posEta__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1down;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_TotalStat__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1up;
		}
	      else if(sys==Systematics::JET_EtaIntercalibration_TotalStat__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1down;
		}
	      else if(sys==Systematics::JET_Flavor_Composition__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Flavor_Composition__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1up;
		}
	      else if(sys==Systematics::JET_Flavor_Composition__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Flavor_Composition__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1down;
		}
	      else if(sys==Systematics::JET_Flavor_Response__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Flavor_Response__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1up;
		}
	      else if(sys==Systematics::JET_Flavor_Response__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Flavor_Response__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1down;
		}
	      else if(sys==Systematics::JET_JER_DataVsMC_MC16__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1up;
		}
	      else if(sys==Systematics::JET_JER_DataVsMC_MC16__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_1__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_1__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_1__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_1__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_2__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_2__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_2__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_2__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_3__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_3__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_3__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_3__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_4__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_4__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_4__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_4__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_5__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_5__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_5__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_5__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_6__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_6__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_6__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_6__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1down;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_7restTerm__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1up;
		}
	      else if(sys==Systematics::JET_JER_EffectiveNP_7restTerm__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1down;
		}
	      else if(sys==Systematics::JET_JvtEfficiency__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JvtEfficiency__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1down;
		}
	      else if(sys==Systematics::JET_JvtEfficiency__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_JvtEfficiency__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1up;
		}
	      else if(sys==Systematics::JET_Pileup_OffsetMu__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_OffsetMu__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1up;
		}
	      else if(sys==Systematics::JET_Pileup_OffsetMu__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_OffsetMu__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1down;
		}
	      else if(sys==Systematics::JET_Pileup_OffsetNPV__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_OffsetNPV__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1up;
		}
	      else if(sys==Systematics::JET_Pileup_OffsetNPV__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_OffsetNPV__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1down;
		}
	      else if(sys==Systematics::JET_Pileup_PtTerm__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_PtTerm__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1up;
		}
	      else if(sys==Systematics::JET_Pileup_PtTerm__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_PtTerm__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1down;
		}
	      else if(sys==Systematics::JET_Pileup_RhoTopology__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_RhoTopology__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1up;
		}
	      else if(sys==Systematics::JET_Pileup_RhoTopology__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_Pileup_RhoTopology__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1down;
		}
	      else if(sys==Systematics::JET_PunchThrough_MC16__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_PunchThrough_MC16__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1up;
		}
	      else if(sys==Systematics::JET_PunchThrough_MC16__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_PunchThrough_MC16__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1down;
		}
	      else if(sys==Systematics::JET_SingleParticle_HighPt__1up)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_SingleParticle_HighPt__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1up;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1up;
		}
	      else if(sys==Systematics::JET_SingleParticle_HighPt__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_SingleParticle_HighPt__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1down;
		}
	      else if(sys==Systematics::JET_fJvtEfficiency__1down)
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_fJvtEfficiency__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1down;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1down;
		}
	      else if(sys==Systematics::JET_fJvtEfficiency__1up )
		{
		  eventWeight*=reader->HGamEventInfo_weight__JET_fJvtEfficiency__1up ;
		  eventWeight*=reader->HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1up ;
		  eventWeight*=reader->HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1up ;
		}
	      

	      if(doTest and nEvents>testEvents) eventWeight*=nEvents/static_cast<double>(testEvents);
	    }

	  // std::cout<<"3"<<std::endl;

	  if(sys==Systematics::Nominal)
	    {
	      if(reader->HGamEventInfoAuxDyn_isPassed!=1) continue;
	      if(doCTagCat and reader->HGamEventInfoAuxDyn_Hc_Atleast1jisloose!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfoAuxDyn_Hc_Atleast1jisloose!=0) continue;
	    }
	  else if(sys==Systematics::JET_BJES_Response__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_BJES_Response__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_BJES_Response__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_BJES_Response__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_1__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_1__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_1__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_1__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_2__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_2__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_2__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_2__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_3__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_3__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_3__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_3__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_4__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_4__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_4__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_4__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_5__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_5__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_5__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_5__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_6__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_6__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_6__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_6__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_7__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_7__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_7__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_7__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_8restTerm__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EffectiveNP_8restTerm__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_Modelling__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_Modelling__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_2018data__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_2018data__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_highE__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_highE__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_negEta__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_negEta__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_posEta__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_posEta__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_TotalStat__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_EtaIntercalibration_TotalStat__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_Flavor_Composition__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Flavor_Composition__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Flavor_Composition__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Flavor_Composition__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_Flavor_Response__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Flavor_Response__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Flavor_Response__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Flavor_Response__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_DataVsMC_MC16__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_DataVsMC_MC16__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_1__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_1__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_2__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_2__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_3__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_3__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_4__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_4__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_5__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_5__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_6__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_6__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_7restTerm__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_JER_EffectiveNP_7restTerm__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JvtEfficiency__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JvtEfficiency__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_JvtEfficiency__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_JvtEfficiency__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_OffsetMu__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_OffsetMu__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_OffsetNPV__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_OffsetNPV__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_PtTerm__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_PtTerm__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_PtTerm__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_PtTerm__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_RhoTopology__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_Pileup_RhoTopology__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_PunchThrough_MC16__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_PunchThrough_MC16__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_PunchThrough_MC16__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_PunchThrough_MC16__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_SingleParticle_HighPt__1up)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1up!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1up!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1up!=0) continue;
	    }
	  else if(sys==Systematics::JET_SingleParticle_HighPt__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_fJvtEfficiency__1down)
	    {
	      if(reader->HGamEventInfo_isPassed__JET_fJvtEfficiency__1down!=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1down!=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1down!=0) continue;
	    }
	  else if(sys==Systematics::JET_fJvtEfficiency__1up )
	    {
	      if(reader->HGamEventInfo_isPassed__JET_fJvtEfficiency__1up !=1) continue;
	      if(doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1up !=1) continue;
	      if(not doCTagCat and reader->HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1up !=0) continue;
	    }

	  if(doSignal and reader->HGamTruthEventInfoAuxDyn_N_j_cjet_had<1) continue;
	  if(not doSignal and reader->HGamTruthEventInfoAuxDyn_N_j_cjet_had!=0) continue;

	  sumW[Sample::Total] += eventWeight;
	  sumW2[Sample::Total] += eventWeight*eventWeight;
	  nEv[Sample::Total] ++;

	  sumW[sample] += eventWeight;
	  sumW2[sample] += eventWeight*eventWeight;
	  nEv[sample] ++;

	  finalNEvents[std::make_pair(Cut::All, sample)]+=eventWeight;
	  finalNEventsSq[std::make_pair(Cut::All, sample)]+=eventWeight*eventWeight;

	  std::vector<int> passedCuts;
	  passedCuts.push_back(Cut::All);
	  if(sys==Systematics::Nominal) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfoAuxDyn_m_yy/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_BJES_Response__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_BJES_Response__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_BJES_Response__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_BJES_Response__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_1__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_1__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_1__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_1__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_2__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_2__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_2__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_2__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_3__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_3__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_3__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_3__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_4__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_4__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_4__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_4__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_5__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_5__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_5__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_5__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_6__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_6__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_6__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_6__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_7__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_7__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_7__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_7__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_8restTerm__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EffectiveNP_8restTerm__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_Modelling__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_Modelling__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_2018data__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_2018data__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_highE__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_highE__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_negEta__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_negEta__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_posEta__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_NonClosure_posEta__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_TotalStat__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_EtaIntercalibration_TotalStat__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Flavor_Composition__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Flavor_Composition__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Flavor_Composition__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Flavor_Composition__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Flavor_Response__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Flavor_Response__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Flavor_Response__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Flavor_Response__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_DataVsMC_MC16__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_DataVsMC_MC16__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_1__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_1__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_2__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_2__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_3__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_3__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_4__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_4__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_5__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_5__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_6__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_6__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_7restTerm__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JER_EffectiveNP_7restTerm__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JvtEfficiency__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JvtEfficiency__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_JvtEfficiency__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_JvtEfficiency__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_OffsetMu__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_OffsetMu__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_OffsetNPV__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_OffsetNPV__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_PtTerm__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_PtTerm__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_PtTerm__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_PtTerm__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_RhoTopology__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_Pileup_RhoTopology__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_PunchThrough_MC16__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_PunchThrough_MC16__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_PunchThrough_MC16__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_PunchThrough_MC16__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_SingleParticle_HighPt__1up) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1up/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_SingleParticle_HighPt__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_fJvtEfficiency__1down) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_fJvtEfficiency__1down/1000., eventWeight, passedCuts);
	  else if(sys==Systematics::JET_fJvtEfficiency__1up ) ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfo_m_yy__JET_fJvtEfficiency__1up /1000., eventWeight, passedCuts);

	  // std::cout<<"4"<<std::endl;
	}

      delete reader;
    }
}


Reader *InitReader(const TString filePath)
{
  TFile *file = TFile::Open(filePath);
  TTree *tree = (TTree*) file->Get(treeName);
  Reader *reader = new Reader(tree);
  return reader;
}


void MakeEfficiencyPlots() // Only makes plots where cut is optimised on same sample
{
  std::cout<<"Making Efficiency Plots"<<std::endl;

  system("mkdir Plots");
  system("mkdir Plots/Efficiencies");

  for(std::vector<int>::iterator cut=ControlHist::cuts.begin(); cut!=ControlHist::cuts.end(); ++cut)
    {
      TCanvas *c = new TCanvas();
      TGraphErrors *plot = new TGraphErrors(ControlHist::samples_signal.size());
      int count=0;

      for(std::vector<int>::iterator sample = ControlHist::samples_signal.begin(); sample!=ControlHist::samples_signal.end(); ++sample)
	{
	  plot->SetPoint(count, ControlHist::int2float_sample[*sample], finalNEvents[std::make_pair(*cut, *sample)]/initialNEvents[*sample]);
	  plot->SetPointError(count, 0, TMath::Sqrt(finalNEventsSq[std::make_pair(*cut, *sample)])/initialNEvents[*sample]);
	  count++;
	}

      plot->SetTitle("");
      plot->GetXaxis()->SetTitle("m_{X}");
      plot->GetYaxis()->SetTitle("Efficiency");
      plot->Draw();
      c->SaveAs("Plots/Efficiencies/Efficiencies_"+ControlHist::int2string_cut[*cut]+".png");

      delete plot;
      delete c;
    }
}


void PrintEfficiencies()
{
  std::cout<<std::endl<<"Printing Efficiencies:"<<std::endl<<std::endl;

  for(std::vector<int>::iterator sample = samples.begin(); sample != samples.end(); ++sample)
    {
      std::cout<<ControlHist::int2string_sample[*sample]<<": Initial events = "<<initialNEvents[*sample]<<std::endl;

      for(std::vector<int>::iterator cut = ControlHist::cuts.begin(); cut != ControlHist::cuts.end(); ++cut)
	{
	  std::cout<<ControlHist::int2string_cut[*cut]<<": Final events = "<<finalNEvents[std::make_pair(*cut, *sample)]<<", efficiency = "<<finalNEvents[std::make_pair(*cut, *sample)]/initialNEvents[*sample]<<"+-"<<TMath::Sqrt(finalNEventsSq[std::make_pair(*cut, *sample)])/initialNEvents[*sample]<<", and correct X efficiency = "<<correctXEvents[std::make_pair(*cut, *sample)]/finalNEvents[std::make_pair(*cut, *sample)]<<"+-"<<TMath::Sqrt(correctXEventsSq[std::make_pair(*cut, *sample)])/finalNEvents[std::make_pair(*cut, *sample)]<<std::endl;
	}

      std::cout<<std::endl;
    }
}


void SetupSamples()
{
  samples=ControlHist::samples_data;
  samples.insert(samples.end(), ControlHist::samples_signal.begin(), ControlHist::samples_signal.end());
  samples.insert(samples.end(), ControlHist::samples_background.begin(), ControlHist::samples_background.end());
}


float GetSumWeights(std::vector<TString> filePaths, const int sample, const int index)
{
  float sumWeights=0;
  for(int i_filePath = 0; i_filePath<filePaths.size(); i_filePath++)
    {
      TString filePath = filePaths[i_filePath];
      TFile *file = TFile::Open(filePath);
      TH1F *info = dynamic_cast<TH1F*>(file->Get(int2histName[sample]));
      sumWeights+=info->GetBinContent(index);
      delete info;
      file->Close();
    }
  return sumWeights;
}


void SetupControlHists()
{
  std::cout<<"Setting Up ControlHists"<<std::endl;
  ControlHists[ControlHistName::myy] = new ControlHist("myy", "; m_{#gamma#gamma} [GeV]; Events / GeV", nBins, 120., 130.);
  // ControlHists[ControlHistName::myy] = new ControlHist("myy", "; m_{#gamma#gamma} [GeV]; Events / GeV", nBins, 105., 160.);
  ControlHists[ControlHistName::DL1] = new ControlHist("DL1", "; DL1 discriminant; frac; Events", 1000, -5, 5, 1000, 0, 1);
  ControlHists[ControlHistName::DL1r] = new ControlHist("DL1r", "; DL1r discriminant; frac; Events", 1000, -5, 5, 1000, 0, 1);
}


void PlotControlHists()
{
  std::cout<<"Plotting ControlHists"<<std::endl;
  plotter = new Plotter(plotter_fileType, plotter_seperateSignal, plotter_seperateUncertainties, plotter_coloredSignal);
  for(std::vector<int>::iterator cut = ControlHist::cuts.begin(); cut != ControlHist::cuts.end(); ++cut)
    {
      for(std::map<int, ControlHist*>::iterator controlHist = ControlHists.begin(); controlHist != ControlHists.end(); ++controlHist)
	{
	  TString plotterOptions = "AWIP labels ";
	  if(normHists) plotterOptions+="normall ";
	  plotter->Plot(controlHist->second, *cut, plotterOptions, "gaus(0)");
	}
    }
}

void SaveControlHists(const TString name, std::vector<int> hists)
{
  system("mkdir "+outputHistPath);
  TFile *outFile = new TFile(outputHistPath+"/"+name, "recreate");
  outFile->cd();
  for(int i=0; i<hists.size(); i++) ControlHists[hists.at(i)]->WriteAll();
  outFile->Close();
  delete outFile;
}
