//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan  5 22:40:20 2023 by ROOT version 6.24/08
// from TTree CollectionTree/xAOD event tree
// found on file: /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/Nominal/mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126_r9364_p4180_h027.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126_r9364_p4180_h027.001.root
//////////////////////////////////////////////////////////

#ifndef Reader_h
#define Reader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class Reader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   Float_t         HGamEventInfoAuxDyn_crossSectionBRfilterEff;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_cjet_had;

   Float_t         HGamEventInfoAuxDyn_weight;
   Float_t         HGamEventInfoAuxDyn_Hc_weightjvt;
   Float_t         HGamEventInfoAuxDyn_Hc_weightCtag;
   Char_t          HGamEventInfoAuxDyn_isPassed;
   Int_t           HGamEventInfoAuxDyn_Hc_Atleast1jisloose;
   Float_t         HGamEventInfoAuxDyn_m_yy;

   Float_t         HGamEventInfo_weight__JET_BJES_Response__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1up;
   Char_t          HGamEventInfo_isPassed__JET_BJES_Response__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1up;
   Float_t         HGamEventInfo_m_yy__JET_BJES_Response__1up;

   Float_t         HGamEventInfo_weight__JET_BJES_Response__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1down;
   Char_t          HGamEventInfo_isPassed__JET_BJES_Response__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1down;
   Float_t         HGamEventInfo_m_yy__JET_BJES_Response__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_1__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_1__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_1__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_1__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_1__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_1__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_2__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_2__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_2__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_2__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_2__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_2__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_3__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_3__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_3__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_3__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_3__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_3__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_4__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_4__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_4__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_4__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_4__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_4__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_5__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_5__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_5__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_5__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_5__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_5__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_6__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_6__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_6__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_6__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_6__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_6__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_7__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_7__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_7__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_7__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_7__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_7__1down;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1up;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1up;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1up;

   Float_t         HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1down;
   Char_t          HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1down;
   Float_t         HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1down;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1up;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1up;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1up;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1down;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1down;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1down;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1up;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1up;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1up;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1down;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1down;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1down;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1up;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1up;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1up;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1down;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1down;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1down;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1up;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1up;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1up;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1down;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1down;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1down;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1up;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1up;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1up;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1down;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1down;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1down;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1up;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1up;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1up;

   Float_t         HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1down;
   Char_t          HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1down;
   Float_t         HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1down;

   Float_t         HGamEventInfo_weight__JET_Flavor_Composition__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1up;
   Char_t          HGamEventInfo_isPassed__JET_Flavor_Composition__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1up;
   Float_t         HGamEventInfo_m_yy__JET_Flavor_Composition__1up;

   Float_t         HGamEventInfo_weight__JET_Flavor_Composition__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1down;
   Char_t          HGamEventInfo_isPassed__JET_Flavor_Composition__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1down;
   Float_t         HGamEventInfo_m_yy__JET_Flavor_Composition__1down;

   Float_t         HGamEventInfo_weight__JET_Flavor_Response__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1up;
   Char_t          HGamEventInfo_isPassed__JET_Flavor_Response__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1up;
   Float_t         HGamEventInfo_m_yy__JET_Flavor_Response__1up;

   Float_t         HGamEventInfo_weight__JET_Flavor_Response__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1down;
   Char_t          HGamEventInfo_isPassed__JET_Flavor_Response__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1down;
   Float_t         HGamEventInfo_m_yy__JET_Flavor_Response__1down;

   Float_t         HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1up;

   Float_t         HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_1__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_1__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_2__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_2__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_3__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_3__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_4__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_4__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_5__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_5__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_6__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_6__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1down;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1up;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1up;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1up;

   Float_t         HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1down;
   Char_t          HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1down;
   Float_t         HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1down;

   Float_t         HGamEventInfo_weight__JET_JvtEfficiency__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1down;
   Char_t          HGamEventInfo_isPassed__JET_JvtEfficiency__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1down;
   Float_t         HGamEventInfo_m_yy__JET_JvtEfficiency__1down;

   Float_t         HGamEventInfo_weight__JET_JvtEfficiency__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1up;
   Char_t          HGamEventInfo_isPassed__JET_JvtEfficiency__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1up;
   Float_t         HGamEventInfo_m_yy__JET_JvtEfficiency__1up;

   Float_t         HGamEventInfo_weight__JET_Pileup_OffsetMu__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1up;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1up;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1up;

   Float_t         HGamEventInfo_weight__JET_Pileup_OffsetMu__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1down;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1down;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1down;

   Float_t         HGamEventInfo_weight__JET_Pileup_OffsetNPV__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1up;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1up;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1up;

   Float_t         HGamEventInfo_weight__JET_Pileup_OffsetNPV__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1down;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1down;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1down;

   Float_t         HGamEventInfo_weight__JET_Pileup_PtTerm__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1up;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_PtTerm__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1up;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_PtTerm__1up;

   Float_t         HGamEventInfo_weight__JET_Pileup_PtTerm__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1down;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_PtTerm__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1down;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_PtTerm__1down;

   Float_t         HGamEventInfo_weight__JET_Pileup_RhoTopology__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1up;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1up;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1up;

   Float_t         HGamEventInfo_weight__JET_Pileup_RhoTopology__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1down;
   Char_t          HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1down;
   Float_t         HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1down;

   Float_t         HGamEventInfo_weight__JET_PunchThrough_MC16__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1up;
   Char_t          HGamEventInfo_isPassed__JET_PunchThrough_MC16__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1up;
   Float_t         HGamEventInfo_m_yy__JET_PunchThrough_MC16__1up;

   Float_t         HGamEventInfo_weight__JET_PunchThrough_MC16__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1down;
   Char_t          HGamEventInfo_isPassed__JET_PunchThrough_MC16__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1down;
   Float_t         HGamEventInfo_m_yy__JET_PunchThrough_MC16__1down;

   Float_t         HGamEventInfo_weight__JET_SingleParticle_HighPt__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1up;
   Char_t          HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1up;
   Float_t         HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1up;

   Float_t         HGamEventInfo_weight__JET_SingleParticle_HighPt__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1down;
   Char_t          HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1down;
   Float_t         HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1down;

   Float_t         HGamEventInfo_weight__JET_fJvtEfficiency__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1down;
   Char_t          HGamEventInfo_isPassed__JET_fJvtEfficiency__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1down;
   Float_t         HGamEventInfo_m_yy__JET_fJvtEfficiency__1down;

   Float_t         HGamEventInfo_weight__JET_fJvtEfficiency__1up ;
   Float_t         HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1up ;
   Float_t         HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1up ;
   Char_t          HGamEventInfo_isPassed__JET_fJvtEfficiency__1up ;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1up ;
   Float_t         HGamEventInfo_m_yy__JET_fJvtEfficiency__1up ;


   // List of branches
   TBranch        *b_HGamEventInfoAuxDyn_crossSectionBRfilterEff;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_cjet_had;   //!

   TBranch        *b_HGamEventInfoAuxDyn_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_weightjvt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_weightCtag;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_Atleast1jisloose;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy;   //!

   TBranch        *b_HGamEventInfo_weight__JET_BJES_Response__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_BJES_Response__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_BJES_Response__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_BJES_Response__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_BJES_Response__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_BJES_Response__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_1__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_1__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_2__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_2__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_3__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_3__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_4__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_4__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_5__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_5__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_6__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_6__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_7__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_7__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_7__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_7__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_7__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_7__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Flavor_Composition__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Flavor_Composition__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Flavor_Composition__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Flavor_Composition__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Flavor_Composition__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Flavor_Composition__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Flavor_Response__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Flavor_Response__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Flavor_Response__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Flavor_Response__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Flavor_Response__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Flavor_Response__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JvtEfficiency__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_JvtEfficiency__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_JvtEfficiency__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_JvtEfficiency__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_PtTerm__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_PtTerm__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_PunchThrough_MC16__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_PunchThrough_MC16__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_SingleParticle_HighPt__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1up;   //!

   TBranch        *b_HGamEventInfo_weight__JET_SingleParticle_HighPt__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_fJvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_fJvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_fJvtEfficiency__1down;   //!

   TBranch        *b_HGamEventInfo_weight__JET_fJvtEfficiency__1up ;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1up ;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1up ;   //!
   TBranch        *b_HGamEventInfo_isPassed__JET_fJvtEfficiency__1up ;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1up ;   //!
   TBranch        *b_HGamEventInfo_m_yy__JET_fJvtEfficiency__1up ;   //!

   Reader(TTree *tree=0);
   virtual ~Reader();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Reader_cxx
Reader::Reader(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("");
      if (!f || !f->IsOpen()) {
         f = new TFile("");
      }
      f->GetObject("CollectionTree",tree);

   }
   Init(tree);
}

Reader::~Reader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Reader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Reader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Reader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   /* fChain->SetMakeClass(1); */

   fChain->SetBranchAddress("HGamEventInfoAuxDyn.crossSectionBRfilterEff", &HGamEventInfoAuxDyn_crossSectionBRfilterEff, &b_HGamEventInfoAuxDyn_crossSectionBRfilterEff);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_cjet_had", &HGamTruthEventInfoAuxDyn_N_j_cjet_had, &b_HGamTruthEventInfoAuxDyn_N_j_cjet_had);

   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weight", &HGamEventInfoAuxDyn_weight, &b_HGamEventInfoAuxDyn_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_weightjvt", &HGamEventInfoAuxDyn_Hc_weightjvt, &b_HGamEventInfoAuxDyn_Hc_weightjvt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_weightCtag", &HGamEventInfoAuxDyn_Hc_weightCtag, &b_HGamEventInfoAuxDyn_Hc_weightCtag);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassed", &HGamEventInfoAuxDyn_isPassed, &b_HGamEventInfoAuxDyn_isPassed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_Atleast1jisloose", &HGamEventInfoAuxDyn_Hc_Atleast1jisloose, &b_HGamEventInfoAuxDyn_Hc_Atleast1jisloose);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy", &HGamEventInfoAuxDyn_m_yy, &b_HGamEventInfoAuxDyn_m_yy);

   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1upAuxDyn.weight", &HGamEventInfo_weight__JET_BJES_Response__1up, &b_HGamEventInfo_weight__JET_BJES_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1up, &b_HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1up, &b_HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_BJES_Response__1up, &b_HGamEventInfo_isPassed__JET_BJES_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_BJES_Response__1up, &b_HGamEventInfo_m_yy__JET_BJES_Response__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1downAuxDyn.weight", &HGamEventInfo_weight__JET_BJES_Response__1down, &b_HGamEventInfo_weight__JET_BJES_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1down, &b_HGamEventInfo_Hc_weightjvt__JET_BJES_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1down, &b_HGamEventInfo_Hc_weightCtag__JET_BJES_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_BJES_Response__1down, &b_HGamEventInfo_isPassed__JET_BJES_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_BJES_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_BJES_Response__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_BJES_Response__1down, &b_HGamEventInfo_m_yy__JET_BJES_Response__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_1__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_1__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_1__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_1__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_1__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_1__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_1__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_1__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_2__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_2__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_2__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_2__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_2__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_2__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_2__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_2__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_3__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_3__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_3__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_3__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_3__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_3__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_3__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_3__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_4__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_4__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_4__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_4__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_4__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_4__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_4__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_4__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_5__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_5__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_5__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_5__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_5__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_5__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_5__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_5__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_6__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_6__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_6__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_6__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_6__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_6__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_6__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_6__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_7__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_7__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_7__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_7__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_7__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_7__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_7__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_7__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1up, &b_HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1up, &b_HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1up, &b_HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1down, &b_HGamEventInfo_weight__JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1down, &b_HGamEventInfo_isPassed__JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1down, &b_HGamEventInfo_m_yy__JET_EffectiveNP_8restTerm__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1up, &b_HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1up, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1up, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1down, &b_HGamEventInfo_weight__JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1down, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1down, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_Modelling__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1up, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1up, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1up, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1down, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_2018data__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_2018data__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_2018data__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1down, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_2018data__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_2018data__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1down, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_2018data__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1up, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1up, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1up, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1down, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1down, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1down, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_highE__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1up, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1up, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1up, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1down, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1down, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1down, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_negEta__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1up, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1up, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1up, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1down, &b_HGamEventInfo_weight__JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1down, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1down, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_NonClosure_posEta__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1up, &b_HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1up, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1up, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1up, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1up, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn.weight", &HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1down, &b_HGamEventInfo_weight__JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1down, &b_HGamEventInfo_Hc_weightjvt__JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1down, &b_HGamEventInfo_Hc_weightCtag__JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1down, &b_HGamEventInfo_isPassed__JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1down, &b_HGamEventInfo_m_yy__JET_EtaIntercalibration_TotalStat__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1upAuxDyn.weight", &HGamEventInfo_weight__JET_Flavor_Composition__1up, &b_HGamEventInfo_weight__JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1up, &b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1up, &b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Flavor_Composition__1up, &b_HGamEventInfo_isPassed__JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Flavor_Composition__1up, &b_HGamEventInfo_m_yy__JET_Flavor_Composition__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1downAuxDyn.weight", &HGamEventInfo_weight__JET_Flavor_Composition__1down, &b_HGamEventInfo_weight__JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1down, &b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1down, &b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Flavor_Composition__1down, &b_HGamEventInfo_isPassed__JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Composition__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Flavor_Composition__1down, &b_HGamEventInfo_m_yy__JET_Flavor_Composition__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1upAuxDyn.weight", &HGamEventInfo_weight__JET_Flavor_Response__1up, &b_HGamEventInfo_weight__JET_Flavor_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1up, &b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1up, &b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Flavor_Response__1up, &b_HGamEventInfo_isPassed__JET_Flavor_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Flavor_Response__1up, &b_HGamEventInfo_m_yy__JET_Flavor_Response__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1downAuxDyn.weight", &HGamEventInfo_weight__JET_Flavor_Response__1down, &b_HGamEventInfo_weight__JET_Flavor_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1down, &b_HGamEventInfo_Hc_weightjvt__JET_Flavor_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1down, &b_HGamEventInfo_Hc_weightCtag__JET_Flavor_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Flavor_Response__1down, &b_HGamEventInfo_isPassed__JET_Flavor_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Flavor_Response__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Flavor_Response__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Flavor_Response__1down, &b_HGamEventInfo_m_yy__JET_Flavor_Response__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1up, &b_HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1up, &b_HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1up, &b_HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1down, &b_HGamEventInfo_weight__JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1down, &b_HGamEventInfo_isPassed__JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_DataVsMC_MC16__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1down, &b_HGamEventInfo_m_yy__JET_JER_DataVsMC_MC16__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_1__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_1__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_1__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_2__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_2__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_2__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_3__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_3__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_3__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_4__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_4__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_4__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_5__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_5__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_5__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_6__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_6__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_6__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1up, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1up, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1up, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1down, &b_HGamEventInfo_weight__JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1down, &b_HGamEventInfo_isPassed__JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1down, &b_HGamEventInfo_m_yy__JET_JER_EffectiveNP_7restTerm__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1downAuxDyn.weight", &HGamEventInfo_weight__JET_JvtEfficiency__1down, &b_HGamEventInfo_weight__JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1down, &b_HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1down, &b_HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JvtEfficiency__1down, &b_HGamEventInfo_isPassed__JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JvtEfficiency__1down, &b_HGamEventInfo_m_yy__JET_JvtEfficiency__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1upAuxDyn.weight", &HGamEventInfo_weight__JET_JvtEfficiency__1up, &b_HGamEventInfo_weight__JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1up, &b_HGamEventInfo_Hc_weightjvt__JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1up, &b_HGamEventInfo_Hc_weightCtag__JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_JvtEfficiency__1up, &b_HGamEventInfo_isPassed__JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_JvtEfficiency__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_JvtEfficiency__1up, &b_HGamEventInfo_m_yy__JET_JvtEfficiency__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_OffsetMu__1up, &b_HGamEventInfo_weight__JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1up, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1up, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1up, &b_HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1up, &b_HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_OffsetMu__1down, &b_HGamEventInfo_weight__JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1down, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1down, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1down, &b_HGamEventInfo_isPassed__JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1down, &b_HGamEventInfo_m_yy__JET_Pileup_OffsetMu__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_OffsetNPV__1up, &b_HGamEventInfo_weight__JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1up, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1up, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1up, &b_HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1up, &b_HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_OffsetNPV__1down, &b_HGamEventInfo_weight__JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1down, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1down, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1down, &b_HGamEventInfo_isPassed__JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1down, &b_HGamEventInfo_m_yy__JET_Pileup_OffsetNPV__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_PtTerm__1up, &b_HGamEventInfo_weight__JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1up, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1up, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_PtTerm__1up, &b_HGamEventInfo_isPassed__JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_PtTerm__1up, &b_HGamEventInfo_m_yy__JET_Pileup_PtTerm__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_PtTerm__1down, &b_HGamEventInfo_weight__JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1down, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1down, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_PtTerm__1down, &b_HGamEventInfo_isPassed__JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_PtTerm__1down, &b_HGamEventInfo_m_yy__JET_Pileup_PtTerm__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_RhoTopology__1up, &b_HGamEventInfo_weight__JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1up, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1up, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1up, &b_HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1up, &b_HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn.weight", &HGamEventInfo_weight__JET_Pileup_RhoTopology__1down, &b_HGamEventInfo_weight__JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1down, &b_HGamEventInfo_Hc_weightjvt__JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1down, &b_HGamEventInfo_Hc_weightCtag__JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1down, &b_HGamEventInfo_isPassed__JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1down, &b_HGamEventInfo_m_yy__JET_Pileup_RhoTopology__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn.weight", &HGamEventInfo_weight__JET_PunchThrough_MC16__1up, &b_HGamEventInfo_weight__JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1up, &b_HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1up, &b_HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_PunchThrough_MC16__1up, &b_HGamEventInfo_isPassed__JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_PunchThrough_MC16__1up, &b_HGamEventInfo_m_yy__JET_PunchThrough_MC16__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn.weight", &HGamEventInfo_weight__JET_PunchThrough_MC16__1down, &b_HGamEventInfo_weight__JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1down, &b_HGamEventInfo_Hc_weightjvt__JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1down, &b_HGamEventInfo_Hc_weightCtag__JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_PunchThrough_MC16__1down, &b_HGamEventInfo_isPassed__JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_PunchThrough_MC16__1down, &b_HGamEventInfo_m_yy__JET_PunchThrough_MC16__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn.weight", &HGamEventInfo_weight__JET_SingleParticle_HighPt__1up, &b_HGamEventInfo_weight__JET_SingleParticle_HighPt__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1up, &b_HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1up, &b_HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1up, &b_HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1up, &b_HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1up);

   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn.weight", &HGamEventInfo_weight__JET_SingleParticle_HighPt__1down, &b_HGamEventInfo_weight__JET_SingleParticle_HighPt__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1down, &b_HGamEventInfo_Hc_weightjvt__JET_SingleParticle_HighPt__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1down, &b_HGamEventInfo_Hc_weightCtag__JET_SingleParticle_HighPt__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1down, &b_HGamEventInfo_isPassed__JET_SingleParticle_HighPt__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_SingleParticle_HighPt__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1down, &b_HGamEventInfo_m_yy__JET_SingleParticle_HighPt__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn.weight", &HGamEventInfo_weight__JET_fJvtEfficiency__1down, &b_HGamEventInfo_weight__JET_fJvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1down, &b_HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1down, &b_HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_fJvtEfficiency__1down, &b_HGamEventInfo_isPassed__JET_fJvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1down);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_fJvtEfficiency__1down, &b_HGamEventInfo_m_yy__JET_fJvtEfficiency__1down);

   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn.weight", &HGamEventInfo_weight__JET_fJvtEfficiency__1up, &b_HGamEventInfo_weight__JET_fJvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1up, &b_HGamEventInfo_Hc_weightjvt__JET_fJvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1up, &b_HGamEventInfo_Hc_weightCtag__JET_fJvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__JET_fJvtEfficiency__1up, &b_HGamEventInfo_isPassed__JET_fJvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__JET_fJvtEfficiency__1up);
   fChain->SetBranchAddress("HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__JET_fJvtEfficiency__1up, &b_HGamEventInfo_m_yy__JET_fJvtEfficiency__1up);

   Notify();
}

Bool_t Reader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Reader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Reader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Reader_cxx
