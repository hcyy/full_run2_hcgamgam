#ifndef PLOTTER
#define PLOTTER


#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "THStack.h"
#include "TGaxis.h"
#include "TArrow.h"
#include <stdio.h>
#include <unistd.h>
#include <vector>
#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <stdio.h>

#include "ControlHist.h"


class Plotter
{
 public:

  Plotter(const TString fileType="png", const bool seperateSignal=false, const bool seperateUncertainties=false, const bool colouredSignal=false);
  ~Plotter();
  void Plot(ControlHist *chist, const int cut, const TString option, const TString fitFunction="");

 private:

  TString m_path = "";
  const TString m_fileType;
  const bool m_seperateSignal;
  const bool m_seperateUncertainties;
  bool m_hasRatio=false;
  TH1D *duplicateBkgdHist;
  const bool m_colouredSignal;
  double GetScale(TH1D *hist, double fudgeFactor=1) const;
  std::map<int, int> colorMap = {{1,2}, {2,3}, {3,4}, {4,6}, {5,7}, {6,17}, {7,5}, {8,8}, {9,9}, {10,28}, {11,11}, {12,12}, {13,13}, {14,14}, {15,15}, {16,16}, {17,1}};
  std::map<int, int> colorMapBkgd = {{1,1}, {2,2}, {3,3}, {4,4}, {5,5}, {6,6}, {7,7}, {8,8}, {9,9}, {10,28}, {11,11}, {12,12}, {13,13}, {14,14}, {15,15}, {16,16}, {17,17}};

  double pad1Scale = 0.7;
  const double pad2Scale = 1-pad1Scale;
  const double fudgeFactor1 = 1.465;
  const double fudgeFactor2 = 0.975;
  const double textSize = 0.035;
  const double lineWidth = 2;
  const double tickLength = 0.03;
  const double fixedRatio = 1.2;
  const double linHistMin = 0;
  const double logHistMin = 1e-5;
  std::vector<TArrow*> arrows;
};

#endif
