#include <vector>
#include <cmath>
#include <iostream>
#include <string>

#include "TRandom3.h"
#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TGraphErrors.h"
#include "RooFitResult.h"
#include "TH1.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
/* #include "RooMinuit.h"

/* #include "TMVA/Tools.h" */
/* #include "TMVA/Reader.h" */
/* #include "TMVA/MethodBDT.h" */

#include "Reader.h"
#include "Plotter.h"
#include "ControlHist.h"

/* #include "TMVA/Factory.h" */
/* #include "TMVA/DataLoader.h" */
/* #include "TMVA/Tools.h" */
/* #include "TMVA/TMVAGui.h" */
/* #include "TMVA/MethodCuts.h" */


// Name of output file path
const TString outputHistPath = "OutputHists";

// Main
int main(int argc, char* argv[]);

// Process samples
enum PileupProfile {MC16a, MC16d, MC16e};
void Process(const std::vector<TString> filePaths, const int sample, const bool isMC=false, const int pileupProfile=PileupProfile::MC16a);
void ProcessAll();

// Initialise ntuple reader class
Reader *InitReader(const TString filePath);

// Alternative method to gather weights
float GetSumWeights(std::vector<TString> filePaths, const int sample, const int index);

// Plotter class
const TString plotter_fileType="png";
const bool plotter_seperateSignal=false;
const bool plotter_seperateUncertainties=false;
const bool plotter_coloredSignal=false;
Plotter *plotter;

// Fixed parameters
const float massZ=91;
const float massPion=0.13957018;
const float massElectron = 0.0005109989461; // in GeV
const float massMuon = 0.1056583745; // in GeV
const float massEtac = 2.9839; // in GeV
const float massJpsi = 3.0969; // in GeV
const TString treeName = "CollectionTree";
/* const float lumi_MC16a=36240.; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger // what I had pre-25Jan23 */
const float lumi_MC16a=36214.; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger // changed 25Jan23 as per Anna message
const float lumi_MC16d=44310.; // 44316.2 nb^-1 was used before 04Jun19, possible derived by including triggers in lumicalc
const float lumi_MC16e=58450.; // 59937.2 nb^-1 pre-04Jun19
/* const float lumi_MC16a=36100.; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger */
/* const float lumi_MC16d=43700.; // 44316.2 nb^-1 was used before 04Jun19, possible derived by including triggers in lumicalc */
/* const float lumi_MC16e=59200.; // 59937.2 nb^-1 pre-04Jun19 */
// const float lumi_MC16a=3219.56+32994.9; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger
// const float lumi_MC16d=44307.4; // 44316.2 nb^-1 was used before 04Jun19, possible derived by including triggers in lumicalc
// const float lumi_MC16e=58450.1; // 59937.2 nb^-1 pre-04Jun19

// ControlHists
std::map<int, ControlHist*> ControlHists;
enum Sample {Data, CombinedBkgd, cH, ggFH, VBFH, WmH, WpH, ZH, ggZH, ttH, bbH, yy, tHjb, tWH, Total};
enum Cut {All, AllBlinded, SR1, MLP, MH, SR2, CR, VR1, VR2, VRC, R1, R2, RC};
enum ControlHistName {myy, DL1, DL1r};
int bkgdSample=-1;
int DefineMainBkgdSample();
void SetupControlHists();
void PlotControlHists();
void SaveControlHists(const TString name = "HZX_HM.root", std::vector<int> hists = {ControlHistName::myy});

void SetupSamples();
std::vector<int> samples;
std::map<int, TString> ControlHist::int2string_sample = {{Sample::Data, "Data"}, {Sample::cH, "cH"}, {Sample::CombinedBkgd, "CombinedBkgd"}, {Sample::VBFH, "VBFH"}, {Sample::ggFH, "ggFH"}, {Sample::WmH, "WmH"}, {Sample::WpH, "WpH"}, {Sample::ZH, "ZH"}, {Sample::ggZH, "ggZH"}, {Sample::ttH, "ttH"}, {Sample::bbH, "bbH"}, {Sample::yy, "yy"}, {Sample::tHjb, "tHjb"}, {Sample::tWH, "tWH"}};
std::map<int, TString> ControlHist::int2displayString_sample = {{Sample::Data, "Data"}, {Sample::cH, "H+c"}, {Sample::CombinedBkgd, "Background"}, {Sample::ggFH, "H (ggF)"}, {Sample::VBFH, "H (VBF)"}, {Sample::WmH, "W^{-}H"}, {Sample::WpH, "W^{+}H"}, {Sample::ZH, "ZH"}, {Sample::ggZH, "ggZH"}, {Sample::ttH, "t#bar{t}H"}, {Sample::bbH, "b#bar{b}H"}, {Sample::yy, "#gamma#gamma"}, {Sample::tHjb, "tHjb"}, {Sample::tWH, "tWH"}};
std::map<int, double> ControlHist::int2float_sample = {{Sample::Data, -999}, {Sample::cH, -999}, {Sample::CombinedBkgd, -999}};
std::map<TString, double> string2float_sample = {{"Data", -999}, {"cH", -999}, {"CombinedBkgd", -999}, {"ggFH", -999}, {"VBFH", -999}, {"WmH", -999}, {"WpH", -999}, {"ZH", -999}, {"ggZH", -999}, {"ttH", -999}, {"bbH", -999}, {"yy", -999}, {"tHjb", -999}, {"tWH", -999}};
std::map<int, TString> ControlHist::int2string_cut = {{Cut::All, "All"}, {Cut::AllBlinded, "AllBlinded"}};

std::map<int, TString> int2histName = {{Sample::cH, "CutFlow_MGPy8_cH_yc_Hgammagamma_noDalitz_weighted"}, {Sample::ggFH, "CutFlow_PowhegPy8_NNLOPS_ggH125_noDalitz_weighted"}, {Sample::VBFH, "CutFlow_PowhegPy8EG_NNPDF30_VBFH125_noDalitz_weighted"}, {Sample::WmH, "CutFlow_PowhegPy8_WmH125J_noDalitz_weighted"}, {Sample::WpH, "CutFlow_PowhegPy8_WpH125J_noDalitz_weighted"}, {Sample::ZH, "CutFlow_PowhegPy8_ZH125J_noDalitz_weighted"}, {Sample::ggZH, "CutFlow_PowhegPy8_ggZH125_noDalitz_weighted"}, {Sample::ttH, "CutFlow_PowhegPy8_ttH125_fixweight_noDalitz_weighted"}, {Sample::bbH, "CutFlow_PowhegPy8_bbH125_noDalitz_weighted"}, {Sample::yy, "CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"}, {Sample::tHjb, "CutFlow_aMCnloPy8_tHjb125_4fl_noDalitz_weighted"}, {Sample::tWH, "CutFlow_aMCnloPy8_tWH125_noDalitz_weighted"}};

// Evaluate efficiencies
void PrintEfficiencies();
void PrintSys4WS();
void PrintStat4WS();
void PrintHerwig4WS();
void MakeEfficiencyPlots();
std::map<int,float> initialNEvents; // mapping from sample
std::map<std::pair<int,int>,float> finalNEvents; // mapping from cut and sample
std::map<std::pair<int,int>,float> finalNEventsSq; // mapping from cut and sample
std::map<std::pair<int,int>,float> correctXEvents; // mapping from cut and sample
std::map<std::pair<int,int>,float> correctXEventsSq; // mapping from cut and sample

// Systematics
enum Systematics {Nominal, JET_BJES_Response__1up, JET_BJES_Response__1down, JET_EffectiveNP_1__1up, JET_EffectiveNP_1__1down, JET_EffectiveNP_2__1up, JET_EffectiveNP_2__1down, JET_EffectiveNP_3__1up, JET_EffectiveNP_3__1down, JET_EffectiveNP_4__1up, JET_EffectiveNP_4__1down, JET_EffectiveNP_5__1up, JET_EffectiveNP_5__1down, JET_EffectiveNP_6__1up, JET_EffectiveNP_6__1down, JET_EffectiveNP_7__1up, JET_EffectiveNP_7__1down, JET_EffectiveNP_8restTerm__1up, JET_EffectiveNP_8restTerm__1down, JET_EtaIntercalibration_Modelling__1up, JET_EtaIntercalibration_Modelling__1down, JET_EtaIntercalibration_NonClosure_2018data__1up, JET_EtaIntercalibration_NonClosure_2018data__1down, JET_EtaIntercalibration_NonClosure_highE__1up, JET_EtaIntercalibration_NonClosure_highE__1down, JET_EtaIntercalibration_NonClosure_negEta__1up, JET_EtaIntercalibration_NonClosure_negEta__1down, JET_EtaIntercalibration_NonClosure_posEta__1up, JET_EtaIntercalibration_NonClosure_posEta__1down, JET_EtaIntercalibration_TotalStat__1up, JET_EtaIntercalibration_TotalStat__1down, JET_Flavor_Composition__1up, JET_Flavor_Composition__1down, JET_Flavor_Response__1up, JET_Flavor_Response__1down, JET_JER_DataVsMC_MC16__1up, JET_JER_DataVsMC_MC16__1down, JET_JER_EffectiveNP_1__1up, JET_JER_EffectiveNP_1__1down, JET_JER_EffectiveNP_2__1up, JET_JER_EffectiveNP_2__1down, JET_JER_EffectiveNP_3__1up, JET_JER_EffectiveNP_3__1down, JET_JER_EffectiveNP_4__1up, JET_JER_EffectiveNP_4__1down, JET_JER_EffectiveNP_5__1up, JET_JER_EffectiveNP_5__1down, JET_JER_EffectiveNP_6__1up, JET_JER_EffectiveNP_6__1down, JET_JER_EffectiveNP_7restTerm__1up, JET_JER_EffectiveNP_7restTerm__1down, JET_JvtEfficiency__1down, JET_JvtEfficiency__1up, JET_Pileup_OffsetMu__1up, JET_Pileup_OffsetMu__1down, JET_Pileup_OffsetNPV__1up, JET_Pileup_OffsetNPV__1down, JET_Pileup_PtTerm__1up, JET_Pileup_PtTerm__1down, JET_Pileup_RhoTopology__1up, JET_Pileup_RhoTopology__1down, JET_PunchThrough_MC16__1up, JET_PunchThrough_MC16__1down, JET_SingleParticle_HighPt__1up, JET_SingleParticle_HighPt__1down, JET_fJvtEfficiency__1down, JET_fJvtEfficiency__1up};
std::map<int, TString> sysStrings = {{Systematics::Nominal, "Nominal"}, {Systematics::JET_BJES_Response__1up, "HGamEventInfo_JET_BJES_Response__1up"}, {Systematics::JET_BJES_Response__1down, "HGamEventInfo_JET_BJES_Response__1down"}, {Systematics::JET_EffectiveNP_1__1up, "HGamEventInfo_JET_EffectiveNP_1__1up"}, {Systematics::JET_EffectiveNP_1__1down, "HGamEventInfo_JET_EffectiveNP_1__1down"}, {Systematics::JET_EffectiveNP_2__1up, "HGamEventInfo_JET_EffectiveNP_2__1up"}, {Systematics::JET_EffectiveNP_2__1down, "HGamEventInfo_JET_EffectiveNP_2__1down"}, {Systematics::JET_EffectiveNP_3__1up, "HGamEventInfo_JET_EffectiveNP_3__1up"}, {Systematics::JET_EffectiveNP_3__1down, "HGamEventInfo_JET_EffectiveNP_3__1down"}, {Systematics::JET_EffectiveNP_4__1up, "HGamEventInfo_JET_EffectiveNP_4__1up"}, {Systematics::JET_EffectiveNP_4__1down, "HGamEventInfo_JET_EffectiveNP_4__1down"}, {Systematics::JET_EffectiveNP_5__1up, "HGamEventInfo_JET_EffectiveNP_5__1up"}, {Systematics::JET_EffectiveNP_5__1down, "HGamEventInfo_JET_EffectiveNP_5__1down"}, {Systematics::JET_EffectiveNP_6__1up, "HGamEventInfo_JET_EffectiveNP_6__1up"}, {Systematics::JET_EffectiveNP_6__1down, "HGamEventInfo_JET_EffectiveNP_6__1down"}, {Systematics::JET_EffectiveNP_7__1up, "HGamEventInfo_JET_EffectiveNP_7__1up"}, {Systematics::JET_EffectiveNP_7__1down, "HGamEventInfo_JET_EffectiveNP_7__1down"}, {Systematics::JET_EffectiveNP_8restTerm__1up, "HGamEventInfo_JET_EffectiveNP_8restTerm__1up"}, {Systematics::JET_EffectiveNP_8restTerm__1down, "HGamEventInfo_JET_EffectiveNP_8restTerm__1down"}, {Systematics::JET_EtaIntercalibration_Modelling__1up, "HGamEventInfo_JET_EtaIntercalibration_Modelling__1up"}, {Systematics::JET_EtaIntercalibration_Modelling__1down, "HGamEventInfo_JET_EtaIntercalibration_Modelling__1down"}, {Systematics::JET_EtaIntercalibration_NonClosure_2018data__1up, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1up"}, {Systematics::JET_EtaIntercalibration_NonClosure_2018data__1down, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_2018data__1down"}, {Systematics::JET_EtaIntercalibration_NonClosure_highE__1up, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1up"}, {Systematics::JET_EtaIntercalibration_NonClosure_highE__1down, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1down"}, {Systematics::JET_EtaIntercalibration_NonClosure_negEta__1up, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1up"}, {Systematics::JET_EtaIntercalibration_NonClosure_negEta__1down, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1down"}, {Systematics::JET_EtaIntercalibration_NonClosure_posEta__1up, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1up"}, {Systematics::JET_EtaIntercalibration_NonClosure_posEta__1down, "HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1down"}, {Systematics::JET_EtaIntercalibration_TotalStat__1up, "HGamEventInfo_JET_EtaIntercalibration_TotalStat__1up"}, {Systematics::JET_EtaIntercalibration_TotalStat__1down, "HGamEventInfo_JET_EtaIntercalibration_TotalStat__1down"}, {Systematics::JET_Flavor_Composition__1up, "HGamEventInfo_JET_Flavor_Composition__1up"}, {Systematics::JET_Flavor_Composition__1down, "HGamEventInfo_JET_Flavor_Composition__1down"}, {Systematics::JET_Flavor_Response__1up, "HGamEventInfo_JET_Flavor_Response__1up"}, {Systematics::JET_Flavor_Response__1down, "HGamEventInfo_JET_Flavor_Response__1down"}, {Systematics::JET_JER_DataVsMC_MC16__1up, "HGamEventInfo_JET_JER_DataVsMC_MC16__1up"}, {Systematics::JET_JER_DataVsMC_MC16__1down, "HGamEventInfo_JET_JER_DataVsMC_MC16__1down"}, {Systematics::JET_JER_EffectiveNP_1__1up, "HGamEventInfo_JET_JER_EffectiveNP_1__1up"}, {Systematics::JET_JER_EffectiveNP_1__1down, "HGamEventInfo_JET_JER_EffectiveNP_1__1down"}, {Systematics::JET_JER_EffectiveNP_2__1up, "HGamEventInfo_JET_JER_EffectiveNP_2__1up"}, {Systematics::JET_JER_EffectiveNP_2__1down, "HGamEventInfo_JET_JER_EffectiveNP_2__1down"}, {Systematics::JET_JER_EffectiveNP_3__1up, "HGamEventInfo_JET_JER_EffectiveNP_3__1up"}, {Systematics::JET_JER_EffectiveNP_3__1down, "HGamEventInfo_JET_JER_EffectiveNP_3__1down"}, {Systematics::JET_JER_EffectiveNP_4__1up, "HGamEventInfo_JET_JER_EffectiveNP_4__1up"}, {Systematics::JET_JER_EffectiveNP_4__1down, "HGamEventInfo_JET_JER_EffectiveNP_4__1down"}, {Systematics::JET_JER_EffectiveNP_5__1up, "HGamEventInfo_JET_JER_EffectiveNP_5__1up"}, {Systematics::JET_JER_EffectiveNP_5__1down, "HGamEventInfo_JET_JER_EffectiveNP_5__1down"}, {Systematics::JET_JER_EffectiveNP_6__1up, "HGamEventInfo_JET_JER_EffectiveNP_6__1up"}, {Systematics::JET_JER_EffectiveNP_6__1down, "HGamEventInfo_JET_JER_EffectiveNP_6__1down"}, {Systematics::JET_JER_EffectiveNP_7restTerm__1up, "HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1up"}, {Systematics::JET_JER_EffectiveNP_7restTerm__1down, "HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1down"}, {Systematics::JET_JvtEfficiency__1down, "HGamEventInfo_JET_JvtEfficiency__1down"}, {Systematics::JET_JvtEfficiency__1up, "HGamEventInfo_JET_JvtEfficiency__1up"}, {Systematics::JET_Pileup_OffsetMu__1up, "HGamEventInfo_JET_Pileup_OffsetMu__1up"}, {Systematics::JET_Pileup_OffsetMu__1down, "HGamEventInfo_JET_Pileup_OffsetMu__1down"}, {Systematics::JET_Pileup_OffsetNPV__1up, "HGamEventInfo_JET_Pileup_OffsetNPV__1up"}, {Systematics::JET_Pileup_OffsetNPV__1down, "HGamEventInfo_JET_Pileup_OffsetNPV__1down"}, {Systematics::JET_Pileup_PtTerm__1up, "HGamEventInfo_JET_Pileup_PtTerm__1up"}, {Systematics::JET_Pileup_PtTerm__1down, "HGamEventInfo_JET_Pileup_PtTerm__1down"}, {Systematics::JET_Pileup_RhoTopology__1up, "HGamEventInfo_JET_Pileup_RhoTopology__1up"}, {Systematics::JET_Pileup_RhoTopology__1down, "HGamEventInfo_JET_Pileup_RhoTopology__1down"}, {Systematics::JET_PunchThrough_MC16__1up, "HGamEventInfo_JET_PunchThrough_MC16__1up"}, {Systematics::JET_PunchThrough_MC16__1down, "HGamEventInfo_JET_PunchThrough_MC16__1down"}, {Systematics::JET_SingleParticle_HighPt__1up, "HGamEventInfo_JET_SingleParticle_HighPt__1up"}, {Systematics::JET_SingleParticle_HighPt__1down, "HGamEventInfo_JET_SingleParticle_HighPt__1down"}, {Systematics::JET_fJvtEfficiency__1down, "HGamEventInfo_JET_fJvtEfficiency__1down"}, {Systematics::JET_fJvtEfficiency__1up, "HGamEventInfo_JET_fJvtEfficiency__1up"}};

