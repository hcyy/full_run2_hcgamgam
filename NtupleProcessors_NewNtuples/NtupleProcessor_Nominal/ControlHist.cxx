#include "ControlHist.h"


ControlHist::ControlHist(const TString name, const TString axes, const int nBins, const double min, const double max, const double maxY, const double minY)
{
  m_name = name;

  m_maxY=maxY;
  m_minY=minY;
  
  m_is2D = false;

  samples=samples_data;
  samples.insert(samples.end(), samples_signal.begin(), samples_signal.end());
  samples.insert(samples.end(), samples_background.begin(), samples_background.end());

  for(std::vector<int>::const_iterator sa = samples.begin(); sa != samples.end(); ++sa)
    {
      for(std::vector<int>::const_iterator cu = cuts.begin(); cu != cuts.end(); ++cu)
	{
	  int theSample = *sa;
	  int theCut = *cu;
	  std::pair<int, int> key = std::make_pair(theSample, theCut);

	  m_hists1D[key] = new TH1D(name+"_"+int2string_sample[key.first]+"_"+int2string_cut[key.second], axes, nBins, min, max);

	  m_hists1D[key]->Sumw2();
	}
    }
}


ControlHist::ControlHist(const TString name, const TString axes, const int nBinsX, const double minX, const double maxX, const int nBinsY, const double minY, const double maxY)
{
  m_name = name;
  
  m_is2D = true;

  samples=samples_data;
  samples.insert(samples.end(), samples_signal.begin(), samples_signal.end());
  samples.insert(samples.end(), samples_background.begin(), samples_background.end());

  for(std::vector<int>::const_iterator sa = samples.begin(); sa != samples.end(); ++sa)
    {
      for(std::vector<int>::const_iterator cu = cuts.begin(); cu != cuts.end(); ++cu)
	{
	  int theSample = *sa;
	  int theCut = *cu;
	  std::pair<int, int> key = std::make_pair(theSample, theCut);
	  m_hists2D[key] = new TH2D(name+"_"+int2string_sample[key.first]+"_"+int2string_cut[key.second], axes, nBinsX, minX, maxX, nBinsY, minY, maxY);
	  m_hists2D[key]->Sumw2();
	}
    }

}


ControlHist::~ControlHist()
{
  for(std::vector<int>::const_iterator sa = samples.begin(); sa != samples.end(); ++sa)
    {
      for(std::vector<int>::const_iterator cu = cuts.begin(); cu != cuts.end(); ++cu)
	{
	  int theSample = *sa;
	  int theCut = *cu;
	  std::pair<int, int> key = std::make_pair(theSample, theCut);
	  if(m_is2D) delete m_hists2D[key];
          else delete m_hists1D[key];
	}
    }
}


void ControlHist::Fill(int theSample, const double value, const double weight, std::vector<int> PassedCuts)
{
  for(std::vector<int>::const_iterator cu = PassedCuts.begin(); cu != PassedCuts.end(); ++cu)
    {
      std::pair<int, int> key = std::make_pair(theSample, *cu);
      m_hists1D[key]->Fill(value,weight);
    }
}


void ControlHist::Fill(int theSample, const double valueX, const double valueY, const double weight, std::vector<int> PassedCuts)
{
  for(std::vector<int>::const_iterator cu = PassedCuts.begin(); cu != PassedCuts.end(); ++cu)
    {
      std::pair<int, int> key = std::make_pair(theSample, *cu);
      m_hists2D[key]->Fill(valueX,valueY,weight);
    }
}


void ControlHist::WriteAll()
{
  for(std::vector<int>::const_iterator sa = samples.begin(); sa != samples.end(); ++sa)
    {
      for(std::vector<int>::const_iterator cu = cuts.begin(); cu != cuts.end(); ++cu)
	{
	  int theSample = *sa;
	  int theCut = *cu;
	  std::pair<int, int> key = std::make_pair(theSample, theCut);
	  if(m_is2D) m_hists2D[key]->Write();
          else m_hists1D[key]->Write();
	}
    }
}


TH1D *ControlHist::GetTotalHist1D(int cut)
{
  TH1D *total = NULL;
  bool firstHist = true;
  for(std::vector<int>::const_iterator sa = samples.begin(); sa != samples.end(); ++sa)
    {
      int theSample = *sa;
      std::pair<int, int> key = std::make_pair(theSample, cut);
      if(firstHist) total = (TH1D*) m_hists1D[key]->Clone("hist");
      else total->Add(m_hists1D[key]);
      firstHist = false;
    }
  return total;
}


TH2D *ControlHist::GetTotalHist2D(int cut)
{
  TH2D *total;
  bool firstHist = true;
  for(std::vector<int>::const_iterator sa = samples.begin(); sa != samples.end(); ++sa)
    {
      int theSample = *sa;
      std::pair<int, int> key = std::make_pair(theSample, cut);
      if(firstHist) total = (TH2D*) m_hists2D[key]->Clone("hist");
      else total->Add(m_hists2D[key]);
      firstHist = false;
    }
  return total;
}
