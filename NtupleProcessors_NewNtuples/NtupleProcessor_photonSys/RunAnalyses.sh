make clean
make

rm -rf Output_bkd_nonCtagCat Output_sig_nonCtagCat Output_bkd_ctagCat Output_sig_ctagCat
mkdir Output_bkd_nonCtagCat Output_sig_nonCtagCat Output_bkd_ctagCat Output_sig_ctagCat

for sys in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
do
    rm -rf Plots OutputHists output.txt Output_bkd_nonCtagCat/SYS${sys}
    ./Analysis 0 0 ${sys} &> output.txt
    mkdir Output_bkd_nonCtagCat/SYS${sys}
    mv output.txt Output_bkd_nonCtagCat/SYS${sys}/.
    mv Plots Output_bkd_nonCtagCat/SYS${sys}/.
    mv OutputHists Output_bkd_nonCtagCat/SYS${sys}/.
    
    rm -rf Plots OutputHists output.txt Output_sig_nonCtagCat/SYS${sys}
    ./Analysis 0 1 ${sys} &> output.txt
    mkdir Output_sig_nonCtagCat/SYS${sys}
    mv output.txt Output_sig_nonCtagCat/SYS${sys}/.
    mv Plots Output_sig_nonCtagCat/SYS${sys}/.
    mv OutputHists Output_sig_nonCtagCat/SYS${sys}/.
    
    rm -rf Plots OutputHists output.txt Output_bkd_ctagCat/SYS${sys}
    ./Analysis 1 0 ${sys} &> output.txt
    mkdir Output_bkd_ctagCat/SYS${sys}
    mv output.txt Output_bkd_ctagCat/SYS${sys}/.
    mv Plots Output_bkd_ctagCat/SYS${sys}/.
    mv OutputHists Output_bkd_ctagCat/SYS${sys}/.
    
    rm -rf Plots OutputHists output.txt Output_sig_ctagCat/SYS${sys}
    ./Analysis 1 1 ${sys} &> output.txt
    mkdir Output_sig_ctagCat/SYS${sys}
    mv output.txt Output_sig_ctagCat/SYS${sys}/.
    mv Plots Output_sig_ctagCat/SYS${sys}/.
    mv OutputHists Output_sig_ctagCat/SYS${sys}/.
done
