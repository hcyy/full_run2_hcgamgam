//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan  5 22:40:20 2023 by ROOT version 6.24/08
// from TTree CollectionTree/xAOD event tree
// found on file: /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027_hc/mc16a/Nominal/mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126_r9364_p4180_h027.root/mc16a.PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126_r9364_p4180_h027.001.root
//////////////////////////////////////////////////////////

#ifndef Reader_h
#define Reader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class Reader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   Float_t         HGamEventInfoAuxDyn_crossSectionBRfilterEff;
   Int_t           HGamTruthEventInfoAuxDyn_N_j_cjet_had;

   Float_t         HGamEventInfoAuxDyn_weight;
   Float_t         HGamEventInfoAuxDyn_Hc_weightjvt;
   Float_t         HGamEventInfoAuxDyn_Hc_weightCtag;
   Char_t          HGamEventInfoAuxDyn_isPassed;
   Int_t           HGamEventInfoAuxDyn_Hc_Atleast1jisloose;
   Float_t         HGamEventInfoAuxDyn_m_yy;

   Float_t         HGamEventInfo_weight__EG_RESOLUTION_ALL__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1down;
   Char_t          HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1down;
   Float_t         HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1down;

   Float_t         HGamEventInfo_weight__EG_RESOLUTION_ALL__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1up;
   Char_t          HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1up;
   Float_t         HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1up;

   Float_t         HGamEventInfo_weight__EG_SCALE_AF2__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1down;
   Char_t          HGamEventInfo_isPassed__EG_SCALE_AF2__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1down;
   Float_t         HGamEventInfo_m_yy__EG_SCALE_AF2__1down;

   Float_t         HGamEventInfo_weight__EG_SCALE_AF2__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1up;
   Char_t          HGamEventInfo_isPassed__EG_SCALE_AF2__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1up;
   Float_t         HGamEventInfo_m_yy__EG_SCALE_AF2__1up;

   Float_t         HGamEventInfo_weight__EG_SCALE_ALL__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1down;
   Char_t          HGamEventInfo_isPassed__EG_SCALE_ALL__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1down;
   Float_t         HGamEventInfo_m_yy__EG_SCALE_ALL__1down;

   Float_t         HGamEventInfo_weight__EG_SCALE_ALL__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1up;
   Char_t          HGamEventInfo_isPassed__EG_SCALE_ALL__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1up;
   Float_t         HGamEventInfo_m_yy__EG_SCALE_ALL__1up;

   Float_t         HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1down;
   Char_t          HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1down;
   Float_t         HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1down;

   Float_t         HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1up;
   Char_t          HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1up;
   Float_t         HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1up;

   Float_t         HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1down;
   Char_t          HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1down;
   Float_t         HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1down;

   Float_t         HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1up;
   Char_t          HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1up;
   Float_t         HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1up;

   Float_t         HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1down;
   Char_t          HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1down;
   Float_t         HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1down;

   Float_t         HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1up;
   Char_t          HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1up;
   Float_t         HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1up;

   Float_t         HGamEventInfo_weight__PRW_DATASF__1down;
   Float_t         HGamEventInfo_Hc_weightjvt__PRW_DATASF__1down;
   Float_t         HGamEventInfo_Hc_weightCtag__PRW_DATASF__1down;
   Char_t          HGamEventInfo_isPassed__PRW_DATASF__1down;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1down;
   Float_t         HGamEventInfo_m_yy__PRW_DATASF__1down;

   Float_t         HGamEventInfo_weight__PRW_DATASF__1up;
   Float_t         HGamEventInfo_Hc_weightjvt__PRW_DATASF__1up;
   Float_t         HGamEventInfo_Hc_weightCtag__PRW_DATASF__1up;
   Char_t          HGamEventInfo_isPassed__PRW_DATASF__1up;
   Int_t           HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1up;
   Float_t         HGamEventInfo_m_yy__PRW_DATASF__1up;

   // List of branches
   TBranch        *b_HGamEventInfoAuxDyn_crossSectionBRfilterEff;   //!
   TBranch        *b_HGamTruthEventInfoAuxDyn_N_j_cjet_had;   //!

   TBranch        *b_HGamEventInfoAuxDyn_weight;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_weightjvt;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_weightCtag;   //!
   TBranch        *b_HGamEventInfoAuxDyn_isPassed;   //!
   TBranch        *b_HGamEventInfoAuxDyn_Hc_Atleast1jisloose;   //!
   TBranch        *b_HGamEventInfoAuxDyn_m_yy;   //!

   TBranch        *b_HGamEventInfo_weight__EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1down;   //!

   TBranch        *b_HGamEventInfo_weight__EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1up;   //!

   TBranch        *b_HGamEventInfo_weight__EG_SCALE_AF2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__EG_SCALE_AF2__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__EG_SCALE_AF2__1down;   //!

   TBranch        *b_HGamEventInfo_weight__EG_SCALE_AF2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__EG_SCALE_AF2__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__EG_SCALE_AF2__1up;   //!

   TBranch        *b_HGamEventInfo_weight__EG_SCALE_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__EG_SCALE_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__EG_SCALE_ALL__1down;   //!

   TBranch        *b_HGamEventInfo_weight__EG_SCALE_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__EG_SCALE_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__EG_SCALE_ALL__1up;   //!

   TBranch        *b_HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1down;   //!

   TBranch        *b_HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1up;   //!

   TBranch        *b_HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1down;   //!

   TBranch        *b_HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1up;   //!

   TBranch        *b_HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1down;   //!

   TBranch        *b_HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1up;   //!

   TBranch        *b_HGamEventInfo_weight__PRW_DATASF__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PRW_DATASF__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PRW_DATASF__1down;   //!
   TBranch        *b_HGamEventInfo_isPassed__PRW_DATASF__1down;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1down;   //!
   TBranch        *b_HGamEventInfo_m_yy__PRW_DATASF__1down;   //!

   TBranch        *b_HGamEventInfo_weight__PRW_DATASF__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightjvt__PRW_DATASF__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_weightCtag__PRW_DATASF__1up;   //!
   TBranch        *b_HGamEventInfo_isPassed__PRW_DATASF__1up;   //!
   TBranch        *b_HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1up;   //!
   TBranch        *b_HGamEventInfo_m_yy__PRW_DATASF__1up;   //!

   Reader(TTree *tree=0);
   virtual ~Reader();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Reader_cxx
Reader::Reader(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("");
      if (!f || !f->IsOpen()) {
         f = new TFile("");
      }
      f->GetObject("CollectionTree",tree);

   }
   Init(tree);
}

Reader::~Reader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Reader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Reader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Reader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   /* fChain->SetMakeClass(1); */

   fChain->SetBranchAddress("HGamEventInfoAuxDyn.crossSectionBRfilterEff", &HGamEventInfoAuxDyn_crossSectionBRfilterEff, &b_HGamEventInfoAuxDyn_crossSectionBRfilterEff);
   fChain->SetBranchAddress("HGamTruthEventInfoAuxDyn.N_j_cjet_had", &HGamTruthEventInfoAuxDyn_N_j_cjet_had, &b_HGamTruthEventInfoAuxDyn_N_j_cjet_had);

   fChain->SetBranchAddress("HGamEventInfoAuxDyn.weight", &HGamEventInfoAuxDyn_weight, &b_HGamEventInfoAuxDyn_weight);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_weightjvt", &HGamEventInfoAuxDyn_Hc_weightjvt, &b_HGamEventInfoAuxDyn_Hc_weightjvt);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_weightCtag", &HGamEventInfoAuxDyn_Hc_weightCtag, &b_HGamEventInfoAuxDyn_Hc_weightCtag);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.isPassed", &HGamEventInfoAuxDyn_isPassed, &b_HGamEventInfoAuxDyn_isPassed);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.Hc_Atleast1jisloose", &HGamEventInfoAuxDyn_Hc_Atleast1jisloose, &b_HGamEventInfoAuxDyn_Hc_Atleast1jisloose);
   fChain->SetBranchAddress("HGamEventInfoAuxDyn.m_yy", &HGamEventInfoAuxDyn_m_yy, &b_HGamEventInfoAuxDyn_m_yy);

   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.weight", &HGamEventInfo_weight__EG_RESOLUTION_ALL__1down, &b_HGamEventInfo_weight__EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1down, &b_HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1down, &b_HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1down, &b_HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1down, &b_HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1down);

   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.weight", &HGamEventInfo_weight__EG_RESOLUTION_ALL__1up, &b_HGamEventInfo_weight__EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1up, &b_HGamEventInfo_Hc_weightjvt__EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1up, &b_HGamEventInfo_Hc_weightCtag__EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1up, &b_HGamEventInfo_isPassed__EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1up, &b_HGamEventInfo_m_yy__EG_RESOLUTION_ALL__1up);

   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1downAuxDyn.weight", &HGamEventInfo_weight__EG_SCALE_AF2__1down, &b_HGamEventInfo_weight__EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1down, &b_HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1down, &b_HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__EG_SCALE_AF2__1down, &b_HGamEventInfo_isPassed__EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__EG_SCALE_AF2__1down, &b_HGamEventInfo_m_yy__EG_SCALE_AF2__1down);

   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1upAuxDyn.weight", &HGamEventInfo_weight__EG_SCALE_AF2__1up, &b_HGamEventInfo_weight__EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1up, &b_HGamEventInfo_Hc_weightjvt__EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1up, &b_HGamEventInfo_Hc_weightCtag__EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__EG_SCALE_AF2__1up, &b_HGamEventInfo_isPassed__EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_AF2__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__EG_SCALE_AF2__1up, &b_HGamEventInfo_m_yy__EG_SCALE_AF2__1up);

   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.weight", &HGamEventInfo_weight__EG_SCALE_ALL__1down, &b_HGamEventInfo_weight__EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1down, &b_HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1down, &b_HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__EG_SCALE_ALL__1down, &b_HGamEventInfo_isPassed__EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__EG_SCALE_ALL__1down, &b_HGamEventInfo_m_yy__EG_SCALE_ALL__1down);

   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.weight", &HGamEventInfo_weight__EG_SCALE_ALL__1up, &b_HGamEventInfo_weight__EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1up, &b_HGamEventInfo_Hc_weightjvt__EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1up, &b_HGamEventInfo_Hc_weightCtag__EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__EG_SCALE_ALL__1up, &b_HGamEventInfo_isPassed__EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__EG_SCALE_ALL__1up, &b_HGamEventInfo_m_yy__EG_SCALE_ALL__1up);

   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn.weight", &HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1down, &b_HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1down, &b_HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1down, &b_HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1down, &b_HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1down, &b_HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1down);

   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn.weight", &HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1up, &b_HGamEventInfo_weight__PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1up, &b_HGamEventInfo_Hc_weightjvt__PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1up, &b_HGamEventInfo_Hc_weightCtag__PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1up, &b_HGamEventInfo_isPassed__PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1up, &b_HGamEventInfo_m_yy__PH_EFF_ID_Uncertainty__1up);

   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn.weight", &HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1down, &b_HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1down, &b_HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1down, &b_HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1down, &b_HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1down, &b_HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1down);

   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn.weight", &HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1up, &b_HGamEventInfo_weight__PH_EFF_ISO_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1up, &b_HGamEventInfo_Hc_weightjvt__PH_EFF_ISO_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1up, &b_HGamEventInfo_Hc_weightCtag__PH_EFF_ISO_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1up, &b_HGamEventInfo_isPassed__PH_EFF_ISO_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_ISO_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1up, &b_HGamEventInfo_m_yy__PH_EFF_ISO_Uncertainty__1up);

   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn.weight", &HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1down, &b_HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1down, &b_HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1down, &b_HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1down, &b_HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1down);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1down, &b_HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1down);

   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn.weight", &HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1up, &b_HGamEventInfo_weight__PH_EFF_TRIGGER_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1up, &b_HGamEventInfo_Hc_weightjvt__PH_EFF_TRIGGER_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1up, &b_HGamEventInfo_Hc_weightCtag__PH_EFF_TRIGGER_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1up, &b_HGamEventInfo_isPassed__PH_EFF_TRIGGER_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__PH_EFF_TRIGGER_Uncertainty__1up);
   fChain->SetBranchAddress("HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1up, &b_HGamEventInfo_m_yy__PH_EFF_TRIGGER_Uncertainty__1up);

   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1downAuxDyn.weight", &HGamEventInfo_weight__PRW_DATASF__1down, &b_HGamEventInfo_weight__PRW_DATASF__1down);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1downAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PRW_DATASF__1down, &b_HGamEventInfo_Hc_weightjvt__PRW_DATASF__1down);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1downAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PRW_DATASF__1down, &b_HGamEventInfo_Hc_weightCtag__PRW_DATASF__1down);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1downAuxDyn.isPassed", &HGamEventInfo_isPassed__PRW_DATASF__1down, &b_HGamEventInfo_isPassed__PRW_DATASF__1down);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1downAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1down, &b_HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1down);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1downAuxDyn.m_yy", &HGamEventInfo_m_yy__PRW_DATASF__1down, &b_HGamEventInfo_m_yy__PRW_DATASF__1down);

   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1upAuxDyn.weight", &HGamEventInfo_weight__PRW_DATASF__1up, &b_HGamEventInfo_weight__PRW_DATASF__1up);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1upAuxDyn.Hc_weightjvt", &HGamEventInfo_Hc_weightjvt__PRW_DATASF__1up, &b_HGamEventInfo_Hc_weightjvt__PRW_DATASF__1up);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1upAuxDyn.Hc_weightCtag", &HGamEventInfo_Hc_weightCtag__PRW_DATASF__1up, &b_HGamEventInfo_Hc_weightCtag__PRW_DATASF__1up);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1upAuxDyn.isPassed", &HGamEventInfo_isPassed__PRW_DATASF__1up, &b_HGamEventInfo_isPassed__PRW_DATASF__1up);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1upAuxDyn.Hc_Atleast1jisloose", &HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1up, &b_HGamEventInfo_Hc_Atleast1jisloose__PRW_DATASF__1up);
   fChain->SetBranchAddress("HGamEventInfo_PRW_DATASF__1upAuxDyn.m_yy", &HGamEventInfo_m_yy__PRW_DATASF__1up, &b_HGamEventInfo_m_yy__PRW_DATASF__1up);

   Notify();
}

Bool_t Reader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Reader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Reader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Reader_cxx
