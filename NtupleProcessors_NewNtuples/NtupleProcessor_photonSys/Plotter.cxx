#include "Plotter.h"
#include "rootlogonATLAS.C"


Plotter::Plotter(const TString fileType, const bool seperateSignal, const bool seperateUncertainties, const bool colouredSignal)
  : m_fileType(fileType), m_seperateSignal(seperateSignal), m_seperateUncertainties(seperateUncertainties), m_colouredSignal(colouredSignal)
{
  // Official ATLAS style file
  rootlogonATLAS();

  // Set pwd
  char cwd[1024];
  getcwd(cwd, sizeof(cwd));
  m_path = (TString) cwd;
  system("mkdir "+m_path+"/Plots");
  for(std::vector<int>::iterator cut = ControlHist::cuts.begin(); cut != ControlHist::cuts.end(); ++cut) system("mkdir Plots/"+ControlHist::int2string_cut[*cut]);
}


Plotter::~Plotter()
{
}


void Plotter::Plot(ControlHist *chist, const int cut, const TString option, const TString fitFunction)
{
  duplicateBkgdHist==NULL;
  arrows.clear();

  const TString plotName = chist->GetName();
  const double plotMaxY = chist->GetMaxY();
  const double plotMinY = chist->GetMinY();
  const double plotHeightModifier = 1.+0.25*TMath::Max(chist->samples.size()-2., 2.);
  
  if(ControlHist::samples_data.size()>1)
    {
      std::cout<<"ERROR in Plotter::Plot: More than 1 data sample"<<std::endl;
      return;
    }

  if(ControlHist::samples_background.size()!=1 and m_seperateUncertainties)
    {
      std::cout<<"ERROR in Plotter::Plot: !=1 background samples and seperate uncertainties"<<std::endl;
      return;
    }

  if(not chist->is2D())
    {
      m_hasRatio = option.Contains("ratio") or fitFunction!="";

      if(not m_hasRatio) pad1Scale=1;

      TCanvas *canvas = new TCanvas("canvas","",600,600);

      canvas->Clear();

      if(option.Contains("log")) canvas->SetLogy(true);
      else canvas->SetLogy(false);

      int sample0;
      if(chist->samples_data.size()>0)
        sample0=chist->samples_data.at(0);
      else if(chist->samples_signal.size()>0)
        sample0=chist->samples_signal.at(0);
      else if(chist->samples_background.size()>0)
        sample0=chist->samples_background.at(0);

      TPad *padMain = new TPad(); // only needed for ratio option
      TString xTitle; // only needed for ratio option
      if(m_hasRatio)
        {
          padMain->SetPad(0, pad2Scale, 1, 1);
          padMain->SetTickx(1);
          padMain->SetTicky(1);
          padMain->SetFillColor(kWhite);
          padMain->SetTopMargin(0.07/pad1Scale);
          padMain->SetRightMargin(0.07);
          padMain->SetBottomMargin(0);
          padMain->SetLeftMargin(0.17);
          padMain->Draw();
          padMain->cd();
          xTitle=chist->GetHist1D(sample0, cut)->GetXaxis()->GetTitle();
        }
      else
        {
          canvas->SetTickx(1);
          canvas->SetTicky(1);
          canvas->SetFillColor(kWhite);
          canvas->SetTopMargin(0.07);
          canvas->SetRightMargin(0.07);
          canvas->SetBottomMargin(0.12);
          canvas->SetLeftMargin(0.12);
          xTitle=chist->GetHist1D(sample0, cut)->GetXaxis()->GetTitle();
        }

      TLatex latex;
      latex.SetTextSize(textSize/pad1Scale);
      latex.SetNDC();

      TGaxis::SetMaxDigits(3);

      TLegend *leg;
      if(m_hasRatio) leg = new TLegend(0.73, 0.8325-0.052*chist->samples.size(), 0.88, 0.84);
      else leg = new TLegend(0.73, 0.885-0.052*chist->samples.size(), 0.88, 0.8925);
      leg->SetBorderSize(0);
      leg->SetFillColor(0);
      leg->SetTextSize(textSize/pad1Scale);
      // leg->SetTextSize(textSize/pad1Scale);
      leg->SetFillStyle(1000);
      leg->SetFillColorAlpha(0, 0);
      leg->SetTextFont(42);
      leg->SetNColumns(1);

      TString stackName=""; stackName+=";"; stackName+=chist->GetHist1D(sample0, cut)->GetXaxis()->GetTitle(); stackName+=";"; stackName+=chist->GetHist1D(sample0, cut)->GetYaxis()->GetTitle();

      THStack *stack = new THStack("stack", stackName);

      //////////////////////////////////////////////////////////////////////////
      // Prepare histograms and stack

      double dataNorm = -1;

      for(std::vector<int>::const_iterator sa=ControlHist::samples_data.begin(); sa!=ControlHist::samples_data.end(); ++sa)
        {
          int sample=*sa;

          // Set any -ve bins to 0
          for(int i=0; i<chist->GetHist1D(sample, cut)->GetNbinsX(); i++)
            if(chist->GetHist1D(sample, cut)->GetBinContent(i)<0)
              chist->GetHist1D(sample, cut)->SetBinContent(i, 0.);

          if(dataNorm==-1) dataNorm=chist->GetHist1D(sample, cut)->Integral();
          else std::cout<<"Plotter: ERROR - Multiple datasets not supported"<<std::endl;
            
          if(option.Contains("normall") and dataNorm!=0) chist->GetHist1D(sample, cut)->Scale(1/dataNorm);

          if(m_hasRatio) chist->GetHist1D(sample, cut)->SetTitle("");

          chist->GetHist1D(sample, cut)->SetStats(false);
          chist->GetHist1D(sample, cut)->SetLineColor(1);
          chist->GetHist1D(sample, cut)->SetLineWidth(lineWidth);
          leg->AddEntry(chist->GetHist1D(sample, cut), ControlHist::int2displayString_sample[sample], "p");
        }

      double stackNorm=0;
      if(option.Contains("normall"))
        for(std::vector<int>::const_iterator sa=ControlHist::samples_background.begin(); sa!=ControlHist::samples_background.end(); ++sa)
          {
            int sample=*sa;
            stackNorm+=chist->GetHist1D(sample, cut)->Integral();
          }
  
      int bkgdCount=ControlHist::samples_background.size()+1;
      for(std::vector<int>::const_iterator sa=ControlHist::samples_background.begin(); sa!=ControlHist::samples_background.end(); ++sa)
        {
          int sample=*sa;

          if(option.Contains("normall") and stackNorm!=0)
            chist->GetHist1D(sample, cut)->Scale(1/stackNorm);

          if(option.Contains("normbkgd") and stackNorm!=0)
            chist->GetHist1D(sample, cut)->Scale(dataNorm/stackNorm);

          if(m_hasRatio) chist->GetHist1D(sample, cut)->SetTitle("");

          chist->GetHist1D(sample, cut)->SetStats(false);
          chist->GetHist1D(sample, cut)->SetLineWidth(lineWidth);

          if(m_seperateUncertainties)
            {
              chist->GetHist1D(sample, cut)->SetLineColor(1);
              chist->GetHist1D(sample, cut)->SetFillColor(0);
              chist->GetHist1D(sample, cut)->SetFillColorAlpha(0, 0.);

              stack->Add(chist->GetHist1D(sample, cut));

              duplicateBkgdHist = (TH1D*) chist->GetHist1D(sample, cut)->Clone("duplicateBkgdHist");
              duplicateBkgdHist->SetLineStyle(1);
              duplicateBkgdHist->SetLineColor(0);
              duplicateBkgdHist->SetLineWidth(0);
              duplicateBkgdHist->SetFillStyle(3004);
              // duplicateBkgdHist->SetFillStyle(3145);
              // gStyle->SetHatchesLineWidth(10);
              // gStyle->SetHatchesSpacing(0.1);
              duplicateBkgdHist->SetFillColor(1);
              // duplicateBkgdHist->SetFillColorAlpha(1, 1.);
            }
          else
            {
              if(ControlHist::samples_background.size()==1)
                {
                  chist->GetHist1D(sample, cut)->SetLineColor(1);
                  chist->GetHist1D(sample, cut)->SetFillColor(0);
                  chist->GetHist1D(sample, cut)->SetFillColorAlpha(0, 0.);
                }
              else
                {
                  chist->GetHist1D(sample, cut)->SetLineColor(colorMapBkgd[bkgdCount]);
                  chist->GetHist1D(sample, cut)->SetFillColor(colorMapBkgd[bkgdCount]);
                  chist->GetHist1D(sample, cut)->SetFillColorAlpha(colorMapBkgd[bkgdCount], 0.);
                  chist->GetHist1D(sample, cut)->SetFillStyle(1001);              
                }
              stack->Add(chist->GetHist1D(sample, cut));
            }

          bkgdCount--;

          leg->AddEntry(chist->GetHist1D(sample, cut), ControlHist::int2displayString_sample[sample], (m_seperateUncertainties or ControlHist::samples_background.size()==1) ? "l" : "f");
        }

      int signalCount=1;
      for(std::vector<int>::const_iterator sa=ControlHist::samples_signal.begin(); sa!=ControlHist::samples_signal.end(); ++sa)
        {
          int sample=*sa;

          if(option.Contains("normall"))
            {
              double norm=chist->GetHist1D(sample, cut)->Integral();
              if(norm!=0) chist->GetHist1D(sample, cut)->Scale(1/norm);
            }

          if(m_hasRatio) chist->GetHist1D(sample, cut)->SetTitle("");

          chist->GetHist1D(sample, cut)->SetStats(false);
          chist->GetHist1D(sample, cut)->SetLineColor(1);
          chist->GetHist1D(sample, cut)->SetLineWidth(lineWidth);
          if(m_colouredSignal) chist->GetHist1D(sample, cut)->SetLineColor(colorMap[signalCount]);
          if(ControlHist::samples_background.size()>0 or ControlHist::samples_data.size()>0)
            chist->GetHist1D(sample, cut)->SetLineStyle(1);
            // chist->GetHist1D(sample, cut)->SetLineStyle(signalCount+1);
          else
            chist->GetHist1D(sample, cut)->SetLineStyle(1);
          signalCount++;
          chist->GetHist1D(sample, cut)->SetFillColor(0);
          chist->GetHist1D(sample, cut)->SetFillStyle(1001);
          if(not m_seperateSignal)
            stack->Add(chist->GetHist1D(sample, cut));

          leg->AddEntry(chist->GetHist1D(sample, cut), ControlHist::int2displayString_sample[sample], "l");
        }


      //////////////////////////////////////////////////////////////////////////
      // Draw histograms

      // Find tallest signal
      double maxSignalHeight=0;
      if(m_seperateSignal)
        {
          for(std::vector<int>::const_iterator sa=ControlHist::samples_signal.begin(); sa!=ControlHist::samples_signal.end(); ++sa)
            {
              int sample = *sa;
              double signalHeight=chist->GetHist1D(sample, cut)->GetMaximum();
              if(maxSignalHeight<signalHeight) maxSignalHeight=signalHeight;
            }
        }

      if(ControlHist::samples_data.size()>0)
        {
          int sample=ControlHist::samples_data.at(0);

          if(option.Contains("log")) padMain->SetLogy(true);
          else padMain->SetLogy(false);

          if(plotMinY!=-999) chist->GetHist1D(sample, cut)->SetMinimum(plotMinY);
          else if(option.Contains("log")) chist->GetHist1D(sample, cut)->SetMinimum(logHistMin);
          else chist->GetHist1D(sample, cut)->SetMinimum(linHistMin);

          if(plotMaxY!=-999)
            chist->GetHist1D(sample, cut)->SetMaximum(plotMaxY);
          else if(option.Contains("log"))
            chist->GetHist1D(sample, cut)->SetMaximum(TMath::Power(TMath::Max(chist->GetHist1D(sample, cut)->GetMaximum(), TMath::Max(stack->GetMaximum(), maxSignalHeight)), plotHeightModifier)/TMath::Power(logHistMin, plotHeightModifier-1.));
          else
            chist->GetHist1D(sample, cut)->SetMaximum(plotHeightModifier*TMath::Max(chist->GetHist1D(sample, cut)->GetMaximum(), TMath::Max(stack->GetMaximum(), maxSignalHeight)));

          chist->GetHist1D(sample, cut)->SetMarkerStyle(20);

          if(m_hasRatio)
            {
              chist->GetHist1D(sample, cut)->GetYaxis()->ChangeLabel(1,-1,0);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelFont(42);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelSize(textSize/pad1Scale);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleSize(textSize/pad1Scale);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleOffset(1.25);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleFont(42);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTickLength(tickLength/pad1Scale);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTickLength(tickLength);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelFont(42);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelSize(textSize/pad1Scale);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleSize(textSize/pad1Scale);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleOffset(2*pad1Scale);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleFont(42);
            }
          else
            {
              chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelFont(42);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelSize(textSize);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleSize(textSize);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleOffset(1.25);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleFont(42);
              chist->GetHist1D(sample, cut)->GetXaxis()->SetTickLength(tickLength);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTickLength(tickLength);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelFont(42);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelSize(textSize);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleSize(textSize);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleOffset(1.5);
              chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleFont(42);
            }

          chist->GetHist1D(sample, cut)->Draw("epx0");

          stack->Draw("histsamex0");

          if(m_seperateUncertainties and duplicateBkgdHist!=NULL)
            {
              duplicateBkgdHist->Draw("e2same");
            }

          if(m_seperateSignal)
            {
              for(std::vector<int>::const_iterator sa=ControlHist::samples_signal.begin(); sa!=ControlHist::samples_signal.end(); ++sa)
                {
                  int sample2=*sa;
                  chist->GetHist1D(sample2, cut)->Draw("histx0same");
                }
            }
          chist->GetHist1D(sample, cut)->Draw("epx0same");
        }
      else
        {
          bool isStack=ControlHist::samples_background.size()>0 or (not m_seperateSignal and ControlHist::samples_signal.size()>0);

          if(isStack)
            {
              if(option.Contains("log")) padMain->SetLogy(true);
              else padMain->SetLogy(false);

              if(plotMinY!=-999) stack->SetMinimum(plotMinY);
              // if(plotMinY!=-999) stack->GetHistogram()->GetYaxis()->SetRangeUser(0.00001, 100.);
              else if(option.Contains("log")) stack->SetMinimum(logHistMin);
              else stack->SetMinimum(linHistMin);

              if(plotMaxY>0)
                stack->SetMaximum(plotMaxY);
              else if(option.Contains("log"))
                stack->SetMaximum(TMath::Power(TMath::Max(stack->GetMaximum(), maxSignalHeight), plotHeightModifier)/TMath::Power(logHistMin, plotHeightModifier-1.));
              else
                stack->SetMaximum(plotHeightModifier*TMath::Max(stack->GetMaximum(), maxSignalHeight));
                // stack->SetMaximum((0.55+TMath::Max(0.13*(chist->samples.size()-2), 0.))*TMath::Max(stack->GetMaximum(), maxSignalHeight));
              
              stack->Draw("histx0");

              if(m_hasRatio)
                {
                  stack->GetYaxis()->ChangeLabel(1,-1,0);
                  stack->GetXaxis()->SetLabelFont(42);
                  stack->GetXaxis()->SetLabelSize(textSize/pad1Scale);
                  stack->GetXaxis()->SetTitleSize(textSize/pad1Scale);
                  stack->GetXaxis()->SetTitleOffset(1.25);
                  stack->GetXaxis()->SetTitleFont(42);
                  stack->GetXaxis()->SetTickLength(tickLength/pad1Scale);
                  stack->GetYaxis()->SetTickLength(tickLength);
                  stack->GetYaxis()->SetLabelFont(42);
                  stack->GetYaxis()->SetLabelSize(textSize/pad1Scale);
                  stack->GetYaxis()->SetTitleSize(textSize/pad1Scale);
                  stack->GetYaxis()->SetTitleOffset(2*pad1Scale);
                  stack->GetYaxis()->SetTitleFont(42);
                }
              else
                {
                  stack->GetXaxis()->SetLabelFont(42);
                  stack->GetXaxis()->SetLabelSize(textSize);
                  stack->GetXaxis()->SetTitleSize(textSize);
                  stack->GetXaxis()->SetTitleOffset(1.25);
                  stack->GetXaxis()->SetTitleFont(42);
                  stack->GetXaxis()->SetTickLength(tickLength);
                  stack->GetYaxis()->SetTickLength(tickLength);
                  stack->GetYaxis()->SetLabelFont(42);
                  stack->GetYaxis()->SetLabelSize(textSize);
                  stack->GetYaxis()->SetTitleSize(textSize);
                  stack->GetYaxis()->SetTitleOffset(1.5);
                  stack->GetYaxis()->SetTitleFont(42);
                }              
            }

          bool isFirstSignal=true;
          for(std::vector<int>::const_iterator sa=ControlHist::samples_signal.begin(); sa!=ControlHist::samples_signal.end(); ++sa)
            {
              int sample=*sa;
              if(not isStack and isFirstSignal)
                {
                  if(option.Contains("log")) padMain->SetLogy(true);
                  else padMain->SetLogy(false);

                  if(plotMinY!=-999) chist->GetHist1D(sample, cut)->SetMinimum(plotMinY);
                  else if(option.Contains("log")) chist->GetHist1D(sample, cut)->SetMinimum(logHistMin);
                  else chist->GetHist1D(sample, cut)->SetMinimum(linHistMin);

                  if(plotMaxY>0)
                    chist->GetHist1D(sample, cut)->SetMaximum(plotMaxY);
                  else if(option.Contains("log"))
                    chist->GetHist1D(sample, cut)->SetMaximum(TMath::Power(TMath::Max(stack->GetMaximum(), maxSignalHeight), plotHeightModifier)/TMath::Power(logHistMin, plotHeightModifier-1.));
                  else
                    chist->GetHist1D(sample, cut)->SetMaximum(plotHeightModifier*TMath::Max(stack->GetMaximum(), maxSignalHeight));
                    // chist->GetHist1D(sample, cut)->SetMaximum((0.55+TMath::Max(0.13*(chist->samples.size()-2), 0.))*TMath::Max(stack->GetMaximum(), maxSignalHeight));
                  chist->GetHist1D(sample, cut)->Draw("histx0");

                  if(m_hasRatio)
                    {
                      chist->GetHist1D(sample, cut)->GetYaxis()->ChangeLabel(1,-1,0);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelFont(42);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelSize(textSize/pad1Scale);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleSize(textSize/pad1Scale);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleOffset(1.25);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleFont(42);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTickLength(tickLength/pad1Scale);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTickLength(tickLength);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelFont(42);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelSize(textSize/pad1Scale);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleSize(textSize/pad1Scale);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleOffset(2*pad1Scale);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleFont(42);
                    }
                  else
                    {
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelFont(42);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetLabelSize(textSize);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleSize(textSize);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleOffset(1.25);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTitleFont(42);
                      chist->GetHist1D(sample, cut)->GetXaxis()->SetTickLength(tickLength);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTickLength(tickLength);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelFont(42);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetLabelSize(textSize);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleSize(textSize);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleOffset(1.5);
                      chist->GetHist1D(sample, cut)->GetYaxis()->SetTitleFont(42);
                    }                  
                }
              else
                {
                  chist->GetHist1D(sample, cut)->Draw("histx0same");
                }

              isFirstSignal=false;
            }

          if(m_seperateUncertainties and duplicateBkgdHist!=NULL)
            {
              duplicateBkgdHist->Draw("e2");
            }
        }

      if(not option.Contains("noleg")) leg->Draw();

      if(m_hasRatio)
        {
          if(option.Contains("AWIP"))
            {
              if(chist->samples_data.size()>0) latex.DrawLatex(0.22, 0.8, "#it{#bf{ATLAS}} Internal");
              else latex.DrawLatex(0.22, 0.8, "#it{#bf{ATLAS}} Simulation Internal");
            }
          else
            {
              if(chist->samples_data.size()>0) latex.DrawLatex(0.22, 0.8, "#it{#bf{ATLAS}}");
              else latex.DrawLatex(0.22, 0.8, "#it{#bf{ATLAS}} Simulation");
            }
            
          if(option.Contains("labels")) latex.DrawLatex(0.22, 0.73, "#sqrt{s}=13 TeV, 139 fb^{-1}");
          if(option.Contains("LTT")) latex.DrawLatex(0.22, 0.66, "LTT");
          if(option.Contains("SLT")) latex.DrawLatex(0.22, 0.66, "SLT");
        }
      else
        {
          if(option.Contains("AWIP"))
            {
              if(chist->samples_data.size()>0) latex.DrawLatex(0.17, 0.855, "#it{#bf{ATLAS}} Internal");
              else latex.DrawLatex(0.17, 0.855, "#it{#bf{ATLAS}} Simulation Internal");
            }
          else
            {
              if(chist->samples_data.size()>0) latex.DrawLatex(0.17, 0.855, "#it{#bf{ATLAS}}");
              else latex.DrawLatex(0.17, 0.855, "#it{#bf{ATLAS}} Simulation");
            }

          if(option.Contains("labels")) latex.DrawLatex(0.17, 0.805, "#sqrt{s}=13 TeV, 139 fb^{-1}");
          if(option.Contains("LTT")) latex.DrawLatex(0.17, 0.755, "LTT");
          if(option.Contains("SLT")) latex.DrawLatex(0.17, 0.755, "SLT");
        }

      // Fit function if requested
      TF1 fit = TF1("fit", fitFunction);
      if(fitFunction!="")
        {
          TH1D *hist = chist->GetTotalHist1D(cut);
          // hist->Fit(&fit, "", "");
          // fit.Draw("same");
        }

      gPad->RedrawAxis();

      TPad *padRatio = new TPad(); // only needed for ratio option
      padRatio->SetTickx(1);
      padRatio->SetTicky(1);
      TH1D *ratio; // only needed for ratio option
      TF1 oneFn; // only needed for ratio option
      TH1D *one; // only needed for ratio option
      TLegend *legRatio;
      if(m_hasRatio)
        {
          canvas->cd();
          padRatio->SetPad(0, 0, 1, pad2Scale);
          padRatio->SetFillColor(kWhite);
          padRatio->SetRightMargin(0.07);
          padRatio->SetTopMargin(0);
          padRatio->SetBottomMargin(0.11/pad2Scale);
          padRatio->SetLeftMargin(0.17);
          padRatio->Draw();
          padRatio->cd();

          ratio=static_cast<TH1D*>(chist->GetHist1D(sample0, cut)->Clone("ratio"));
          TH1D *sum=static_cast<TH1D*>(ratio->Clone("sum"));
          sum->Reset("ICES");
          if(fitFunction!="")
            {
              TList *histos=stack->GetHists();
              TIter next(histos);
              TH1D *hist;
              while((hist=(TH1D*) next())) sum->Add(hist);
              ratio=sum;
              for(int i=1; i<=ratio->GetNbinsX(); i++)
                {
                  double x = ratio->GetBinCenter(i);
                  double y = ratio->GetBinContent(i);
                  double ey = ratio->GetBinError(i);
                  y/=fit.Eval(x);
                  ey/=fit.Eval(x);
                  ratio->SetBinContent(i, y);
                  ratio->SetBinError(i, ey);
                }
            }
          else
            {
              TList *histos=stack->GetHists();
              TIter next(histos);
              TH1D *hist;
              while((hist=(TH1D*) next())) sum->Add(hist);
              ratio->Divide(sum);
              if(m_seperateUncertainties)
                {
                  for(int i=1; i<=ratio->GetNbinsX(); i++)
                    {
                      ratio->SetBinError(i, chist->GetHist1D(sample0, cut)->GetBinError(i)/sum->GetBinContent(i));
                    }
                }
            }
          
          ratio->SetMarkerStyle(20);
          ratio->SetLineColor(1);
          ratio->SetLineWidth(lineWidth);
          ratio->SetStats(0);
          ratio->GetYaxis()->SetNdivisions(505);
          ratio->GetYaxis()->SetTitle("Data / Bkgd ");
          ratio->GetYaxis()->SetTitleSize(textSize/pad2Scale);
          ratio->GetYaxis()->SetTitleFont(42);
          ratio->GetYaxis()->SetTitleOffset(2*pad2Scale*fudgeFactor2);
          ratio->GetYaxis()->SetLabelFont(42);
          ratio->GetYaxis()->SetLabelSize(textSize/pad2Scale);
          ratio->GetXaxis()->SetTickLength(tickLength/pad2Scale);
          ratio->GetYaxis()->SetTickLength(tickLength*fudgeFactor1);
          ratio->GetXaxis()->SetTitle(xTitle);
          ratio->GetXaxis()->SetTitleSize(textSize/pad2Scale);
          ratio->GetXaxis()->SetTitleOffset(1.25);
          ratio->GetXaxis()->SetTitleFont(42);
          ratio->GetXaxis()->SetLabelFont(42);
          ratio->GetXaxis()->SetLabelSize(textSize/pad2Scale);
          ratio->Draw("epx0");

          oneFn=TF1("M", "1", -1000000., 1000000.);
          oneFn.SetLineColor(1);
          oneFn.SetLineWidth(lineWidth);
          oneFn.Draw("same");

          if(m_seperateUncertainties)
            {
              one = (TH1D*) ratio->Clone("one");
              one->SetFillStyle(3004);
              one->SetLineColor(1);
              one->SetFillColor(1);
              one->SetMarkerStyle(0);
              for(int i=1; i<=one->GetNbinsX(); i++)
                {
                  one->SetBinContent(i, 1.);
                  if(chist->GetHist1D(sample0, cut)->GetBinContent(i)==0 and sum->GetBinContent(i)==0) one->SetBinError(i,0);
                  else one->SetBinError(i, sum->GetBinError(i)/sum->GetBinContent(i));
                }
              one->Draw("e2same");

              legRatio = new TLegend(0.32, 0.445, 0.47, 0.575);
              legRatio->SetBorderSize(0);
              legRatio->SetFillColor(0);
              legRatio->SetTextSize(textSize/pad2Scale);
              legRatio->SetFillStyle(1000);
              legRatio->SetFillColorAlpha(0, 0);
              legRatio->SetTextFont(42);
              legRatio->SetNColumns(1);
              TLegendEntry *entry = legRatio->AddEntry(duplicateBkgdHist, "Bkgd MC Stat", "f");
              entry->SetLineStyle(1);
              entry->SetLineColor(0);
              entry->SetLineWidth(0);
              entry->SetFillStyle(3004);
              entry->SetFillColor(1);
              if(not option.Contains("noleg")) legRatio->Draw();
            }

          if(option.Contains("fixedratio"))
            {
              ratio->GetYaxis()->SetRangeUser(1-fixedRatio, 1+fixedRatio);

              for(int i=1; i<=ratio->GetNbinsX(); i++)
                {
                  if(chist->GetHist1D(sample0, cut)->GetBinContent(i)==0 and sum->GetBinContent(i)==0)
                    {
                      continue;
                    }
                  else if(ratio->GetBinContent(i)>1+fixedRatio)
                    {
                      arrows.push_back(new TArrow(ratio->GetBinCenter(i),1.6,ratio->GetBinCenter(i),2.16,0.02,"|>"));
                      arrows.at(arrows.size()-1)->SetAngle(40);
                      arrows.at(arrows.size()-1)->SetLineWidth(2);
                      arrows.at(arrows.size()-1)->Draw(); 
                   }
                  else if(ratio->GetBinContent(i)<1-fixedRatio)
                    {
                      arrows.push_back(new TArrow(ratio->GetBinCenter(i),0.4,ratio->GetBinCenter(i),-0.16,0.02,"|>"));
                      arrows.at(arrows.size()-1)->SetAngle(40);
                      arrows.at(arrows.size()-1)->SetLineWidth(2);
                      arrows.at(arrows.size()-1)->Draw();
                    }
                }
            }
          else
            {
              double yScale=GetScale(ratio, 5);
              ratio->GetYaxis()->SetRangeUser(1-yScale, 1+yScale);
            }
        }

      if(m_fileType=="All")
        {
          canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+".png");
          canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+".C");
          canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+".eps");
          canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+".pdf");
        }
      else
        {
          canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+"."+m_fileType);
        }

      if(not m_hasRatio) pad1Scale=0.7;

      delete leg;
      if(m_hasRatio and m_seperateUncertainties) delete legRatio;
      delete stack;
      delete padRatio;
      delete padMain;
      delete canvas;
    }
  else
    {
      for(std::vector<int>::const_iterator sa=chist->samples.begin(); sa!=chist->samples.end(); ++sa)
        {
          int sample=*sa;

          TCanvas *canvas = new TCanvas("canvas", "", 600, 600);
          canvas->Clear();
          canvas->SetTopMargin(0.07);
          canvas->SetRightMargin(0.17);
          canvas->SetBottomMargin(0.12);
          canvas->SetLeftMargin(0.12);

          TLatex latex;
          latex.SetNDC();

          TGaxis::SetMaxDigits(3);

          gStyle->SetPaintTextFormat("0.1f");

          //////////////////////////////////////////////////////////////////////////
          // Set any -ve bins to 0

          // for(int i=0; i<chist->GetHist2D(sample, cut)->GetNbinsX(); i++)
          //   for(int ii=0; ii<chist->GetHist2D(sample, cut)->GetNbinsY(); ii++)
          //     if(chist->GetHist2D(sample, cut)->GetBinContent(i, ii)<0)
          //       chist->GetHist2D(sample, cut)->SetBinContent(i, ii, 0.);

          //////////////////////////////////////////////////////////////////////////
          // Prepare histograms and stack

          if(option.Contains("normall"))
            {
              double norm=chist->GetHist2D(sample, cut)->Integral();
              if(norm!=0) chist->GetHist2D(sample, cut)->Scale(1/norm);
            }

          chist->GetHist2D(sample, cut)->SetStats(false);

          //////////////////////////////////////////////////////////////////////////
          // Draw histograms
          
          if(option.Contains("textBins"))
            chist->GetHist2D(sample, cut)->Draw("colztext");
          else
            chist->GetHist2D(sample, cut)->Draw("colz");

          if(option.Contains("AWIP"))
            {
              if(chist->samples_data.size()>0) latex.DrawLatex(0.22,0.84,"#it{#bf{ATLAS}} Internal");
              else latex.DrawLatex(0.22,0.84,"#it{#bf{ATLAS}} Simulation Internal");
            }

          gPad->RedrawAxis();

          if(m_fileType=="All")
            {
              canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+"_"+ControlHist::int2string_sample[sample]+".png");
              canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+"_"+ControlHist::int2string_sample[sample]+".C");
              canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+"_"+ControlHist::int2string_sample[sample]+".eps");
              canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+"_"+ControlHist::int2string_sample[sample]+".pdf");
            }
          else
            {
              canvas->SaveAs(m_path+"/Plots/"+ControlHist::int2string_cut[cut]+"/"+plotName+"_"+ControlHist::int2string_sample[sample]+"."+m_fileType);
            }

          delete canvas;
        }
    }
}


double Plotter::GetScale(TH1D *hist, double fudgeFactor) const
{
  std::vector<double> errors;

  for(int bin=1; bin<=hist->GetNbinsX(); bin++) // Find avg error
    if(hist->GetBinError(bin)!=0)
      errors.push_back(hist->GetBinError(bin));

  if(errors.size()==0)
    {
      return 1.;
    }
  else
    {
      std::sort(errors.begin(), errors.end());
      double avgErr=errors[(int) errors.size()/2.];
      double avg=0;
      int n=0;

      for(int bin=1; bin<=hist->GetNbinsX(); bin++) // Find avg deviation from 1 for bins with error less than average
	if(hist->GetBinError(bin)<avgErr && hist->GetBinError(bin)!=0)
	  {
	    n++;
	    avg+=fabs(hist->GetBinContent(bin)-1.);
	  }
      avg/=n;

      double yScale=1.; // Set ratio y-axis scale to triple the average devation from 1 of bins with error<avg
      if(n>1)
	yScale=avg;

      return yScale*fudgeFactor;
    }
}


// // Used to use mean, now uses median
// double Plotter::GetScale(TH1D *hist) const
// {
//   double avgErr=0.;
//   double n=0;

//   for(int bin=1; bin<=hist->GetNbinsX(); bin++) // Find avg error
//     if(hist->GetBinError(bin)!=0)
//       {
// 	n++;
// 	avgErr+=hist->GetBinError(bin);
//       }
//   avgErr/=n;
//   n=0;
//   double avg=0.;

//   for(int bin=1; bin<=hist->GetNbinsX(); bin++) // Find avg deviation from 1 for bins with error less than average
//     if(hist->GetBinError(bin)<avgErr && hist->GetBinError(bin)!=0)
//       {
// 	n++;
// 	avg+=fabs(hist->GetBinContent(bin)-1.);
//       }
//   avg/=n;

//   double yScale=1.; // Set ratio y-axis scale to triple the average devation from 1 of bins with error<avg
//   if(n>1)
//     yScale=3.*avg;

//   return yScale;
// }
