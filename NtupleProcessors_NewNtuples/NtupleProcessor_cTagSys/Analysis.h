#include <vector>
#include <cmath>
#include <iostream>
#include <string>

#include "TRandom3.h"
#include "TFile.h"
#include "TH2.h"
#include "TF1.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"
#include "TVector.h"
#include "TError.h"
#include "TGraphErrors.h"
#include "RooFitResult.h"
#include "TH1.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
/* #include "RooMinuit.h"

/* #include "TMVA/Tools.h" */
/* #include "TMVA/Reader.h" */
/* #include "TMVA/MethodBDT.h" */

#include "Reader.h"
#include "Plotter.h"
#include "ControlHist.h"

/* #include "TMVA/Factory.h" */
/* #include "TMVA/DataLoader.h" */
/* #include "TMVA/Tools.h" */
/* #include "TMVA/TMVAGui.h" */
/* #include "TMVA/MethodCuts.h" */


// Name of output file path
const TString outputHistPath = "OutputHists";

// Main
int main(int argc, char* argv[]);

// Process samples
enum PileupProfile {MC16a, MC16d, MC16e};
void Process(const std::vector<TString> filePaths, const int sample, const bool isMC=false, const int pileupProfile=PileupProfile::MC16a);
void ProcessAll();

// Initialise ntuple reader class
Reader *InitReader(const TString filePath);

// Alternative method to gather weights
float GetSumWeights(std::vector<TString> filePaths, const int sample, const int index);

// Plotter class
const TString plotter_fileType="png";
const bool plotter_seperateSignal=false;
const bool plotter_seperateUncertainties=false;
const bool plotter_coloredSignal=false;
Plotter *plotter;

// Fixed parameters
const float massZ=91;
const float massPion=0.13957018;
const float massElectron = 0.0005109989461; // in GeV
const float massMuon = 0.1056583745; // in GeV
const float massEtac = 2.9839; // in GeV
const float massJpsi = 3.0969; // in GeV
const TString treeName = "CollectionTree";
/* const float lumi_MC16a=36240.; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger // what I had pre-25Jan23 */
const float lumi_MC16a=36214.; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger // changed 25Jan23 as per Anna message
const float lumi_MC16d=44310.; // 44316.2 nb^-1 was used before 04Jun19, possible derived by including triggers in lumicalc
const float lumi_MC16e=58450.; // 59937.2 nb^-1 pre-04Jun19
/* const float lumi_MC16a=36100.; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger */
/* const float lumi_MC16d=43700.; // 44316.2 nb^-1 was used before 04Jun19, possible derived by including triggers in lumicalc */
/* const float lumi_MC16e=59200.; // 59937.2 nb^-1 pre-04Jun19 */
// const float lumi_MC16a=3219.56+32994.9; // nb^-1 // numbers as per online lumicalc tool without triggers // 32995.4 for 2016 without any trigger
// const float lumi_MC16d=44307.4; // 44316.2 nb^-1 was used before 04Jun19, possible derived by including triggers in lumicalc
// const float lumi_MC16e=58450.1; // 59937.2 nb^-1 pre-04Jun19

// ControlHists
std::map<int, ControlHist*> ControlHists;
enum Sample {Data, CombinedBkgd, cH, ggFH, VBFH, WmH, WpH, ZH, ggZH, ttH, bbH, yy, tHjb, tWH, Total};
enum Cut {All, AllBlinded, SR1, MLP, MH, SR2, CR, VR1, VR2, VRC, R1, R2, RC};
enum ControlHistName {myy, DL1, DL1r};
int bkgdSample=-1;
int DefineMainBkgdSample();
void SetupControlHists();
void PlotControlHists();
void SaveControlHists(const TString name = "HZX_HM.root", std::vector<int> hists = {ControlHistName::myy});

void SetupSamples();
std::vector<int> samples;
std::map<int, TString> ControlHist::int2string_sample = {{Sample::Data, "Data"}, {Sample::cH, "cH"}, {Sample::CombinedBkgd, "CombinedBkgd"}, {Sample::VBFH, "VBFH"}, {Sample::ggFH, "ggFH"}, {Sample::WmH, "WmH"}, {Sample::WpH, "WpH"}, {Sample::ZH, "ZH"}, {Sample::ggZH, "ggZH"}, {Sample::ttH, "ttH"}, {Sample::bbH, "bbH"}, {Sample::yy, "yy"}, {Sample::tHjb, "tHjb"}, {Sample::tWH, "tWH"}};
std::map<int, TString> ControlHist::int2displayString_sample = {{Sample::Data, "Data"}, {Sample::cH, "H+c"}, {Sample::CombinedBkgd, "Background"}, {Sample::ggFH, "H (ggF)"}, {Sample::VBFH, "H (VBF)"}, {Sample::WmH, "W^{-}H"}, {Sample::WpH, "W^{+}H"}, {Sample::ZH, "ZH"}, {Sample::ggZH, "ggZH"}, {Sample::ttH, "t#bar{t}H"}, {Sample::bbH, "b#bar{b}H"}, {Sample::yy, "#gamma#gamma"}, {Sample::tHjb, "tHjb"}, {Sample::tWH, "tWH"}};
std::map<int, double> ControlHist::int2float_sample = {{Sample::Data, -999}, {Sample::cH, -999}, {Sample::CombinedBkgd, -999}};
std::map<TString, double> string2float_sample = {{"Data", -999}, {"cH", -999}, {"CombinedBkgd", -999}, {"ggFH", -999}, {"VBFH", -999}, {"WmH", -999}, {"WpH", -999}, {"ZH", -999}, {"ggZH", -999}, {"ttH", -999}, {"bbH", -999}, {"yy", -999}, {"tHjb", -999}, {"tWH", -999}};
std::map<int, TString> ControlHist::int2string_cut = {{Cut::All, "All"}, {Cut::AllBlinded, "AllBlinded"}};

std::map<int, TString> int2histName = {{Sample::cH, "CutFlow_MGPy8_cH_yc_Hgammagamma_noDalitz_weighted"}, {Sample::ggFH, "CutFlow_PowhegPy8_NNLOPS_ggH125_noDalitz_weighted"}, {Sample::VBFH, "CutFlow_PowhegPy8EG_NNPDF30_VBFH125_noDalitz_weighted"}, {Sample::WmH, "CutFlow_PowhegPy8_WmH125J_noDalitz_weighted"}, {Sample::WpH, "CutFlow_PowhegPy8_WpH125J_noDalitz_weighted"}, {Sample::ZH, "CutFlow_PowhegPy8_ZH125J_noDalitz_weighted"}, {Sample::ggZH, "CutFlow_PowhegPy8_ggZH125_noDalitz_weighted"}, {Sample::ttH, "CutFlow_PowhegPy8_ttH125_fixweight_noDalitz_weighted"}, {Sample::bbH, "CutFlow_PowhegPy8_bbH125_noDalitz_weighted"}, {Sample::yy, "CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"}, {Sample::tHjb, "CutFlow_aMCnloPy8_tHjb125_4fl_noDalitz_weighted"}, {Sample::tWH, "CutFlow_aMCnloPy8_tWH125_noDalitz_weighted"}};

// Evaluate efficiencies
void PrintEfficiencies();
void PrintSys4WS();
void PrintStat4WS();
void PrintHerwig4WS();
void MakeEfficiencyPlots();
std::map<int,float> initialNEvents; // mapping from sample
std::map<std::pair<int,int>,float> finalNEvents; // mapping from cut and sample
std::map<std::pair<int,int>,float> finalNEventsSq; // mapping from cut and sample
std::map<std::pair<int,int>,float> correctXEvents; // mapping from cut and sample
std::map<std::pair<int,int>,float> correctXEventsSq; // mapping from cut and sample

// Systematics
enum Systematics {Nominal, FT_EFF_Eigen_B_0_CTag__1down, FT_EFF_Eigen_B_0_CTag__1up, FT_EFF_Eigen_B_1_CTag__1down, FT_EFF_Eigen_B_1_CTag__1up, FT_EFF_Eigen_B_2_CTag__1down, FT_EFF_Eigen_B_2_CTag__1up, FT_EFF_Eigen_C_0_CTag__1down, FT_EFF_Eigen_C_0_CTag__1up, FT_EFF_Eigen_C_1_CTag__1down, FT_EFF_Eigen_C_1_CTag__1up, FT_EFF_Eigen_C_2_CTag__1down, FT_EFF_Eigen_C_2_CTag__1up, FT_EFF_Eigen_Light_0_CTag__1down, FT_EFF_Eigen_Light_0_CTag__1up, FT_EFF_Eigen_Light_1_CTag__1down, FT_EFF_Eigen_Light_1_CTag__1up, FT_EFF_Eigen_Light_2_CTag__1down, FT_EFF_Eigen_Light_2_CTag__1up, FT_EFF_extrapolation_from_charm__1down, FT_EFF_extrapolation_from_charm__1up, ttbar_PowHW7__1down, ttbar_PowHW7__1up, ttbar_aMcPy8__1down, ttbar_aMcPy8__1up};
std::map<int, TString> sysStrings = {{Systematics::Nominal, "Nominal"}, {Systematics::FT_EFF_Eigen_B_0_CTag__1down, "FT_EFF_Eigen_B_0_CTag__1down"}, {Systematics::FT_EFF_Eigen_B_0_CTag__1up, "FT_EFF_Eigen_B_0_CTag__1up"}, {Systematics::FT_EFF_Eigen_B_1_CTag__1down, "FT_EFF_Eigen_B_1_CTag__1down"}, {Systematics::FT_EFF_Eigen_B_1_CTag__1up, "FT_EFF_Eigen_B_1_CTag__1up"}, {Systematics::FT_EFF_Eigen_B_2_CTag__1down, "FT_EFF_Eigen_B_2_CTag__1down"}, {Systematics::FT_EFF_Eigen_B_2_CTag__1up, "FT_EFF_Eigen_B_2_CTag__1up"}, {Systematics::FT_EFF_Eigen_C_0_CTag__1down, "FT_EFF_Eigen_C_0_CTag__1down"}, {Systematics::FT_EFF_Eigen_C_0_CTag__1up, "FT_EFF_Eigen_C_0_CTag__1up"}, {Systematics::FT_EFF_Eigen_C_1_CTag__1down, "FT_EFF_Eigen_C_1_CTag__1down"}, {Systematics::FT_EFF_Eigen_C_1_CTag__1up, "FT_EFF_Eigen_C_1_CTag__1up"}, {Systematics::FT_EFF_Eigen_C_2_CTag__1down, "FT_EFF_Eigen_C_2_CTag__1down"}, {Systematics::FT_EFF_Eigen_C_2_CTag__1up, "FT_EFF_Eigen_C_2_CTag__1up"}, {Systematics::FT_EFF_Eigen_Light_0_CTag__1down, "FT_EFF_Eigen_Light_0_CTag__1down"}, {Systematics::FT_EFF_Eigen_Light_0_CTag__1up, "FT_EFF_Eigen_Light_0_CTag__1up"}, {Systematics::FT_EFF_Eigen_Light_1_CTag__1down, "FT_EFF_Eigen_Light_1_CTag__1down"}, {Systematics::FT_EFF_Eigen_Light_1_CTag__1up, "FT_EFF_Eigen_Light_1_CTag__1up"}, {Systematics::FT_EFF_Eigen_Light_2_CTag__1down, "FT_EFF_Eigen_Light_2_CTag__1down"}, {Systematics::FT_EFF_Eigen_Light_2_CTag__1up, "FT_EFF_Eigen_Light_2_CTag__1up"}, {Systematics::FT_EFF_extrapolation_from_charm__1down, "FT_EFF_extrapolation_from_charm__1down"}, {Systematics::FT_EFF_extrapolation_from_charm__1up, "FT_EFF_extrapolation_from_charm__1up"}, {Systematics::ttbar_PowHW7__1down, "ttbar_PowHW7__1down"}, {Systematics::ttbar_PowHW7__1up, "ttbar_PowHW7__1up"}, {Systematics::ttbar_aMcPy8__1down, "ttbar_aMcPy8__1down"}, {Systematics::ttbar_aMcPy8__1up, "ttbar_aMcPy8__1up"}};
/* TString sysPath = "SYS0"; */
/* std::map<int, TString> sysStrings = {{-1, "PRW"}, {1, "PRW"}, {-2, "TRIG"}, {2, "TRIG"}, {-3, "LEP"}, {3, "LEP"}, {-4, "JVT"}, {4, "JVT"}, {61, "JET_EtaIntercalibration_NonClosure_highE__1up"}, {62, "JET_EtaIntercalibration_NonClosure_highE__1down"}, {63, "JET_EtaIntercalibration_NonClosure_negEta__1up"}, {64, "JET_EtaIntercalibration_NonClosure_negEta__1down"}, {65, "JET_EtaIntercalibration_NonClosure_posEta__1up"}, {66, "JET_EtaIntercalibration_NonClosure_posEta__1down"}, {67, "JET_Flavor_Response__1up"}, {68, "JET_Flavor_Response__1down"}, {69, "JET_GroupedNP_1__1up"}, {70, "JET_GroupedNP_1__1down"}, {71, "JET_GroupedNP_2__1up"}, {72, "JET_GroupedNP_2__1down"}, {73, "JET_GroupedNP_3__1up"}, {74, "JET_GroupedNP_3__1down"}, {75, "JET_JER_DataVsMC__1up"}, {76, "JET_JER_DataVsMC__1down"}, {77, "JET_JER_EffectiveNP_1__1up"}, {78, "JET_JER_EffectiveNP_1__1down"}, {79, "JET_JER_EffectiveNP_2__1up"}, {80, "JET_JER_EffectiveNP_2__1down"}, {81, "JET_JER_EffectiveNP_3__1up"}, {82, "JET_JER_EffectiveNP_3__1down"}, {83, "JET_JER_EffectiveNP_4__1up"}, {84, "JET_JER_EffectiveNP_4__1down"}, {85, "JET_JER_EffectiveNP_5__1up"}, {86, "JET_JER_EffectiveNP_5__1down"}, {87, "JET_JER_EffectiveNP_6__1up"}, {88, "JET_JER_EffectiveNP_6__1down"}, {89, "JET_JER_EffectiveNP_7restTerm__1up"}, {90, "JET_JER_EffectiveNP_7restTerm__1down"}}; */

