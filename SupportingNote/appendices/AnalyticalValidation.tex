\section{Analysis Strategy Validation with Analytical Modelling}
\label{app:validation}

The nominal analysis strategy uses a binned likelihood fit, a MC-based histogram as a signal model, and a GPR-based background model. To cross-check this approach, an unbinned likelihood fit using analytical signal and background models is also pursued. This appendix describes the progress made in developing this cross-check.

In order to extract a limit on the $H+c$ production cross section, fitting of the analytical function to the di-photon invariant mass spectrum is required.
Thus, the precise knowledge of the signal and background shape is necessary. The analytical function is the sum of a signal contribution, normalised to the signal yield,
and of a background contribution, normalised to the background yield. The background function is the sum of the SM Higgs boson background, normalised to the expected SM Higgs boson yield,
and of the non-resonant backgrounds, normalised to the non-resonant background yield. In the following section, the signal and background modelling techniques will be introduced.



\subsection{Signal modelling}
\label{app:modelling:sig}

The $m_{\gamma\gamma}$ distribution for the signal process is resonant.
In the absence of interference with the $pp\rightarrow\gamma\gamma$ background process, it is expected to follow a Breit-Wigner curve, which peaks at the Higgs boson mass ($m_{H}=125$~GeV),
and has a narrow width of 4 MeV in the Standard Model. However, the  $m_{\gamma\gamma}$  distributions observed in the data are smeared by the finite resolution of the measured photon energies which
dominates their shape. For this reason, the  $m_{\gamma\gamma}$  distribution is modelled by a function that is representative of the diphoton mass resolution.

The signal modelling procedure uses a signal normalisation and shape taken from the signal MC, for the $c$-tagged and non $c$-tagged categories separately, with the shape parameterised using an analytical function.
This strategy closely resembles that of many other $H\to\gamma\gamma$ analyses, by using the \textsc{HGamCore} package to fit the signal MC. The fit uses 1~GeV bins between 113~GeV and 138~GeV.\\

As a first iteration of the analysis, the signal model was chosen by comparing the $\chi^2/N_\text{d.o.f.}$ values of fits of a bifurcated Gaussian distribution, a Landau distribution, a Voigtian distribution, a double-sided Crystal Ball distribution,
the sum of a Crystal Ball and a Gaussian distribution, the sum of a crystal Ball and a Voigtian distribution. This test was performed using EM topo jets, and the double-sided Crystal Ball distribution was found to give
the best fit quality, and was adopted as the signal model. The details of this study can be found in Appendix~\ref{app:sigmodel}.
The DSCB function is defined as:

\begin{equation}
   N\cdot %\left\{
   \begin{cases}
   e^{-t^{2}/2} & \mbox{if $-\alpha_{\textrm{low}} \le t \le \alpha_{\textrm{high}}$}\\
   \frac{ e^{-0.5\alpha_\textrm{low}^2}}{\left[\frac{\alpha_\textrm{low}}{n_\textrm{low}} \left(\frac{n_\textrm{low}}{\alpha_\textrm{low}} - \alpha_\textrm{low} -t \right)\right]^{n_\textrm{low}} } & \mbox {if $t < -\alpha_\textrm{low} $}\\
   \frac{ e^{-0.5\alpha_{\textrm{high}}^{2} }}{ \left[\frac{\alpha_{\textrm{high}}}{n_{\textrm{high}}} \left(\frac{n_{\textrm{high}}}{\alpha_{\textrm{high}}} - \alpha_{\textrm{high}}  + t \right)\right]^{n_{\textrm{high}}} } & \mbox {if $t >             \alpha_{\textrm{high}}$},\\
   \end{cases}
   \label{eq:DSCB}
\end{equation}
%
where $t = (\myy - \mu_\textrm{CB})/\sigma_\textrm{CB}$ with $\mu_\textrm{CB}$ the peak of the Gaussian distribution and $\sigma_\textrm{CB}$ represents the width of the Gaussian part of the function; $N$ is a normalisation parameter;
$\alpha_{\textrm{low}}$ ($\alpha_\textrm{high}$) is the position of the junction between the Gaussian and power law on the low (high) mass side in units of $t$; and $n_\textrm{low}$ ($n_\textrm{high}$) is the exponent of this power law.
%The parameter    $\Delta \mX = \mu_\textrm{CB} - \mX$ is defined as the difference between the peak of the Gaussian and the reference mass value.
An illustrative drawing of the double-sides Crystal Ball function is provided in Figure~\ref{fig:modelling:crystalball}.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/modelling/DCBDiagram.pdf}}
  \caption{Description of the double-sided Crystal Ball function parameters. The parameters are described in the text.}
  \label{fig:modelling:crystalball}
\end{figure}


For the second round of the analysis, when the new signal defintion has been adopted, the signal parametrisation using double-sided Crystal Ball distribution has been re-evaluated. The fits to the all signal MC in c-tagged and non-c-tagged category are shown in Figure~\ref{fig:modelling:sigmodels}.

%For the first round of circulatiWhen evaluated using PFlow jets, the double-sided Crystal Ball distribution had a $\chi^2/N_\text{d.o.f.}$ of 1.34 (1.50) in the (non) $c$-tagged category, corresponding to a $p$-value of 6.28\% (1.82\%).
%These fits are shown in Figure~\ref{fig:modelling:sigmodels}, which also give the yields and fit parameters in the two categories.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/modelling/SignalFits_May_fid/plot_singleRes_m12500_c0.pdf}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/modelling/SignalFits_May_fid/plot_singleRes_m12500_c1.pdf}}
  \caption{Fit of double-sided Crystal Ball distribution to signal MC, in the (a) non-$c$-tagged and (b) $c$-tagged categories, from which the signal model is extracted.}
  \label{fig:modelling:sigmodels}
\end{figure}




\subsection{Background modelling}
\label{app:modelling:bkd}

The main sources of background in this analysis can be divided into several categories:
\begin{itemize}
\item $irreducible$ resonant background, originating from the SM Higgs boson decay to a pair of real photons
\item $irreducible$ non-resonant background, originating from SM production of real photon pairs
\item $reducible$ background, which includes events in which one or both of the reconstructed photon candidates come from a jet, faking the photon signature ( $\gamma-jet$ and $jets - jet$)
\end{itemize}

The distinction of the two latter sources relates closely, because \textit{reducible} background can be reduced with photon identification and isolation selections,
while the \textit{irreducible} background is a source of true photons with similar properties to photons from a decay of a resonant signal.
Both processes result in a background spectrum that is continuous and decreasing in the diphoton mass variable $m_{\gamma\gamma}$ and it is referred to as the continuum background.
The exact rate and composition of the continuum background is difficult to model, and the background estimation in the $H\rightarrow\gamma\gamma$ analyses is done using the data-driven method.


\noindent \textbf{2$\times$2D SB background decomposition and template.} In most of the $H\rightarrow\gamma\gamma$ analysis, the background shape is modelled by fitting the reducible + irreducible template with an
analytical function. To choose the form of the analytical function, a signal-free background template is constructed based on the data-driven estimation of the relative fractions of the reducible and irreducible components.
The estimation of the shape of each component is done with MC-simulated events (irreducible) and data (reducible), because none of the MC can predict a precise description of that.

To estimate the relative fractions of reducible and irreducible background and to obtain the correct shape, the 2$\times$2D sideband decomposition method is used, as described in detail in Ref.~\cite{Carminati:1450063}.
In this method, each of the background components $\gamma\gamma$, $\gamma j$, and $jj$ are estimated with a double two-dimensional sideband method by exploiting the information of photon ID and isolation variables.
Each photon enters one of the four categories by passing or failing the \texttt{Tight} ID or isolation criteria. Each event with the two photons can be put into one of the 16 categories. By fitting the fraction of the
di-photon $\gamma\gamma$, $\gamma j$, and $jj$ contribution in these 16 categories, the purity of the diphoton sample in the \texttt{Tight} ID and isolation category can be obtained.

In order to build a total background template, the shapes of the different background components are determined from either a MC simulation or data and then are added according to their relative fractions that are measured with the decomposition method.

The output of the 2x2D sideband method is shown in Figure~\ref{fig:modelling:bkg} for the c-tagged and non c-tagged categories.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/modelling/bkgTemplate_data1518_All_Hc_Atleast1jisloose_bin1.pdf}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/modelling/bkgTemplate_data1518_All_Hc_Atleast1jisloose_bin2.pdf}}
  \caption{MC background teplates created using the 2x2D sideband method for c-ctagged and non c-tagged categories.}
  \label{fig:modelling:bkg}
\end{figure}


\FloatBarrier
\noindent \textbf{Spurious signal test.}  The continuum background distribution is modelled by a smoothly falling analytic function. The functional form is chosen using the spurious signal test to maximise sensitivity
to signal and minimise the systematic uncertainty on the background shape. The spurious signal test is used to determine the form of the background function for the final signal-plus-background fit.


The background templates fitted with a signal+background model in 105 GeV $< m_{\gamma\gamma} <$ 160 GeV, where the background shape is the function under evaluation.
The signal shape in this fit is a DSCB function with the parameters determined according to the procedure outlined in above (see Section~\ref{app:modelling:sig}). The number of fitted signal events is computed for Higgs
boson masses varying in the intervals of 1 GeV from 121 GeV to 129 GeV. The negative fitted signal is allowed: this indicates that the fit function over-predicts the continuum background.  The spurious signal $N_{sp}$
is taken to be the number of fitted signal events in 8 GeV window under signal+background hypothesis test and it measures the bias on the signal yield associated to the choice of a particular background parameterisation.


The spurious signal test is performed for a variety of candidate functions:
\begin{itemize}
\item Exponential function: $f(m_{\gamma\gamma}=em^{c\gamma\gamma}$; \item Exponential function of a second order polynomial: $f(m_{\gamma\gamma})=em^{{c_{1}m_{\gamma\gamma}}^{2}+c_{2}m_{\gamma\gamma}}$
\item Bernstein polynomial of order N=3,4,5: $B_{N}(m_{\gamma\gamma})=\sum{}_{i=0}^{N}c_{i}\binom{N}{i}m_{\gamma\gamma}^{i}{(1-m_{\gamma\gamma})}^{N-i}$
\item Power law function: $f(m_{\gamma\gamma})=m_{\gamma\gamma}^{c}$
\end{itemize}

The criteria to consider a functional form is that $N_{sp}$ satisfies at least one of the following two criteria:
\begin{itemize}
    \item $N_{sp}\pm x\cdot\sigma_{S}$ should be less than 20\% of the expected signal statistical uncertainty ($\delta S$) or
    \item $N_{sp}\pm x\cdot\sigma_{S}$ should be less than 10\% of the number of expected signal events
\end{itemize}
$\sigma_{S}$ is the uncertainty of S due to MC statistics used to build the background template. If more than one function passes the above criteria, the function with the fewest free parameters is selected.
In cases where multiple functions with the same number of free parameters is selected, the function with the smaller value of $N_{sp}$ is chosen.


%Background templates for different categories are available from the $H\rightarrow\gamma\gamma$ cross-section measurements~\cite{Nomidis:2718255}. In particular, $H+1j$ category template is studied in order to determine whether $H+c$ analysis can use it, without a construction of a new template, since it has a similar final state.
%For this purpose the $H+1j$ template is scaled to number of data events to represent the template for the c-tagged and non-c-tagged categories. The Figure~\ref{fig:ss} shows the scaled $H\rightarrow\gamma\gamma$ cross section template as well fitted data (in sidebands) for c-tagged and non-c-tagged category.

%\begin{figure}[!htbp]
%  \centering
%  \includegraphics[width=0.6\textwidth]{figures/modelling/ss_tests/templ_ctag.pdf}
%  \includegraphics[width=0.6\textwidth]{figures/modelling/ss_tests/templ_noctag.pdf}
%  \caption{}
%  \label{fig:ss}
%\end{figure}

The resulting SS test using the temples in shown in Figure~\ref{fig:ss_1} and detailed in Tables~\ref{table:ss1} and ~\ref{table:ss2}. The absolute value of the SS can be considered as the systematic uncertainty on the signal yield associated with the background modelling. From the SS tests the ExpoPoly2 function is chosen for the $c$-tagged category and Power2 function for non $c$-tagged category.


\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.4\textwidth]{figures/modelling/ss_tests/fid/ctag_all_Mu_legend.pdf}
  \includegraphics[width=0.4\textwidth]{figures/modelling/ss_tests/fid/ctag_all_Z_legend.pdf}\\
  \includegraphics[width=0.4\textwidth]{figures/modelling/ss_tests/fid/nonctag_all_Mu_legend.pdf}
  \includegraphics[width=0.4\textwidth]{figures/modelling/ss_tests/fid/nonctag_all_Z_legend.pdf}
  \caption{Results of spurious signal fits in the range 121–129 GeV for the c-tagged (upper plots) and non-c-tagged(lower plots) categories.
  The resulting spurious signal is plotted with respect to the expected signal yield in the left plot and with respect to the expected signal statistical error in the right plot.
  The shaded areas correspond to the uncertainty from the MC statistics of the template.}
  \label{fig:ss_1}
\end{figure}


\begin{table}[!h]
\centering
\noindent\begin{tabular}{l|c|c|c|c|c|c|c|c}
Function  & S/$\delta S$ & S/$S_{ref}$[\%] &   S   & $N_{d.o.f}$ & $p(\chi^{2})$ [\%] 	& Result \\
ExpPoly2  &  8.28        & 8.35            & 6.59  &     2       &  69.7             	& \textbf{PASS}   \\
ExpPol3   &  8.22        & 8.34            & 6.58  &     3       &  66.1             	& PASS\\
Bern3     &  11.9        & 11.4            & 9.02  &     3       &  70.3             	& PASS\\
Bern4     &  9.85        & 11.3            & 8.84  &     4       &  70.7             	& PASS\\
Bern5     &  9.64        & 11              & 8.75  &     5       &  68.1              	& PASS\\
Pow2      &  22.9        & 23.1            & 18.2  &     3       &  53               	& PASS\\
Expo      &  -77.2       & -68.4           & -53.9 &     1       &  0                  	& FAIL\\
 Pow      &  98.2        & 94.2            & 74.6  &     1       &  0					& FAIL\\


\end{tabular}
\caption{Results of spurious signal fits for a set of functions with c-tagged background template. The largest value found over the scanned range is taken as the s.s. estimate (S) for each function. The $\chi^{2}$
test for the background-only fit is also shown for each function. The table also shows the maximum spurious signal divided by the reference signal (based on MC) (S/$S_{ref}$) and the spurious signal relative to the expected statistical uncertainty on the signal (S/$\delta S$) }
\label{table:ss1}
\end{table}


\begin{table}[!h]
\centering
\noindent\begin{tabular}{l|c|c|c|c|c|c|c|c}
Function  & S/$\delta S$ & S/$S_{ref}$[\%] &   S   & $N_{d.o.f}$ & $p(\chi^{2})$ [\%]  & Result \\
Pow2      & 25.6         & 31.5            & 61.4  &     3       &  1.9                & \textbf{PASS}\\
ExpPoly2  & -34.4        & -45.4           & -90   &     2       &  56.7               & PASS.   \\
ExpPol3   & -36.7        & -45.5           & -89.9 &     3       &  52.8               & PASS\\
Bern4     & 42.5         & 54.2            & 107   &     4       &  87.1               & PASS\\
Bern5     & 40.1         & -51.5           & -102  &     5       &  91.3               & FAIL\\
Bern3     & 59.1         & 73.9            & 146   &     3       &  2.92               & FAIl\\
Pow.      & 279          & 317             & 629   &     1       &  0                  & FAIL\\
Expo      & -301         & -362            & -715  &     1       &  0                  & FAIL\\
\end{tabular}
\caption{Results of spurious signal fits for a set of functions with non-c-tagged background template.}
\label{table:ss2}
\end{table}


\subsection{Statistical interpretation}
\label{app:modelling:stat}
The expected signal strength $mu$ is obtained through a simultaneous fit on the $m_\gamma\gamma$ asimov distribution in two categories, using a PDF which combines signal parametrisation, resonant background parametrisation and the background model.
The likelihood fits to Asimov are used to cross check the GPR background model, that is used in the analysis.
The asimov data is constructed from resonant background parametrisation and non resonant background model as shown in Figure~\ref{fig:asimov}.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/modelling/ss_tests/fid/cHyy_blinded_fid_c0.pdf}
   \includegraphics[width=0.8\textwidth]{figures/modelling/ss_tests/fid/cHyy_blinded_fid_c1.pdf}
  \caption{Asimov template used in the fit in non-ctagged (upper) an c-tagged (lower) categories}
  \label{fig:asimov}
\end{figure}

The asimov data was fit simultaneously in both categories to extract the expect limit on $\mu$.  The only one added systematic uncertainty on the fit was the amount of the spurious signal  that was evaluated.
The limit was extracted from the fit and the expected limits are provided in Table~\ref{tab:expectedlimits}.

\begin{table}
\centering
  \begin{tabular}{lc}
    \toprule
    Category   & 95\% CL$_\text{s}$ limit on signal strength \\
    \midrule
    No $c$-tag & 2.64                    \\
    $c$-tag    & 1.97                  \\
    Combined   & \BF{1.57}                     \\
    \bottomrule
  \end{tabular}
  \label{tab:expectedlimits}
  \caption{95\% CL$_\text{s}$ limits on signal strength in the $c$-tag category, non-$c$-tag category, and simultaneous fit to both categories for the alternative fit model using SS.}
\end{table}


The results were compared to the nominal fits to the asimov data (see Table~\ref{tab:expectedlimitsnom}). It was found out that both analysis strategy agree within 5\%.

\begin{table}
\centering
  \begin{tabular}{lc}
    \toprule
    Category   & 95\% CL$_\text{s}$ limit on signal strength \\
    \midrule
    No $c$-tag & 2.7                     \\
    $c$-tag    & 2.04                    \\
    Combined   & \BF{1.63}                \\
    \bottomrule
  \end{tabular}
  \label{tab:expectedlimitsnom}
  \caption{95\% CL$_\text{s}$ limits on signal strength in the $c$-tag category, non-$c$-tag category, and simultaneous fit to both categories for the Nominal analysis.}
\end{table}
