\section{GPR validation studies}
\label{app:GPRValidation}

The GPR-based background estimation strategy adopted in this analysis has several strengths, as described in subsubsection~\ref{sec:modelling:bkd:gpr:motivation}. One of these strengths is that the uncertainty due to limited data statistics and the interpolation into the signal region are automatically accounted for. An assumption underlying this GPR estimation is that the choice of an RBF kernel prior is suitable for our use case, which is motivated by the assumed smoothness of these backgrounds. This assumption (and for some tests the robustness of the method more generally) is studied by varying the choice of prior, by fitting both full-sim and fast-sim background-only MC samples in order to confirm that the extracted signal is compatible with zero, by cross-checking this method again the analytical fit approach, and by a series of toy-based studies. These studies are described in this subsection. Many of the studies documented in this section used the h026 MxAOD tuples, and many use the old signal and background definitions. However, given that these studies involve only non resonant background we do not expect any significant change in the conclusions (though the absolute scale of $\mu$ is not always compatible with the latest definitions).

\subsection{Prior independence studies}
\label{app:GPRValidationPriorIndependenceStudies}

In order to estimate the size of any potential bias introduced by the choice of prior, the GPR-based background estimate is re-evaluated with alternative priors: a Mat\'ern kernel with different values of the smoothness parameter, and the Gibbs kernel. The Mat\'ern kernel becomes equivalent to the Radial Basis Function kernel in the limit that the smoothness parameter tends to infinite. The code for the Gibbs kernel used here is not officially implemented in \textsc{scikit-learn}, but was developed by Rob Fletcher. The largest fractional change in GPR mean and diagonal covariance matrix element for the 124.5--125~GeV bin in the $c$-tag (non-$c$-tag) category with these changes are given in Table~\ref{tab:modelling:gprlengthscalectag} (Table~\ref{tab:modelling:gprlengthscalenonctag}). From this it can be seen that the prior has little-to-no effect on the GPR posterior mean, which is expected due to the large number of events in the sidebands. The change of prior, however, has a significant impact on the diagonal covariance matrix element in this bin for the Mat\'ern kernel with small values of the smoothness parameter, because a less smooth prior more easily allows for large non-smooth variations in the blinded signal region. A Mat\'ern kernel smoothness parameter of 0.5 was also tried, though the posterior distribution was deemed to be unphysical by looking at fits to the data sidebands, and so the results are not included in this subsection. The initial value of the Radial Basis Function kernel prior hyperparameters used in this test for the comparison with the Mat\'ern kernel may differ from those used in the nominal GPR-estimate, though this is not expected to have a significant impact on the tests since these parameters are then optimised in the data sidebands. The initial value of the Radial Basis Function kernel prior hyperparameters used in this test for the comparison with the Gibbs kernel is the value used for the nominal GPR estimate. An old version of the analysis was used for the studies in this subsection.

\begin{table}[!htbp]
  \centering
  \caption{Largest fractional change in GPR mean and diagonal covariance matrix element for the 124.5--125~GeV bin in the $c$-tag category with changes to the prior. The prior used in the nominal background estimation is the Radial Basis Function, while the alternative priors are the Mat\'ern kernel with different values of the smoothness parameter, and the Gibbs kernel.}
  \begin{tabular}{lcc}
    \toprule
    Alternative Kernel & Change in GPR Mean & Change in GPR Covariance \\
    \midrule
    Gibbs                                 & -0.65\%            & -5.4\%                  \\
    Mat\'ern, 1.5                         & 0.035\%            & 400\%                    \\
    Mat\'ern, 2.5                         & -0.40\%            & 49\%                     \\
    Mat\'ern, 10                          & 0.0048\%           & -0.94\%                  \\
    \bottomrule
  \end{tabular}
  \label{tab:modelling:gprlengthscalectag}
\end{table}

\begin{table}[!htbp]
  \centering
  \caption{Largest fractional change in GPR mean and diagonal covariance matrix element for the 124.5--125~GeV bin in the non-$c$-tag category with changes to the prior. The prior used in the nominal background estimation is the Radial Basis Function, while the alternative priors are the Mat\'ern kernel with different values of the smoothness parameter, and the Gibbs kernel.}
  \begin{tabular}{lcc}
    \toprule
    Alternative Kernel & Change in GPR Mean & Change in GPR Covariance \\
    \midrule
    Gibbs                                 & 0.14\%             & 11\%                  \\
    Mat\'ern, 1.5                         & 0.16\%             & 1200\%                   \\
    Mat\'ern, 2.5                         & 0.24\%             & 170\%                    \\
    Mat\'ern, 10                          & -0.057\%           & 11\%                     \\
    \bottomrule
  \end{tabular}
  \label{tab:modelling:gprlengthscalenonctag}
\end{table}

%% In order to estimate the size of any potential bias introduced by the choice of prior, the GPR-based background estimate is re-evaluated with the distance parameter and amplitude of its prior distribution of half and double the nominal value. The largest fractional change in GPR mean and diagonal covariance matrix element for the 124.5--125~GeV bin in the $c$-tag category with these changes are given in Table~\ref{tab:modelling:gprlengthscale}. From this it can be seen that the prior has little-to-no effect on the posterior, which is expected due to the large number of events in the sidebands.

%% \begin{table}[!htbp]
%%   \centering
%%   \caption{Largest fractional change in GPR mean and diagonal covariance matrix element for the 124.5--125~GeV bin in the $c$-tag category with changes to the prior.}
%%   \begin{tabular}{lcc}
%%     \toprule
%%                                    & GPR Mean           & GPR Covariance     \\
%%     \midrule
%%     0.5$\times$ distance parameter & $4\times 10^{-8}$  & $8\times 10^{-7}$ \\
%%     2$\times$ distance parameter   & $4\times 10^{-9}$  & $4\times 10^{-7}$  \\
%%     0.5$\times$ prior amplitude    & 0                  & 0                  \\
%%     2$\times$ prior amplitude      & 0                  & 0                  \\
%%     \bottomrule
%%   \end{tabular}
%%   \label{tab:modelling:gprlengthscale}
%% \end{table}


\subsection{Full-simulation $\gamma\gamma$ MC studies}

A spurious signal (SS)--like test is performed for this method using the full-simulation $\gamma\gamma$ MC. The unweighted number of events in the full-simulation $\gamma\gamma$ MC sample in the SR of the $c$-tagged (non-$c$-tagged) category is 60043 (565102), and the weighted number of events in the SR of the $c$-tagged (non-$c$-tagged) category is 8120 (72500). The MC is normalised to the total $\gamma\gamma$ cross section, as per the recommendations of the ATLAS Statistics Forum for the spurious signal uncertainties~\cite{Berger:2719221}. The initial value of the prior hyperparameters used in this test may differ from those used in the nominal GPR-estimate, though this is not expected to have a significant impact on the tests since these parameters are then optimised in the data sidebands. The fit used for this test also does not include all of the systematic uncertainties used in this analysis, and does not include the subtraction of events (described in Section~\ref{sec:trutholr}). The GPR background estimates derived from the blinded sideband MC can be seen in Figure~\ref{fig:modelling:gprmc}. The output of this GPR estimate is then used alongside the signal and resonant background MC samples to perform a binned likelihood fit in the signal region, as seen in Figure~\ref{fig:modelling:gprmcfits}. The best fit signal strength value was $0\pm 9$ in the $c$-tagged category, $1\pm 13$ in the non-$c$-tagged category, and $0\pm 8$ in the combined fit, where these uncertainties are determined from the likelihood fit. The mean number of events expected in the SR as evaluated using the function that maximises the GPR posterior distribution is 8150 (72700) in the $c$-tagged (non-$c$-tagged) category, evaluated using the GPR estimate derived using the full-simulation $\gamma\gamma$ MC sample.% If the full SS procedure was applied, it would result in a 220\% uncertainty on the signal normalisation, which would roughly add in quadrature with the other uncertainties, degrading the total sensitivity by 7\%.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/cTaggedMC.png}}\\
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/nonCTaggedMC.png}}
  \caption{GPR background estimates derived from the sidebands of the blinded non-resonant full-simulation $\gamma\gamma$ MC in the (a) $c$-tagged and (b) non-$c$-tagged categories on the left, and their ratio on the right.}
  \label{fig:modelling:gprmc}
\end{figure}

\begin{figure}[!htbp]
  \centering
  %% \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotCTagAsimov.png}}
  %% \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotNonCTagAsimov.png}}\\
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotCTagMC.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotNonCTagMC.png}}
  \caption{Binned likelihood fits of the GPR background estimates derived from the sidebands of the blinded non-resonant full-simulation $\gamma\gamma$ MC and the signal and resonant background MC samples to the full-simulation $\gamma\gamma$ background MC and resonant background MC samples in the SR for (a) the $c$-tagged category and (b) the non-$c$-tagged category. The studies are done for the old definition of the signal and the background.}
  %% \caption{Binned likelihood fits of the GPR background estimates derived from the sidebands of the blinded non-resonant $\gamma\gamma$ MC and the signal and resonant background MC samples to (a) the Asimov background dataset as estimated for the GPR in the $c$-tagged category and (b) the non-$c$-tagged category, and (c) the background MC in the SR for the $c$-tagged category and (d) the non-$c$-tagged category.}
  \label{fig:modelling:gprmcfits}
\end{figure}

The GPR estimate is much more flexible than an analytical fit, and is expected to be able to model the true background shape. In all cases, it is found to be compatible with zero within the uncertainties on the GPR estimate and due to poisson statistics. The poisson uncertainties, however, are taken from the normalisation, which are an overestimate of the uncertainty due to MC statistics. As such, a similar fit is performed using unweighted non-resonant $\gamma\gamma$ MC events, to use the full statistical power of the sample. In this case, the GPR estimates are shown in Figure~\ref{fig:modelling:gprmcunw}. It should, however, be emphasised that this test requires any mismodelling due to this method be detectable with significantly more statistical power than is present in our dataset, and so is highly stringent. The best fit signal strength value was $-39\pm 25$ in the $c$-tagged category, $-3.7\pm 35.0$ in the non-$c$-tagged category, and $-27\pm 20$ in the combined fit. This demonstrates that the GPR is correctly modelling our non-resonant $\gamma\gamma$ background, with the GPR posterior uncertainty covering the uncertainties due to the limited data statistics and interpolation into the signal region. The fit results are shown in Figure~\ref{fig:modelling:gprmcfitsunw}. An old version of the analysis was used for the studies in this subsection.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/cTaggedMCUnw.png}}\\
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/nonCTaggedMCUnw.png}}
  \caption{GPR background estimates derived from the sidebands of the unweighted and blinded non-resonant full-simulation $\gamma\gamma$ MC in the (a) $c$-tagged and (b) non-$c$-tagged categories on the left, and their ratio on the right.}
  \label{fig:modelling:gprmcunw}
\end{figure}

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotCTagMCUnw.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotNonCTagMCUnw.png}}
  \caption{Binned likelihood fits of the GPR background estimates derived from the sidebands of the unweighted and blinded non-resonant full-simulation $\gamma\gamma$ MC and the signal (cH) and resonant background MC samples  to the unweighted full-simulation $\gamma\gamma$ background MC and resonant background MC samples in the SR in (a) the $c$-tagged category and (b) the non-$c$-tagged category. The studies are done for the old definition of the signal and the background.}
  \label{fig:modelling:gprmcfitsunw}
\end{figure}


\subsection{Fast-simulation $\gamma\gamma$ MC studies}

To further establish the robustness of the GPR-based background estimation procedure, the test described in the last subsubsection is repeated using a high-statistics non-resonant $\gamma\gamma$ MC, produced using ATLAS fast-simulation. The unweighted number of events in the fast-simulation $\gamma\gamma$ MC sample in the SR of the $c$-tagged (non-$c$-tagged) category is 408529 (4190602), and the weighted number of events in the SR of the $c$-tagged (non-$c$-tagged) category is 7330 (71400). The initial value of the prior hyperparameters used in this test may differ from those used in the nominal GPR-estimate, though this is not expected to have a significant impact on the tests since these parameters are then optimised in the data sidebands. The fit used for this test also does not include all of the systematic uncertainties used in this analysis, and does not include the subtraction of events (described in Section~\ref{sec:trutholr}). The GPR background estimates derived from the blinded sideband MC normalised to the expected data normalisation can be seen in Figure~\ref{fig:modelling:gprmcfastsim}. The output of this GPR estimate is then used alongside the signal and resonant background MC samples to perform a binned likelihood fit in the signal region, as seen in Figure~\ref{fig:modelling:gprmcfitsfastsim}. The best fit signal strength value was $-2\pm 9$ in the $c$-tagged category, $-1\pm 13$ in the non-$c$-tagged category, and $-2\pm 7$ in the combined fit.
%% The weighted number of events in the SR estimated using the fast-simulation $\gamma\gamma$ MC sample in the $c$-tagged (non-$c$-tagged) category is 7330 (71400), and the mean number of events expected in the SR as evaluated using the function that maximises the GPR posterior distribution is 7320 (71300) in the $c$-tagged (non-$c$-tagged) category, evaluated using the GPR estimate derived using the fast-simulation $\gamma\gamma$ MC sample.
The mean number of events expected in the SR as evaluated using the function that maximises the GPR posterior distribution is 7320 (71300) in the $c$-tagged (non-$c$-tagged) category, evaluated using the GPR estimate derived using the fast-simulation $\gamma\gamma$ MC sample.

% If the fast SS procedure was applied, it would result in a 220\% uncertainty on the signal normalisation, which would roughly add in quadrature with the other uncertainties, degrading the total sensitivity by 7\%.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/cTaggedMCFastSim.png}}\\
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/nonCTaggedMCFastSim.png}}
  \caption{GPR background estimates derived from the sidebands of the blinded non-resonant fast-simulation $\gamma\gamma$ MC in the (a) $c$-tagged and (b) non-$c$-tagged categories on the left, and their ratio on the right.}
  \label{fig:modelling:gprmcfastsim}
\end{figure}

\begin{figure}[!htbp]
  \centering
  %% \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotCTagAsimovFastSim.png}}
  %% \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotNonCTagAsimovFastSim.png}}\\
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotCTagMCFastSim.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotNonCTagMCFastSim.png}}
  \caption{Binned likelihood fits of the GPR background estimates derived from the sidebands of the blinded non-resonant fast-simulation $\gamma\gamma$ MC and the signal and resonant background MC samples to the fast-simulation $\gamma\gamma$ background MC and resonant background MC samples in the SR for (a) the $c$-tagged category and (b) the non-$c$-tagged category. The studies are done for the old definition of the signal and the background.}
  %% \caption{Binned likelihood fits of the GPR background estimates derived from the sidebands of the blinded non-resonant $\gamma\gamma$ MC and the signal and resonant background MC samples to (a) the Asimov background dataset as estimated for the GPR in the $c$-tagged category and (b) the non-$c$-tagged category, and (c) the background MC in the SR for the $c$-tagged category and (d) the non-$c$-tagged category.}
  \label{fig:modelling:gprmcfitsfastsim}
\end{figure}

As in the previous subsection, a similar fit is performed using unweighted non-resonant $\gamma\gamma$ MC events to use the full statistical power of the sample, which is significantly larger in the case of this high-statistics fast-simulation sample. In this case, the GPR estimates are shown in Figure~\ref{fig:modelling:gprmcunwfastsim}. The best fit signal strength value was $-25\pm 68$ in the $c$-tagged category, $-110\pm 100$ in the non-$c$-tagged category, and $-46\pm 44$ in the combined fit. This demonstrates that the GPR is correctly modelling our non-resonant $\gamma\gamma$ background, with the GPR posterior uncertainty covering the uncertainties due to the limited data statistics and interpolation into the signal region. The fit results are shown in Figure~\ref{fig:modelling:gprmcfitsunwfastsim}. For this unweighted test the allowed range of the signal normalisation parameter is extended from $\pm 100$ to $\pm 1000$ due to the greater number of background events. An old version of the analysis was used for the studies in this subsection.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/cTaggedMCUnwFastSim.png}}\\
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/nonCTaggedMCUnwFastSim.png}}
  \caption{GPR background estimates derived from the sidebands of the unweighted and blinded non-resonant fast-simulation $\gamma\gamma$ MC in the (a) $c$-tagged and (b) non-$c$-tagged categories on the left, and their ratio on the right.}
  \label{fig:modelling:gprmcunwfastsim}
\end{figure}

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotCTagMCUnwFastSim.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{../figures/modelling/GPRFitPlotNonCTagMCUnwFastSim.png}}
  \caption{Binned likelihood fits of the GPR background estimates derived from the sidebands of the unweighted and blinded non-resonant fast-simulation $\gamma\gamma$ MC and the signal (cH) and resonant background MC samples to the the fast-simulation $\gamma\gamma$ background MC and resonant background MC samples in the SR in (a) the $c$-tagged category and (b) the non-$c$-tagged category. The studies are done for the old definition of the signal and the background.}
  \label{fig:modelling:gprmcfitsunwfastsim}
\end{figure}


\subsection{Cross-check using analytical fit}

To validate the GPR background modelling the expected limits on the signal strength were derived with the alternative background modelling strategy more like what is often used in HGam group, which uses the analytical functional form for the non resonant background. The the fits were performed on Asimov data under the signal-plus-background hypothesis with only the spurious signal uncertainty included. The expected limits between two approaches are found to agree within 3\%. An alternative modelling and interpretation strategy, described in Appendix~\ref{app:validation}, was considered as a validation of the nominal analysis strategy described in the main body of this note. This study used h026 MxAOD tuples for the Sherpa MC and h027 MxAOD tuples for the data.


\subsection{Newer Toy study}
\label{app:newtoystudies}

Additional toy studies were conducted to validate the Gaussian Process Regression (GPR) for background modeling. This process involved generating 1000 toy datasets based on the Pow2 and ExpoPoly2 functions for both the non-ctagged and c-tagged categories, as selected from the SS studies discussed in Section \ref{app:validation}. The toys were produced using parameters identified from the SS test.
The number of events in each toy was randomly determined using a Poisson distribution. The mean value was set equal to the expected number of background events in each category, specifically, 385,940 and 36,899. The datasets were generated within the $m_{\gamma\gamma}$ distribution, with a bin width of 0.5 GeV.

The $m_{\gamma\gamma}$ sidebands of each of these toy datasets was used to produce a GPR background estimate, as though the real data was being used. This GPR background estimate is then used to fit the signal region of the toy dataset as though it were the real data. The distribution of the cross-sections extracted from fits to these datasets (where the MC is added to account for the resonant backgrounds, and no signal is injected) is then used to estimate the possible bias resulting from the use of the GPR approach. A major difference to the real data fit is that systematic uncertainties, except for the GPR uncertainties, are not included in these fits. Another such difference is that these fits are performed in the $c$-tag and non-$c$-tag categories separately. Cross-sections extracted in this way are shown in Figure~\ref{fig:modelling:newertoys}. These distributions are fit with Gaussian distributions in order to extract the means of these Gaussian distributions, which are used to estimate the possible biases resulting from the use of the GPR approach in the respective categories.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.45\textwidth]{../figures/modelling/ToyPulls_cTagCat_09Aug23.png}}
  \hspace{0.03\textwidth}
  \subfigure[]{\includegraphics[width=0.45\textwidth]{../figures/modelling/ToyPulls_nonCTagCat_09Aug23.png}}
  \caption{Distribution of extracted cross-sections from fits to the toy datasets described in subsection~\ref{app:newtoystudies}. These are shown for (a) the $c$-tag and for (b) the non-$c$-tag categories.}
  \label{fig:modelling:newertoys}
\end{figure}

The means extracted from the Gaussian fits to the extracted cross sections are 0.34~pb and 0.86~pb in the $c$-tag and non-$c$-tag categories, respectively. These numbers are assigned as additive uncertainties in the respective categories in the final data fit.

The distribution of the two-category extracted signal divided by its uncertainty on a toy-by-toy basis was also fit with a Gaussian, and the sigma parameter arising from this fit was 1.00. Cross-sections divided by their uncertainties extracted in this way are shown in Figure~\ref{fig:modelling:newertoysboth}.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.6\textwidth]{../figures/modelling/ToyPulls_bothCats_09Aug23.png}
  \caption{Distribution of extracted cross-sections divided by their uncertainties from fits to the toy datasets described in subsection~\ref{app:newtoystudies}. These are shown for the fits to both categories simultaneously.}
  \label{fig:modelling:newertoysboth}
\end{figure}


\subsection{Older Toy study}

Studies into the use of GPR as a background estimation method using 1000 toy experiments generated from an exponential distribution loosely based on our $c$-tagged signal region were performed using a bin width of 0.25~GeV. These studies were performed using toy MC rather than real simulation, and a preliminary version (not exactly what is done now for the nominal strategy) of the estimation strategy was used. It is not practical to show plots of all 1000 toys, but Figure~\ref{fig:modelling:toyexample} is an example figure for one of the toys. The mean credible interval at $m_{\gamma\gamma}=125$~GeV had a width of 1.1\%, while the standard deviation of the toys has a similar value of 1.3\%. The mean estimate of the bin content at $m_{\gamma\gamma}=125$~GeV was 0.49\% lower than the true value, which was thought to be due to the interpolation with limited sideband statistics. In order to investigate this, 1000 similar toy experiments were generated using $10^4$ times as many events, the credible interval width reduced to 0.16\%, the standard deviation of the toys reduced to 0.018\%, and the mean estimate of the bin content improved to -0.0086\% from the true value. These toy experiments demonstrated that the GPR estimate is well-behaved around our expected sample size. The model from which the toy experiments were generated was also used to perform an Asimov fit to the background, assuming a toy Gaussian signal with 2~GeV width, resulting in an uncertainty on the signal strength modifier of 10.8. An ExpPoly2 background model was also used for comparison, which resulted in an uncertainty on the signal strength modifier of 10.5. This 4.4\% higher uncertainty comes from the added flexibility from the looser restrictions on the fitted function.%, which guarantees that the GPR can model the background.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.9\textwidth]{../figures/modelling/GPFit_realStats.png}
  \caption{Example GPR fit to one of the 1000 toys described in this section. The myy distribution and GPR Mean are both shown after pre-processing. The target function is seen as a black dashed line in the rightmost plot.}
  \label{fig:modelling:toyexample}
\end{figure}


\subsection{Exponential factorisation studies}
\label{app:exptest}

A further study of the GPR background estimate was conducted, in which an exponential distribution is fit to the data sidebands prior to the GPR application, and the GPR is applied on the ratio of the data to this exponential. The final estimate is then taken as the exponential multiplied by this GPR estimate. The results of the fits to the ratios of the data to these exponentials are shown in Figure~\ref{fig:modelling:exptest}.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/GPRExpTest_cTag.png}}
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/GPRExpTest_nonCTag.png}}
  \caption{Results of the fits to the ratios of the data to these exponentials, described in subsection~\ref{app:exptest}, for (a) the $c$-tag and (b) the non-$c$-tag categories.}
  \label{fig:modelling:exptest}
\end{figure}

The difference between this estimate and the nominal one in the non-$c$-tag category is 0.02\%, showing very good agreement. The corresponding difference in the $c$-tag category is 1.7\%, however, the uncertainty is now much larger (with the diagonal covariance matrix elements roughly the same size as this difference). In this case, the fit that determined the hyperparameters of this GPR estimate resulted in a length scale of just 1.7~GeV. The optimiser for the hyperparameter optimisation was restarted 100 times, and the initial values of the hyperparameters input to the optimisation were changed, and the resulting length scale was 1.7~GeV in all cases. It is therefore likely that almost all of the real shape was successfully fit away with the exponential, so the GPR was taking largely statistical fluctuations as input, meaning that it preferred a very small length scale on the scale of these fluctuations. The conclusion is that the fit agrees well in the non-$c$-tag category, but that in the $c$-tag category the fit did not have any substantial real (non-fluctuations) shape to fit, and so it gave a very small length scale, a bad estimate and a large uncertainty. This is not an issue for the analysis, since the GPR is being used for modelling smooth backgrounds in the nominal case, and so this issue does not present itself there.


\subsection{Signal contamination studies}
\label{app:signalcontamination}

In order to test the impact of potential contamination of the sidebands used in the GPR estimate from signal and resonant backgrounds, five times the signal plus five times the uncertainty on the resonant background (taken from the inclusive $H\to\gamma\gamma$ and $H\to ZZ^*$ measurement) is injected into the sidebands. The GPR estimate is then regenerated from these adjusted sidebands. The mean GPR estimate in the bin with the highest expected signal content is compared between this and the nominal GPR estimate, and the difference was found to be 0.1\% (0.1\%) in the $c$-tag (non-$c$-tag) category. For comparison, in the $c$-tag (non-$c$-tag) category the post-fit uncertainty on the non-resonant background in this same bin is 1\% (0.3\%), and the statistical uncertainty on the non-resonant background in this bin is 5.2\% (1.6\%).
