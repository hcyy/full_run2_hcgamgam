\subsection{GPR-based non-resonant background modelling}
\label{sec:modelling:bkd:gpr}

The non-resonant backgrounds in this analysis are modelled using a data-driven method based on Gaussian Process Regression (GPR), separately in the $c$-tagged and non-$c$-tagged categories. A Gaussian Process is ``a collection of random variables, any finite number of which have a joint Gaussian distribution''~\cite{gprbook}. The use of GPR in background estimation has been discussed previously in Ref.~\cite{Frate:2017mai}.% Maybe use GPR ref: C. E. Rasmussen & C. K. I. Williams, Gaussian Processes for Machine Learning, the MIT Press, 2006


\subsubsection{GPR-based background motivation}
\label{sec:modelling:bkd:gpr:motivation}

GPR-based background estimation has been recommended by ATLAS for the modelling of smooth backgrounds in a recent PUB note~\cite{ATL-PHYS-PUB-2020-028}, and some pros (and also cons) of such GPR-based methods are described in that note.
The GPR-based background estimation technique introduced in this search has a few strengths that we highlight here.
First, the GPR-based approach explores a complete space of possible background models, and so is guaranteed to be able to model the true functional form, given sufficient constraining power from the data in the SR.
Second, due to its flexibility, this method performs well with increasing luminosity because it automatically accounts for the information gained about the shape of the background from the additional data, and so this method could prove useful in this and other searches as the size of the dataset increases.
This was seen in early toy studies, based on a previous iteration of this analysis, that used $\sim 10^4\times$ data statistics.
Third, this method automatically provides uncertainties on the shape and normalisation of the background model, due to the limited number of events in the sidebands and the interpolation into the signal region.
It is expected that the only remaining source of uncertainty on the non-resonant background estimate in this analysis will arise from the choice of prior in the GPR estimation. %, though for a reasonable space of possible priors this is not expected to be significant due to the amount of data in the sidebands.
However, it has been noted by ATLAS~\cite{ATL-PHYS-PUB-2020-028} in the context of GPR-based smooth background estimation that there is ``Generally limited dependence on the kernel and Gaussian mean, for choices that are reasonably well-suited to the problem'', and so we do not expect this to be a substantial source of uncertainty.
Nonetheless, this expectation and the robustness of the method more generally have both been tested, and various tests of this method are mentioned in subsubsection~\ref{sec:GPRValidation} and described in detail in Appendix~\ref{app:GPRValidation}.


\subsubsection{GPR-based background method}
\label{sec:modelling:bkd:gpr:method}

In this approach, the data is used to construct a histogram in $m_{\gamma\gamma}$ using 0.5~GeV bins between 105~GeV and 160~GeV, and the signal region (120--130~GeV) is blinded.
The bin centres are linearly transformed onto the range [0, 1], and the content of the histogram bins are linearly transformed to have a mean of 0 and a standard deviation of 1.
A pair of GPR algorithms (one for each signal category) are then trained using the transformed bin centres and contents to estimate the transformed bin contents given the transformed bin centres.
The GPR algorithms use a Radial Basis Function (RBF) kernel prior with an initial value of the amplitude of 1 and an initial value of the length scale equal to the reciprocal of the number of bins (including blinded bins).
These hyperparameters of the prior are then optimised by maximizing the log-marginal-likelihood of the data in the sidebands.
The transformed post-fit hyperparameters of the kernel are $0.997$ ($1.08$) for the length scale and $3.87^2$ ($6.18^2$) for the constant value in the $c$-tag (non-$c$-tag) category.
The \textsc{scikit-learn} package is used for the training of the GPR algorithm, and the optimisation of the prior hyperparameters.
The RBF kernel is motivated by the assumption that these backgrounds are smooth.
In addition to the estimates of the transformed bin contents the GPR outputs a covariance matrix, and together these describing a multi-dimensional Gaussian posterior distribution over the transformed bin contents. This is a Bayesian posterior distribution that is derived from the prior and the data in the sidebands.
The resulting estimates of the transformed bin contents and their covariance matrices are linearly transformed back to the original space of bin contents and $m_{\gamma\gamma}$.
This results in an estimate of the background in each $m_{\gamma\gamma}$ bin, along with a data-driven uncertainty estimate.
Lastly, a small damping constant (a fractional increase of 0.001 and 0.0001 in the $c$-tag and non-$c$-tag categories, respectively) is added to the diagonal of the covariance matrices to ensure that it is numerically stable in the final fit used later for the statistical interpretation (described in Section~\ref{sec:statanalysis}).
These sets of bin contents and covariance matrices then define posterior distributions for the non-resonant backgrounds, which can be used to provide the non-resonant background estimate and its uncertainty in a binned likelihood fit to the data in the signal region.

As described in subsubsection~\ref{sec:modelling:bkd:gpr:motivation}, this method provides a highly flexible background modelling strategy, in addition to an uncertainty on the background estimate. Uncertainties arise from the limited data statistics in the sidebands, and the interpolation into the signal regions, and these are accounted for in the covariance matrix of the posterior distribution. A final potential source of uncertainty can arise from the choice of prior, which is studied as mentioned in subsubsection~\ref{sec:GPRValidation} (described in detail in Appendix~\ref{app:GPRValidation}). GPR background estimates derived from the blinded sideband data can be seen in Figure~\ref{fig:modelling:gprdata}.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/GP_cTag_110bins_h027.png}}\\
  \subfigure[]{\includegraphics[width=0.9\textwidth]{../figures/modelling/GP_noCTag_110bins_h027.png}}
  \caption{GPR background estimates derived from the sidebands of the blinded data in the (a) $c$-tagged and (b) non-$c$-tagged categories on the left, and their ratio on the right.}
  \label{fig:modelling:gprdata}
\end{figure}


\subsubsection{GPR validation studies}
\label{sec:GPRValidation}

The GPR-based background estimation strategy adopted in this analysis has several strengths, as described in subsubsection~\ref{sec:modelling:bkd:gpr:motivation}. One of these strengths is that the uncertainty due to limited data statistics and the interpolation into the signal region are automatically accounted for. An assumption underlying this GPR estimation is that the choice of an RBF kernel prior is suitable for our use case, which is motivated by the assumed smoothness of these backgrounds. It has been noted by ATLAS~\cite{ATL-PHYS-PUB-2020-028} in the context of GPR-based smooth background estimation that there is ``Generally limited dependence on the kernel and Gaussian mean, for choices that are reasonably well-suited to the problem'', and so we expect this assumption to be robust. Nonetheless, this assumption (and for some of these tests the robustness of the method more generally) is studied by varying the choice of prior, by fitting both full-sim and fast-sim background-only MC samples in order to confirm that the extracted signal is compatible with zero, by cross-checking this method again the analytical fit approach, and by a series of toy-based studies. The main conclusions of these studies are described in brief here, and more details are provided in Appendix~\ref{app:GPRValidation}.

Many of the studies documented in this section used the h026 MxAOD tuples, and many use the old signal and background definitions. However, given that these studies involve only non resonant background we do not expect any significant change in the conclusions (though the absolute scale of $\mu$ is not always compatible with the latest definitions).

Multiple alternative priors were tested, and the changes studied in the GPR mean were found to be small in all cases, though the changes to the covariance matrices could be large for unreasonable choices of prior for which a smoothness parameter was set to a low value. Both full-sim and fast-sim background-only MC samples in order to confirm that the extracted signal is negligible, and the extracted signal was found to be below effective data statistical uncertainties. These studies were discussed in a Statistics Forum Meeting (\href{https://indico.cern.ch/event/1129597/}{Indico link here}), and it was noted in the minutes that ``The effect of the spurious signal seems to be negligible here.'' The signal has since increased due to the adoption of a new signal definition, so this conclusion should hold more strongly now. The expected limit (with only the relevant uncertainties) was compared to an approach using an analytical fit, and the results were found to be compatible to within 3\%. Toy-based studies were also performed at various stages to study the properties of the method, and the results were found to be promising, hence why this method was pursued to now. Lastly, the possiblity of signal contamination in the GPR sidebands was investigated, and found to be negligible.
