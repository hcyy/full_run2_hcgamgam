\section{Results}
\label{sec:results}

\subsection{Asimov fit results}
\label{sec:results:asimov}

Likelihood fits to the Asimov dataset are used to understand the analysis sensitivity, and check to the statistical procedure. Specifically, two distinct Asimov datasets are considered:
\begin{itemize}
\item An Asimov dataset with no injected signal ($\mu=0$)
\item An Asimov dataset with an injected signal ($\mu=1$)
\end{itemize}


Figure~\ref{fig:results:fits} shows the post-fit $m_{\gamma\gamma}$ distributions in the two analysis categories based on the Asimov data without any injected signal ($\mu=0$) . The (Hesse-based) uncertainties on the signal yield are found to be 3.2~pb, 4.9~pb, and 2.8~pb the SM expectation in the $c$-tag category, non-$c$-tag category, and simultaneous fit to both categories, respectively. The expected limits from fits to the $c$-tag category, non-$c$-tag category, and simultaneous fit to both categories is provided in Table~\ref{tab:results:expectedlimits}.% 3.35964e+00, 5.18100e+00, 2.87135e+00


\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.65\textwidth]{../figures/results/GPRFitPlotCTag_mu0_postu.png}}\\
  \subfigure[]{\includegraphics[width=0.65\textwidth]{../figures/results/GPRFitPlotNonCTag_mu0_postu.png}}
  \caption{$m_{\gamma\gamma}$ distributions in the two analysis categories, after a simultaneous likelihood fit to the Asimov dataset in the two categories under background-only hypothesis. The normalisation of the signal model is scaled up by a factor of 100 to enhance visibility.}
  \label{fig:results:fits}
\end{figure}


\begin{table}[!htbp]
  \centering
  \caption{95\% CL$_\text{s}$ limits on cross-section under background-only hypothesis in the $c$-tag category, non-$c$-tag category, and simultaneous fit to both categories.}
  \begin{tabular}{l|c|c}
    \toprule
    Category   & 95\% CL$_\text{s}$ limit on cross-section  & Best fit\\
    \midrule
    No $c$-tag & 11.6~pb                   & 0.0$\pm$4.9~pb\\ % 11.5819
    $c$-tag    & 7.0~pb                    & -2.47$\times 10^{-7}\pm$3.2~pb\\ % 7.03193
    Combined   & \BF{6.1~pb}               & 0.0$\pm$2.8~pb     \\ % 6.14118
    \bottomrule
  \end{tabular}
  \label{tab:results:expectedlimits}
\end{table}

In order to understand the impact of the systematic uncertainties described in Section~\ref{sec:systematics} on the measurement, the fraction increase in the uncertainty on the $H+c$ signal process is evaluated from introducing the relevant NP(s) from the fit. The results are shown in Table~\ref{tab:results:systematics}.
Additionally, ranking of systematic uncertainties based on their effect on the signal strength $\mu$ is presented in Figure \ref{fig:results:ranking}. It's noteworthy that, generally speaking, none of the nuisance parameters are expected to be constrained in the analysis. Uncertainties predicted to be most impactful and therefore ranking highly in this list include the ad hoc uncertainty in Light SF calibration, denoted as cTagLightAdHoc, PS modeling in ggF, QCD uncertainties related to jet bin migration, and those affecting the photon resolution.


\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.65\textwidth]{../figures/results/pulls_mu_125_mu0_postu.png}\\
  \caption{The impact of different uncertainty sources on the expected signal strength (top blue) under background-only hypothesis when fixing each systematic uncertainty at +1$\sigma$ and -1$\sigma$, ranked from highest to lowest. Only first 20 uncertainties in the ranking are shown.}
  \label{fig:results:ranking}
\end{figure}


%% Uncertainty mu all systematics: 1.55662e-07   1.01474e+01  -9.86196e+00   1.04661e+01 10.16403 0
%% Uncertainty mu no systematics: 1.11001e-08   6.35255e+00  -6.33672e+00   6.37704e+00 6.35688 0.7802810119
%% Uncertainty mu only GPR systematics: 1.11001e-08   7.75806e+00  -7.66225e+00   7.69795e+00 7.6801 0.6550152743
%% Uncertainty mu GPR systematics: 2.38700e-07   9.18014e+00  -8.88259e+00   9.49121e+00 9.1869 0.4278200719
%% Uncertainty mu $c$-tag systematics: 5.55005e-10   9.51465e+00  -9.37115e+00   9.69256e+00 9.531855 0.3471686257
%% Uncertainty mu xsec systematics: 2.22001e-07   9.50682e+00  -9.41540e+00   1.00194e+01 9.7174 0.2931782962
%% Uncertainty mu H+HF systematics: 1.11001e-06   8.57046e+00  -8.49045e+00   8.93293e+00 8.71169 0.5151337143
%% Uncertainty mu OLS systematics: 2.22001e-07   9.87676e+00  -9.78887e+00   1.04355e+01 10.112185 0.1008743955
%\begin{table}[!htbp]
%  \centering
%  \caption{Increase in the uncertainty on the $H+c$ signal process from the introduction of the NP(s) which represent that source of uncertainty in the likelihood fit, which is calculated by subtracting the uncertainty on the signal yield with the relevant nuisance parameters fixed in quadrature from the uncertainty on the signal yield with the relevant nuisance parameters free in the fit. The fit is performed to the background-only Asimov dataset. The uncertainties involved in the subtraction are taken as the mean of the upwards and downward MINOS-based uncertainties. The results are then presented as percentages of the total uncertainty. The ``Systematic (including GPR)'' category contains all of the sources of systematic uncertainty listed below it, and no others. The ``Systematic (excluding GPR)'' category contains the same sources of systematic uncertainty as the ``Systematic (including GPR)'' category, except for the ``GPR background'' uncertainties, which are not included. The ``Statistical (excluding GPR)'' category does not include any systematic uncertainties, and the ``Statistical (including GPR)'' includes only the statistical uncertainties and GPR systematic uncertainties.}
%  \begin{tabular}{lc}
%    \toprule
%    Uncertainty & Impact on $\sigma(\hat{\mu})^*$ \\% total uncertainty: old=8.71826e+00, new=8.684925
%    \midrule
%    \BF{Statistical (excluding GPR)} & 63\% \\
%    \BF{Statistical (including GPR)} & 76\% \\
%    \BF{Systematic (excluding GPR)} & 66\% \\
%    \BF{Systematic (including GPR)} & 78\% \\
%    ~~~Higgs$+$HF & 52\% \\
%    ~~~GPR background & 43\% \\
%    ~~~$c$-tagging & 35\% \\
%    ~~~Higgs background cross-section & 29\% \\
%    ~~~$H+c$/ggF Higgs overlap subtraction & 10\% \\
%    \bottomrule
%  \end{tabular}\\
%  \label{tab:results:systematics}
%\end{table}


\begin{table}[!htbp]
  \centering
  \caption{Increase in the uncertainty on the $H+c$ signal process from the introduction of the NP(s) which represent that source of uncertainty in the likelihood fit, which is calculated by subtracting the uncertainty on the signal yield with the relevant nuisance parameters fixed in quadrature from the uncertainty on the signal yield with the relevant nuisance parameters free in the fit. The fit is performed to the background-only Asimov dataset. The uncertainties involved in the subtraction are taken as the mean of the upwards and downward MINOS-based uncertainties. The results are then presented as percentages of the total uncertainty. The ``Systematic (including GPR)'' category contains all of the sources of systematic uncertainty listed below it, and no others. The ``Systematic (excluding GPR)'' category contains the same sources of systematic uncertainty as the ``Systematic (including GPR)'' category, except for the ``GPR background'' uncertainties, which are not included. The ``Statistical (excluding GPR)'' category does not include any systematic uncertainties, and the ``Statistical (including GPR)'' includes only the statistical uncertainties and GPR systematic uncertainties. Please note that the GPR uncertainty is not purely statistical in nature.}
  \begin{tabular}{lc}
    \toprule
    Uncertainty & Impact on $\sigma(\hat{\sigma(H+c)})$ \\
    \midrule
    \BF{Statistical (including GPR)} & 83\% \\
    ~~~GPR background & 48\% \\
    \BF{Systematic (excluding GPR)} & 56\% \\
    ~~~PS, PDF, QCD, $\alpha_s$ \& BR($H\rightarrow \gamma\gamma$) & 35\% \\
    ~~~$c$-tagging & 28\% \\
    ~~~Photons & 25\% \\
    ~~~Jets & 21\% \\
    ~~~SS & 12\% \\
    ~~~$H+\text{HF}$ & 9\% \\
    ~~~PRW & 2\% \\
    \bottomrule
  \end{tabular}\\
  \label{tab:results:systematics}
\end{table}
%% Uncertainty	MINOS Down	MINOS Up	MINOS Average	Fraction of total
%% Total	2.78444e+00	 2.97923e+00	2.881835	100%
%% Stat-only (incl. GPR)	2.37181e+00	2.38381e+00	2.37781	0.8251027557
%% Systematics (incl. GPR)	-	-	-	-
%% Systematics (excl. GPR)	-	-	-	0.5649826922
%% GPR	2.45771e+00	2.59939e+00	2.52855	47.9741813762
%% c-tagging	2.66423e+00	2.86170e+00	2.762965	28.4243995573
%% Photons	2.74184e+00	2.84246e+00	2.79215	24.7534015105
%% Jets	2.71432e+00	2.91961e+00	2.816965	21.0981582356
%% PS+PDF+QCD+alphaS+BR	2.62980e+00	2.77192e+00	2.70086	34.878847963
%% PRW	2.78435e+00	2.97853e+00	2.88144	1.6556331277
%% SS	2.76256e+00	2.95889e+00	2.860725	12.08169244029
%% HHF	2.77070e+00	2.97055e+00	2.870625	8.8117161416

In Figures~\ref{fig:results:fits_mu1}, post-fit distributions $m_{\gamma\gamma}$ across two analysis categories are displayed, derived from fits to an Asimov dataset with an injected signal ($\mu=1$).  Corresponding expected upper limits on the cross-section at 95\% CL are detailed in Table~\ref{tab:results:expectedlimits_mu1}.
The expected upper limit for the combined category at 95\% CL is determined to be 8.6 pb. The best-fit value for this signal-injected dataset is 2.9 $\pm$2.9 pb, which aligns with the predicted cross-section for the studied process.
Additionally, Figure \ref{fig:results:ranking_mu1} provides a ranking/pull plot that showcases the impact of various systematic uncertainties on the analysis. The behavior of these uncertainties appears to be consistent with the analysis conducted without an injected signal.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.65\textwidth]{../figures/results/GPRFitPlotCTag_mu1_postu.png}}\\
  \subfigure[]{\includegraphics[width=0.65\textwidth]{../figures/results/GPRFitPlotNonCTag_mu1_postu.png}}
  \caption{$m_{\gamma\gamma}$ distributions in the two analysis categories, after a simultaneous likelihood fit to the Asimov dataset in the two categories under signal-plus-background hypothesis. The normalisation of the signal model is scaled up by a factor of 100 to enhance visibility.}
  \label{fig:results:fits_mu1}
\end{figure}



\begin{table}[!htbp]
  \centering
  \caption{95\% CL$_\text{s}$ limits on cross-section under signal-plus-background hypothesis in the $c$-tag category, non-$c$-tag category, and simultaneous fit to both categories.}
  \begin{tabular}{l|c|c}
    \toprule
    Category   & 95\% CL$_\text{s}$ limit on cross-section & Best fit\\
    \midrule
    No $c$-tag & 14~pb                     &2.9$\pm$5.4\\ % 11.5819
    $c$-tag    & 9.6~pb                    &2.9$\pm$3.4 \\ % 7.03193
    Combined   & \BF{8.6~pb}               &2.9$\pm$2.9      \\ % 6.14118
    \bottomrule
  \end{tabular}
  \label{tab:results:expectedlimits_mu1}
\end{table}


\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.65\textwidth]{../figures/results/pulls_mu_125_mu1_postu.png}\\
  \caption{The impact of different uncertainty sources on the expected signal strength (top blue) under signal-plus-background hypothesis when fixing each systematic uncertainty at +1$\sigma$ and -1$\sigma$, ranked from highest to lowest. Only first 20 uncertainties in the ranking are showen.}
  \label{fig:results:ranking_mu1}
\end{figure}





As a check that the fit is functioning as expected, an ``injection test'' is performed by ``injecting'' signals with varying normalisation into the Asimov dataset, and then fitting the signal. As shown in Figure~\ref{fig:results:injectiontest}, the fit extracts a value for the signal normalisation very close to the injected value.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.9\textwidth]{../figures/results/InjectionTest_09Aug23.pdf}
  \caption{Results of an ``injection test'', which is performed by ``injecting'' signals with varying normalisation into the Asimov dataset, and then fitting the signal. The point with an injected signal normalisation around 6 is injected at the value at which the limit is set. The uncertainties are based on HESSE.}
  \label{fig:results:injectiontest}
\end{figure}

The above results are calculated with the resonant backgrounds set to their SM expectations, however, an additional fit is performed in which the resonant backgrounds are collectively scaled by a free parameter $\mu_\text{bkd}$. The resulting expected limit on the cross section is 7.8 pb, which is 27\% higher than the corresponding limit on the cross-section with constrained resonant background within its theoretical uncertainties (in which case the expected limit is 6.1 pb). These results are calculated with the same uncertainty model as the nominal fit.
%The expected limit on $\mu_\text{bkd}$ with $\mu$ free in the fit is 0.37. The correlation between $\mu$ and $\mu_\text{bkd}$ in the fit with both free is -69\%. There is a possibility that the interpretation will be changed to a 2D interpretation at a later date. These results are calculated with the same uncertainty model as the nominal fit.






\subsection{Observed fit results}
\label{sec:results:observed}


A simultaneous binned profile likelihood fit to the unblinded data, under the signal-plus-background hypothesis, is conducted across two analysis categories.
Figure \ref{fig:results:fits:data} shows the post-fit $m_{\gamma\gamma}$ distributions in the $c$-tagged and non-$c$-tagged categories.
The observed (expected) upper limits on the cross-section at 95\% CL, as well as the corresponding observed best-fit values, are provided in Table \ref{tab:results:observedlimits}.
For the combined category, the observed (expected) upper limit at 95\% CL on the $H+c$ cross-section is 10.4 pb (14 pb for $\mu=1$).


\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.65\textwidth]{../figures/results/GPRFitPlotCTag_data_postu.png}}\\
  \subfigure[]{\includegraphics[width=0.65\textwidth]{../figures/results/GPRFitPlotNonCTag_data_postu.png}}
  \caption{$m_{\gamma\gamma}$ distributions in the two analysis categories, after a simultaneous likelihood fit to the unblinded data under signal-plus-background hypothesis. The normalisation of the signal model is scaled up by a factor of 100 to enhance visibility.}
  \label{fig:results:fits:data}
\end{figure}




\begin{table}[!htbp]
  \centering
  \begin{tabular}{l|c|c|c}
    \hline
    Category   & Observed 95\% CL$_\text{s}$ limit [pb]& Expected 95\% CL$_\text{s}$ limit [b]& Observed Best fit [pb] \\
    \hline
    No $c$-tag & 11.2          &         14  & 0.09 $\pm$ 5.3\\ % 1.44327
    $c$-tag    & 14.6          &         9.6 & 7.7 $\pm$ 3.7 \\ % 1.93083
    Combined   & 10.4          &         8.6 & 5.2 $\pm$ 3.0\\ % 1.16782
    \hline
  \end{tabular}

   \caption{95\% CL$_\text{s}$ limits on the cross-section $\sigma(H+c)$ and the extracted best fit values in the $c$-tag category, non-$c$-tag category, and their combination extracted from the fits to the unblinded data.}
  \label{tab:results:observedlimits}
\end{table}


The ranking/pull plot generated from fits to the unblinded data is depicted in Figure \ref{fig:results:ranking:data}. Notably, this plot reveals a consistent lack of constraints on the nuisance parameters, echoing the patterns observed in fits to the Asimov data. Among these, a few pulls stand out, with the most prominent being related to the ad hoc NP for light jet SF. The test has been performed where the originally correlated uncertainty between signal and resonant background was uncorrelated to see whether the constraint is removed. This resulted in change of the best fit value from 5.2$\pm$3.0 pb to 5.3$\pm$3.1 pb. The constraint on the NP decreased from 94\% to 99\% for the NP for the non-c-tag category, 96\% for the NP for the background in the c-tag category, and 99\% for the NP for the signal in the c-tag category.
The dominant sources of uncertainty are are also associated with QCD uncertainties, specifically those related to the $0\rightarrow 1$ jet bin migration in the ggF production mode, photon resolution, and the branching ratio. Additional studies have been performed, in which the theory uncertainties have been uncorrelated between the signal and the resonant backgrounds, and the resulting cross-section changes from 5.2$\pm$3.0 pb to 5.1$\pm$2.9 pb, and the expected uncertainty on the cross-section changes from 2.6 pb to 2.9 pb.
 Furthermore, additional significant uncertainties are linked to jet energy, as well as QCD uncertainties pertaining to the $0\rightarrow 1$ jet bin migration in the ggZH production mode. Overall, the hierarchy of parameters in the unblinded data closely matches that observed in the Asimov fit results.

Table \ref{tab:results:bestfit} summarizes the best-fit values obtained from fits to both the Asimov and unblinded data, alongside the predicted value. The observed best-fit value for the $H+c$ cross-section is 5.2$\pm$3.0 pb. In contrast, the expected best-fit values are 0.0$\pm$2.8 pb for $\mu=0$ and 2.9$\pm$2.9 pb for $\mu=1$. Importantly, while the observed value is 1.8 times higher than the predicted value, this discrepancy corresponds to a significance of just 0.76$\sigma$, indicating no statistically significant deviation from the expected results.


\begin{figure}[!htbp]
  \centering
\includegraphics[width=0.65\textwidth]{../figures/results/pulls_mu_125_data_postu.png}
  \caption{The impact of different uncertainty sources on the observed signal strength when fixing each systematic uncertainty at +1$\sigma$ and -1$\sigma$, ranked from highest to lowest. Only first 20 uncertainties in the ranking are shown. }
  \label{fig:results:ranking:data}
\end{figure}


\begin{table}[!htbp]
  \centering
  \begin{tabular}{l|c}
    \hline
    Dataset & Best fit result [pb] \\
    \hline
    Predicted $\sigma (H+c)$ & 2.9 \\
    Expected $\sigma (H+c)$, ($\mu=0$) & 0.0 $\pm$ 2.8 \\
    Expected $\sigma (H+c)$, ($\mu=1$) & 2.9 $\pm$ 2.9 \\
    Observed $\sigma (H+c)$   & 5.2 $\pm$ 3.0 \\
    \hline
  \end{tabular}
   \caption{Summary of the best fit values extracted in Asimov and unblinded datasets. }
  \label{tab:results:bestfit}
\end{table}




The result of the observed 2D profile likelihood scans shown in Figure \ref{fig:results:contour}. The 2D scan is shown for a simultaneous fit to $\mu$ and $\mu_{bkd}$. When both, signal and resonant background normalisations are left free in the fit the measured signal cross-section changes from 5.2$\pm$3.0 pb to 8.5$\pm$4.0 pb, and the observed limit changes from 10 pb to 16 pb.

\begin{figure}[!htbp]
  \centering
\includegraphics[width=0.65\textwidth]{../figures/results/contours_2D_preety.png}
  \caption{Observed profile likelihood scans. A red cross corresponds to the best fit values of $\mu$ and $\mu_{bkd}$. }
  \label{fig:results:contour}
\end{figure}

