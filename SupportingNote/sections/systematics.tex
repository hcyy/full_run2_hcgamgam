\section{Systematic uncertainties}
\label{sec:systematics}

\subsection{Overview of the systematic uncertainties}
Systematic uncertainties are taken into account in this analysis.
% The dominant systematic uncertainties arise form the both experimental ($c$-tagging) and theory prediction.
The sources of these systematic uncertainties can be grouped in multiple categories: uncertainties associated with the background and signal modelling; experimental uncertainties; and theoretical and modelling uncertainties. The following systematic uncertainties have been included, or are intended to be included, in the statistical interpretation (described in Section~\ref{sec:statanalysis}):

\begin{itemize}
   \item Signal and resonant background theoretical uncertainties;
   \item Signal and background modelling uncertainty on the acceptances;
   \item Higgs + Heavy flavour jet background normalisation uncertainty;
   %\item Uncertainty of the signal-background overlap removal procedure (100\%);
   \item Signal and resonant background experimental uncertainties:
         \begin{itemize}
             \item Luminosity, contributes an uncertainty of 0.84\% to the total event yield
             \item Trigger;
             \item Pileup reweighting uncertainty;
             \item CP uncertainties of jets and photons;
             \item $c$-tagging uncertainties;
         \end{itemize}
   \item Non-resonant background modelling uncertainty, arising from the data-driven background estimate, as detailed in subsection~\ref{sec:modelling:bkd:gpr}.
\end{itemize}

No $H+c$ analysis--level smoothing has been applied to any of the systematics described here. Any nuisance parameters associated only with the normalisation of the signal and/or background processes in the two event categories, where the upward and downward impacts on all signal and background processes in both categories are all less than 1\%, are not included in the fit. Additionally, the impact of the $c$-tagging uncertainty variations on the shape of the $m_{\gamma\gamma}$ distribution was found to be minor and so is neglected.

\subsection{Experimental uncertainties}

The experimental uncertainties are estimated following the recommendations provided by the combined performance groups. The corresponding software tools are the ones provided with \textit{AnalysisBase-21.2.237}. The uncertainties are varied within $\pm 1 \sigma$.
There are several options of "uncertainty scheme" provided with the either a full set of nuisance parameters, or with a reduced set of parameters. Given that this analysis is has large statistical uncertainties and is not expected to be used in a combined fit with other analyses, it was decided to use different reduction than the $H\rightarrow \gamma\gamma$ analysis for this uncertainty. Thus, the simplified uncertainty for photons and jets is considered. The uncertainties considered for each reconstructed object are listed below.
The list of experimental systematic uncertainties provided on per-event basis is shown in Table~\ref{table:systematics}

\subsubsection*{Jet uncertainties}
The jet uncertainties are estimated using the techniques described in~\cite{Aaboud_2017_jets}. All jet uncertainties, including the $c$-tagging, are applied on both, resonant background and the signal.
\begin{itemize}
\item \textbf{The jet energy scale and energy resolution}: The category \textit{R4\_GlobalReduction\_SimpleJER} model for the nuisance parameter is used. This corresponds to 28 NP for JES and JER. Table~\ref{tab:systematics:jesjer} shows the pre-fit size of the jet uncertainties on the two analysis regions, for those which are included in the fit, after the pruning.
\item \textbf{Jet Vertex Tagger (JVT)} efficiency uncertainties are taken into account (JET-JvtEfficiency). The JVT uncertainty for background and signal is found to be less then 1\% and was not included in the fit.
\end{itemize}


\begin{table}[!htbp]
  \centering
  \caption{Pre-fit size of the jet uncertainties on the two analysis categories, for those which are included in the fit after pruning. The upward magnitude of the uncertainties are given as superscript and the downward magnitude of the uncertainties are given as subscript.}
  \begin{tabular}{lcccc}
    \toprule
    \multirow{2}{*}{Uncertainty} & \multicolumn{2}{c}{Signal}             & \multicolumn{2}{c}{Background} \\
                                      & $c$-tagged cat.       & Non-$c$-tagged cat.    & $c$-tagged cat.        & Non-$c$-tagged cat. \\
    \midrule
    JET-EffectiveNP-1 				  & $^{0.25\%}_{-0.28\%}$ & $^{0.22\%}_{-0.2\%}$   & $^{1.13\%}_{-1.12\%}$  & $^{1.01\%}_{-1.01\%}$ \\
    JET-EtaIntercalibration-Modelling & $^{0.24\%}_{-0.25\%}$ & $^{0.24\%}_{-0.22\%}$  & $^{1.16\%}_{-1.14\%}$  & $^{1.08\%}_{-1.09\%}$ \\
    JET-Flavor-Composition            & $^{0.81\%}_{-0.94\%}$ & $^{0.70\%}_{-0.71\%}$  & $^{3.38\%}_{-3.27\%}$   & $^{3.38\%}_{-3.32\%}$ \\
    JET-Flavor-Response 	 		  & $^{-0.47\%}_{0.43\%}$ & $^{-0.34\%}_{0.38\%}$  & $^{-1.64\%}_{1.69\%}$  & $^{-1.70\%}_{1.70\%}$ \\
    JET-JER-EffectiveNP-4 	 		  & $^{-0.14\%}_{-0.14\%}$& $^{0.02\%}_{0.02\%}$   & $^{1.14\%}_{1.14\%}$   & $^{1.0\%}_{1.0\%}$ \\
    JET-Pileup-RhoTopology	 		  & $^{0.60\%}_{-0.69\%}$ & $^{0.54\%}_{-0.51\%}$  & $^{2.96\%}_{-2.83\%}$  & $^{2.6\%}_{-2.49\%}$ \\
    JET-Pileup-OffsetNPV	          & $^{0.16\%}_{-0.18\%}$ & $^{0.18\%}_{-0.16\%}$  & $^{0.89\%}_{-0.83\%}$  & $^{1\%}_{-0.98\%}$ \\
    \bottomrule
  \end{tabular}\\
  \label{tab:systematics:jesjer}
\end{table}


\subsubsection{$c$-tagging uncertainties}
Following the recommendations~\cite{Neep:2750738}, the Tight eigen vector reduction is used for the $c-$ tagging uncertainties, corresponding to 11 nuisance parameters.
The modeling uncertainties for the parton shower generation (Herwig 7 vs Pythia 8) and NLO matching (Powheg vs aMC@NLO) are excluded from the eigen vector reduction. The CDI file that is used to evaluate the uncertainties was produced privately~\cite{Neep:2750738}.
%where the central value of the scale factors 1 for all jet $p_\text{T}$ and $\eta$.
Normalisation systematics are included in the fit where the change in the normalisation for signal or the resonant background is larger than 1\% in either category.
%% A table with the impact of these uncertainties is shown in Section~\ref{sec:results}, and a table with the pre-fit values of these uncertainties is to be added soon.
%% Table~\ref{tab:results:systematics} shows the ...
Table~\ref{tab:systematics:ctagging} shows the pre-fit size of the $c$-tagging uncertainties on the two analysis categories, for those which are included in the fit.
Shape systematics are not included in the fit, since they are found to be negligible, as shown in Appendix~\ref{app:ctagshapesys}.

Due to non-standard calibration used in this analysis, systematic uncertainties are assigned to account for potential errors in the light jet SF. Studies were performed to evaluate the impact of the light jet SF on the analysis, and after discussion with the FTAG conveners, we opted to adopted the following approach. The light jet SF was set to 1, to understand its impact on our analysis regions. These differences were then doubled to produce a more conservative estimate of this uncertainty. This resulted in normalisation uncertainties of 0.72\% and 0.36\% to the signal in the $c$-tag and non-$c$-tag categories, respectively, and normalisation uncertainties of 29.58\% and 2.2\% to the resonant background in the $c$-tag and non-$c$-tag categories, respectively. These uncertainties are correlated between signal and background in the fit, and anti-correlated between the $c$-tag and non-$c$-tag categories.

Due to the fact that both the $c$-tagging algorithm and the $c$-tag calibration used in this analysis were derived using the standard primary vertex, an additional uncertainty was assigned to account for the fact that we the HGam NN-based vertex in this analysis. To account for this, after exchanging messages with the FTAG conveners, we adopted a strategy of assigning a 40\% uncertainty to events in which the NN-based and standard primary vertex algorithms select different vertices. This number was derived by comparing the $c$-tagging efficiencies in both data and MC for events reconstructed using the different vertices, and then doubled to account for the fact that we allow either of two jets in this analysis to be $c$-tagged. This resulted in normalisation uncertainties of 16\% and 16\% to the signal in the $c$-tag and non-$c$-tag categories, respectively, and normalisation uncertainties of 16\% and 3.9\% to the resonant background in the $c$-tag and non-$c$-tag categories, respectively. These uncertainties are uncorrelated between signal and background in the fit, but anti-correlated between the $c$-tag and non-$c$-tag categories for both the signal and background uncertainties. % was added ato account for potential discrepancies. % and standard jet collection container

Both studies are documented in Appendix~\ref{app:ctag_studies}.

\begin{table}[!htbp]
  \centering
  \caption{Pre-fit size of the $c$-tagging uncertainties on the two analysis categories, for those which are included in the fit after pruning. The upward magnitude of the uncertainties are given as superscript and the downward magnitude of the uncertainties are given as subscript.}
  \begin{tabular}{lcccc}
    \toprule
    \multirow{2}{*}{Uncertainty} & \multicolumn{2}{c}{Signal}             & \multicolumn{2}{c}{Background} \\
                     & $c$-tagged cat.       & Non-$c$-tagged cat.     & $c$-tagged cat.       & Non-$c$-tagged cat. \\
    \midrule
    FT-EFF-Eigen-C-0 & $^{-2.45\%}_{2.45\%}$ & $^{1.67\%}_{-1.67\%}$   & $^{-0.02\%}_{0.02\%}$ & $^{0.002\%}_{0.002\%}$ \\
    FT-EFF-Eigen-L-0 & $^{0.04\%}_{-0.04\%}$ & $^{-0.03\%}_{0.03\%}$   & $^{1.58\%}_{-1.58\%}$ & $^{-0.122\%}_{0.122\%}$ \\
    FT-EFF-Eigen-L-1 & $^{-0.15\%}_{0.15\%}$ & $^{0.08\%}_{-0.08\%}$   & $^{-1.71\%}_{1.71\%}$ & $^{0.125\%}_{-0.125\%}$ \\
    ttbar\_PowHW7 	 & $^{7.8\%}_{-7.85\%}$  & $^{-5.31\%}_{5.33\%}$   & $^{-2.26\%}_{2.25\%}$ & $^{0.16\%}_{-0.16\%}$ \\
    ttbar\_aMCPy8 	 & $^{2.12\%}_{-2.13\%}$ & $^{-1.45\%}_{1.45\%}$   & $^{-0.96\%}_{0.96\%}$ & $^{0.07\%}_{-0.07\%}$ \\
    \bottomrule
  \end{tabular}\\
  \label{tab:systematics:ctagging}
\end{table}

\subsubsection*{Photon uncertainties}
    \begin{itemize}
    \item \textbf{Photon identification}: The photon identification efficiency was measured using 2015-2018 data using three data-driven methods and is used to compute data to MC scale factors for each method separately as well as combined~\cite{Petit:2243638}. The values are provided by the $e\gamma$ group via the \texttt{PhotonEfficiencyCorrection} tool~\cite{PhotonEffRun2}. The associated up and down variation are propagated to the analysis (PH-EFF-ID-Uncertainty). This uncertainty is applied on both, the signal and resonant background.
    \item \textbf{Photon isolation}: The uncertainty of the photon isolation efficiency has contributions from both the calorimeter and charged-particle tracking portions of the requirement. The systematic uncertainty on the yield is obtained by applying  a data-driven shift to the calorimeter isolation and a $p_{T}$-dependent shift to the track isolation. The shifts on the correction factor are calculated separately and combined in quadrature (PH-EFF-ISO-Uncertainty). This uncertainty is applied on both, the signal and resonant background.
    \item \textbf{Photon trigger}: The trigger efficiency is measured with data for single-photon triggers. For the diphoton triggers used in the analysis, a bootstrap method is used~\cite{Min:2309522_v2} to estimate the efficiency (PH-EFF-TRIGGER). This uncertainty is applied on both, the signal and resonant background.
    \item \textbf{Photon energy scale and resolution (PES, PER)}: Uses the simplified "1NP" model, with a single parameter for the scale and AFII samples (EG-SCALE-ALL, EG-SCALE-AF2) and a single parameter for the resolution (EG-RESOLUTION-ALL). This uncertainty is applied on both, the signal and resonant background.
    \end{itemize}

Table~\ref{tab:systematics:photons} shows the pre-git size of the photon uncertainties on the two analysis regions, for those which are included in the fit.
The photon energy scale and resolution uncertainties are alse added to the fit. Figure~\ref{fig:systematics:photonshaperes} and Figure~\ref{fig:systematics:photonshapescale} show the PER and PES uncertainties, respectively.
The sigma parameters of Gaussian distributions fit to the signal in the $c$-tag (non-$c$-tag) category are 1.7~GeV, 1.6~GeV and 1.8~GeV (1.7~GeV, 1.6~GeV and 1.9~GeV), for the nominal, downward and upward photon resolution NP variations respectively.
The sigma parameters of Gaussian distributions fit to the resonant background in the $c$-tag (non-$c$-tag) category are 1.7~GeV, 1.6~GeV and 1.9~GeV (1.8~GeV, 1.7~GeV and 1.9~GeV), for the nominal, downward and upward photon resolution NP variations respectively.
The PES/PER shape systematics were implemented in the fit using the ratio of variation to the nominal value.
Appendix~\ref{app:photonshape} contains the result of a validation study performed to check the implementation of the PER uncertainty.

\begin{figure}[!htbp]
  \centering
 \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ResSigCTag.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ResSigNonCTag.png}}
   \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ResBkdCTag.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ResBkdNonCTag.png}}
  \caption{Photon resolution uncertainties for signal (a-b) and background (c-d) in c-tagged (a,c) and non-ctagged (b,d) categories.}
  \label{fig:systematics:photonshaperes}
\end{figure}

\begin{figure}[!htbp]
  \centering
 \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ScaleSigCTag.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ScaleSigNonCTag.png}}
   \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ScaleBkdCTag.png}}
  \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ScaleBkdNonCTag.png}}
  \caption{Photon scale uncertainties for signal (a-b) and background (c-d) in c-tagged (a,c) and non-ctagged (b,d) categories.}
  \label{fig:systematics:photonshapescale}
\end{figure}

%% \begin{figure}[!htbp]
%%   \centering
%%    \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ScaleUp.png}}
%%   \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ScaleDown.png}}
%%     \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ResUp.png}}
%%   \subfigure[]{\includegraphics[width=0.475\textwidth]{figures/sys_plots/photons/ResDown.png}}
%%   \caption{Photon scale (a-b) and resolution(c-d) uncertainties for background and signal in c-tagged and non-ctagged categories. The up fluctuations are shown in (a,c) and down fluctuations are shown in (b,d)}
%%   \label{fig:systematics:photonshape}
%% \end{figure}


\begin{table}[!htbp]
  \centering
  \caption{Pre-fit size of the photon uncertainties on the two analysis categories. Photon ID and Isolation uncertainties are included in the fit, the photon trigger uncertainty was pruned away. The upward magnitude of the uncertainties are given as superscript and the downward magnitude of the uncertainties are given as subscript.}
  \begin{tabular}{lcccc}
    \toprule
    \multirow{2}{*}{Uncertainty} & \multicolumn{2}{c}{Signal}             & \multicolumn{2}{c}{Background} \\
                   & $c$-tagged cat.       & Non-$c$-tagged cat.     & $c$-tagged cat.         & Non-$c$-tagged cat. \\
    \midrule
    PH-EFF-ID      & $^{1.63\%}_{-1.61\%}$ & $^{1.64\%}_{-1.63\%}$   & $^{1.68\%}_{-1.67\%}$   & $^{1.71\%}_{-1.70\%}$ \\
    PH-EFF-ISO     & $^{1.57\%}_{-1.56\%}$ & $^{1.58\%}_{-1.57\%}$   & $^{1.60\%}_{-1.59\%}$   & $^{1.62\%}_{-1.60\%}$ \\
    PH-EFF-TRIGGER & $^{0.98\%}_{-0.97\%}$ & $^{0.97\%}_{-0.96\%}$   & $^{0.97\%}_{-0.96\%}$   & $^{0.95\%}_{-0.95\%}$ \\
    \bottomrule
  \end{tabular}\\
  \label{tab:systematics:photons}
\end{table}


\subsubsection*{Pile-up reweighting uncertainty}
A variation in the pile-up reweighting of the simulation is performed to cover the difference between the predicted and measured inelastic $pp$ cross-section in the fiducial volume of the detector~\cite{STDM-2015-05} (PRW-DATASF). This uncertainty is  applied both, signal and resonant background. The pre-fit values of the pileup reweighing uncertainties are estimated to be equal or lower then 1\% (see Table~\ref{tab:systematics:prw})


\begin{table}[!htbp]
  \centering
  \caption{Pre-fit size of the pile-up reweighing uncertainties on the two analysis categories. The upward magnitude of the uncertainties are given as superscript and the downward magnitude of the uncertainties are given as subscript.}
  \begin{tabular}{lcccc}
    \toprule
    \multirow{2}{*}{Uncertainty} & \multicolumn{2}{c}{Signal}             & \multicolumn{2}{c}{Background} \\
                   & $c$-tagged cat.         & Non-$c$-tagged cat.     & $c$-tagged cat.         & Non-$c$-tagged cat. \\
    \midrule
    PRW-DATASF      & $^{-0.105\%}_{0.98\%}$ & $^{-0.50\%}_{0.46\%}$   & $^{-0.83\%}_{0.73\%}$   & $^{-0.19\%}_{0.18\%}$ \\
    \bottomrule
  \end{tabular}\\
  \label{tab:systematics:prw}
\end{table}

\begin{table}[!htbp]
\centering
\noindent\begin{tabular}{l|c|c|c}
Uncertainty name            						&   Property affected       &    applied to (objects) 	& applied to (sample)\\
\hline
EG-RESOLUTION-ALL         					    	&   sig. resolution       &    photons              	& sig., res. backg. \\
EG-SCALE-ALL              				 			&   sig. scale            &						 		& sig., res. backg.\\
\hline
PH-EFF-ID-Uncertainty       						&   sig. yields photons   &    photons             		& sig., res. backg.\\
PH-EFF-ISO-Uncertainty 								& 	sig. yields			&								& sig., res. backg.\\
PH-EFF-TRIGGER-Uncertainty  						& 	sig. yields			&								& sig., res. backg.\\
\hline
JET-EtaIntercalibration-NonClosure-2018data 		&	sig. yields 			&	jets 					& sig., res. backg.\\
JET-EtaIntercalibration-NonClosure-highE 			&	sig. yields			& 								& sig., res. backg.\\
JET-EtaIntercalibration-NonClosure-negEta			&	sig. yields			& 								& sig., res. backg.\\
JET-EtaIntercalibration-NonClosure-posEta			&	sig. yields			& 								& sig., res. backg.\\
JET-EtaIntercalibration-Modelling					&	sig. yields			& 								& sig., res. backg.\\
JET-EtaIntercalibration-TotalStat					&	sig. yields			& 								& sig., res. backg.\\
JET-EffectiveNP[1-7]								&	sig. yields			& 								& sig., res. backg.\\
JET-EffectiveNP-8restTerm							&	sig. yields			& 								& sig., res. backg.\\
JET-BJES-Response 							        &	sig. yields			& 								& sig., res. backg.\\
JET-Pileup-OffsetMu 							    &	sig. yields			& 								& sig., res. backg.\\
JET-Pileup-OffsetNPV 							    &	sig. yields			& 								& sig., res. backg.\\
JET-Pileup-PtTerm 							    	&	sig. yields			& 								& sig., res. backg.\\
JET-Pileup-RhoTopology 							    &	sig. yields			& 								& sig., res. backg.\\
JET-Pileup-PunchThrough-MC16 						&	sig. yields			& 								& sig., res. backg.\\
JET-Pileup-SingleParticle-HighPt 					&	sig. yields			& 								& sig., res. backg.\\
JET-JER-DataVsMC-MC16								&	sig. yields			& 								& sig., res. backg.\\
JET-JER-EffectiveNP[1-6]							&	sig. yields			& 								& sig., res. backg.\\
JET-JER-EffectiveNP-7restTerm						&	sig. yields			& 								& sig., res. backg.\\
JET-Flavor-Response									&	sig. yields			& 								& sig., res. backg.\\
JET-Flavor-Composition								&	sig. yields			& 								& sig., res. backg.\\
JET-JvtEfficiency									&	sig. yields			& 								& sig., res. backg.\\
\hline
PRW-DATASF 							        		&	sig. yields 			&	event					& sig., res. backg.\\
\hline
FT-EFF-Eigen-B[0-1]									&	sig. yields 			&	jets 					& sig., res. backg.\\
FT-EFF-Eigen-C[0-2]									&	sig. yields			& 								& sig., res. backg.\\
FT-EFF-Eigen-Light[0-2]								&	sig. yields			& 								& sig., res. backg.\\
FT-EFF-extrapolation-from-charm						&	sig. yields			& 								& sig., res. backg.\\
ttbar-PowHW7										&	sig. yields			& 								& sig., res. backg.\\
ttbar-aMCPy8   								        &	sig. yields			& 								& sig., res. backg.\\
\end{tabular}
\caption{The list of experimental systematic uncertainties used by the analysis. They are implemented as nuisance parameters in the fit model.}
\label{table:systematics}
\end{table}


\subsection{Theoretical uncertainties}
\label{theo}
%The impact of uncertainties in the cross-section on the global normalisation the resonant background has been taken into account. The inclusive uncertainties on the yield from the CERN Yellow Report 4~\cite{deFlorian:2016spz} are used. The values of these can be found in the Table~\ref{table:theory}. Additional uncertainty of 1.73\% on the $H\rightarrow \gamma\gamma$ decay is included according to Ref.~\cite{deFlorian:2016spz}.
%Additionally, the
%
%\begin{table}[!htbp]
%\centering
%\noindent\begin{tabular}{l|c|c}
%Process            				&   QCD scale [\%]    &   PDF + $\alpha_{s}$ [\%] \\
%\hline
%ggH (N3LO)  					&	+3.9 /-3.9 		  &    +3.2 / -3.2  \\
%\hline
%VBF 							&	+0.4 / -0.3 	  &		+2.1 / -2.1 \\
%\hline
%$WH$ 							&	+0.5 / -0.7 	  &		+1.9 / -1.9\\
%\hline
%$ZH$ 							&	+3.8 / -3.0 	  &		+1.6 / -1.6 \\
%\hline
%$t\bar tH$ 						&	+5.8 / -9.2 	  &		+3.6 / -3.6 \\
%\hline
%$b\bar bH$						&	+20.2/-23.9 (both)&                 \\
%\hline
%
%\end{tabular}
%\caption{QCD scale and PDF+$\alpha_{s}$ uncertainties on inclusive cross section for Higgs production modes for $\sqrt{s}=13$ TeV and $m_{H}$ = 125 GeV.}
%\label{table:theory}
%\end{table}
%
%
%Additionally, the theoretical uncertainties also include QCD scale uncertainties, parton density functions (PDF), the strong couplings constant ($\alpha_s$) and branching ratios. These uncertainties are calculated using TruthWeightTools. The tool provides systematic uncertainties on per-event basis in the form of
%modified event weight resulting from +1$\sigma$ and -1$\sigma$ variation of the corresponding nuisance parameter. In total there scheme provides 30 NP related to PDF, 8 NP related to QCD and 1 for $\alpha_s$ that are calculated for all signal and backgrounds except cH and bbH contributions.

All theoretical uncertainties, along with their respective magnitudes that pass the pruning algorithm and are incorporated into the fit, are summarised in Table \ref{chap5_tab_systematics_theo}. Several theoretical uncertainties have been considered in the analysis:
\begin{itemize}
    \item \textbf{Branching ratio.} Uncertainty related to the $H\rightarrow \gamma\gamma$ branching ratio, denoted as alpha\_THBR. This uncertainty arises from theoretical (THU) and parametric uncertainties (PU) due to dependencies on the partial width from quark masses and the strong coupling constant. The uncertainty in the branching ratio is:
    $$^{1.73\%}_{-1.72\%}(THU)^{0.97\%}_{-0.94\%}(PU(m_q))^{0.66\%}_{-0.61}(PU(\alpha_s))=^{2.90\%}_{-2.84\%}(TOTAL)$$
    \item \textbf{PDF and $\alpha_s$.} Uncertainties associated with PDFs and the strong coupling constant, $\alpha_s$, have also been considered. In total, 30 eigenvalues were used to calculate the PDF uncertainty for the signal and background, using the master formula for Hessian sets, as described in \cite{Butterworth:2015oua}. A single NP is allocated to account for the uncertainty in $\alpha_s$. Following the pruning process, only the PDF uncertainty for the $VBF$ process propagated to the fit, corresponding to a peak value of 1.1\%
    \item \textbf{QCD scale uncertainties.} For the $ggF$ Higgs production process, the QCD uncertainties are estimated using the Boughezal-Liu-Petriello-Tackmann-Walsh (BLPTW) uncertainty scheme. This scheme introduces four parameters to account for the overall scale uncertainty (QCDscale\_ggH\_mu), the resummation scale uncertainty (QCDscale\_ggH\_res), the migration between the $0\rightarrow 1$ jet bins (QCDscale\_ggH\_mig01), and the migration between the $1\rightarrow 2$ jet bins (QCDscale\_ggH\_mig12) as detailed in Ref. \cite{LHCHiggsCrossSectionWorkingGroup:2016ypw}.

    For the remaining Higgs production modes - $VBF$, $VH$, and $ttH$ - similar uncertainty estimation schemes are utilised. In the case of $VH$, three nuisance parameters are introduced to account for the overall scale ($\mu$) and bin by bin migrations ($0\rightarrow 1$ and $1\rightarrow 2$), as discussed in Ref. \cite{ATL-PHYS-PUB-2018-035}. For the $VBF$ process, two nuisance parameters are considered to account for the overall scale ($\mu$) and $0\rightarrow 1$ jet bin migration, as discussed in Ref. \cite{vbf_wg}. Lastly, for the $ttH$ process, a single nuisance parameter is considered accounting for the overall scale.
\end{itemize}


Apart from the ones described above, no other theoretical uncertainties are added to the fit.

\begin{table}[!htbp]
  \centering
  \begin{tabular}{l|cc|cc}
    \hline
    \multirow{2}{*}{Uncertainty} & \multicolumn{2}{c}{Signal}             & \multicolumn{2}{c}{Background} \\
                   & $c$-tagged       & non-$c$-tagged     & $c$-tagged         & non-$c$-tagged \\
    \hline
    PDF-VBF    & 1.13\% & 0.85\% & 0.11\% & 0.10\%\\
    QCD-MU-ggF & 1.77\% & 1.72\% & 1.13\% & 0.86\%\\
    QCD-RES-ggF & 3.72\% & 3.63\% & 2.46\% & 2.06\%\\
    QCD-MIG01-ggF & 5.48\% & 5.31\% & 3.89\% & 3.87\%\\
    QCD-MIG12-ggF & 4.53\% & 4.52\% & 2.4\% & 0.81\%\\
    QCD-MU-ggZH & -5.7\% & -4.76\% & -2.13\% & -1.49\%\\
    QCD-MIG01-ggZH & 1.2\% & 2.13\% & 1.28\% & 3.35\%\\
    QCD-MIG12-ggZH & 0.66\% & 1\% & 0.46\% & 0.42\%\\
    ALPHAS-ggF     & $^{1.66\%}_{-1.66\%}$ & $^{1.48\%}_{-1.48\%}$ & $^{0.9\%}_{-0.95\%}$ & $^{0.78\%}_{0.83\%}$ \\
    H+HF           &  -      & -   & 14.95\%  & 2.1\% \\
    \hline
  \end{tabular}
   \caption{The table displays the pre-fit sizes of the PDF, QCD, $\alpha_s$, and H+HF uncertainties across the two analysis categories, both for signal and background. }
  \label{chap5_tab_systematics_theo}
\end{table}



\subsection{Background modelling uncertainties}

The GPR process described in Section~\ref{sec:modelling:bkd:gpr} uses Bayesian inference to estimate the posterior distribution of the non-resonant background. This posterior uncertainty is then included in the likelihood fit described in Section~\ref{sec:statanalysis} as a constraint on the normalisation and shape of the non-resonant background. This posterior distribution is described in Section~\ref{sec:modelling:bkd:gpr}.

In addition to this uncertainty, a bias can result from the choice of prior used in the method. This is estimated using the toys study described in Appendix subsection~\ref{app:newtoystudies}, and the uncertainties included in the analysis are 6.4~pb and 17~pb in the $c$-tag and non-$c$-tag categories, respectively. The apparent bias from this study could arise from the choice of prior in the method. It could, however, be due to the interpolation, which would mean it is also being covered by the GPR uncertainty described in Section~\ref{sec:modelling:bkd:gpr}. A conservative approach is used here in which this uncertainty is included in the analysis. Its impact on the expected limit is $<1\%$, which provides confidence that if the inclusive of this uncertainty is not needed that it does not substantially impact the expected limit.


\subsection{Signal and background PS modelling uncertainties}

In order to evaluate the parton shower uncertainties for the signal and the background, additional samples are produced. The PS generator uncertainties are found to be affecting only the overall normalisations. The shape effects are found to be negligible. The plots, comparing Nominal and alternative samples are shown in Figure~\ref{fig:nom_vs_alt}. The rest of the relevant plots can be found in Appendix~\ref{app:modelpssys}.
Table~\ref{tab:systematics:modelps} shows the pre-fit size of the parton shower modeling uncertainties on the two analysis categories for those which are included in the fit.

\begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.45\textwidth]{figures/sys_plots/ps/NEW_FID/figs_model_0_0.pdf}}
  \subfigure[]{\includegraphics[width=0.45\textwidth]{figures/sys_plots/ps/NEW_FID/figs_model_1_0.pdf}}
  \subfigure[]{\includegraphics[width=0.45\textwidth]{figures/sys_plots/ps/NEW_FID/figs_model_2_0.pdf}}
  \subfigure[]{\includegraphics[width=0.45\textwidth]{figures/sys_plots/ps/NEW_FID/figs_model_3_0.pdf}}
  \caption{Figures the plots for shape and normalisation variations of $ggF$ signal and background in c-tagged and non c-tagged categories in 120 - 130 GeV mass range. In the plots $H+X$ denoted as a resonant $ggF$ background.}
  \label{fig:nom_vs_alt}
\end{figure}


\begin{table}[!htbp]
  \centering
  \caption{Pre-fit size of the parton shower modelling uncertainties on the two analysis categories, for those which are included in the fit. }
  \begin{tabular}{lcccc}
    \toprule
    \multirow{2}{*}{Uncertainty} & \multicolumn{2}{c}{Signal}             & \multicolumn{2}{c}{Background} \\
                  & $c$-tagged cat. & Non-$c$-tagged cat.  & $c$-tagged cat. & Non-$c$-tagged cat. \\
    \midrule
    MOD-ALGPS-GGF & $-4.07\%$       & $-5.97\%$            & $5.94\%$        & $3.26\%$  \\
    MOD-ALGPS-VBF & $-3.38\%$       & $-1.40\%$            & $-0.29\%$       & $0.08\%$   \\
    \bottomrule
  \end{tabular}\\
  \label{tab:systematics:modelps}
\end{table}


\subsection{Higgs + heavy flavour jet uncertainty}
\label{sec:systematics:higgshf}

In the final fit an additional normalisation uncertainty on the Higgs boson + heavy-flavour (HF) jet background is applied.
This uncertainty accounts for any possible mismodelling of the HF jets that do not arise from the hard-scatter process, but rather from other processes such as gluon-splitting, simultaneous $pp$ collisions, pileup, the underlying event, etc.
%Typically, a 100\% normalisation uncertainty is applied (see for example~\cite{MorenoLlacer:2684286}), which would have become the dominant uncertainty for the search if applied to all events.
This uncertainty, however, is only relevant to events where the reconstructed $c$-jet arises from a truth-level HF jet that is not from the hard scatter process. A summary of the H+HF uncertainty for each category is presented in Table \ref{chap5_tab_systematics_theo}.
The 14.95\% uncertainty was applied to the background events in the $c$-tagged category where the $c$-tagged jets are truth-matched to truth $c$- or $b$-jet
%As such, the 100\% uncertainty is scaled to account for the fact that is should only be applied to events where the $c$-tagged jets are truth-matched to truth $c$- or $b$-jet,
and the fraction of such events in the $c$-tagged category is 18.1\% for ggF and 13.8\% for VBF. The 2.1\% uncertainty was applied to the background events in the non-$c$-tagged category where the jets are truth-matched to the $c$ or $b$-jet and the fraction such events is 3\% in ggF and 2.5\% in VBF. This correction is calculated using the dominant ggF and VBF Higgs boson contribution, and uses raw event counts
without any event weights, scale factors etc.
These uncertainties are fully anti-correlated between the event categories.%
The fractions of jets matched to different flavours of truth-jet are shown in Figure~\ref{fig:hffrac}.
Additionally, this uncertainty should (to a good approximation) only be relevant for processes that are without a HF jet that arises from the hard-scatter process,
which is calculated from the (rounded) numbers in Table~\ref{table:expectedsig} to be around 85.3\% of the total resonant background in the c-tagged and 93\% in the non-c-tagged category. The incorporation of the H+HF systematics introduces a 2\% variation in the final results.

%The resulting uncertainty is 38.1\%, and it is applied to the sum of the resonant backgrounds.

\begin{figure}[!htbp]
 \centering
 \subfigure[]{\includegraphics[width=0.7\textwidth]{figures/mc/ctag_hf.pdf}}
 \subfigure[]{\includegraphics[width=0.7\textwidth]{figures/mc/nonctag_hf.pdf}}
 \caption{Figures present the fractions of jets that are matched to different flavours of truth-jet. These fractions were calculated based on the ggF and VBF samples, specifically in the c-tagged (a) and non-c-tagged (b) categories. Truth flavour matching is done using flavour label \texttt{HadronConeExclTruthLabelID}}
 \label{fig:hffrac}
\end{figure}


%\subsection{Signal-Background overlap removal uncertainty}
%As it was mentioned in Section~\ref{sec:trutholr}., the procedure to remove the signal events form the $ggF$ backgrounf wwas performed. In order to account for any potential mismodelling between the signal and the background events, 100\% uncertainty is applied on the subtraction procedure.
%
