\section{Data and Monte Carlo samples}
\label{sec:data}

This section describes the data and Monte-Carlo simulated data (MC) samples used in the analysis. The MC samples are used to determine the shape of the di-photon mass spectra for the signal and the sub-dominant background processes, to optimise the selection criteria for the analysis, and to evaluate the efficiencies for signal and background processes where relevant. The MC samples are reconstructed using mc16a, mc16d and mc16e campaigns that reflect the change in the pile-up, triggers, and data-taking conditions in data.


The analysis is performed on data and simulation samples reconstructed with Athena release 21. The HIGG1D1 derivations are used and a full list of input HIGG1D1 DAODs is given in Appendix~\ref{sec:datasets}.

A custom p-flow jet collection is built using the di-photon vertex as the primary vertex, instead of the hardest primary vertex; the JVT and c-tagging are re-calculated to properly account for the change of primary vertex candidate. The extensive studies of the impact of the NN vertex was performed in HGam group and documented in common HGam note~\cite{Nomidis:2718255}.

All samples are analysed within HGamCore analysis framework, where mini xAODs are produced. The h027 tag was used for the MxAOD production, on top of AnalysisBase 21.2.237. At the MxAOD level all particle candidates are already calibrated, the weights and corrections are computed and added. The list of all output MxAODs relevant for the analysis is given in Appendix~\ref{sec:datasets}.

\subsection{Data samples}
The analysis is performed on the full Run~2 (2015--2018) $\sqrt{s}=13$~TeV \pp collision dataset, which used a proton bunch spacing of 25~ns. The data is recorded with the lowest-\Et~ unprescaled di-photon triggers, with have additional criteria on the photon identification and isolation to reduce the rates. The dataset obtained in 2015--2016 used the HLT\_g35\_loose\_g25\_loose\footnote{HLT\_g35\_loose\_g25\_loose is a high-level trigger (HLT) with \Et~ threshold of 35~GeV and 25~GeV for a leading and subleading photons, with \texttt{loose} shower shape criteria.} trigger, that collected 36.65~fb$^{-1}$ of luminosity. The 2017 and 2018 datasets were obtained with the HLT\_g35\_medium\_g25\_medium\_L12EM20VH\footnote{In 2017--2018 tighter selection was used. That required using \texttt{medium} shower shape criteria and a seed from the L1 trigger with more than 20~GeV of EM calorimeter deposition (L12EM20VH).} trigger, that collected 44.6~fb$^{-1}$ and 58.8~fb$^{-1}$ of $pp$ collision data, respectively. For all the years the single photon triggers HLT\_g120\_loose and HLT\_g140\_loose have been added too. The performance of the mentioned triggers is studied in detail in Ref.~\cite{Aad_2020} and Ref.~\cite{Monticelli:2652154}, and the average efficiency of the trigger system at HLT level was found to be greater than 99.2\%. The total integrated luminosity sampled by the trigger system, after data quality requirements, corresponds to (140.1$\pm$1.2)~fb$^{-1}$~\cite{DAPR-2021-01}.

\begin{table}[!htbp]
\centering
\noindent\begin{tabular}{|l|c|c|c|c|}
\hline
Year            &  Trigger   \\
\hline
2015+2016       &  \texttt{HLT\_g35\_loose\_g25\_loose} \\
\hline
2017+2018       &  \texttt{HLT\_g35\_medium\_g25\_medium\_L12EM20VH}\\
\hline
\end{tabular}
\caption{Summary of di-photon triggers used to select events in this analysis}
\label{table:grl}
\end{table}

\subsubsection{GRL}
The data analysed satisfies the "Good Run List" selection, summarised in Table~\ref{table:grl}, for which the LHC had stable beams, the ATLAS detector was operating sufficiently, and with both solenoid and toroid fields being at nominal conditions.

\begin{table}[!htbp]
\small
\centering
\noindent\begin{tabular}{|l|c|c|c|c|}
\hline
Year           &  GRL                                                                                                                        &   Lumin. [fb$^{-1}$]          & Uncert. [fb$^{-1}$]\\
\hline
2015           & \texttt{data15\_13TeV.periodAllYear\_DetStatus-v89-pro21-02}                                                                &   3.24  						 &  					\\
               & \texttt{\_Unknown\_PHYS\_StandardGRL\_All\_Good\_25ns.xml}                                                                  &         						 &  					\\
\hline
2016           & \texttt{data16\_13TeV.periodAllYear\_DetStatus-v89-pro21-01\_}                                                              &   33.4 						 &  					\\
               & \texttt{DQDefects-00-02-04\_PHYS\_StandardGRL\_All\_Good\_25ns.xml}                                                         &         						 &  					\\
\hline
		      &                                  																							 &   36.65						 &       0.32			\\
\hline
2017           & \texttt{data17\_13TeV.periodAllYear\_DetStatus-v99-pro22-01\_Unknown\_}   													 &   44.63 						 &       0.37			\\
               & \texttt{PHYS\_StandardGRL\_All\_Good\_25ns\_Triggerno17e33prim.xml}                                                         &         						 &						\\
\hline
2018           & \texttt{data18\_13TeV.periodAllYear\_DetStatus-v102-pro22-04\_Unknown\_} 													 &   58.79 						 &		 0.49		    \\
               & \texttt{PHYS\_StandardGRL\_All\_Good\_25ns\_Triggerno17e33prim.xml }                                                        &           					 &  					\\
\hline
Total          &																															 &   140.1 						 &       1.2		    \\
\hline
\end{tabular}
\caption{The breakdown of 2015-2018 GRLs and collected integrated luminosity with its uncertainty at 13~TeV.}
\label{table:grl}
\end{table}

\subsection{Signal and resonant background defintion}
\label{subsec:sigvsbkg}

All inclusively produced Higgs samples described below include not only $H+c$ signal events, but also the background events, which do not contain charm hadrons, thus considered as a resonant background to $H+c$ signal.
To differentiate the signal from the background events in different Higgs boson production modes, the truth matching is used. The event is considered to be a signal if it has at least one truth level c-jet with $p_T$ above 25 GeV and $|\eta|<2.5$.
If no such jets is present, the event is considered as a background. In order to identify the truth-level c-jet the flavour tagging label \texttt{HadronConeExclTruthLabelID}\footnote{Flavour label is done using B-hadrons, D-hadrons and tau leptons. The matching criteria between a truth jet and a hadron is $dR$<0.3, with hadrons transverse momenta being above 5 GeV } is used. The justification of choosing this particular signal definition is discussed in Appendix~\ref{sec:sigdef}. The distribution of the transverse momenta of truth and the charm jets are shown in Figure~\ref{fig:sig_sel}. The predicted cross-section for the $H+c$ signal defined in this way is 2.9~pb.

\begin{figure}[!htbp]
 \centering
 \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/mc/true_jets_pt.pdf}}
 \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/mc/true_Cjets_pt.pdf}}
 \caption{The transverse momenta distributions of truth level jets (a) as well as the charm jets (b) for different Higgs production modes. }
 \label{fig:sig_sel}
\end{figure}


\subsection{Nominal sample simulation}
\label{sec:data:sigMC}
Six Higgs boson production modes are considered: gluon--gluon fusion (ggF), vector boson fusion (VBF), and associated $WH$, $ZH$, $t\bar{t}H$ and  $b\bar{b}H$ production.
The rest of the Higgs boson production sources, such as $WtH$ and $tHq$ are considered to be negligible %($\sigma\times BR(tHbj)=0.169$~fb and $\sigma\times BR(WtH)=0.034$~fb).

The matrix elements for all the relevant samples are produced by {\sc Powheg}~\cite{Alioli_2009,Nason_2010,Luisoni_2013}. In particular, ggF samples are generated with NNLOPS~\cite{Hamilton_2013},
that reaches NNLO+NNLL accuracy in Higgs boson \pt and rapidity. All the generated samples are interfaced with \textsc{Pythia}~8 with the PDF4LHC15 PDF set and AZNLO CTEQ6L1 set of tuned parameters,
except for the $t\bar{t}H$, $b\bar{b}H$ samples for which the NNPDF2.3LO PDF set and the A14 set of tuned parameters are used. In all generated samples the Higgs boson mass is assumed to be 125~GeV and
its natural width is set to $\Gamma_{H}$ = 4.07~MeV. For more information on the Higgs boson simulations can be found in the common HGam note~\cite{Min:2309522}.

All of the samples are normalised with the latest available theoretical calculations of the SM production cross sections. The values of the cross sections are taken from~\href{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageAt13TeV}{CERNYellowReportPageAt13TeV},
while the values of the branching ratio, calculated at 125.09~GeV is taken from~\href{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageBR}{CERNYellowReportPageBR}.


Additionally to the six main Higgs boson production modes, the sensitive to the Charm Yukawa $g+c \rightarrow H+c $ MC sample has been simulated using \MADGRAPH~\cite{Alwall_2011} at LO\footnote{The syntax used in \MADGRAPH is generate p p > h c HIG=0, add process p p > h c~ HIG=0 } using Higgs Effective Couplings UFO model~\cite{DEGRANDE20121201}.
The decay of the Higgs boson into two photons is performed using \textsc{Pythia}~8~\cite{Sj_strand_2008}. The A14~\cite{ATL-PHYS-PUB-2014-021} set of tuned parameters and the NNPDF2.3LO PDF set are used.
%The choice of generator used to model the Higgs boson production is motivated by its ability to accommodate the effective $ggH$ vertex and two $y_{c}$ vertices (see Figure~\ref{fig:hc}) simultaneously.
Generator-level jets are required to have $p_\text{T}^{j}>10$~GeV and $|\eta^{j}|<4.7$. The Higgs boson mass $m_{H}$ is set to 125~GeV. The mass of the charm-quark in MadGraph is set at the Z-boson scale, $m_(Z)$=0.63~GeV~\cite{GIZHKO2017233}.
%The dominant Higgs boson production contribution comes from the effective vertex, and the change of the charm-quark mass will not significantly affect the results, thus the default value of 1.27~GeV for the mass is used. (See additional studies in Appendix~\ref{sec:signal_mc} for more details.)
The generated decay topology of the Higgs boson assumes BR($H\to\gamma\gamma$) = 100\%. Selected MC validation plots are shown in Figure~\ref{fig:sig_mc}. The details about the generation and validation plots can be found in \href{https://its.cern.ch/jira/browse/ATLMCPROD-10237}{ATLMCPROD-10237}.


Details about the samples, cross-sections and number of generated events can be found in Table~\ref{table:mcsamples}.


\begin{figure}[!htbp]
 \centering
 \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/mc/mc_noggf/N_j.pdf}}
 \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/mc/mc_noggf/pT_yy.pdf}}
 \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/mc/mc_noggf/pT_j1.pdf}}
 \caption{Selected validation plots for MC simulation at truth level:~(a)~jet multiplicity (b)~di-photon transverse momentum distribution; (c)~jet transverse momentum distribution. The red dotted plots are the old cH  definition where the ggF vertex is switched on, the back dotted plots are the new defintion where only charm Yukawa coupling vertices are generated.}
 \label{fig:sig_mc}
\end{figure}



\begin{table}[!htbp]
\scriptsize
\centering
\noindent\begin{tabular}{l|c|c|c|c|c|c}
\hline
Process           &  DSID   &   Generator(ME+PS) 	&      PDF(ME) 	&   PDF tune     & events generated (M)  & $\sigma\times BR$ [fb] \\
				  &         &				        &			    &                & mc16a/mc16d/mc16e     &         				  \\
\hline
ggF               & 343981  & Powheg NNLOPS+Pythia8 &    PDF4LHC15  &    AZNLOCTEQ6  & 5/5/8.3               & 110.1                  \\
VBF               & 346214  & Powheg-Box+Pythia8    &    PDF4LHC15  &    AZNLOCTEQ6  & 2/2.5/2.5			 & 8.578			      \\
W$^{-}$H          & 345317  & Powheg-Box+Pythia8    &    PDF4LHC15  &    AZNLOCTEQ6  & 0.2/0.2/0.2           & 1.206                  \\
W$^{+}$H          & 345318  & Powheg-Box+Pythia8    &    PDF4LHC15  &    AZNLOCTEQ6  & 0.2/0.2/0.2           & 1.902				  \\
$gg\to$ZH		  & 345061  & Powheg-Box+Pythia8    &    PDF4LHC15  &    AZNLOCTEQ6  & 0.05/0.05/0.05        & 0.2782				  \\
$qq\to$ZH         & 345319  & Powheg-Box+Pythia8    &    PDF4LHC15  &    AZNLOCTEQ6  & 0.5/0.5/0.5  	     & 1.725				  \\
$t\bar{t}H$       & 346525  & Powheg-Box+Pythia8    &    PDF4LHC15  &    A14NNPDF23  & 2/2.5/3.3             & 1.150				  \\
$bbH$             & 345315  & Powheg-Box+Pythia8    &    PDF4LHC15  &    A14NNPDF23  & 0.1/0.125/0.17        & 1.104				  \\
$Hc$              & 346394  & MG5+Pythia8           &       NNPDF   &    A14NNPDF23  & 0.1/0.125/0.17        & 0.064468               \\
$\gamma\gamma$    & 364352  & Sherpa(ME@NLO+PS)     &   NNPDF3.0NNLO&    Sherpa      & 260/322/443           & 51.823                  \\
\hline
\end{tabular}
\caption{List of Nominal simulated datasets for $Hc$ analysis.}
\label{table:mcsamples}
\end{table}

%Additionally, an alternative signal sample was generated (not yet requested; to be produced officially), to study the matrix element and parton shower uncertainties. The alternative sample was generated and showered using LO Sherpa~2.2.11~\cite{Schumann:2007mg} generator using HEFT model, that also includes the three relevant vertices, shown in Figure~\ref{fig:hc}.

\subsection{Alternative sample simulation}
\label{sec:data:alt}
Alternative samples were also generated in order to estimate the uncertainties related to the modeling of the parton shower. For this purpose the $ggF$, $VBF$, $WH$, $ZH$, $ttH$ samples used the same events from the matrix element generator of the nominal sample but were showered with Herwig 7.1.3 (Herwig 7.0.4) with H7UE tune instead of Pythia 8. The summary of the alternative samples and their statistics is shown in Table~\ref{table:mcsamplesalt}


\begin{table}[!htbp]
\scriptsize
\centering
\noindent\begin{tabular}{l|c|c|c|c|c|c}
\hline
Process           &  DSID   &   Generator(ME+PS) 	&      PDF(ME) 	&   PDF tune     & events generated (M)  & $\sigma\times BR$ [fb] \\
				  &         &				        &			    &                & mc16a/mc16d/mc16e     &         				  \\
\hline
ggF               & 346797  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE        & 5/5/8.3               & 110.1                  \\
VBF               & 346878  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE  		 & 2/2.5/2.5			 & 8.578			      \\
W$^{-}$H          & 346880  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE  		 & 0.2/0.2/0.2           & 1.206                  \\
W$^{+}$H          & 346881  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE  		 & 0.2/0.2/0.2           & 1.902				  \\
$gg\to$ZH		  & 346882  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE  		 & 0.05/0.05/0.05        & 0.2782				  \\
$qq\to$ZH         & 346879  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE  		 & 0.5/0.5/0.5  	     & 1.725				  \\
$t\bar{t}H$       & 346526  & Powheg NNLOPS+Herwig  &    PDF4LHC15  &    H7UE  		 & 2/2.5/3.3             & 1.150				  \\
\hline
\end{tabular}
\caption{List of Alternative simulated datasets for $Hc$ analysis.}
\label{table:mcsamplesalt}
\end{table}


\subsection{Background MC}
\label{sec:data:bkdMC}



%\subsubsection{Resonant background}
%Due to inclusively produced Higgs MC samples, which include not only $H + c$ signal, but also the events without charm hadron, the resonant background is estimated using the same MC as for the signal. For more details on signal and background definition see Section~\ref{subsec:sigvsbkg}

\subsubsection{Di-photon background}

The dominant background in this search arises from non-resonant irreducible QCD $\gamma\gamma$ production. The $\gamma\gamma$ samples are generated using Sherpa~2.1.4~\cite{Gleisberg_2009,Bothmann_2019}, following a multi-leg scheme in which $\gamma\gamma$+0,1 jets are generated at NLO, while $\gamma\gamma$+2.3 jets are generated at LO. Sherpa default tuning is used for the underlying event. The phase-space of the generation is adjusted to be between 90~GeV and 175~GeV, so that only events around the Higgs mass region are generated. The matrix elements that are calculated at NLO and LO are merged with the Sherpa parton shower~\cite{Schumann_2008} according to the ME+PS@NLO prescription~\cite{H_che_2012}. The PDF set used is the NNPDF3.0NNLO~\cite{Ball_2015}.

In addition to the full-sim samples, fast-sim samples are used with a total of 720~M events. These samples are used in the analysis to validate the background estimation procedure and Spurious signal studies to create a background template.

\subsection{Detector simulation}

All MC samples include a simulation of pileup interactions, and are processed through a full simulation of the detector response~\cite{Aad_2010} based on {\sc Geant4}~\cite{AGOSTINELLI2003250}, and the same reconstruction software as the data. The effect of multiple interactions (in-time pileup) is modelled by generating minimum-bias events with \textsc{Pythia}~8 and overlaying them onto the generated hard-scatter events. The out-of-time pile up is also included by adding the detector signals from the previous bunch crossings. These minimum bias events are also simulated using \textsc{Pythia}~8. The only exception to this is the fast-simulation non-resonant $\gamma\gamma$ MC sample, which uses a fast-simulation of the ATLAS detector that includes a parameterised  calorimeter response.

When using \textsc{Pythia}~8 to force the Higgs boson to decay to two photons, in some small fraction of events (approx. 6.1\%) the Higgs boson undergoes a Dalitz decay, $H\to\gamma\gamma^{*}\to\gamma ff$, with $\gamma^{*}$ being an off-shell photon and $f$ any charged fermion. The Dalitz events are not removed at the reconstruction level and those events contribute to the measured signal and background events.



Event weights are applied to the simulation to account for data/MC discrepancy. Energies and momentum corrections and smearing are also applied on the reconstructed particles so that momentum sales and resolutions will match in Data and MC.

the sum of generator-level events weights, luminosity, the Higgs production cross sections and the BR to di-photons are used to determine the normalisation factor for simulated events.
