#!/bin/bash

# cc7

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh"
lcg_reqs="Python
pip
numpy
pandas
scikitlearn
keras
tensorflow"
for req in ${lcg_reqs}; do
    echo "Setting up ${req}"
    lsetup "lcgenv -p LCG_96b x86_64-centos7-gcc62-opt ${req}" > /dev/null
done
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
