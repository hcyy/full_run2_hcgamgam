#ifndef CONTROLHIST
#define CONTROLHIST


#include "TFile.h"
#include "TH3.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TColor.h"
#include "TStyle.h"

#include <algorithm>
#include <map>
#include <vector>


class ControlHist
{
 public:

  ControlHist(const TString name, const TString axes, const int nBins, const double min, const double max, const double maxY=-999, const double minY=-999);
  ControlHist(const TString name, const TString axes, const int nBinsX, const double minX, const double maxX, const int nBinsY, const double minY, const double maxY);
  ~ControlHist();

  bool is2D() {return m_is2D;};

  TString GetName() {return m_name;};

  double GetMaxY() {return m_maxY;};
  double GetMinY() {return m_minY;};

  void Fill(int theSample, const double value, const double weight, std::vector<int> PassedCuts);
  void Fill(int theSample, const double valueX, const double valueY, const double weight, std::vector<int> PassedCuts);
  void WriteAll();
  TH1D *GetHist1D(int sample, int cut) {return m_hists1D[std::make_pair(sample, cut)];};
  TH2D *GetHist2D(int sample, int cut) {return m_hists2D[std::make_pair(sample, cut)];};
  TH1D *GetTotalHist1D(int cut);
  TH2D *GetTotalHist2D(int cut);
  void SetHist1D(int sample, int cut, TH1D *hist) {m_hists1D[std::make_pair(sample, cut)]=hist;};
  void SetHist2D(int sample, int cut, TH2D *hist) {m_hists2D[std::make_pair(sample, cut)]=hist;};

  static std::vector<int> samples_data;
  static std::vector<int> samples_signal;
  static std::vector<int> samples_background;
  std::vector<int> samples;
  static std::vector<int> cuts;

  static std::map<int, TString> int2string_sample;
  static std::map<int, TString> int2displayString_sample;
  static std::map<int, double> int2float_sample;
  static std::map<int, TString> int2string_cut;

 private:

  TString m_name;
  double m_maxY=-999;
  double m_minY=-999;
  bool m_is2D;
  std::map<std::pair<int, int>, TH1D*> m_hists1D;
  std::map<std::pair<int, int>, TH2D*> m_hists2D;
};


#endif
