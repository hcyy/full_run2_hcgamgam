#include "Analysis.h"

//////////////////////////////////////////////////////////////
// Configurable Options

//////////////////////////////////////////////////////////////
// Samples to Run Over

std::vector<int> ControlHist::samples_data = {};
std::vector<int> ControlHist::samples_signal = {Sample::cH};
std::vector<int> ControlHist::samples_background = {Sample::ggFH, Sample::VBFH, Sample::WmH, Sample::WpH, Sample::ZH, Sample::ggZH, Sample::ttH, Sample::bbH, Sample::yy};
std::vector<int> ControlHist::cuts = {Cut::All};
const bool doTest = false;
const int testEvents = 1000;
const bool doMC16a = true;
const bool doMC16d = true;
const bool doMC16e = true;
const bool doLeadJetCutflow = false;
const bool doTightCTag = false;
// const int cutflowIndex = 0;
const int cutflowIndexForNorm = 3;
const float BRHyy = 0.00227;
const bool normHists = false;

double passPtpassEta=0;
double passBothInd=0;
double passPtnotEta=0;
double notPtpassEta=0;
double notPtnotEta=0;

double sumW = 0.;
double nEv = 0.;

int main(int argc, char* argv[])
{
  SetupSamples();
  SetupControlHists();

  // // Old ntuples
  // Process({"/disk/moose/atlas/etjr/cHAnalysis/SignalNtuples_19Aug20/mc16a.MGPy8_cH_Hgammagamma.MxAODDetailed.e7180_s3126_r9364_p4097_h025.root"}, Sample::cH, true, PileupProfile::MC16a);
  // Process({"/disk/moose/atlas/etjr/cHAnalysis/SignalNtuples_19Aug20/mc16d.MGPy8_cH_Hgammagamma.MxAODDetailed.e7180_s3126_r10201_p4097_h025.root"}, Sample::cH, true, PileupProfile::MC16d);
  // Process({"/disk/moose/atlas/etjr/cHAnalysis/SignalNtuples_19Aug20/mc16e.MGPy8_cH_Hgammagamma.MxAODDetailed.e7180_s3126_r10724_p4097_h025.root"}, Sample::cH, true, PileupProfile::MC16e);

  // New ntuples
  Process({"/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16a.MGPy8_cH_Hgammagamma.MxAODDetailed.e7180_s3126_r9364_p4097_h025.root"}, Sample::cH, true, PileupProfile::MC16a);
  Process({"/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16d.MGPy8_cH_Hgammagamma.MxAODDetailed.e7180_s3126_r10201_p4097_h025.root"}, Sample::cH, true, PileupProfile::MC16d);
  Process({"/disk/moose/atlas/etjr/cHAnalysis/Ntuples_25Oct20/mc16e.MGPy8_cH_Hgammagamma.MxAODDetailed.e7180_s3126_r10724_p4097_h025.root"}, Sample::cH, true, PileupProfile::MC16e);

  // TH1D *myy = ControlHists[ControlHistName::myy]->GetTotalHist1D(Cut::All);

  // TF1 fit = TF1("fit", "gaus(0)");
  // myy->Fit(&fit, "", "");
  // fit.Draw("SAME");
  
  SaveControlHists("Signals.root", {ControlHistName::DL1, ControlHistName::DL1r, ControlHistName::myy});

  PlotControlHists();
  PrintEfficiencies();

  const double total = passPtpassEta+passBothInd+passPtnotEta+notPtpassEta+notPtnotEta;
  std::cout<<"passPtpassEta="<<passPtpassEta/total*100.<<"%"<<std::endl;
  std::cout<<"passBothInd="<<passBothInd/total*100.<<"%"<<std::endl;
  std::cout<<"passPtnotEta="<<passPtnotEta/total*100.<<"%"<<std::endl;
  std::cout<<"notPtpassEta="<<notPtpassEta/total*100.<<"%"<<std::endl;
  std::cout<<"notPtnotEta="<<notPtnotEta/total*100.<<"%"<<std::endl;

  std::cout<<"sumW = "<<sumW<<std::endl;
  std::cout<<"nEv = "<<nEv<<std::endl;

  TH1D *hist = ControlHists[ControlHistName::myy]->GetHist1D(Sample::cH, Cut::All);
  for(int i=1; i<=hist->GetNbinsX(); i++)
    {
      if(i==hist->GetNbinsX()) std::cout<<hist->GetBinContent(i)<<std::endl;
      else std::cout<<hist->GetBinContent(i)<<", ";
    }

  return 0;
}


// Define the function which processes the events
void Process(const std::vector<TString> filePaths, const int sample, const bool isMC, const int pileupProfile)
{
  std::cout<<"Processing: ";
  for(int i=0; i<filePaths.size(); i++) std::cout<<filePaths[i];
  std::cout<<std::endl;

  float norm=1;
  if(isMC)
    {
      // if(pileupProfile==PileupProfile::MC16a) norm*=lumi_MC16a;
      // else if(pileupProfile==PileupProfile::MC16d) norm*=lumi_MC16d;
      // else if(pileupProfile==PileupProfile::MC16e) norm*=lumi_MC16e;
      if(pileupProfile==PileupProfile::MC16a) norm*=lumi_MC16a/GetSumWeights(filePaths, sample, cutflowIndexForNorm);
      else if(pileupProfile==PileupProfile::MC16d) norm*=lumi_MC16d/GetSumWeights(filePaths, sample, cutflowIndexForNorm);
      else if(pileupProfile==PileupProfile::MC16e) norm*=lumi_MC16e/GetSumWeights(filePaths, sample, cutflowIndexForNorm);
    }

  const bool isSignal = std::any_of(ControlHist::samples_signal.cbegin(), ControlHist::samples_signal.cend(), [&](int ch_sample){return ch_sample==sample;});

  bool firstEvent = true;

  for(int i_filePath = 0; i_filePath<filePaths.size(); i_filePath++)
    {
      TString filePath = filePaths[i_filePath];

      Reader *reader = InitReader(filePath);

      const int nEvents = reader->fChain->GetEntries();

      for(int event=0; event<nEvents; event++)
        { //Event loop
          if(event!=0 and event%10000==0) std::cout<<event<<"/"<<nEvents<<" processed successfully"<<std::endl;

          if(doTest and event==testEvents)
            {
              delete reader;
              return;
            }

          reader->GetEntry(event);

          if(isMC and firstEvent)
            {
              float initialNEventsFile = reader->HGamEventInfoAuxDyn_crossSectionBRfilterEff;
              // if(isSignal) initialNEventsFile *= BRHyy;
              if(pileupProfile==PileupProfile::MC16a) initialNEventsFile *= lumi_MC16a;
              else if(pileupProfile==PileupProfile::MC16d) initialNEventsFile *= lumi_MC16d;
              else if(pileupProfile==PileupProfile::MC16e) initialNEventsFile *= lumi_MC16e;
              initialNEvents[sample] += initialNEventsFile;
              firstEvent = false;
            }


          float eventWeight=norm;
          if(isMC)
            {
              eventWeight*=reader->HGamEventInfoAuxDyn_weight;
              eventWeight*=reader->HGamEventInfoAuxDyn_weightJvt;
              // eventWeight*=reader->HGamEventInfoAuxDyn_Hc_weight;
              eventWeight*=reader->HGamEventInfoAuxDyn_crossSectionBRfilterEff;
              // eventWeight*=reader->HGamEventInfoAuxDyn_pileupWeight;
              // eventWeight*=reader->HGamEventInfoAuxDyn_vertexWeight;
              // eventWeight*=reader->HGamEventInfoAuxDyn_weightSF;
              // if(isSignal) eventWeight*=BRHyy;
              if(doTest and nEvents>testEvents) eventWeight*=nEvents/static_cast<double>(testEvents);

              // if(systematic==1) eventWeight*=reader->PileupWeightUp;
              // else if(systematic==-1) eventWeight*=reader->PileupWeightDown;
              // else eventWeight*=reader->PileupWeight;
          
              // if(systematic==2) eventWeight*=reader->TrigSF_up;
              // else if(systematic==-2) eventWeight*=reader->TrigSF_down;
              // else eventWeight*=reader->TrigSF;
          
              // if(systematic==3) eventWeight*=reader->LeptonsSF_up;
              // else if(systematic==-3) eventWeight*=reader->LeptonsSF_down;
              // else eventWeight*=reader->LeptonsSF;
          
              // if(not ignoreJvtSF)
              //   {
              //     if(systematic==4) eventWeight*=reader->JvtSF_up;
              //     else if(systematic==-4) eventWeight*=reader->JvtSF_down;
              //     else eventWeight*=reader->JvtSF;
              //   }
            }

          //////////////////////////////////////////////////////////////
          // Setup Variables and Selection

          // std::cout<<"---"<<std::endl;
          // std::cout<<"Event "<<event<<": "<<reader->HGamEventInfoAuxDyn_m_yy/1000.<<std::endl;

          if(reader->HGamEventInfoAuxDyn_isPassed!=1) continue;

          // std::cout<<"====="<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isDalitz = "<<reader->HGamEventInfoAuxDyn_isDalitz<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isPassedBasic = "<<reader->HGamEventInfoAuxDyn_isPassedBasic<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isPassedIsolation = "<<reader->HGamEventInfoAuxDyn_isPassedIsolation<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isPassedJetEventClean = "<<reader->HGamEventInfoAuxDyn_isPassedJetEventClean<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isPassedPID = "<<reader->HGamEventInfoAuxDyn_isPassedPID<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isPassedPreselection = "<<reader->HGamEventInfoAuxDyn_isPassedPreselection<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_isPassedTriggerMatch = "<<reader->HGamEventInfoAuxDyn_isPassedTriggerMatch<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_pT_j1 = "<<reader->HGamEventInfoAuxDyn_pT_j1<<std::endl;
          // std::cout<<"HGamEventInfoAuxDyn_pT_j2 = "<<reader->HGamEventInfoAuxDyn_pT_j2<<std::endl;
          // std::cout<<"reader->HGamEventInfoAuxDyn_pileupWeight = "<<reader->HGamEventInfoAuxDyn_pileupWeight<<std::endl;
          // std::cout<<"reader->HGamEventInfoAuxDyn_vertexWeight = "<<reader->HGamEventInfoAuxDyn_vertexWeight<<std::endl;
          // std::cout<<"reader->HGamEventInfoAuxDyn_weightSF = "<<reader->HGamEventInfoAuxDyn_weightSF<<std::endl;
          // std::cout<<"reader->HGamEventInfoAuxDyn_weightTrigSF = "<<reader->HGamEventInfoAuxDyn_weightTrigSF<<std::endl;
          // std::cout<<"NN jet pTs: ";
          // for(int i=0; i<reader->HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt->size(); i++)
          //   {
          //     std::cout<<reader->HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn_pt->at(i)<<" ";
          //   }
          // std::cout<<std::endl;

          // std::cout<<"1"<<std::endl;

          if(reader->HGamEventInfoAuxDyn_Hcgam_Atleast1jisloose!=1) continue;

          // std::cout<<"2"<<std::endl;

          // std::cout<<"--------"<<std::endl;
          // std::cout<<"norm = "<<norm<<std::endl;
          // std::cout<<"weight = "<<reader->HGamEventInfoAuxDyn_weight<<std::endl;
          // std::cout<<"weightJvt = "<<reader->HGamEventInfoAuxDyn_weightJvt<<std::endl;
          // std::cout<<"xsecBRFE = "<<reader->HGamEventInfoAuxDyn_crossSectionBRfilterEff<<std::endl;

          sumW += eventWeight;
          nEv ++;
          
          finalNEvents[std::make_pair(Cut::All, sample)]+=eventWeight;
          finalNEventsSq[std::make_pair(Cut::All, sample)]+=eventWeight*eventWeight;

          std::vector<int> passedCuts;
          passedCuts.push_back(Cut::All);
          ControlHists[ControlHistName::myy]->Fill(sample, reader->HGamEventInfoAuxDyn_m_yy/1000., eventWeight, passedCuts);
        }

      delete reader;
    }
}


Reader *InitReader(const TString filePath)
{
  TFile *file = TFile::Open(filePath);
  TTree *tree = (TTree*) file->Get(treeName);
  Reader *reader = new Reader(tree);
  return reader;
}


void MakeEfficiencyPlots() // Only makes plots where cut is optimised on same sample
{
  std::cout<<"Making Efficiency Plots"<<std::endl;

  system("mkdir Plots");
  system("mkdir Plots/Efficiencies");

  for(std::vector<int>::iterator cut=ControlHist::cuts.begin(); cut!=ControlHist::cuts.end(); ++cut)
    {
      TCanvas *c = new TCanvas();
      TGraphErrors *plot = new TGraphErrors(ControlHist::samples_signal.size());
      int count=0;
  
      for(std::vector<int>::iterator sample = ControlHist::samples_signal.begin(); sample!=ControlHist::samples_signal.end(); ++sample)
        {
          plot->SetPoint(count, ControlHist::int2float_sample[*sample], finalNEvents[std::make_pair(*cut, *sample)]/initialNEvents[*sample]);
          plot->SetPointError(count, 0, TMath::Sqrt(finalNEventsSq[std::make_pair(*cut, *sample)])/initialNEvents[*sample]);
          count++;
        }
  
      plot->SetTitle("");
      plot->GetXaxis()->SetTitle("m_{X}");
      plot->GetYaxis()->SetTitle("Efficiency");
      plot->Draw();
      c->SaveAs("Plots/Efficiencies/Efficiencies_"+ControlHist::int2string_cut[*cut]+".png");

      delete plot;
      delete c;
    }
}


void PrintEfficiencies()
{
  std::cout<<std::endl<<"Printing Efficiencies:"<<std::endl<<std::endl;

  for(std::vector<int>::iterator sample = samples.begin(); sample != samples.end(); ++sample)
    {
      std::cout<<ControlHist::int2string_sample[*sample]<<": Initial events = "<<initialNEvents[*sample]<<std::endl;

      for(std::vector<int>::iterator cut = ControlHist::cuts.begin(); cut != ControlHist::cuts.end(); ++cut)
        {
          std::cout<<ControlHist::int2string_cut[*cut]<<": Final events = "<<finalNEvents[std::make_pair(*cut, *sample)]<<", efficiency = "<<finalNEvents[std::make_pair(*cut, *sample)]/initialNEvents[*sample]<<"+-"<<TMath::Sqrt(finalNEventsSq[std::make_pair(*cut, *sample)])/initialNEvents[*sample]<<", and correct X efficiency = "<<correctXEvents[std::make_pair(*cut, *sample)]/finalNEvents[std::make_pair(*cut, *sample)]<<"+-"<<TMath::Sqrt(correctXEventsSq[std::make_pair(*cut, *sample)])/finalNEvents[std::make_pair(*cut, *sample)]<<std::endl;
        }

      std::cout<<std::endl;
    }
}


void SetupSamples()
{
  samples=ControlHist::samples_data;
  samples.insert(samples.end(), ControlHist::samples_signal.begin(), ControlHist::samples_signal.end());
  samples.insert(samples.end(), ControlHist::samples_background.begin(), ControlHist::samples_background.end());
}


float GetSumWeights(std::vector<TString> filePaths, const int sample, const int index)
{
  float sumWeights=0;
  for(int i_filePath = 0; i_filePath<filePaths.size(); i_filePath++)
    {
      TString filePath = filePaths[i_filePath];
      TFile *file = TFile::Open(filePath);
      TH1F *info = dynamic_cast<TH1F*>(file->Get(int2histName[sample]));
      sumWeights+=info->GetBinContent(index);
      delete info;
      file->Close();
    }
  return sumWeights;
}


void SetupControlHists()
{
  std::cout<<"Setting Up ControlHists"<<std::endl;
  ControlHists[ControlHistName::myy] = new ControlHist("myy", "; m_{#gamma#gamma} [GeV]; Events / GeV", 50, 120., 130.);
  ControlHists[ControlHistName::DL1] = new ControlHist("DL1", "; DL1 discriminant; frac; Events", 1000, -5, 5, 1000, 0, 1);
  ControlHists[ControlHistName::DL1r] = new ControlHist("DL1r", "; DL1r discriminant; frac; Events", 1000, -5, 5, 1000, 0, 1);
}


void PlotControlHists()
{
  std::cout<<"Plotting ControlHists"<<std::endl;
  plotter = new Plotter(plotter_fileType, plotter_seperateSignal, plotter_seperateUncertainties, plotter_coloredSignal);
  for(std::vector<int>::iterator cut = ControlHist::cuts.begin(); cut != ControlHist::cuts.end(); ++cut)
    {
      for(std::map<int, ControlHist*>::iterator controlHist = ControlHists.begin(); controlHist != ControlHists.end(); ++controlHist)
	{
          TString plotterOptions = "AWIP labels fixedratio ";
          if(normHists) plotterOptions+="normall ";
          plotter->Plot(controlHist->second, *cut, plotterOptions, "gaus(0)");
	}
    }
}

void SaveControlHists(const TString name, std::vector<int> hists)
{
  system("mkdir "+outputHistPath);
  TFile *outFile = new TFile(outputHistPath+"/"+name, "recreate");
  outFile->cd();
  for(int i=0; i<hists.size(); i++) ControlHists[hists.at(i)]->WriteAll();
  outFile->Close();
  delete outFile;
}
