#!/usr/bin/env python


import os
import matplotlib
matplotlib.use("Agg") # stops matplotlib crashing when remote disconneted for a while
import matplotlib.pyplot as plt
import numpy as np


if __name__ == "__main__":

    ############################################
    ## Read nominal file

    nFiles = 268
    doCTagCat = 1
    doSignal = 1
    print("Doing doCTagCat = %d and doSignal = %d" % (doCTagCat, doSignal))

    filenameNom = "201.out"
    fileNom = open(filenameNom, "r")
    linesNom = fileNom.readlines()

    doCTagCatStringNom = ""
    doSignalStringNom = ""
    binStringNom = ""
    for line in linesNom:
        if line.find("doCTagCat = ") != -1:
            doCTagCatStringNom = str(line)
        if line.find("doSignal = ") != -1:
            doSignalStringNom = str(line)
        if line.find("Total bin contents: ") != -1:
            binStringNom = str(line)

    doCTagCatStringNom = doCTagCatStringNom.replace("doCTagCat = ", "")
    doCTagCatNom = int(doCTagCatStringNom)
    doSignalStringNom = doSignalStringNom.replace("doSignal = ", "")
    doSignalNom = int(doSignalStringNom)

    if doCTagCatNom != doCTagCat:
        print("ERROR: doCTagCatNom != doCTagCat")
        exit()

    if doSignalNom != doSignal:
        print("ERROR: doSignalNom != doSignal")
        exit()

    binStringNom = binStringNom.replace("Total bin contents: ", "")
    binsStringsNom = binStringNom.split(", ")

    binsNom = []
    for b in range(len(binsStringsNom)):
        binsNom.append(float(binsStringsNom[b]))

    totalNom = sum(binsNom)
    shapeNom = [b / totalNom for b in binsNom]

    ############################################
    ## Loop over alternative files

    for filenumber in range(1, nFiles):

        ############################################
        ## Read alternative files

        filenameAlt = str(filenumber) + ".out"
        fileAlt = open(filenameAlt, "r")
        linesAlt = fileAlt.readlines()

        doCTagCatStringAlt = ""
        doSignalStringAlt = ""
        sysStringAlt = ""
        binStringAlt = ""
        for line in linesAlt:
            if line.find("doCTagCat = ") != -1:
                doCTagCatStringAlt = str(line)
            if line.find("doSignal = ") != -1:
                doSignalStringAlt = str(line)
            if line.find("sys = ") != -1:
                sysStringAlt = str(line)
            if line.find("Total bin contents: ") != -1:
                binStringAlt = str(line)

        if doCTagCatStringAlt == "" or doSignalStringAlt == "" or sysStringAlt == "" or binStringAlt == "":
            continue

        doCTagCatStringAlt = doCTagCatStringAlt.replace("doCTagCat = ", "")
        doCTagCatAlt = int(doCTagCatStringAlt)
        doSignalStringAlt = doSignalStringAlt.replace("doSignal = ", "")
        doSignalAlt = int(doSignalStringAlt)

        if doCTagCatAlt != doCTagCat:
            continue

        if doSignalAlt != doSignal:
            continue

        sysStringAlt = sysStringAlt.replace("sys = ", "")
        sysStringAlt = sysStringAlt.replace("\n", "")

        binStringAlt = binStringAlt.replace("Total bin contents: ", "")
        binsStringsAlt = binStringAlt.split(", ")

        binsAlt = []
        for b in range(len(binsStringsAlt)):
            # print(binsStringsAlt[b])
            binsAlt.append(float(binsStringsAlt[b]))

        totalAlt = sum(binsAlt)
        shapeAlt = [b / totalAlt for b in binsAlt]

        ############################################
        ## Calculate normalisation uncertainties

        uncNorm = (totalAlt-totalNom)/totalNom
        if(abs(uncNorm)>0.01):
            print("Systematic %s: %f <<< Include!!!" % (sysStringAlt, uncNorm))
        else:
            print("Systematic %s: %f" % (sysStringAlt, uncNorm))

        ############################################
        ## Plots shapes

        if not os.path.exists("Plots"): os.mkdir("Plots")
        fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8,8))
        axs.plot(shapeNom, color="black")
        axs.plot(shapeAlt, color="red")
        axs.title.set_text("")
        plt.xlabel("myy")
        plt.ylabel("Events (unit norm)")
        fig.tight_layout(pad=2.0)
        fig.savefig("Plots/"+sysStringAlt+".png")
        plt.close()

        
