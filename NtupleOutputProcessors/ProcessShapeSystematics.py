#!/usr/bin/env python


import os
import matplotlib
matplotlib.use("Agg") # stops matplotlib crashing when remote disconneted for a while
import matplotlib.pyplot as plt
import numpy as np


def MakePlot(name, xaxis, hists, colours, labels, yaxislabel, doNorm=False, doNormByFirstHist=False, printBins=False):
    if not os.path.exists("Plots"): os.mkdir("Plots")
    fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8,8))
    for i in range(len(hists)):
        if doNormByFirstHist:
            if i==0: continue
            histNorm = []
            for b in range(len(hists[0])):
                histNorm.append(hists[i][b]/hists[0][b])
                print(hists[i][b]/hists[0][b]-1.)
            print()
            axs.plot(xaxis, histNorm, color=colours[i-1], label=labels[i-1])
        elif doNorm:
            total = sum(hists[i])
            histNorm = [b / total for b in hists[i]]
            axs.plot(xaxis, histNorm, color=colours[i], label=labels[i-1])
        else:
            axs.plot(xaxis, hists[i], color=colours[i], label=labels[i])
    axs.title.set_text("")
    axs.legend(frameon=False)
    plt.xlabel(r"$m_{\gamma\gamma}$ [GeV]")
    plt.ylabel(yaxislabel)
    fig.tight_layout(pad=2.0)
    fig.savefig("Plots/"+name+".png")
    plt.close()


if __name__ == "__main__":

    histNomSigCTag = [0.401022, 0.574305, 0.960126, 1.48851, 2.33558, 3.67582, 5.47223, 7.77245, 9.94603, 11.4229, 11.6818, 10.4844, 8.41741, 6.29332, 4.28614, 2.96802, 1.93322, 1.23316, 0.788577, 0.478298]
    histNomSigNonCTag = [2.04991, 3.16771, 4.79914, 7.45163, 11.5625, 18.0304, 26.4813, 37.0817, 46.8472, 53.5773, 54.4998, 49.6271, 40.9413, 31.0866, 22.4418, 15.3028, 10.0465, 6.69252, 4.28264, 2.72341]
    histNomBkdCTag = [0.467719, 0.686945, 1.06565, 1.65842, 2.47173, 3.85836, 5.77486, 7.86951, 9.74931, 11.3249, 11.3507, 10.3507, 8.42196, 6.40762, 4.57036, 3.02151, 2.10021, 1.33317, 0.891956, 0.571346]
    histNomBkdNonCTag = [9.00785, 13.524, 20.8673, 32.6287, 50.3845, 76.4746, 112.859, 153.464, 193.429, 219.042, 223.245, 203.483, 168.019, 128.829, 93.7797, 64.7061, 43.4909, 28.4182, 18.5061, 11.6879]

    histResDownSigCTag = [0.333573, 0.518255, 0.774922, 1.25187, 2.04991, 3.29102, 5.2429, 7.77795, 10.444, 12.395, 12.5565, 11.0036, 8.53716, 6.09152, 4.08342, 2.65876, 1.66929, 1.08438, 0.673434, 0.412665]
    histResDownSigNonCTag = [1.72714, 2.66643, 4.07111, 6.40921, 10.2828, 16.2325, 25.5194, 37.2203, 49.2021, 57.4192, 58.3536, 52.054, 41.6159, 30.7436, 21.2069, 14.1626, 9.13095, 5.77351, 3.77799, 2.33343]
    histResDownBkdCTag = [0.393954, 0.620223, 0.883192, 1.44168, 2.26976, 3.51472, 5.51444, 7.99532, 10.2298, 12.0842, 12.1956, 10.8623, 8.59028, 6.19916, 4.27512, 2.87931, 1.78074, 1.23058, 0.732024, 0.518203]
    histResDownBkdNonCTag = [7.85201, 11.7066, 17.839, 28.1868, 44.7572, 70.7289, 108.791, 155.419, 202.15, 234.205, 238.248, 212.737, 171.501, 127.392, 89.1931, 60.0096, 39.0745, 25.402, 16.1873, 10.1406]

    histResUpSigCTag = [0.500799, 0.79521, 1.18117, 1.84842, 2.79359, 4.13124, 5.70275, 7.61575, 9.26999, 10.1635, 10.3944, 9.72414, 8.14898, 6.44307, 4.70296, 3.32739, 2.28701, 1.52707, 0.952823, 0.63842]
    histResUpSigNonCTag = [2.62075, 3.97112, 5.95949, 9.20384, 13.6459, 19.8173, 27.6656, 36.1587, 43.6634, 48.5763, 49.1369, 45.7849, 39.5231, 31.3959, 23.837, 16.848, 11.8174, 7.88766, 5.30112, 3.42347]
    histResUpBkdCTag = [0.576737, 0.876173, 1.31738, 1.91332, 2.95751, 4.31663, 5.97168, 7.59309, 9.17422, 10.1728, 10.3364, 9.47653, 8.14513, 6.51873, 4.86556, 3.40009, 2.42039, 1.63339, 1.08546, 0.704143]
    histResUpBkdNonCTag = [11.204, 17.1181, 25.9081, 38.9843, 58.299, 84.4404, 115.906, 150.16, 180.824, 198.988, 202.769, 188.873, 161.914, 129.991, 98.5881, 71.3016, 49.8162, 33.6652, 22.2957, 14.7424]

    histScaleDownSigCTag = [0.587248, 0.93413, 1.43607, 2.18417, 3.44299, 5.08165, 7.1257, 9.1792, 11.0912, 11.4858, 10.8746, 8.97474, 6.83588, 4.75779, 3.2773, 2.11971, 1.34931, 0.877861, 0.518807, 0.339252]
    histScaleDownSigNonCTag = [3.09781, 4.65957, 7.12109, 10.9273, 16.6907, 24.3912, 33.7614, 44.059, 51.5309, 54.0939, 51.0691, 43.2185, 33.7699, 24.5847, 16.822, 11.1893, 7.4314, 4.66403, 3.05229, 1.90025]
    histScaleDownBkdCTag = [0.712873, 1.02678, 1.53002, 2.43248, 3.54656, 5.2044, 7.32581, 9.16631, 10.917, 11.268, 10.6367, 8.96767, 6.82316, 5.09997, 3.35446, 2.27282, 1.51211, 0.97546, 0.637924, 0.366334]
    histScaleDownBkdNonCTag = [13.3196, 20.2231, 30.9596, 47.5392, 70.857, 103.003, 142.086, 180.687, 210.827, 221.624, 209.788, 177.986, 138.813, 102.734, 71.4458, 48.124, 31.5115, 20.3504, 12.9417, 8.28681]

    histScaleUpSigCTag = [0.290834, 0.413514, 0.6423, 1.04647, 1.64095, 2.57969, 4.04835, 6.06564, 8.3194, 10.4303, 11.5195, 11.2987, 9.8976, 7.923, 5.76836, 4.06624, 2.70487, 1.83014, 1.17062, 0.753071]
    histScaleUpSigNonCTag = [1.49663, 2.27559, 3.4008, 5.21339, 8.20692, 12.9848, 19.8328, 29.253, 39.6959, 48.8982, 54.0514, 53.1643, 46.9213, 38.384, 28.9296, 20.8754, 14.044, 9.54594, 6.25578, 4.09547]
    histScaleUpBkdCTag = [0.342721, 0.5037, 0.733012, 1.16577, 1.80331, 2.753, 4.35604, 6.28837, 8.42577, 10.1907, 11.3971, 11.0966, 9.73274, 7.85956, 5.92626, 4.21221, 2.84243, 1.98263, 1.25875, 0.850816]
    histScaleUpBkdNonCTag = [6.70636, 9.7284, 14.8241, 22.7509, 36.1391, 56.0536, 85.1318, 123.405, 164.797, 201.245, 220.883, 218.313, 192.53, 157.144, 119.895, 86.4152, 60.2516, 40.5185, 26.9681, 17.5262]

    xaxis = []
    for i in range(20):
        xaxis.append(120.25+i/2.)
    
    colours = ["black", "red", "blue"]
    labels = ["Nominal", "Sys. Down", "Sys. Up"]
    MakePlot("ResSigCTag", xaxis, [histNomSigCTag, histResDownSigCTag, histResUpSigCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ResSigNonCTag", xaxis, [histNomSigNonCTag, histResDownSigNonCTag, histResUpSigNonCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ResBkdCTag", xaxis, [histNomBkdCTag, histResDownBkdCTag, histResUpBkdCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ResBkdNonCTag", xaxis, [histNomBkdNonCTag, histResDownBkdNonCTag, histResUpBkdNonCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ScaleSigCTag", xaxis, [histNomSigCTag, histScaleDownSigCTag, histScaleUpSigCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ScaleSigNonCTag", xaxis, [histNomSigNonCTag, histScaleDownSigNonCTag, histScaleUpSigNonCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ScaleBkdCTag", xaxis, [histNomBkdCTag, histScaleDownBkdCTag, histScaleUpBkdCTag], colours, labels, "Events / 0.5 GeV")
    MakePlot("ScaleBkdNonCTag", xaxis, [histNomBkdNonCTag, histScaleDownBkdNonCTag, histScaleUpBkdNonCTag], colours, labels, "Events / 0.5 GeV")

    coloursFour = ["black", "red", "blue", "green"]
    labelsFour = ["Signal c-tag cat.", "Signal non-c-tag cat.", "Background c-tag cat.", "Background non-c-tag cat."]
    MakePlot("Nom", xaxis, [histNomSigCTag, histNomSigNonCTag, histNomBkdCTag, histNomBkdNonCTag], coloursFour, labelsFour, "Events (unit norm.) / 0.5 GeV", True)
    MakePlot("ResDown", xaxis, [histResDownSigCTag, histResDownSigNonCTag, histResDownBkdCTag, histResDownBkdNonCTag], coloursFour, labelsFour, "Events (unit norm.) / 0.5 GeV", True)
    MakePlot("ResUp", xaxis, [histResUpSigCTag, histResUpSigNonCTag, histResUpBkdCTag, histResUpBkdNonCTag], coloursFour, labelsFour, "Events (unit norm.) / 0.5 GeV", True)
    MakePlot("ScaleDown", xaxis, [histScaleDownSigCTag, histScaleDownSigNonCTag, histScaleDownBkdCTag, histScaleDownBkdNonCTag], coloursFour, labelsFour, "Events (unit norm.) / 0.5 GeV", True)
    MakePlot("ScaleUp", xaxis, [histScaleUpSigCTag, histScaleUpSigNonCTag, histScaleUpBkdCTag, histScaleUpBkdNonCTag], coloursFour, labelsFour, "Events (unit norm.) / 0.5 GeV", True)

    coloursTwo = ["red", "blue"]
    labelsTwo = ["Sys. Down", "Sys. Up"]
    MakePlot("RatioResSigCTag", xaxis, [histNomSigCTag, histResDownSigCTag, histResUpSigCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioResSigNonCTag", xaxis, [histNomSigNonCTag, histResDownSigNonCTag, histResUpSigNonCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioResBkdCTag", xaxis, [histNomBkdCTag, histResDownBkdCTag, histResUpBkdCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioResBkdNonCTag", xaxis, [histNomBkdNonCTag, histResDownBkdNonCTag, histResUpBkdNonCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioScaleSigCTag", xaxis, [histNomSigCTag, histScaleDownSigCTag, histScaleUpSigCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioScaleSigNonCTag", xaxis, [histNomSigNonCTag, histScaleDownSigNonCTag, histScaleUpSigNonCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioScaleBkdCTag", xaxis, [histNomBkdCTag, histScaleDownBkdCTag, histScaleUpBkdCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)
    MakePlot("RatioScaleBkdNonCTag", xaxis, [histNomBkdNonCTag, histScaleDownBkdNonCTag, histScaleUpBkdNonCTag], coloursTwo, labelsTwo, "(Events (sys.) / 0.5 GeV) / (Events (nom..) / 0.5 GeV)", False, True, True)

    
