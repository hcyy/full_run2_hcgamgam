\begin{table}[!htb]
\caption{Mean and resolution for the PDF in each category.}
\label{tab:fitMeanAndRes}
\centering
\begin{tabular}{lcc}
\hline
Category & Mean [GeV] & Resolution [GeV] \\
\hline
\hline
category 0 & 125.114 & 1.76601 \\
\hline
\end{tabular}
\end{table}
